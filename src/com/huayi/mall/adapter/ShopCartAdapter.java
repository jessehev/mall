package com.huayi.mall.adapter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.Cart;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.util.IsNetWorkConnected;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.view.ImageViewSubClass;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ShopCartAdapter extends BaseAdapter {

	private List<Cart> arrayList = new ArrayList<Cart>();

	private Context context;

	private ViewHolder viewholdel;
	private ViewHolder2 viewholdel2;

	public static final int STYLE_COMPLETE = 0;// 第一种布局
	public static final int STYLE_DELETE = 1;// 有删除布局

	private Handler handler;

	public ShopCartAdapter(Context context, List<Cart> arrayList,
			Handler handler) {
		this.context = context;
		this.arrayList = arrayList;
		this.handler = handler;

	}

	public void setData(List<Cart> arrayList) {
		this.arrayList = arrayList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub

		return arrayList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub

		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		viewholdel = null;
		// 根据type决定加载何种布局
		int type = getItemViewType(position);

		if (type == STYLE_COMPLETE) {
			if (convertView == null) {

				convertView = View.inflate(context,
						R.layout.fragment_shoppingcart_item_top, null);
				viewholdel = new ViewHolder(convertView);
				convertView.setTag(viewholdel);
			} else {
				viewholdel = (ViewHolder) convertView.getTag();
			}
			// 解决复用乱序问题。
			final Cart cart = arrayList.get(position);

			// 加
			viewholdel.add.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					cart.number++;
					viewholdel.count.setText(cart.number + "");
					notifyDataSetChanged();

					handler.sendMessage(handler.obtainMessage(13)); // 更新价格
																	// arrayList_0
				}
			});
			// 减
			viewholdel.sub.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (cart.number > 0) {
						cart.number--;
						viewholdel.count.setText(cart.number + "");
						notifyDataSetChanged();
						handler.sendMessage(handler.obtainMessage(13));// 更新价格
																		// arrayList_0
					}
				}
			});

			// 初始化状态
			if (cart.select) {
				viewholdel.circle_iv.setImageResource(R.drawable.check_icon);
			} else {
				viewholdel.circle_iv.setImageResource(R.drawable.circle_cart);
			}
			// 单选
			viewholdel.circle_iv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (cart.select) {
						cart.select = false;
						viewholdel.circle_iv
								.setImageResource(R.drawable.check_icon);
						notifyDataSetChanged();
					} else {
						cart.select = true;
						viewholdel.circle_iv
								.setImageResource(R.drawable.circle_cart);
						notifyDataSetChanged();
					}
					// 发送handler消息判断时候是全部选中
					handler.sendMessage(handler.obtainMessage(10,
							isAllSelected()));

				}
			});

			viewholdel.price.setText("¥"+cart.price + "");
			viewholdel.oldPrice.setText(cart.oldPrice + "");
			viewholdel.descibe.setText(cart.describe);
			viewholdel.number.setText("X" + cart.number + "");
			viewholdel.name.setText(cart.name);
			viewholdel.count.setText(cart.number + "");

			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost + cart.picture, viewholdel.picture,
					ImageLoaderCfg.options);

			return convertView;
		} else if (type == STYLE_DELETE) {

			if (convertView == null) {

				convertView = View.inflate(context,
						R.layout.delete_shopcart_item, null);
				viewholdel2 = new ViewHolder2(convertView);
				convertView.setTag(viewholdel2);
			} else {
				viewholdel2 = (ViewHolder2) convertView.getTag();
			}
			// 解决复用乱序问题。
			final Cart cart = arrayList.get(position);

			// 加
			viewholdel2.add.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					cart.number++;
					viewholdel2.count.setText(cart.number + "");
					notifyDataSetChanged();

					handler.sendMessage(handler.obtainMessage(14));// 更新价格
																	// arrayList_1
				}
			});
			// 减
			viewholdel2.sub.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (cart.number > 0) {
						cart.number--;
						viewholdel2.count.setText(cart.number + "");
						notifyDataSetChanged();

						handler.sendMessage(handler.obtainMessage(14));// 更新价格
																		// arrayList_1
					}
				}
			});
			// 初始化状态
			if (cart.select) {
				viewholdel2.img_circle.setImageResource(R.drawable.check_icon);

			} else {
				viewholdel2.img_circle.setImageResource(R.drawable.circle_cart);

			}
			// 单选
			viewholdel2.img_circle.setOnClickListener(new OnClickListener() {
				// setImageResource
				@Override
				public void onClick(View v) {
					if (cart.select) {
						cart.select = false;
						viewholdel2.img_circle
								.setImageResource(R.drawable.check_icon);
						System.out.println(cart.select);
						notifyDataSetChanged();
					} else {
						cart.select = true;
						viewholdel2.img_circle
								.setImageResource(R.drawable.circle_cart);
						System.out.println(cart.select);
						notifyDataSetChanged();
					}
					// 发送handler消息判断时候是全部选中
					handler.sendMessage(handler.obtainMessage(11,
							isAllSelected()));

				}
			});
			// 删除
			viewholdel2.delete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					deleteDialog(cart);
				}
			});

			viewholdel2.describe.setText(cart.describe);
			viewholdel2.count.setText(cart.number + "");

			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost + cart.picture, viewholdel2.picture,
					ImageLoaderCfg.options);
			return convertView;

		}
		return convertView;
	}

	//
	public void deleteDialog(final Cart cart) {
		LinearLayout lin_dailog_item = (LinearLayout) View.inflate(context,
				R.layout.dialog_cart, null);
		final Dialog dialog = new AlertDialog.Builder(context).create();
		dialog.show();
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();

		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		int height = wm.getDefaultDisplay().getHeight() / 3;
		int weith = wm.getDefaultDisplay().getWidth() / 5 * 3;
		params.width = weith;
		params.height = height;
		params.gravity = Gravity.TOP;
		params.y = wm.getDefaultDisplay().getHeight() / 5;

		dialog.getWindow().setContentView(lin_dailog_item);
		dialog.getWindow().setAttributes(params);
		dialog.getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
						| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		dialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		ImageView cancle = (ImageView) lin_dailog_item
				.findViewById(R.id.cancle_dialog_cart);
		TextView sure = (TextView) lin_dailog_item
				.findViewById(R.id.sure_dialog_cart);
		sure.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// 请求服务器删除商品
				if (IsNetWorkConnected.isNetworkConnected(context)) {
					xutilRequest(cart);
					dialog.dismiss();
				} else {
					Toast.makeText(context, "请检查网络连接", 1).show();
				}

			}
		});
		cancle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog.dismiss();

			}
		});

	}

	// 删除
	private void xutilRequest(final Cart cart) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("cart_id", SpfUtil.getCartId() + "");
		params.addBodyParameter("delIds", "[" + cart.id + "]");
		System.out.println("商品id" + cart.id);
		// params.addBodyParameter("delIds", "["+position+"]");
		httpUtils.send(HttpMethod.POST, GlobalUrl.deleteFromCart, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						System.out.println("dasdasdasd----" + arg1);
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							System.out.println("delete" + arg0.result);

							JSONObject jsonObj = new JSONObject(arg0.result);

							if (jsonObj.optBoolean("success")) {
								arrayList.remove(cart);
								notifyDataSetChanged();
								Toast.makeText(context,
										jsonObj.optString("message"), 1).show();

								// 发送handler消息通知页面数据发生变化
								handler.sendMessage(handler.obtainMessage(12));
							}

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {

					}
				});
	}

	// 完成状态
	class ViewHolder {
		public EditText count;
		public Button sub, add;
		public TextView name, price, oldPrice, descibe, number;
		public ImageView circle_iv;
		public ImageViewSubClass picture;

		public ViewHolder(View convertView) {
			picture = (ImageViewSubClass) convertView
					.findViewById(R.id.pro_image);

			name = (TextView) convertView.findViewById(R.id.pro_name);
			sub = (Button) convertView.findViewById(R.id.pro_sub);
			add = (Button) convertView.findViewById(R.id.pro_add);
			count = (EditText) convertView.findViewById(R.id.pro_count);

			price = (TextView) convertView
					.findViewById(R.id.shopPrice_shopcart);
			oldPrice = (TextView) convertView
					.findViewById(R.id.old_shopPrice_shopcart);
			descibe = (TextView) convertView
					.findViewById(R.id.descibe_shopcart);

			number = (TextView) convertView.findViewById(R.id.number_shopcart);

			circle_iv = (ImageView) convertView.findViewById(R.id.circle_iv);

		}
	}

	// 编辑状态
	class ViewHolder2 {
		public EditText count;
		public Button sub, add;
		public ImageView img_circle;

		public TextView describe, delete;

		public ImageViewSubClass picture;

		public ViewHolder2(View convertView) {
			picture = (ImageViewSubClass) convertView
					.findViewById(R.id.pro_image_delete);

			sub = (Button) convertView.findViewById(R.id.pro_sub_delete);
			add = (Button) convertView.findViewById(R.id.pro_add_delete);
			count = (EditText) convertView.findViewById(R.id.pro_count_delete);
			img_circle = (ImageView) convertView
					.findViewById(R.id.img_circle_delete);
			describe = (TextView) convertView
					.findViewById(R.id.describe_delete);
			delete = (TextView) convertView.findViewById(R.id.tv_delete);
		}
	}

	/**
	 * 根据数据源的position返回需要显示的的layout的type
	 * 
	 * type的值必须从0开始
	 * 
	 * */
	@Override
	public int getItemViewType(int position) {

		Cart cart = arrayList.get(position);
		int type = cart.type;
		return type;
	}

	/**
	 * 返回所有的layout的数量
	 * 
	 * */
	@Override
	public int getViewTypeCount() {
		return 2;
	}

	/**
	 * 判断是否购物车中所有的商品全部被选中
	 * 
	 * @return true所有条目全部被选中 false还有条目没有被选中
	 */
	private boolean isAllSelected() {
		boolean flag = true;
		for (int i = 0; i < arrayList.size(); i++) {
			if (arrayList.get(i).select) {
				flag = true;
				continue;
			} else {
				return false;
			}
		}
		return flag;
	}
}