package com.huayi.mall.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.activity.BigimageActivityTWO;
import com.huayi.mall.common.Comment;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.view.CircleImageView;
import com.huayi.mall.view.RatioImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class LoockforCommentAdapter extends BaseAdapter {
	private List<Comment> arrayList;
	private LayoutInflater inflater;
	private ViewHolder viewHolder;

	Activity activity;

	public LoockforCommentAdapter(Activity activity, List<Comment> arrayList) {
		this.inflater = LayoutInflater.from(activity);
		this.arrayList = arrayList;
		this.activity = activity;
	}

	public void setData(List<Comment> arrayList) {
		this.arrayList = arrayList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		viewHolder = null;
		if (convertView == null) {
			convertView = inflater.inflate(
					R.layout.activity_lookfor_comment_item, null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		final Comment comment = arrayList.get(position);

		viewHolder.name.setText(comment.nickName);
		viewHolder.content.setText(comment.content);
		viewHolder.time.setText(comment.time);
		ImageLoader.getInstance().displayImage(
				GlobalUrl.serviceHost + comment.headPicture, viewHolder.head,
				ImageLoaderCfg.options);
		ImageLoader.getInstance().displayImage(
				GlobalUrl.serviceHost + comment.shopPicture,
				viewHolder.shopImg, ImageLoaderCfg.options);

		viewHolder.shopImg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(activity, BigimageActivityTWO.class);
				intent.putExtra("bigImage_lookfor", GlobalUrl.serviceHost
						+ comment.shopPicture);
				activity.startActivity(intent);
			}
		});

		return convertView;
	}

	class ViewHolder {
		public CircleImageView head;
		public TextView name, content, time;
		public RatioImageView shopImg;

		public ViewHolder(View convertView) {
			shopImg = (RatioImageView) convertView
					.findViewById(R.id.shopImg_comment_item);
			name = (TextView) convertView.findViewById(R.id.name_comment_item);
			content = (TextView) convertView
					.findViewById(R.id.content_comment_item);
			time = (TextView) convertView.findViewById(R.id.time_comment_item);
			head = (CircleImageView) convertView
					.findViewById(R.id.head_comment_item);

		}
	}

}
