package com.huayi.mall.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.Goods;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.view.RatioImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FiveShopAdapter extends BaseAdapter {
	private List<Goods> arrayList;
	private LayoutInflater inflater;
	private ViewHolder viewHolder;
	private Context context;

	public FiveShopAdapter(Context context, List<Goods> arrayList) {
		this.inflater = LayoutInflater.from(context);
		this.arrayList = arrayList;
		this.context = context;
	}

	public void setData(List<Goods> arrayList) {
		this.arrayList = arrayList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		viewHolder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.activity_fiveshop_item,
					null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		final Goods goods = arrayList.get(position);

		viewHolder.name.setText(goods.name);
		viewHolder.price.setText(goods.price);

		ImageLoader.getInstance().displayImage(
				GlobalUrl.serviceHost + goods.picture, viewHolder.picture,
				ImageLoaderCfg.options);

		return convertView;
	}

	class ViewHolder {
		public RatioImageView picture;
		public TextView name, price;

		public ViewHolder(View convertView) {
			picture = (RatioImageView) convertView
					.findViewById(R.id.img_fiveshop_item);
			name = (TextView) convertView.findViewById(R.id.name_fiveshop_item);
			price = (TextView) convertView
					.findViewById(R.id.price_fiveshop_item);

		}

	}

}
