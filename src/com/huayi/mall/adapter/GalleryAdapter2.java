package com.huayi.mall.adapter;

import java.util.List;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.base.GoodsListBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.nostra13.universalimageloader.core.ImageLoader;

public class GalleryAdapter2 extends  
RecyclerView.Adapter<GalleryAdapter2.ViewHolder>  
{  
	private Context context;
	
	// 图片封装为一个数组
	private int[] icon = { R.drawable.score, R.drawable.miaokill,
			R.drawable.recommended, R.drawable.newshop, R.drawable.ticket,
			R.drawable.recharge, R.drawable.newmessage, R.drawable.fireshop };
	private String[] iconName = { "积分", "秒杀", "推荐", "新品", "优惠券", "充值", "资讯",
	"热销" };
	private String[] iconName2 = { "活动" };

	/**
	 * ItemClick的回到接口
	 */
	public interface OnItemClickLitener{
		void onItemClick(View view,int position);

	}
	private OnItemClickLitener mOnItemClickLitener;
	public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener)  
	{  
		this.mOnItemClickLitener = mOnItemClickLitener;  
	}  

	private LayoutInflater mInflater;  
	private List<Integer> mDatas;  

	public GalleryAdapter2(Context context, List<Integer> datats)  
	{  
		mInflater = LayoutInflater.from(context);  
		mDatas = datats;  
		this.context=context;
	}  

	public static class ViewHolder extends RecyclerView.ViewHolder  
	{  
		public ViewHolder(View arg0)  
		{  
			super(arg0);  
		}  

		ImageView mImg;  
		TextView name;  
		TextView mprice;  
		TextView nprice;  
		GridView gridView;
	}  

	@Override  
	public int getItemCount()  
	{  
		return mDatas.size();  
	}  

	/** 
	 * 创建ViewHolder 
	 */  
	@Override  
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)  
	{  
		View view = mInflater.inflate(R.layout.grid,  
				viewGroup, false);  
		ViewHolder viewHolder = new ViewHolder(view);

		/*viewHolder.mImg = (ImageView) view  
				.findViewById(R.id.id_index_gallery_item_image);  
		viewHolder.name = (TextView) view  
				.findViewById(R.id.name);  
		viewHolder.nprice = (TextView) view  
				.findViewById(R.id.nprice);  
		viewHolder.mprice = (TextView) view  
				.findViewById(R.id.mprice);  */
		viewHolder.gridView=(GridView) view.findViewById(R.id.inhomegr);
		

		return viewHolder;  
	}  

	/** 
	 * 设置值 
	 */  
	@Override  
	public void onBindViewHolder(final ViewHolder viewHolder, final int i)  
	{  
		
		viewHolder.gridView.setAdapter(new GridAdapter());
		/*viewHolder.name.setText(mDatas.get(i).getGoodsName());
		viewHolder.nprice.setText("¥ "+mDatas.get(i).getPrice()+"");
		viewHolder.mprice.setText("¥ "+mDatas.get(i).getMarketPrice()+"");
		String [] s = mDatas.get(i).getGoodsPicture().split(",");
		ImageLoader.getInstance().displayImage(
				GlobalUrl.serviceHost1
				+ s[0],
				viewHolder.mImg, ImageLoaderCfg.options2);*/

		//如果设置了回调，则设置点击事件  
		if (mOnItemClickLitener != null)  
		{  
			viewHolder.itemView.setOnClickListener(new OnClickListener()  
			{  
				@Override  
				public void onClick(View v)  
				{  
					mOnItemClickLitener.onItemClick(viewHolder.itemView, i);  
				}  
			});  

		} 

	} 
	public class GridAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return iconName.length;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(context, R.layout.gridview_item, null);
			ImageView image = (ImageView) arg1.findViewById(R.id.image);
			TextView text = (TextView) arg1.findViewById(R.id.text);
			image.setImageResource(icon[arg0]);
			text.setText(iconName[arg0]);
			return arg1;
		}

	}

}  