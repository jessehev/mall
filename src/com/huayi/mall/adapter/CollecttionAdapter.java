package com.huayi.mall.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.common.Cart;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.view.ImageViewSubClass;
import com.nostra13.universalimageloader.core.ImageLoader;

public class CollecttionAdapter extends BaseAdapter {
	private ArrayList<Cart> arrayList;
	private LayoutInflater inflater;
	private ViewHolder viewHolder;
	private Handler mHandler;

	public CollecttionAdapter(Context context, ArrayList<Cart> arrayList,
			Handler mHandler) {
		this.inflater = LayoutInflater.from(context);

		this.arrayList = arrayList;
		this.mHandler = mHandler;

	}

	public void setData(ArrayList<Cart> arrayList) {
		this.arrayList = arrayList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrayList.size();

	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		viewHolder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.activity_collection_item,
					null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final Cart cart = arrayList.get(position);

		if (cart.select) {
			viewHolder.circle_iv.setImageResource(R.drawable.check_icon);

		} else {
			viewHolder.circle_iv.setImageResource(R.drawable.circle_cart);

		}

		if (cart.visible) {// 显示园
			viewHolder.circle_iv.setVisibility(View.VISIBLE);
		} else {// 隐藏
			viewHolder.circle_iv.setVisibility(View.GONE);

		}
		// final ViewHolder viewHolder2 = viewHolder;
		viewHolder.circle_iv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (cart.select) {
					cart.select = false;
					viewHolder.circle_iv
							.setImageResource(R.drawable.check_icon);
					System.out.println(cart.select);
					notifyDataSetChanged();
				} else {
					cart.select = true;
					viewHolder.circle_iv
							.setImageResource(R.drawable.circle_cart);
					System.out.println(cart.select);
					notifyDataSetChanged();
				}
				// 发送handler消息判断时候是全部选中
				mHandler.sendMessage(mHandler
						.obtainMessage(11, isAllSelected()));

			}
		});

		viewHolder.name.setText(cart.name);
		viewHolder.oldPrice.setText("¥"+cart.oldPrice+"");
		viewHolder.newPrice.setText("¥"+cart.price+"");

		ImageLoader.getInstance().displayImage(
				GlobalUrl.serviceHost + cart.picture, viewHolder.picture,
				ImageLoaderCfg.options);

		return convertView;
	}

	class ViewHolder {
		public ImageView circle_iv;
		public TextView name, remai, oldPrice, newPrice;

		public ImageViewSubClass picture;

		public ViewHolder(View convertView) {
			circle_iv = (ImageView) convertView.findViewById(R.id.img_icon);

			picture = (ImageViewSubClass) convertView
					.findViewById(R.id.img_icon2);// 图片
			name = (TextView) convertView.findViewById(R.id.tv_title); // 名字
			remai = (TextView) convertView.findViewById(R.id.tv_price); // 热卖
			newPrice = (TextView) convertView.findViewById(R.id.tv_percent); // 现价
			oldPrice = (TextView) convertView.findViewById(R.id.tv_num);// 原价
		}

	}

	/**
	 * 判断是否购物车中所有的商品全部被选中
	 * 
	 * @return true所有条目全部被选中 false还有条目没有被选中
	 */
	private boolean isAllSelected() {
		boolean flag = true;
		for (int i = 0; i < arrayList.size(); i++) {
			if (arrayList.get(i).select) {
				flag = true;
				continue;
			} else {
				return false;
			}
		}
		return flag;
	}

}
