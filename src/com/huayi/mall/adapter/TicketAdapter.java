package com.huayi.mall.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.base.OrderBean;
import com.huayi.mall.common.BrandBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.common.TicketBean;
import com.huayi.mall.view.MyListView;
import com.huayi.mall.view.RatioImageView;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class TicketAdapter extends BaseAdapter {
	private List<TicketBean> arrayList;
	private LayoutInflater inflater;
	private ViewHolder viewHolder;
	Context context;
	int selectItem = -1;

	public TicketAdapter(Context context, List<TicketBean> arrayList) {
		this.inflater = LayoutInflater.from(context);
		this.arrayList = arrayList;
		this.context = context;
	}

	public void setData(List<TicketBean> arrayList) {
		this.arrayList = arrayList;
		this.notifyDataSetChanged();
	}

	public void setSelectItem(int selectItem) {
		this.selectItem = selectItem;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		viewHolder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.ticket_item, null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		TicketBean tb = arrayList.get(position);
		if (tb.visible) { // 全部 显示、隐藏圆圈
			viewHolder.cicle.setVisibility(View.VISIBLE);
		} else {
			viewHolder.cicle.setVisibility(View.GONE);

		}
		if (selectItem == position) { // 单选
			viewHolder.cicle.setImageResource(R.drawable.check);
		} else {
			viewHolder.cicle.setImageResource(R.drawable.circle_cart);
		}

		if (!tb.state) { // 优惠券已过期

			viewHolder.picture.setBackgroundResource(R.drawable.ticketno);
		}

		viewHolder.price.setText(tb.cutPrice + "");
		viewHolder.fullAndCut.setText("订单满" + tb.fullPrice + "减" + tb.cutPrice);
		viewHolder.startTime.setText(tb.starttime);
		viewHolder.endTime.setText(tb.expiretime);
		return convertView;
	}

	class ViewHolder {
		public TextView name, price, fullAndCut, startTime, endTime;
		public ImageView cicle;
		public RatioImageView picture;

		public ViewHolder(View convertView) {

			cicle = (ImageView) convertView.findViewById(R.id.cicle_ticket);
			price = (TextView) convertView.findViewById(R.id.price_ticket);
			fullAndCut = (TextView) convertView
					.findViewById(R.id.full_cut_price_ticket);
			startTime = (TextView) convertView
					.findViewById(R.id.startTime_ticket);
			endTime = (TextView) convertView.findViewById(R.id.endTime_ticket);

			picture = (RatioImageView) convertView
					.findViewById(R.id.ticketimage);
		}
	}

}
