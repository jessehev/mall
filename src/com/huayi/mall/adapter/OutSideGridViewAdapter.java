package com.huayi.mall.adapter;

import java.util.ArrayList;
import java.util.List;

import android.R.array;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.BrandBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.nostra13.universalimageloader.core.ImageLoader;

public class OutSideGridViewAdapter extends BaseAdapter {

	private List<BrandBean> arrayList = new ArrayList<BrandBean>();
	private Context context;

	public OutSideGridViewAdapter(Context context, List<BrandBean> arrayList) {
		this.context = context;
		this.arrayList = arrayList;
	}

	public void setData(List<BrandBean> brands) {
		this.arrayList = brands;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		arg1 = View.inflate(context, R.layout.gridview_item_outside, null);
		ImageView image = (ImageView) arg1
				.findViewById(R.id.brands_image_outside);

		BrandBean brand = arrayList.get(position);

		ImageLoader.getInstance().displayImage(
				GlobalUrl.serviceHost + brand.picture, image,
				ImageLoaderCfg.options);

		return arg1;
	}
}