package com.huayi.mall.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.LookTuiMoneyActivity;
import com.huayi.mall.R;
import com.huayi.mall.activity.EvaluateActivity;
import com.huayi.mall.activity.MyOrdernActivity;
import com.huayi.mall.activity.PayActivity;
import com.huayi.mall.activity.WuLiuActivity;
import com.huayi.mall.base.OrderBean;
import com.huayi.mall.view.MyListView;


public class ServiceAdapter extends BaseAdapter {
	private List arrayList;
	private LayoutInflater inflater;
	Activity context;

	public ServiceAdapter(Activity context, List arrayList) {
		this.inflater = LayoutInflater.from(context);
		this.arrayList = arrayList;
		this.context = context;
	}

	public void setData(List arrayList) {
		this.arrayList = arrayList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = View.inflate(context, R.layout.order_item, null);
		MyListView orinlist = (MyListView) convertView.findViewById(R.id.inorderlist);
		InorderAdapter adapter = new InorderAdapter();
		orinlist.setAdapter(adapter);
		//state 0待收货 1待评价2 代付款3已完成4关闭交易5退款中6退款成功
		TextView time = (TextView) convertView.findViewById(R.id.ordertime);
		final TextView orderid = (TextView) convertView.findViewById(R.id.orderid);

		TextView state = (TextView) convertView.findViewById(R.id.orderstate);
		Button bt_one =  (Button) convertView.findViewById(R.id.orderbtone);
		Button bt_two =  (Button) convertView.findViewById(R.id.orderbttwo);
		OrderBean orderBean = (OrderBean) arrayList.get(position);

		orderid.setText(orderBean.getOrder());
		switch (orderBean.getState()) {
		case 0:
			state.setText("待收货");
			bt_one.setText("查看物流");
			bt_two.setText("确认收货");
			bt_one.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					context.startActivity(new Intent(context,WuLiuActivity.class));
				}
			});
			bt_two.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(context, orderid.getText().toString()+"确认收货", 0).show();

				}
			});
			break;
		case 1:
			state.setText("待评价");
			bt_one.setText("删除订单");
			bt_two.setText("立即评价");
			bt_one.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(context, orderid.getText().toString()+"删除订单", 0).show();
				}
			});
			bt_two.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					context.startActivity(new Intent(context,EvaluateActivity.class));

				}
			});
			break;
		case 2:
			state.setText("待付款");
			bt_one.setText("取消订单");
			bt_two.setText("立即付款");
			bt_one.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(context, orderid.getText().toString()+"删除订单", 0).show();
				}
			});
			bt_two.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					context.startActivity(new Intent(context,PayActivity.class));

				}
			});
			break;
		case 3:
			state.setText("已完成");
			bt_one.setText("删除订单");
			bt_two.setText("申请售后");
			bt_one.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(context, orderid.getText().toString()+"删除订单", 0).show();
				}
			});
			bt_two.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(context, orderid.getText().toString()+"立即评价", 0).show();

				}
			});
			break;
		case 4:
			state.setText("关闭交易");
			bt_one.setText("删除订单");
			bt_two.setVisibility(View.GONE);
			bt_one.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(context, orderid.getText().toString()+"删除订单", 0).show();
				}
			});
			bt_two.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(context, orderid.getText().toString()+"立即评价", 0).show();

				}
			});
			break;
		case 5:
			state.setText("退款中");
			bt_one.setVisibility(View.GONE);
			bt_two.setText("查看详情");
			
			bt_two.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					context.startActivity(new Intent(context,LookTuiMoneyActivity.class));
				}
			});
			break;
		case 6:
			state.setText("退款成功");
			bt_one.setVisibility(View.GONE);
			bt_two.setText("查看退款");
			
			bt_two.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					context.startActivity(new Intent(context,LookTuiMoneyActivity.class));
				}
			});
			break;

		default:
			break;
		}
		return convertView;
	}




	class InorderAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 2;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(context, R.layout.inorder_item, null);
			return arg1;
		}}
}
