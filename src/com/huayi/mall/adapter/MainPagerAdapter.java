package com.huayi.mall.adapter;

import com.huayi.mall.R;
import com.huayi.mall.base.FragmentFactory;
import com.huayi.mall.util.GetRes;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author Ruby
 *	创建fragment的工厂
 */
public class MainPagerAdapter extends FragmentStatePagerAdapter{

	private String[] tabArr={"1","2","3","4","5"};//存放标题
	public MainPagerAdapter(FragmentManager fm, String[] tabArr) {
		super(fm);
		this.tabArr = tabArr;
	}

	public MainPagerAdapter(FragmentManager fm) {
		super(fm);
		this.tabArr =tabArr;
	}

	@Override
	public Fragment getItem(int arg0) {
		return FragmentFactory.create(arg0);
	}

	@Override
	public int getCount() {
		return tabArr.length;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return tabArr[position];
	}
	 

}