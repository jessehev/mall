package com.huayi.mall.adapter;

import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.activity.HdDataActivity;
import com.huayi.mall.activity.MyApplication;
import com.huayi.mall.base.OrderBean;
import com.huayi.mall.common.ActivityBean;
import com.huayi.mall.common.BrandBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.common.SpecialBean;
import com.huayi.mall.util.FormatTime;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.MyListView;
import com.huayi.mall.view.RatioImageView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class InforAdapter extends BaseAdapter {
	private List<ActivityBean> arrayList;
	private LayoutInflater inflater;
	private ViewHolder viewHolder;
	Context context;

	public InforAdapter(Context context, List<ActivityBean> arrayList) {
		this.inflater = LayoutInflater.from(context);
		this.arrayList = arrayList;
		this.context = context;
	}

	public void setData(List<ActivityBean> arrayList) {
		this.arrayList = arrayList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		viewHolder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.activity_info_item, null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final ActivityBean ab = (ActivityBean) arrayList.get(position);

		ImageLoader.getInstance().displayImage(
				GlobalUrl.serviceHost + ab.image, viewHolder.picture,
				ImageLoaderCfg.options);

		viewHolder.time.setText(FormatTime.format(ab.publishTime + ""));
		viewHolder.title.setText(ab.name);
		viewHolder.content.setText(ab.descs);

//		viewHolder.look.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//
//				// 点击查看活动详情
//				Intent intent = new Intent(context, HdDataActivity.class);
//
//				intent.putExtra("id", ab.id);
//				context.startActivity(intent);
//			}
//		});

		return convertView;
	}

	class ViewHolder {
		public TextView time, title, content;
		public RatioImageView picture;

		public ViewHolder(View convertView) {

			time = (TextView) convertView.findViewById(R.id.time_info);
			title = (TextView) convertView.findViewById(R.id.title_info);
			picture = (RatioImageView) convertView
					.findViewById(R.id.picture_info);
			content = (TextView) convertView.findViewById(R.id.content_info);
		}
	}

}
