package com.huayi.mall.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.common.Cart;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.view.ImageViewSubClass;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ConfirmAdapter extends BaseAdapter {
	private List<Cart> arrayList;
	private LayoutInflater inflater;
	private ViewHolder viewHolder;

	public ConfirmAdapter(Context context, List<Cart> arrayList) {
		this.inflater = LayoutInflater.from(context);
		this.arrayList = arrayList;

	}

	public void setData(List<Cart> arrayList) {
		this.arrayList = arrayList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		viewHolder = null;
		if (convertView == null) {
			convertView = inflater
					.inflate(R.layout.activity_confirm_item, null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final Cart cart = (Cart) arrayList.get(position);
		
		 viewHolder.name.setText(cart.name);
		 viewHolder.price.setText("¥"+cart.price+"");
		 viewHolder.describe.setText(cart.describe);
		
		 ImageLoader.getInstance().displayImage(
		 GlobalUrl.serviceHost + cart.picture, viewHolder.image,
		 ImageLoaderCfg.options);
		return convertView;
	}

	class ViewHolder {
		public TextView name, price, describe;
		public ImageViewSubClass image;

		public ViewHolder(View convertView) {
			name = (TextView) convertView.findViewById(R.id.name__confirm_item);
			price = (TextView) convertView
					.findViewById(R.id.shopPrice_confirm_item);
			describe = (TextView) convertView
					.findViewById(R.id.describe__confirm_item);
			image = (ImageViewSubClass) convertView
					.findViewById(R.id.image_confirm_item);
		}

	}

}
