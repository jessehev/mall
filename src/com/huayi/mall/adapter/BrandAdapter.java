package com.huayi.mall.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.base.OrderBean;
import com.huayi.mall.common.BrandBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.view.MyListView;
import com.huayi.mall.view.RatioImageView;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class BrandAdapter extends BaseAdapter {
	private List arrayList;
	private LayoutInflater inflater;
	private ViewHolder viewHolder;
	Context context;

	public BrandAdapter(Context context, List arrayList) {
		this.inflater = LayoutInflater.from(context);
		this.arrayList = arrayList;
		this.context = context;
	}

	public void setData(List arrayList) {
		this.arrayList = arrayList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		viewHolder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.activity_brand_item, null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		BrandBean bb = (BrandBean) arrayList.get(position);
		
		ImageLoader.getInstance().displayImage(
				GlobalUrl.serviceHost + bb.picture, viewHolder.img,
				ImageLoaderCfg.options);
		viewHolder.name.setText(bb.name);

		return convertView;
	}

	class ViewHolder {
		public TextView name;
		public RatioImageView img;

		public ViewHolder(View convertView) {

			name = (TextView) convertView.findViewById(R.id.name_brand_item);
			img = (RatioImageView) convertView
					.findViewById(R.id.img_brand_item2);

		}
	}

}
