package com.huayi.mall.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.base.OrderBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.common.SpecialBean;
import com.huayi.mall.view.MyListView;
import com.huayi.mall.view.RatioImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MotherBabyAdapter extends BaseAdapter {
	private List arrayList;
	private LayoutInflater inflater;
	private ViewHolder viewHolder;
	Context context;

	public MotherBabyAdapter(Context context, List arrayList) {
		this.inflater = LayoutInflater.from(context);
		this.arrayList = arrayList;
		this.context = context;
	}

	public void setData(List arrayList) {
		this.arrayList = arrayList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		viewHolder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.activity_motherbaby_item,
					null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		SpecialBean sb = (SpecialBean) arrayList.get(position);

		ImageLoader.getInstance().displayImage(
				GlobalUrl.serviceHost + sb.picture, viewHolder.picture,
				ImageLoaderCfg.options);

		viewHolder.name.setText(sb.goodsName);
		viewHolder.price.setText("¥"+sb.marketPrice);

		return convertView;
	}

	class ViewHolder {
		public RatioImageView picture;
		public TextView name, price;

		public ViewHolder(View convertView) {

			picture = (RatioImageView) convertView
					.findViewById(R.id.picture_mothItem);
			name = (TextView) convertView.findViewById(R.id.name_mothItem);
			price = (TextView) convertView.findViewById(R.id.price_mothItem);
		}

	}

}
