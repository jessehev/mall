package com.huayi.mall.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.base.GuessYouLikeBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.view.RatioImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ShopCartGridViewApater extends BaseAdapter {

	private List<GuessYouLikeBean> arrayList = new ArrayList<GuessYouLikeBean>();

	private Context context;

	private ViewHolder viewHolder;

	public ShopCartGridViewApater(Context context,
			List<GuessYouLikeBean> arrayList) {
		this.context = context;
		this.arrayList = arrayList;

	}

	public void setData(List<GuessYouLikeBean> arrayList) {
		this.arrayList = arrayList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub

		return arrayList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub

		viewHolder = null;
		if (convertView == null) {
			convertView = View.inflate(context,
					R.layout.fragment_shoppingcart_item_bottom, null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		GuessYouLikeBean mGuessYouLikeBean = arrayList.get(position);
		viewHolder.name.setText(mGuessYouLikeBean.getGoodsName());
		viewHolder.price.setText("￥" + mGuessYouLikeBean.getPrice() + "");

		String pictures = mGuessYouLikeBean.getGoodsPicture();
		if (pictures != null && pictures.contains(",")) {

			String[] s = mGuessYouLikeBean.getGoodsPicture().split(",");
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost
							+ s[0],
					viewHolder.picture, ImageLoaderCfg.options);
		} else {
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost1 + pictures, viewHolder.picture,
					ImageLoaderCfg.options2);

		}
//		ImageLoader.getInstance().displayImage(
//				GlobalUrl.serviceHost
//						+ mGuessYouLikeBean.getGoodsPicture().split(",")[0],
//				viewHolder.picture, ImageLoaderCfg.options);
		return convertView;
	}

	class ViewHolder {
		public TextView name, price;
		public RatioImageView picture;

		public ViewHolder(View convertView) {

			name = (TextView) convertView.findViewById(R.id.name_cart);
			price = (TextView) convertView.findViewById(R.id.price_cart);
			picture = (RatioImageView) convertView
					.findViewById(R.id.image_cart);

		}
	}
}