package com.huayi.mall.adapter;

import java.util.List;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.base.GoodsListBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.nostra13.universalimageloader.core.ImageLoader;

public class GalleryAdapter extends  
RecyclerView.Adapter<GalleryAdapter.ViewHolder>  
{  
	/**
	 * ItemClick的回到接口
	 */
	public interface OnItemClickLitener{
		void onItemClick(View view,int position);

	}
	private OnItemClickLitener mOnItemClickLitener;
	public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener)  
	{  
		this.mOnItemClickLitener = mOnItemClickLitener;  
	}  

	private LayoutInflater mInflater;  
	private List<GoodsListBean.RowsBean> mDatas;  

	public GalleryAdapter(Context context, List<GoodsListBean.RowsBean> datats)  
	{  
		mInflater = LayoutInflater.from(context);  
		mDatas = datats;  
	}  

	public static class ViewHolder extends RecyclerView.ViewHolder  
	{  
		public ViewHolder(View arg0)  
		{  
			super(arg0);  
		}  

		ImageView mImg;  
		TextView name;  
		TextView mprice;  
		TextView nprice;  
	}  

	@Override  
	public int getItemCount()  
	{  
		return mDatas.size();  
	}  

	/** 
	 * 创建ViewHolder 
	 */  
	@Override  
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)  
	{  
		System.out.println();
		View view = mInflater.inflate(R.layout.recyclerview_item,  
				viewGroup, false);  
		ViewHolder viewHolder = new ViewHolder(view);

		viewHolder.mImg = (ImageView) view  
				.findViewById(R.id.id_index_gallery_item_image);  
		viewHolder.name = (TextView) view  
				.findViewById(R.id.name);  
		viewHolder.nprice = (TextView) view  
				.findViewById(R.id.nprice);  
		viewHolder.mprice = (TextView) view  
				.findViewById(R.id.mprice);  
		
		return viewHolder;  
	}  

	/** 
	 * 设置值 
	 */  
	@Override  
	public void onBindViewHolder(final ViewHolder viewHolder, final int i)  
	{  
		viewHolder.name.setText(mDatas.get(i).getGoodsName());
		viewHolder.nprice.setText("¥ "+mDatas.get(i).getPrice()+"");
		viewHolder.mprice.setText("¥ "+mDatas.get(i).getMarketPrice()+"");
		
		String s =mDatas.get(i).getGoodsPicture();
		
		String[] split ;
		if(s!=null&&s.contains(",")){
			split = s.split(",");
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost1 + split[0], viewHolder.mImg,
					ImageLoaderCfg.options2);
		}else {
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost1 + s, viewHolder.mImg,
					ImageLoaderCfg.options2);
		}

		
		//如果设置了回调，则设置点击事件  
		if (mOnItemClickLitener != null)  
		{  
			viewHolder.itemView.setOnClickListener(new OnClickListener()  
			{  
				@Override  
				public void onClick(View v)  
				{  
					mOnItemClickLitener.onItemClick(viewHolder.itemView, i);  
				}  
			});  

		} 

	}  

}  
