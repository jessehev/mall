package com.huayi.mall;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.huayi.mall.activity.ConfirmActivity;
import com.huayi.mall.activity.LoginActivity;
import com.huayi.mall.activity.LookforCommentActivity;
import com.huayi.mall.base.ChimaBean;
import com.huayi.mall.base.FormatOptionBean;
import com.huayi.mall.base.FormatOptionBean.FormatOptionsBean;
import com.huayi.mall.base.GuessYouLikeBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.MyGridview;
import com.huayi.mall.view.MyListView;
import com.huayi.mall.view.NoSclloListview;
import com.huayi.mall.view.RatioImageView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.loopj.android.image.SmartImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MiaoGoodsDataActivity extends Activity implements OnPageChangeListener {
	/**
	 * 装点点的ImageView数组
	 */
	private ImageView[] tips;

	/**
	 * 装ImageView数组
	 */
	private ImageView[] mImageViews;
	/**
	 * 图片资源id
	 */
	private String[] imgIdArray;

	private ViewGroup group;

	private ViewPager viewPager;

	private NoSclloListview noSclloListview;

	private List1Adapter adapter = new List1Adapter();
	private Noscllolistpic adapter2 = new Noscllolistpic();
	private GoodsdataList adapter3 = new GoodsdataList();
	/**
	 * 大图
	 */
	String[] bigpic;

	private NoSclloListview noscllolistpic;
	private String optionIds[];
	private MyGridview goodsdataList;

	private ImageView shoucang;

	private TextView gotopinpai;

	private TextView gotoshop;
	private PopupWindow window;
	private GridviewAdapter adapter4 = new GridviewAdapter();
	private List<ChimaBean> list = new ArrayList<ChimaBean>();
	private List<ChimaBean> list2 = new ArrayList<ChimaBean>();
	private TextView it_two;
	private boolean isfollow;
	private GridView gdpopwinow;// 尺码
	private GridView gdtopopwinow;// 颜色
	private int count = 1;// 商品数量
	private int price[];// 商品总价
	private List<GuessYouLikeBean> guesslist = new ArrayList<GuessYouLikeBean>();
	private TextView pro_count;

	private TextView popprice;

	private RelativeLayout rl_include_title;

	private View cline;

	private ImageView back;

	private int gowhere;// 判断是进入购物车 还是立即购买 1购物车 2立即购买
	private List<String> optionsid = new ArrayList<String>();
	private String id;
	private JSONObject jsonObject2;
	private List<FormatOptionBean> list1 = new ArrayList<FormatOptionBean>();

	private MyListView poplisit;

	private TextView price2;

	private TextView marketPrice;

	

	private TextView goodsname;

	private TextView brandName;
	private JSONObject jsonObject;
	private TextView classifyName;
	private String[] pics;//
	private TextView discountgo;

	private TextView postage;

	private TextView pinjiacont;
	private MyCount mc;

	private TextView daojishi;

	private long endtime;

	private String secondKillId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_miaogoods_data);
		group = (ViewGroup) findViewById(R.id.viewGroup);
		viewPager = (ViewPager) findViewById(R.id.goodsvp);
		noSclloListview = (NoSclloListview) findViewById(R.id.noscllolist);
		noscllolistpic = (NoSclloListview) findViewById(R.id.noscllolistpic);
		goodsdataList = (MyGridview) findViewById(R.id.GoodsdataList);
		shoucang = (ImageView) findViewById(R.id.shoucang);
		gotopinpai = (TextView) findViewById(R.id.gotopinpai);
		gotoshop = (TextView) findViewById(R.id.goshop);
		it_two = (TextView) findViewById(R.id.it_two);
		rl_include_title = (RelativeLayout) findViewById(R.id.rl_include_title);
		price2 = (TextView) findViewById(R.id.price);// 实际售价
		marketPrice = (TextView) findViewById(R.id.marketPrice);// 吊牌价
		
		//导航
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		
		goodsname = (TextView) findViewById(R.id.goodsname);// 商品名称
		brandName = (TextView) findViewById(R.id.brandName);// 品牌分类
		classifyName = (TextView) findViewById(R.id.classifyName);// 标签分类
		discountgo = (TextView) findViewById(R.id.discountgo);// 标签折扣
		postage = (TextView) findViewById(R.id.postage);// 运费
		back = (ImageView) findViewById(R.id.back);
		cline = findViewById(R.id.cline);
		daojishi = (TextView) findViewById(R.id.jishi);
		pinjiacont = (TextView) findViewById(R.id.pinjiacont);
		Intent intent = getIntent();
		id = intent.getStringExtra("id");
		endtime = intent.getLongExtra("endtime", 0);
		secondKillId = intent.getStringExtra("secondKillId");
		init();
		onClick();
		initwindow();
		send();
		xutilRequest(1, id);
		guessYouLike();

	}

	private void onClick() {
		goodsdataList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MiaoGoodsDataActivity.this,
						MiaoGoodsDataActivity.class);

				intent.putExtra("id", guesslist.get(arg2).getId() + "");
				startActivity(intent);
			}
		});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				MiaoGoodsDataActivity.this.finish();

			}
		});

		// TODO Auto-generated method stub
		it_two.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(MiaoGoodsDataActivity.this,
						LookforCommentActivity.class);
				intent.putExtra("goods.id", id);
				startActivity(intent);
			}
		});
		// TODO Auto-generated method stub
		shoucang.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (SpfUtil.getId() == 0) {
					startActivity(new Intent(MiaoGoodsDataActivity.this,
							LoginActivity.class));
				} else {
					if (!isfollow) {

						// 进入购物车

						System.out.println("optionIds==========="
								+ SpfUtil.getCartId());
						HttpUtils httpUtils = new HttpUtils();
						RequestParams params = new RequestParams();
						params.addBodyParameter("customerId", SpfUtil.getId()
								+ "");
						params.addBodyParameter("goodsId", id);

						httpUtils.send(HttpMethod.POST, GlobalUrl.followGoods,
								params, new RequestCallBack<String>() {

									@Override
									public void onFailure(HttpException arg0,
											String arg1) {

										Toast.makeText(MiaoGoodsDataActivity.this,
												"失败", 0).show();
										System.out.println(arg1);
									}

									@Override
									public void onSuccess(
											ResponseInfo<String> arg0) {
										System.out.println(arg0.result);
										String result = arg0.result;
										System.out.println("====" + result);
										shoucang.setImageResource(R.drawable.shoucangc);
										isfollow = true;
									}

								});

					} else {

						System.out.println("optionIds==========="
								+ SpfUtil.getCartId());
						HttpUtils httpUtils = new HttpUtils();
						RequestParams params = new RequestParams();
						params.addBodyParameter("customerId", SpfUtil.getId()
								+ "");
						params.addBodyParameter("goodsId", id);

						httpUtils.send(HttpMethod.POST,
								GlobalUrl.deleteFollowGoods, params,
								new RequestCallBack<String>() {

									@Override
									public void onFailure(HttpException arg0,
											String arg1) {

										Toast.makeText(MiaoGoodsDataActivity.this,
												"失败", 0).show();
										System.out.println(arg1);
									}

									@Override
									public void onSuccess(
											ResponseInfo<String> arg0) {
										System.out.println(arg0.result);
										String result = arg0.result;
										System.out.println("是否关注dele" + result);
										shoucang.setImageResource(R.drawable.shoucangnc);
										isfollow = false;
									}

								});

					}
				}
			}
		});
		// 加入购物车
		gotopinpai.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (SpfUtil.getId() == 0) {
					startActivity(new Intent(MiaoGoodsDataActivity.this,
							LoginActivity.class));
				} else {

					gowhere = 1;
					showwindow();
					WindowManager.LayoutParams lp = getWindow().getAttributes();
					lp.alpha = 0.5f; // 0.0-1.0
					getWindow().setAttributes(lp);
				}
			}
		});
		// 立即购买
		gotoshop.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				if (SpfUtil.getId() == 0) {
					startActivity(new Intent(MiaoGoodsDataActivity.this,
							LoginActivity.class));
				} else {
					gowhere = 2;

					showwindow();
					WindowManager.LayoutParams lp = getWindow().getAttributes();
					lp.alpha = 0.5f; // 0.0-1.0
					getWindow().setAttributes(lp);
				}
			}
		});
	}

	private void init() {
		if (endtime>System.currentTimeMillis()) {
			mc=new MyCount(endtime-System.currentTimeMillis(), 1000);
			mc.start();
		}
		
		LayoutParams layoutParams = back.getLayoutParams();
		layoutParams.height = 60;
		layoutParams.width = 60;
		back.setLayoutParams(layoutParams);
		back.setVisibility(View.VISIBLE);
		back.setBackgroundResource(R.drawable.fback);
		cline.setVisibility(View.GONE);
		rl_include_title.setBackgroundColor(Color.parseColor("#ffffff"));
		// 绑定适配器
		noSclloListview.setAdapter(adapter);

		goodsdataList.setAdapter(adapter3);

		// TODO Auto-generated method stub
		// 载入图片资源ID

		// 将点点加入到ViewGroup中
		if (imgIdArray != null) {

			tips = new ImageView[imgIdArray.length];
			for (int i = 0; i < tips.length; i++) {
				ImageView imageView = new ImageView(this);
				imageView.setLayoutParams(new LayoutParams(10, 10));
				tips[i] = imageView;
				if (i == 0) {
					tips[i].setBackgroundResource(R.drawable.page_indicator_focused);
				} else {
					tips[i].setBackgroundResource(R.drawable.page_indicator_unfocused);
				}

				LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(
						new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
				layoutParams1.leftMargin = 5;
				layoutParams1.rightMargin = 5;
				group.addView(imageView, layoutParams1);
			}

			// 将图片装载到数组中
			mImageViews = new ImageView[imgIdArray.length];
			for (int i = 0; i < mImageViews.length; i++) {
				SmartImageView imageView = new SmartImageView(this);
				mImageViews[i] = imageView;

				imageView.setImageUrl(GlobalUrl.serviceHost + imgIdArray[i],
						R.drawable.ic_error);
			}

			// 设置Adapter
			viewPager.setAdapter(new MyAdapter());
			// 设置监听，主要是设置点点的背景
			viewPager.setOnPageChangeListener(this);
			// 设置ViewPager的默认项, 设置为长度的100倍，这样子开始就能往左滑动
			viewPager.setCurrentItem((mImageViews.length) * 100);
		}
	}

	public class MyAdapter extends PagerAdapter {
		/**
		 * 图片视图缓存列表
		 */
		private ArrayList<RatioImageView> mImageViewCacheList = new ArrayList<RatioImageView>();;

		/**
		 * 图片资源列表
		 */
		private ArrayList<String> mAdList = new ArrayList<String>();

		/**
		 * 广告图片点击监听
		 */
		@Override
		public int getCount() {
			return Integer.MAX_VALUE;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void destroyItem(View container, int position, Object object) {

			RatioImageView view = (RatioImageView) object;
			viewPager.removeView(view);
			mImageViewCacheList.add(view);

		}

		/**
		 * 载入图片进去，用当前的position 除以 图片数组长度取余数是关键
		 */
		@Override
		public Object instantiateItem(View container, int position) {

			String imageUrl = imgIdArray[position % imgIdArray.length];
			Log.i("imageUrl", imageUrl);
			RatioImageView imageView = null;
			if (mImageViewCacheList.isEmpty()) {
				imageView = new RatioImageView(MiaoGoodsDataActivity.this);
				imageView.setRatioW(0.99f);
				imageView.setLayoutParams(new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

				// test
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
				// 设置图片点击监听

			} else {
				imageView = mImageViewCacheList.remove(0);
			}
			imageView.setTag(imageUrl);
			((ViewGroup) container).addView(imageView);
			// 第二个参数为加载本地默认的资源文件
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost1
							+ imgIdArray[position % imgIdArray.length],
					imageView, ImageLoaderCfg.options2);

			return imageView;

			/*
			 * ViewGroup parent = (ViewGroup) mImageViews[position %
			 * mImageViews.length].getParent();
			 * 
			 * 
			 * 
			 * ((ViewPager)container).addView(mImageViews[position %
			 * mImageViews.length], 0);
			 * 
			 * 
			 * return mImageViews[position % mImageViews.length];
			 */

		}

	}

	public void onPageScrollStateChanged(int arg0) {

	}

	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	public void onPageSelected(int arg0) {
		setImageBackground(arg0 % mImageViews.length);
	}

	/**
	 * 设置选中的tip的背景
	 * 
	 * @param selectItems
	 */
	private void setImageBackground(int selectItems) {
		for (int i = 0; i < tips.length; i++) {
			if (i == selectItems) {
				tips[i].setBackgroundResource(R.drawable.page_indicator_focused);
			} else {
				tips[i].setBackgroundResource(R.drawable.page_indicator_unfocused);
			}
		}
	}

	// 商品 参数 lsit
	class List1Adapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 2;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(MiaoGoodsDataActivity.this,
					R.layout.canshulist_item, null);
			TextView fenleione = (TextView) arg1.findViewById(R.id.it_one);
			TextView fenletwo = (TextView) arg1.findViewById(R.id.it_two);
			switch (arg0) {
			case 0:
				fenleione.setText("发货地");
				if (jsonObject != null) {
					try {
						fenletwo.setText(jsonObject.getString("startAddress"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			case 1:
				fenleione.setText("配送时间");
				if (jsonObject != null) {
					try {
						fenletwo.setText(jsonObject.getString("sendTime"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				break;

			default:
				break;
			}

			return arg1;
		}
	}

	class Noscllolistpic extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return bigpic.length;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub

			arg1 = View.inflate(MiaoGoodsDataActivity.this, R.layout.noscpic_item,
					null);
			RatioImageView goodslist_bigpic = (RatioImageView) arg1
					.findViewById(R.id.goodslist_bigpic);
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost1 + bigpic[arg0], goodslist_bigpic,
					ImageLoaderCfg.options2);
			return arg1;
		}
	}

	class GoodsdataList extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return guesslist.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(MiaoGoodsDataActivity.this,
					R.layout.goodslist_item, null);
			TextView goodsname = (TextView) arg1.findViewById(R.id.goodsname);
			TextView goodsprice = (TextView) arg1.findViewById(R.id.goodsprice);
			RatioImageView actiontwo = (RatioImageView) arg1
					.findViewById(R.id.actiontwo);
			goodsname.setText(guesslist.get(arg0).getGoodsName());
			goodsprice.setText("¥ " + guesslist.get(arg0).getPrice() + "");
			

			String s =guesslist.get(arg0).getGoodsPicture();

			String[] split ;
			if(s!=null&&s.contains(",")){
				split = s.split(",");

				ImageLoader.getInstance().displayImage(
						GlobalUrl.serviceHost1 + split[0], actiontwo,
						ImageLoaderCfg.options2);
			}else {
				ImageLoader.getInstance().displayImage(
						GlobalUrl.serviceHost1 + s, actiontwo,
						ImageLoaderCfg.options2);
			}

			return arg1;
		}
	}

	class GridviewAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list1.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int positon, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(MiaoGoodsDataActivity.this,
					R.layout.goodsdatalist_item, null);
			TextView format = (TextView) arg1.findViewById(R.id.format);
			MyGridview gridview = (MyGridview) arg1
					.findViewById(R.id.gdpopwinow);

			final FormatOptionBean formatOptionBean = list1.get(positon);
			gridview.setTag(positon);
			if ((Integer) gridview.getTag() == positon) {
				gridview.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						formatOptionBean.getFormatOptions().get(arg2)
								.setWhere(arg2);
						final Iterator<FormatOptionsBean> it = formatOptionBean
								.getFormatOptions().iterator();
						while (it.hasNext()) {
							FormatOptionsBean next = it.next();

							if (next.getWhere() == arg2) {
								next.setState(1);
							} else {
								next.setState(0);
							}
						}
						for (int i = 0; i < formatOptionBean.getFormatOptions()
								.size(); i++) {
							System.out.println("位置="
									+ formatOptionBean.getFormatOptions()
											.get(i).getWhere());
						}
						for (int i = 0; i < formatOptionBean.getFormatOptions()
								.size(); i++) {
							System.out.println("状态="
									+ formatOptionBean.getFormatOptions()
											.get(i).getState());
						}

						adapter4.notifyDataSetChanged();
						optionIds[positon] = formatOptionBean
								.getFormatOptions().get(arg2).getId()
								+ "";
						optionsid = Arrays.asList(optionIds);
						System.out.println(optionsid + "");
						getProductByOptionIds();
					}
				});
			}

			format.setText(formatOptionBean.getFormatName());
			gridview.setAdapter(new BaseAdapter() {

				@Override
				public int getCount() {
					// TODO Auto-generated method stub
					return formatOptionBean.getFormatOptions().size();
				}

				@Override
				public Object getItem(int arg0) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public long getItemId(int arg0) {
					// TODO Auto-generated method stub
					return 0;
				}

				@Override
				public View getView(final int posito1n, View arg1,
						ViewGroup arg2) {
					// TODO Auto-generated method stub
					arg1 = View.inflate(MiaoGoodsDataActivity.this,
							R.layout.gd_item, null);
					TextView xinhao = (TextView) arg1.findViewById(R.id.xinhao);

					FormatOptionBean.FormatOptionsBean formatOptions = formatOptionBean
							.getFormatOptions().get(posito1n);

					xinhao.setText(formatOptions.getFormatOptionName());
					if (formatOptions.getState() == 1) {

						xinhao.setBackgroundColor(Color.parseColor("#ff7070"));
						xinhao.setTextColor(Color.parseColor("#ffffff"));
					} else {

						xinhao.setBackgroundResource(R.drawable.huodongb_shapre);
						xinhao.setTextColor(Color.parseColor("#3d4245"));
					}

					return arg1;
				}
			});
			return arg1;
		}
	}

	public void initwindow() {

		// TODO Auto-generated method stub
		View view = View.inflate(MiaoGoodsDataActivity.this, R.layout.popwindow,
				null);
		WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
		int height = wm.getDefaultDisplay().getHeight();
		window = new PopupWindow(view, WindowManager.LayoutParams.MATCH_PARENT,
				height / 2);
		// 实例化一个ColorDrawable颜色为半透明
		ColorDrawable dw = new ColorDrawable(0xb0000000);
		window.setBackgroundDrawable(dw);
		window.setFocusable(true);
		window.update();
		window.setAnimationStyle(R.style.mypopwindow_anim_style);
		window.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss() {
				WindowManager.LayoutParams lp = getWindow().getAttributes();
				lp.alpha = 1.0f; // 0.0-1.0
				getWindow().setAttributes(lp);

			}
		});
		poplisit = (MyListView) view.findViewById(R.id.popwindlist);

		// 绑定适配器
		poplisit.setAdapter(adapter4);
		Button button = (Button) view.findViewById(R.id.isok);
		TextView add = (TextView) view.findViewById(R.id.pro_addd);
		TextView sub = (TextView) view.findViewById(R.id.pro_subb);
		popprice = (TextView) view.findViewById(R.id.popprice);
		pro_count = (TextView) view.findViewById(R.id.pro_countt);
		pro_count.setText(count + "");

		add.setOnClickListener(new OnClickListener() {// 增加

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				count++;
				pro_count.setText(count + "");
			}
		});
		sub.setOnClickListener(new OnClickListener() {// 增加

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (count > 1) {
					count--;

					pro_count.setText(count + "");
				}
			}
		});

		button.setOnClickListener(new OnClickListener() {// 选好了
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (gowhere == 1) {
					// 进入购物车

					System.out.println("optionIds==========="
							+ SpfUtil.getCartId());
					HttpUtils httpUtils = new HttpUtils();
					RequestParams params = new RequestParams();
					params.addBodyParameter("cart_id", SpfUtil.getCartId() + "");
					params.addBodyParameter("goods_id", id);
					params.addBodyParameter("product_num", pro_count.getText()
							.toString());
					params.addBodyParameter("optionIds", optionsid.toString());

					httpUtils.send(HttpMethod.POST,
							GlobalUrl.saveProductToCart, params,
							new RequestCallBack<String>() {

								private CustomProgressDialog dialog;

								@Override
								public void onFailure(HttpException arg0,
										String arg1) {
									dialog.dismiss();
									Toast.makeText(MiaoGoodsDataActivity.this,
											"失败", 0).show();
									System.out.println(arg1);
								}

								@Override
								public void onSuccess(ResponseInfo<String> arg0) {
									System.out.println(arg0.result);
									String result = arg0.result;
									try {
										JSONObject jsonObject = new JSONObject(
												result);
										Toast.makeText(
												MiaoGoodsDataActivity.this,
												jsonObject.getString("message"),
												0).show();
										dialog.dismiss();
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

								@Override
								public void onStart() {
									dialog = new CustomProgressDialog(
											MiaoGoodsDataActivity.this, "获取中。。。",
											R.anim.frame2);
									dialog.show();
								}
							});

				} else {

					// 直接购买

					System.out.println("optionIds==========="
							+ SpfUtil.getCartId());
					HttpUtils httpUtils = new HttpUtils();
					RequestParams params = new RequestParams();
					params.addBodyParameter("customerId", SpfUtil.getId() + "");
					params.addBodyParameter("goodsId", id);
					System.out.println("goodsid=="+id);
					params.addBodyParameter("productNum", pro_count.getText()
							.toString());
					params.addBodyParameter("optionIds", optionsid.toString());
					params.addBodyParameter("secondKillId", secondKillId);
			

					httpUtils.send(HttpMethod.POST, GlobalUrl.secondKillBuy,
							params, new RequestCallBack<String>() {

								private CustomProgressDialog dialog;

								@Override
								public void onFailure(HttpException arg0,
										String arg1) {
									System.out.println("购买失败===="+arg1);
									dialog.dismiss();
									Toast.makeText(MiaoGoodsDataActivity.this,
											"失败", 0).show();
									System.out.println(arg1);
								}

								@Override
								public void onSuccess(ResponseInfo<String> arg0) {
									System.out.println("购买成功===="+arg0.result);
									try {                 
										JSONObject jsonObject = new JSONObject(
												arg0.result);
										if (jsonObject.toString().contains("success")
												) {
											System.out.println("=="
													+ arg0.result);
											String result = arg0.result;
											Intent intent = new Intent(
													MiaoGoodsDataActivity.this,
													ConfirmActivity.class);
											intent.putExtra("obj", result);
											intent.putExtra("orderType", 2);
											startActivityForResult(intent, 1);

										} else {
											Toast.makeText(
													MiaoGoodsDataActivity.this,
													"购买失败", 0).show();
										}
										dialog.dismiss();
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										dialog.dismiss();
									}

								}

								@Override
								public void onStart() {
									dialog = new CustomProgressDialog(
											MiaoGoodsDataActivity.this, "获取中。。。",
											R.anim.frame2);
									dialog.show();
								}
							});

				}
			}
		});

	}

	public void showwindow() {
		window.showAtLocation(shoucang, Gravity.BOTTOM, 0, 0);
	}

	public void send() {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		final CustomProgressDialog dialog = new CustomProgressDialog(
				MiaoGoodsDataActivity.this, "获取中。。。", R.anim.frame2);
		dialog.show();
		params.addBodyParameter("id", id);//商品id
		if (SpfUtil.getId() != 0) {
			params.addBodyParameter("customerId", SpfUtil.getId() + "");//用户id
		}
		httpUtils.send(HttpMethod.POST, GlobalUrl.findGoodsDetailById, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
						
						System.out.println("失败==="+arg1);
						Toast.makeText(MiaoGoodsDataActivity.this, "获取失败", 0)
								.show();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {

						System.out.println("成功===="+arg0.result);

						String result = arg0.result;
						try {
							jsonObject2 = new JSONObject(result);
							jsonObject = jsonObject2.getJSONObject("goods");
							String formats = jsonObject.getString("formats");
							Gson gson = new Gson();
							Type listType = new TypeToken<List<FormatOptionBean>>() {
							}.getType();
							list1 = gson.fromJson(formats, listType);
							optionIds = new String[list1.size()];
							for (int i = 0; i < list1.size(); i++) {
								optionIds[i] = list1.get(i).getFormatOptions()
										.get(0).getId()
										+ "";
								list1.get(i).getFormatOptions().get(0)
										.setState(1);
							}
							optionsid = Arrays.asList(optionIds);
							System.out.println("asdadsasdasdsad========="
									+ optionsid.toString());
							// 界面数据写入

							price2.setText(jsonObject.getString("price"));
							marketPrice.setText(jsonObject
									.getString("marketPrice"));
						
							goodsname.setText(jsonObject.getString("goodsName"));
							brandName.setText(jsonObject.getJSONObject("brand")
									.getString("brandName"));
							classifyName.setText(jsonObject.getJSONObject(
									"classify").getString("classifyName"));
							discountgo.setText("限时"
									+ jsonObject.getString("discount") + "抢购");
							if (jsonObject.getString("postage").equals("0")) {
								postage.setText("包邮");
							} else {
								postage.setText("运费 "
										+ jsonObject.getString("postage"));
							}
							String s =jsonObject.getString("goodsPicture");

							String[] split ;
							if(s!=null&&s.contains(",")){
								split = s.split(",");

								imgIdArray = s
										.split(",");
							}else {
								imgIdArray [0]= s;
							}

							
							init();
							isfollow = jsonObject2.optBoolean("isFollow");
							if (isfollow) {
								shoucang.setImageResource(R.drawable.shoucangc);
							} else {
								shoucang.setImageResource(R.drawable.shoucangnc);
							}
							bigpic = jsonObject.getString("bigPicture").split(
									",");
							if (bigpic != null) {
								noscllolistpic.setAdapter(adapter2);
							}
							getProductByOptionIds();
							adapter4.notifyDataSetChanged();
							adapter.notifyDataSetChanged();

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							dialog.dismiss();
						}
						dialog.dismiss();

					}

					@Override
					public void onStart() {

					}
				});

	};

	public void getProductByOptionIds() {

		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("goods_id", id);//商品id
		params.addBodyParameter("optionIds", optionsid.toString());//ID
		httpUtils.send(HttpMethod.POST, GlobalUrl.getProductByOptionIds,
				params, new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {

						Toast.makeText(MiaoGoodsDataActivity.this, "获取失败", 0)
								.show();
						System.out.println(arg1);
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {

						Toast.makeText(MiaoGoodsDataActivity.this, "获取成功", 0)
								.show();
						System.out.println("商品价格" + arg0.result);
						try {
							JSONObject jsonObject = new JSONObject(arg0.result);
							popprice.setText("¥ "
									+ jsonObject.getString("actualPrice"));
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {

					}
				});

	}

	public void guessYouLike() {
		// app/goods/guessYouLike

		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		System.out.println("用户Id============" + SpfUtil.getId());
		if (SpfUtil.getId() != 0) {

			params.addBodyParameter("customerId", SpfUtil.getId() + "");
		}
		httpUtils.send(HttpMethod.POST, GlobalUrl.guessYouLike, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {

						System.out.println(arg1);
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {

						System.out.println("猜你喜欢" + arg0.result);
						Gson gson = new Gson();
						Type listType = new TypeToken<List<GuessYouLikeBean>>() {
						}.getType();
						guesslist = gson.fromJson(arg0.result, listType);
						adapter3.notifyDataSetChanged();

					}

				});

	}
	// 查询
		private void xutilRequest(final int currentPage, String goodsId) {

			HttpUtils httpUtils = new HttpUtils();
			RequestParams params = new RequestParams();

			params.addBodyParameter("goods.id", goodsId);
			params.addBodyParameter("currentPage", currentPage + "");
			httpUtils.send(HttpMethod.POST, GlobalUrl.lookForComment, params,
					new RequestCallBack<String>() {

						@Override
						public void onFailure(HttpException arg0, String arg1) {
							// handler.sendEmptyMessage(2);
							

						}

						@Override
						public void onSuccess(ResponseInfo<String> arg0) {
							try {
								JSONObject obj = new JSONObject(arg0.result);
								System.out.println("评价数据====================="+obj);
								pinjiacont.setText(" ("+obj.getString("totalCount")+")");

							} catch (JSONException e) {
								e.printStackTrace();
							}
						}

						@Override
						public void onStart() {
						}
					});
		}
		
		 /*定义一个倒计时的内部类*/  
	    class MyCount extends CountDownTimer {     
	        public MyCount(long millisInFuture, long countDownInterval) {     
	            super(millisInFuture, countDownInterval);     
	        }     
	        @Override     
	        public void onFinish() {     
	        	daojishi.setText("已经结束"); 
	        }     
	        @Override     
	        public void onTick(long millisUntilFinished) {  
	        	SimpleDateFormat dateFormat = new SimpleDateFormat("HH时mm分ss秒");
	        	dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
	        	Date date = new Date();
	        	date.setTime(millisUntilFinished);
	        	String format = dateFormat.format(date);
	        	daojishi.setText("距结束"+format);     
	        }    
	    } 
	    @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    	// TODO Auto-generated method stub
	    	super.onActivityResult(requestCode, resultCode, data);
	    	if (requestCode==1&&resultCode==30054) {
				Bundle bundleExtra = data.getExtras();
				String orderId = bundleExtra.getString("orderId");
				if (orderId!=null) {
					HttpUtils httpUtils = new HttpUtils();
					RequestParams params = new RequestParams();

					params.addBodyParameter("orderId", orderId);
					
					httpUtils.send(HttpMethod.POST, GlobalUrl.deleteOrder, params,
							new RequestCallBack<String>() {

								@Override
								public void onFailure(HttpException arg0, String arg1) {
									// handler.sendEmptyMessage(2);
									

								}

								@Override
								public void onSuccess(ResponseInfo<String> arg0) {
									try {
										JSONObject obj = new JSONObject(arg0.result);
										System.out.println("评价数据====================="+obj);
										pinjiacont.setText(" ("+obj.getString("totalCount")+")");

									} catch (JSONException e) {
										e.printStackTrace();
									}
								}

								@Override
								public void onStart() {
								}
							});
				}
			}
	    }
}
