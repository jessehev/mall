package com.huayi.mall;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.huayi.mall.activity.EvaluateActivity;
import com.huayi.mall.activity.MyOrdernActivity;
import com.huayi.mall.base.GoodsListBean;
import com.huayi.mall.base.OrderBeanto;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.util.ACache;
import com.huayi.mall.util.ParesData2Obj;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.view.ImageViewSubClass;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PinJiaActivity extends Activity {

	private TextView tv_title_logo;
	private ImageView bakc;
	private ListView pinjialist;
	private PinjiaAdapter adapter = new PinjiaAdapter();
	private OrderBeanto bean;
	private List<OrderBeanto.OrderListBean.OrderItemListBean> list=new ArrayList<OrderBeanto.OrderListBean.OrderItemListBean>();;
	private ACache aCache;
	private int orderid;
	private int weizhi;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pin_jia);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		bakc = (ImageView) findViewById(R.id.back);
		pinjialist = (ListView) findViewById(R.id.pinjialist);
		aCache = ACache.get(this);

		Intent intent = getIntent();
		String stringExtra = intent.getStringExtra("list");
		orderid=intent.getIntExtra("orderid", -1);
		System.out.println("json数据=="+stringExtra);
		Gson gson = new Gson();

		Type listType = new TypeToken<List<OrderBeanto.OrderListBean.OrderItemListBean>>(){}.getType();
		list = gson.fromJson(stringExtra, listType); 
		adapter.notifyDataSetChanged();
		
		System.out.println("集合大小===="+list.size());
		init();
		onclick();
	}

	private void onclick() {
		// TODO Auto-generated method stub
		bakc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				PinJiaActivity.this.finish();
			}
		});
	}

	private void init() {
		// TODO Auto-generated method stub
		tv_title_logo.setText("订单评价");
		bakc.setVisibility(View.VISIBLE);
		pinjialist.setAdapter(adapter);
	}
	class PinjiaAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int postion, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(PinJiaActivity.this, R.layout.inorder_item, null);
			TextView ordername = (TextView) arg1.findViewById(R.id.ordername);
			TextView orderprice = (TextView)arg1.findViewById(R.id.orderprice);
			Button pinjia = (Button) arg1.findViewById(R.id.orderbttwo);
			TextView option = (TextView) arg1.findViewById(R.id.options);
			option.setVisibility(View.GONE);
			final ImageViewSubClass image = (ImageViewSubClass) arg1.findViewById(R.id.orderimage);
			String[] imagepic  = list.get(postion).getProduct().getProductPicture().split(",");
			ordername.setText(list.get(postion).getProduct().getProductName());
			orderprice.setText("¥ "+list.get(postion).getProduct().getActualPrice()+"");
			
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost1
					+ imagepic[0],
					image, ImageLoaderCfg.options2);
			if (list.get(postion).getItemStatus()==0) {
				pinjia.setVisibility(View.VISIBLE);
			}else {
				
				pinjia.setVisibility(View.VISIBLE);
				pinjia.setBackgroundResource(R.drawable.order_shapre);
				pinjia.setTextColor(Color.parseColor("#000000"));
				pinjia.setText("已评价");
			}
			
			pinjia.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					if (orderid!=-1&list.get(postion).getItemStatus()==0) {				
						weizhi=postion;
						Intent intent
						 = new Intent(PinJiaActivity.this,EvaluateActivity.class);
						image.buildDrawingCache();
						Bitmap bitmap = image.getDrawingCache();
						intent.putExtra("bitmap", bitmap);
						intent.putExtra("orderid", orderid);
						intent.putExtra("orderlistid", list.get(postion).getId());
						startActivityForResult(intent, 1);
					}
					

				}
			});

			return arg1;
		}}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode==1&resultCode==1) {
			list.get(weizhi).setItemStatus(1);
			adapter.notifyDataSetChanged();
		}
	}
}
