package com.huayi.mall;

import java.io.InputStream;
import java.io.OutputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.huayi.mall.activity.NewAddressActivity;
import com.huayi.mall.activity.ShengShiQuActivity;
import com.huayi.mall.base.AddressBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.ACache;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class ReviseActivity extends Activity {
	private ImageView shenshiqu;
	private ImageView back;
	private TextView tv_title_logo;
	private EditText shengshiqu;
	private Button bt_biannji;
	private ACache aCache;
	private EditText name;
	private EditText phone;
	private EditText address;
	private CheckBox moren ;
	private Intent intent;
	private AddressBean addressBean;
	private Boolean ischeck=false;
	private String id;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_revise);
		shenshiqu = (ImageView) findViewById(R.id.shenshiqu);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		shengshiqu = (EditText) findViewById(R.id.shengshiqu);
		bt_biannji = (Button) findViewById(R.id.bt_save);
		name = (EditText) findViewById(R.id.name);
		phone = (EditText) findViewById(R.id.phone);
		address = (EditText) findViewById(R.id.addresss);
		moren = (CheckBox) findViewById(R.id.morenche);
		intent = getIntent();
		Bundle bundleExtra = intent.getBundleExtra("bundle");
		addressBean = (AddressBean) bundleExtra.getSerializable("bean");
		name.setText(addressBean.getReceiver());
		phone.setText(addressBean.getPhone());
		id = addressBean.getId();
		String[] addresss = addressBean.getAddress().split("/");
		shengshiqu.setText(addresss[0]);
		address.setText(addresss[1]);
		System.out.println("id=============="+id);
		init();
		onclik();
	}
	private void onclik() {
		
		moren.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				ischeck=arg1;
			}
		});
		// TODO Auto-generated method stub
		// 省市区
				shenshiqu.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						startActivityForResult(new Intent(ReviseActivity.this,
								ShengShiQuActivity.class), 1);
					}
				});
				// 返回
				back.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						ReviseActivity.this.finish();
					}
				});
				// 点击保存
				bt_biannji.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if (TextUtils.isEmpty(shengshiqu.getText().toString())
								| TextUtils.isEmpty(address.getText().toString())
								| TextUtils.isEmpty(name.getText().toString())
								| TextUtils.isEmpty(phone.getText().toString())
								| phone.getText().toString().length() != 11) {

							Toast.makeText(ReviseActivity.this, "请填写完整的地址信息", 0)
									.show();
						} else {
							send();
							/*// TODO Auto-generated method stub
							AddressBean addressBean = new AddressBean();
							addressBean.setAddress(shengshiqu.getText().toString()
									+ address.getText().toString());
							addressBean.setName(name.getText().toString());
							addressBean.setPhone(phone.getText().toString());
							Gson gson = new Gson();
							if (asJSONArray != null) {
								System.out.println(asJSONArray.toString());
								List<AddressBean> ps = gson.fromJson(
										asJSONArray.toString(),
										new TypeToken<List<AddressBean>>() {
										}.getType());
								ps.add(0, addressBean);
								aCache.put("address", gson.toJson(ps));
								Toast.makeText(NewAddressActivity.this, "保存成功", 0)
										.show();
								System.out.println("11111111111111111111");
							} else {
								List<AddressBean> list = new ArrayList<AddressBean>();
								list.add(0, addressBean);
								String json = gson.toJson(list);
								aCache.put("address", json);
								System.out.println(json);
								Toast.makeText(NewAddressActivity.this, "保存成功", 0)
										.show();
								System.out.println("2222222222222222222222");
							}*/
							

						}

					}
				});
	}
	private void init() {
		// TODO Auto-generated method stub
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("修改地址");
		

	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			if (resultCode == 1) {
				String stringExtra = data.getStringExtra("mate");
				System.out.println(stringExtra);
				shengshiqu.setText(stringExtra);
			}
		}
	}
	private void send() {
		HttpUtils httpUtils = new HttpUtils();
		/*customer.id 然后就是其他的    
	    private String receiver;
	    private String address;
	    private String phone;
	    private boolean defaultAddress;*/
		RequestParams params = new RequestParams();
		
		params.addBodyParameter("id", id);
		params.addBodyParameter("customer.id", SpfUtil.getId()+"");
		
		params.addBodyParameter("receiver", name.getText().toString());//收件人
		params.addBodyParameter("phone", phone.getText().toString());//手机
		params.addBodyParameter("address", shengshiqu.getText().toString()+"/"+address.getText().toString());//省市区
		params.addBodyParameter("defaultAddress",ischeck.toString());

		httpUtils.send(HttpMethod.POST, GlobalUrl.updateAddress, params, new RequestCallBack<String>() {

			private CustomProgressDialog dialog;

			@Override
			public void onFailure(HttpException arg0, String arg1) {
			dialog.dismiss();
			Toast.makeText(ReviseActivity.this, "修改失败", 0).show();
			System.out.println(arg1);
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				
				Toast.makeText(ReviseActivity.this, "保存成功", 0).show();
				System.out.println(arg0.result);
				dialog.dismiss();
				ReviseActivity.this.finish();
			}

			@Override
			public void onStart() {
				dialog = new CustomProgressDialog(ReviseActivity.this,"保存中。。。", R.anim.frame2);
				dialog.show();
			}
		});
	}
	
}
