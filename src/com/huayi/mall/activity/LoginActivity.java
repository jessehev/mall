package com.huayi.mall.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.ACache;
import com.huayi.mall.util.IsPhoneNumber;
import com.huayi.mall.util.MD5;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.util.TimeButton;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class LoginActivity extends Activity {

	private EditText phone_et, pwd_et;

	private TextView login_tv, forget_tv, register_tv;

	private LinearLayout look_layout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		findViewById();
		initView();
		// initData();
		onClick();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		if (keyCode == 4) {
			// System.exit(0);
			// Intent startMain = new Intent(Intent.ACTION_MAIN);
			// startMain.addCategory(Intent.CATEGORY_HOME);
			// startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			// startActivity(startMain);
			// System.exit(0);// 退出程序
			// ActivityManager activityMgr = (ActivityManager)
			// getSystemService(ACTIVITY_SERVICE);
			// activityMgr.restartPackage(getPackageName());
			// if (MyApplication.activityList.size() != 0) {
			// Activity a = MyApplication.activityList.get(0);
			// a.finish();
			// }
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	public void initData() {

	}

	public void findViewById() {
		phone_et = (EditText) findViewById(R.id.et_phone_login);
		pwd_et = (EditText) findViewById(R.id.et_pwd_login);
		login_tv = (TextView) findViewById(R.id.bt_login);
		forget_tv = (TextView) findViewById(R.id.tv_forget_login);
		register_tv = (TextView) findViewById(R.id.tv_register_login);
		look_layout = (LinearLayout) findViewById(R.id.layout_look_login);
	}

	private void initView() {
		// TODO Auto-generated method stub
		SystemBar.initSystemBar(this,"#EC3F6C");
		SystemBar.setTranslucentStatus(this, true);
		phone_et.setHintTextColor(Color.parseColor("#ffffff"));
		pwd_et.setHintTextColor(Color.parseColor("#ffffff"));

	}

	private void xutilRequest(String phone, final String password) {
		HttpUtils httpUtils = new HttpUtils(10000);
		RequestParams params = new RequestParams();
		params.addBodyParameter("phone", phone);
		params.addBodyParameter("password", password);
		httpUtils.send(HttpMethod.POST, GlobalUrl.login, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						dialog.dismiss();
						try {
							JSONObject obj = new JSONObject(arg0.result);
							System.out.println("obj0--------------" + obj);

							if (obj.optBoolean("success")) {
								// 记录成功登录状态
								SpfUtil.setLoginStatus(true);
								JSONObject JsonObj = obj
										.optJSONObject("customer");
								long id = JsonObj.optLong("id");

								JSONObject cart = JsonObj.optJSONObject("cart");
								SpfUtil.setCartId(cart.optLong("id"));

								SpfUtil.setId(id);
								MyApplication.aCache.put("password", password,
										ACache.TIME_DAY * 365);

								SpfUtil.setPhone(JsonObj.optString("phone"));
								startActivity(new Intent(LoginActivity.this,
										MainActivity.class));
								finish();
							} else {
								String message = obj.optString("message");
								Toast.makeText(LoginActivity.this, message, 1)
										.show();
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onStart() {
						dialog = new CustomProgressDialog(LoginActivity.this,
								"正在登录中", R.anim.frame2);

						dialog.show();
					}
				});
	}

	private void onClick() {

		look_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this, MainActivity.class));
				finish();
			}
		});
		// TODO Auto-generated method stub
		login_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String phone = phone_et.getText().toString();
				String pwd = pwd_et.getText().toString();

				if (phone.length()!=11) {
					Toast.makeText(getApplicationContext(), "请输入正确手机号码", 0)
							.show();
				} else if (TextUtils.isEmpty(pwd)) {
					Toast.makeText(getApplicationContext(), "请输入登录密码", 0)
							.show();
				} else {

					xutilRequest(phone, pwd);
				}
			}
		});
		register_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this,
						RegisterActivity.class));
			}
		});
		forget_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this,
						ForgetActivity.class));
			}
		});

	}

}
