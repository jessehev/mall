package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.adapter.TicketAdapter;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.TicketBean;
import com.huayi.mall.util.FormatTime;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class TicketActivity extends Activity {

	private ListView listView;
	private TextView totmoney, head1;
	private ImageView back;
	private TextView tv_title_logo;
	private TicketAdapter adapter;
	private ScrollView sc;

	private List<TicketBean> arrayList = new ArrayList<TicketBean>();
	private String confirmToTicket, sumMoney;

	private LinearLayout head2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ticket);

		findViewById();
		initView();
		initApater();
		onclick();
		initData();
		xutilRequest(sumMoney);

	}

	public void findViewById() {
		// head1 = (TextView) findViewById(R.id.head1_ticket);
		// head2 = (LinearLayout) findViewById(R.id.head2_ticket);
		listView = (ListView) findViewById(R.id.ticketls);
		// totmoney = (TextView) findViewById(R.id.totmoney);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		sc = (ScrollView) findViewById(R.id.sc);

	}

	public void initData() {
		System.out.println("SpfUtil.getId()" + SpfUtil.getId());
		Intent intent = getIntent();

		confirmToTicket = intent.getStringExtra("confirmToTicket");
		sumMoney = intent.getStringExtra("totalMoney");

		// if (confirmToTicket != null) {
		// // 遍历list显示前面的圆圈
		// setItemVisibility(true);
		// }

	}

	public void setData() {
		String result = MyApplication.aCache.getAsString("ticket");
		try {
			if (result != null) {
				arrayList.clear();
				// int totmoneyStr = 0;
				JSONObject jsonObj = new JSONObject(result);
				JSONArray jsonArray = jsonObj.optJSONArray("couponList");
				if (jsonArray != null) {
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject coupList = (JSONObject) jsonArray.get(i);
						TicketBean tb = new TicketBean();
						tb.cutPrice = coupList.optInt("cutPrice");// 送50
						tb.fullPrice = coupList.optInt("fullPrice"); // 满500
						tb.id = coupList.optString("id");
						tb.state = coupList.optBoolean("status"); // 优惠券状态

						tb.expiretime = FormatTime.format2(coupList
								.optString("expiretime"));// 结束时间
						tb.starttime = FormatTime.format2(coupList
								.optString("starttime")); // 开始时间
						arrayList.add(tb);
						//
						// if (coupList.optBoolean("status")) {
						//
						// totmoneyStr = totmoneyStr
						// + coupList.optInt("cutPrice");
						// }
					}
				}
				adapter.setData(arrayList);
				// System.out.println("totmoney" + totmoneyStr);

				if (confirmToTicket != null) {
					// 遍历list显示前面的圆圈
					setItemVisibility(true);
				}
				// totmoney.setText(totmoneyStr + "");

			}
			// if(arrayList.size() == 0) {
			// head1.setVisibility(View.GONE);
			// head2.setVisibility(View.GONE);
			// }

			// 计算优惠券总价

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// 查询优惠券
	private void xutilRequest(String totalMoney) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();

		final CustomProgressDialog dialog = new CustomProgressDialog(
				TicketActivity.this, "加载中。。。", R.anim.frame2);
		dialog.show();
		System.out.println("使用优惠券总价金额"+totalMoney);
		params.addBodyParameter("customerId", SpfUtil.getId() + "");
		
		if (totalMoney != null) {
			params.addBodyParameter("totalMoney", totalMoney);
		}

		httpUtils.send(HttpMethod.POST, GlobalUrl.ticket, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						setData();
						dialog.dismiss();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							String result = arg0.result;

							MyApplication.aCache.put("ticket", result);

							System.out.println("result-----------s" + result);
							setData();

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						dialog.dismiss();
					}

					@Override
					public void onStart() {

					}
				});

	}

	/**
	 * 
	 * @param visibility
	 *            true：显示全部园，false 隐藏全部园
	 * @return
	 */
	public void setItemVisibility(boolean visibility) { // 获取listview的所有item对象

		for (int i = 0; i < arrayList.size(); i++) {
			arrayList.get(i).visible = visibility;
		}
		adapter.notifyDataSetChanged();

	}

	private void onclick() {
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {

				if (confirmToTicket != null) {
					adapter.setSelectItem(position);

					Bundle bundle = new Bundle();
					String aa = arrayList.get(position).id;
					bundle.putString("id", arrayList.get(position).id);//id
					bundle.putString("ticketmoney", arrayList.get(position).cutPrice+"");//金额
					TicketActivity.this.setResult(50012,
							getIntent().putExtras(bundle));

					finish();
				}
			}
		});
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				TicketActivity.this.finish();
			}
		});
	}

	public void initApater() {

		View view = getLayoutInflater().inflate(R.layout.activity_empty, null);

		addContentView(view, new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		listView.setEmptyView(view);
		adapter = new TicketAdapter(this, arrayList);
		listView.setAdapter(adapter);
	}

	private void initView() {

		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);

		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("优惠券");
		// sc.smoothScrollTo(0, 20);
	}

}
