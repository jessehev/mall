package com.huayi.mall.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.ACache;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class PointActivity extends Activity {

	private TextView tv_title_logo;
	private ImageView back;
	private Button goshop;
	private TextView credit;
	private ACache aCache;
	private TextView bili;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_point);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		back = (ImageView) findViewById(R.id.back);
		goshop = (Button) findViewById(R.id.goshop);
		credit = (TextView) findViewById(R.id.credit);
		aCache = ACache.get(PointActivity.this);
		bili = (TextView) findViewById(R.id.bili);
		
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		onclcik();
		initdata();
	}

	private void onclcik() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				PointActivity.this.finish();
			}
		});
		goshop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				PointActivity.this.finish();
			}
		});
	}

	private void initdata() {
		// TODO Auto-generated method stub
		tv_title_logo.setText("积分");
		back.setVisibility(View.VISIBLE);
		getUserInfo();
	}

	// 获取用户信息
	public void getUserInfo() {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		System.out.println(SpfUtil.getId() + "======================");
		params.addBodyParameter("customerId", SpfUtil.getId() + "");
		httpUtils.send(HttpMethod.POST, GlobalUrl.getCustomerCredit, params,
				new RequestCallBack<String>() {
					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
						Toast.makeText(PointActivity.this, "获取失败", 0).show();
						System.out.println(arg1);
						
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {

						System.out.println("积分消息===="+arg0.result);
						try {
							JSONObject jsonObject = new JSONObject(arg0.result);
							credit.setText(jsonObject.getString("credit"));
							bili.setText(jsonObject.getString("propertion")+"积分=1元");
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						dialog.dismiss();
					}

					@Override
					public void onStart() {
						dialog = new CustomProgressDialog(PointActivity.this,
								"获取中。。。", R.anim.frame2);
						dialog.show();
					}
				});
	}

/*	public void getUserInfofcach() {
		// 获取缓存用户信息
		JSONObject asJSONObject = aCache.getAsJSONObject("userinfo");
		if (asJSONObject != null) {
			try {

				credit.setText(asJSONObject.getString("credit"));

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}*/
}
