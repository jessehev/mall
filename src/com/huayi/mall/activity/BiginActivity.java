package com.huayi.mall.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.huayi.mall.R;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;

public class BiginActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bigin);
		Button gonow = (Button) findViewById(R.id.gonow);
		gonow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (SpfUtil.isFrisit()) {
					startActivity(new Intent(BiginActivity.this,
							GuideActivity.class));
					SpfUtil.setFrisit(false);
				} else {
					if (SpfUtil.getLoginStatus()) {
						startActivity(new Intent(BiginActivity.this,
								MainActivity.class));
					} else {
						startActivity(new Intent(BiginActivity.this,
								LoginActivity.class));
					}
				}
				finish();
			}
		});
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.imagefangda);
		ImageView imageView = (ImageView) findViewById(R.id.begin);
		animation.setFillAfter(true);
		imageView.setAnimation(animation);
		new Thread() {

			public void run() {

				try {
					this.sleep(3000);
					if (SpfUtil.isFrisit()) {
						startActivity(new Intent(BiginActivity.this,
								GuideActivity.class));
						SpfUtil.setFrisit(false);
					} else {
						if (SpfUtil.getLoginStatus()) {

							startActivity(new Intent(BiginActivity.this,
									MainActivity.class));
						} else {
							startActivity(new Intent(BiginActivity.this,
									LoginActivity.class));
						}
					}
					finish();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			};
		}.start();

	}

}
