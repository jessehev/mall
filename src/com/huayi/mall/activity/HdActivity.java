package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.adapter.HdAdapter;
import com.huayi.mall.common.ActivityBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.MyListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class HdActivity extends Activity {

	private MyListView listview;

	private List<ActivityBean> arrayList = new ArrayList<ActivityBean>();
	private HdAdapter adapter;

	private ImageView back;

	private TextView tv_title_logo;

	private ScrollView mScrollView;
	public int currentPage = 1;
	public int totalPage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hd);
		findViewById();
		initView();
		initAdapter();
		onClick();
		xutilRequest(currentPage);
	}

	public void findViewById() {
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		listview = (MyListView) findViewById(R.id.listview_hd);
		mScrollView = (ScrollView) findViewById(R.id.ScrollView_hd);

	}

	private void initAdapter() {
		// TODO Auto-generated method stub

		adapter = new HdAdapter(this, arrayList);
		listview.setAdapter(adapter);
	}

	private void initView() {
		// TODO Auto-generated method stub
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("活动");
	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				HdActivity.this.finish();
			}
		});

		mScrollView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {

				int scrollY = view.getScrollY();
				int height = view.getHeight();
				int scrollViewMeasuredHeight = mScrollView.getChildAt(0)
						.getMeasuredHeight();
				if (scrollY == 0) {
					System.out.println("       ˶    view.getScrollY()="
							+ scrollY);
				}
				if ((scrollY + height) == scrollViewMeasuredHeight) {
					if ((currentPage) < totalPage) {
						xutilRequest(++currentPage);
						System.out.println("=================");
					} else {
						Toast.makeText(HdActivity.this, "没有更多了...", 0).show();
					}

				}

				return false;
			}
		});

	}

	public void setData() {

		JSONObject jsonObj = MyApplication.aCache.getAsJSONObject("HdActivity");
		if (jsonObj != null) {
			currentPage = jsonObj.optInt("currentPage");
			totalPage = jsonObj.optInt("totalPage");

			JSONArray rows = jsonObj.optJSONArray("rows");

			try {
				for (int i = 0; i < rows.length(); i++) {
					JSONObject obj = (JSONObject) rows.get(i);
					ActivityBean ab = new ActivityBean();
					ab.id = obj.optString("id");
					ab.publishTime = obj.optString("publishTime");
					ab.name = obj.optString("name");
					ab.descs = obj.optString("descs");
					ab.image = obj.optString("image");
					arrayList.add(ab);

				}
			} catch (Exception e) {

			}
		}
		adapter.setData(arrayList);

	}

	private void xutilRequest(int currentPage) {
		HttpUtils httpUtils = new HttpUtils();

		RequestParams params = new RequestParams();
		params.addBodyParameter("currentPage", currentPage + "");

		params.addBodyParameter("status", "1");

		httpUtils.send(HttpMethod.POST, GlobalUrl.huodong, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						setData();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							JSONObject jsonObj = new JSONObject(arg0.result);

							MyApplication.aCache.put("HdActivity", jsonObj);
							setData();

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();

						}

					}

					@Override
					public void onStart() {

					}
				});
	}
}
