package com.huayi.mall.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.ImagePath;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.ImageViewSubClass;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class EvaluateActivity extends Activity {

	private ImageViewSubClass imag1;
	private ImageViewSubClass imag2;
	private ImageViewSubClass imag3;
	private ImageView takeph;
	private Dialog dialog2;
	private String pic1=null;
	private String pic2=null;
	private String pic3=null;
	private List<String>list = new ArrayList<String>();
	private int i=0;//记录拍了几张照
	private static  String IMAGE_FILE_NAME = "face.jpg";  
	/* 请求码 */  
	private static final int IMAGE_REQUEST_CODE = 0;  
	private static final int SELECT_PIC_KITKAT = 3;  
	private static final int CAMERA_REQUEST_CODE = 1;  
	private static final int RESULT_REQUEST_CODE = 2;
	private RelativeLayout rl_include_title;
	private ImageView bakc;
	private TextView right_text;
	private View view;
	private EditText et_evalute;
	private RatingBar mine_ratingbar;
	private RatingBar mine_ratingbar2;
	private RatingBar mine_ratingbar3;
	private Bitmap bitmap;
	private int orderid;
	private int orderlistid;
	private TextView tv_title_logo; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_evaluate);
		mine_ratingbar = (RatingBar) findViewById(R.id.mine_ratingbar1);
		mine_ratingbar2 = (RatingBar) findViewById(R.id.mine_ratingbar2);
		mine_ratingbar3 = (RatingBar) findViewById(R.id.mine_ratingbar3);
		imag1 = (ImageViewSubClass) findViewById(R.id.picone);
		imag2 = (ImageViewSubClass) findViewById(R.id.pictwo);
		imag3 = (ImageViewSubClass) findViewById(R.id.picthree);
		takeph = (ImageView) findViewById(R.id.takeph);
		ImageViewSubClass evaImage = (ImageViewSubClass) findViewById(R.id.evaImage);
		rl_include_title = (RelativeLayout) findViewById(R.id.rl_include_title);
		bakc = (ImageView) findViewById(R.id.back);
		right_text = (TextView) findViewById(R.id.querentijiao);
		view = findViewById(R.id.cline);

		//导航栏
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		et_evalute = (EditText) findViewById(R.id.et_evalute);
		Intent intent = getIntent();
		orderid = intent.getIntExtra("orderid", -1);
		orderlistid = intent.getIntExtra("orderlistid", -1);
		bitmap = intent.getParcelableExtra("bitmap");
		evaImage.setImageBitmap(bitmap);
		init();
		onClick();
	}

	private void onClick() {
		// 点击拍照
		takeph.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {


				LinearLayout lin_dailog_item = (LinearLayout) View.inflate(EvaluateActivity.this,
						R.layout.dialog_choosephoto, null);
				dialog2 = new AlertDialog.Builder(EvaluateActivity.this).create();
				dialog2.show();
				WindowManager.LayoutParams params = dialog2.getWindow().getAttributes();
				WindowManager wm = (WindowManager)EvaluateActivity.this.getSystemService(Context.WINDOW_SERVICE);
				int height = wm.getDefaultDisplay().getHeight() / 4;
				int weith = wm.getDefaultDisplay().getWidth() / 4 * 3;
				params.width = weith;
				params.height = height;
				dialog2.getWindow().setContentView(lin_dailog_item);
				dialog2.getWindow().setAttributes(params);
				dialog2.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
						| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
				dialog2.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
				RadioGroup rg_photo = (RadioGroup) lin_dailog_item.findViewById(R.id.rg_photo);
				rg_photo.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						switch (checkedId) {
						case R.id.fromcamera:
							Intent intentFromCapture = new Intent(  
									MediaStore.ACTION_IMAGE_CAPTURE);  
							// 判断存储卡是否可以用，可用进行存储  

							intentFromCapture.putExtra(  
									MediaStore.EXTRA_OUTPUT,  
									Uri.fromFile(new File(Environment  
											.getExternalStorageDirectory(),  
											IMAGE_FILE_NAME)));  

							startActivityForResult(intentFromCapture,  
									CAMERA_REQUEST_CODE);  

							dialog2.dismiss();
							break;
						case R.id.fromphoto:

							Intent intent1=new Intent(Intent.ACTION_GET_CONTENT);  
							intent1.addCategory(Intent.CATEGORY_OPENABLE);  
							intent1.setType("image/*");  
							if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {  
								startActivityForResult(intent1,SELECT_PIC_KITKAT);  
							} else {  
								startActivityForResult(intent1,IMAGE_REQUEST_CODE);  
							}  
							dialog2.dismiss();
							break;


						default:
							break;
						}
					}
				});

			}
		});
		imag1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(EvaluateActivity.this,BigimageActivity.class);
				intent.putExtra("lujing", pic1);
				startActivity(intent);
			}
		});
		imag2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(EvaluateActivity.this,BigimageActivity.class);
				intent.putExtra("lujing", pic2);
				startActivity(intent);
			}
		});
		imag3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(EvaluateActivity.this,BigimageActivity.class);
				intent.putExtra("lujing", pic3);
				startActivity(intent);
			}
		});
		//提交
		right_text.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (!TextUtils.isEmpty(et_evalute.getText().toString())&orderid!=-1&orderlistid!=-1
						&pic1!=null) 
				{
					
						HttpUtils httpUtils = new HttpUtils();
						RequestParams params = new RequestParams();
						params.addBodyParameter("picture", new File(pic1));
						httpUtils.send(HttpMethod.POST, GlobalUrl.uploadImage, params,
								new RequestCallBack<String>() {
							private CustomProgressDialog dialog;
							private String picuir;
							@Override
							public void onFailure(HttpException arg0, String arg1) {
								Toast.makeText(EvaluateActivity.this, "获取失败", 0).show();
								System.out.println(arg1);

							}
							@Override
							public void onSuccess(ResponseInfo<String> arg0) {
								System.out.println("图片上传回调"+arg0.result);
								try {
									JSONObject jsonObject = new JSONObject(arg0.result);
									picuir = jsonObject.getString("picture");
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								HttpUtils httpUtils = new HttpUtils();
								RequestParams params = new RequestParams();
								params.addBodyParameter("customer.id", SpfUtil.getId()+"");
								params.addBodyParameter("evaluationPicture", picuir);

								params.addBodyParameter("level", ((mine_ratingbar.getRating()+mine_ratingbar2.getRating()+mine_ratingbar3.getRating())/3)+"");
								params.addBodyParameter("evaluationContent", et_evalute.getText().toString());
								params.addBodyParameter("itemId", orderlistid+"");
								params.addBodyParameter("orderId", orderid+"");
								httpUtils.send(HttpMethod.POST, GlobalUrl.insertEvaluation, params, new RequestCallBack<String>() {
									@Override
									public void onFailure(HttpException arg0, String arg1) {
										System.out.println(arg1);
										Toast.makeText(EvaluateActivity.this, "添加评论失败", 0).show();

									}

									@Override
									public void onSuccess(ResponseInfo<String> arg0) {
										System.out.println(arg0.result);
										dialog.dismiss();
										setResult(1);
										EvaluateActivity.this.finish();

									}

									@Override
									public void onStart() {
										dialog = new CustomProgressDialog(EvaluateActivity.this,"获取中。。。", R.anim.frame2);
										dialog.show();
									}
								});											
							}					
						});}

			}
		});
		bakc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				EvaluateActivity.this.finish();
			}
		});
	}

	private void init() {
		// TODO Auto-generated method stub
		/*rl_include_title*/
		/*rl_include_title.setBackgroundColor(Color.parseColor("#ffffff"));
		LayoutParams layoutParams = bakc.getLayoutParams();
		layoutParams.height=40;
		layoutParams.width=40;
		bakc.setLayoutParams(layoutParams);
		bakc.setBackgroundResource(R.drawable.close);*/
		tv_title_logo.setText("发表评价");
		bakc.setVisibility(View.VISIBLE);
		view.setVisibility(View.GONE);


	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {  
		// 结果码不等于取消时候  
		if (resultCode != EvaluateActivity.this.RESULT_CANCELED) {  
			switch (requestCode) {  
			case IMAGE_REQUEST_CODE:  
				startPhotoZoom(data.getData());  
				break;  
			case SELECT_PIC_KITKAT:  
				startPhotoZoom(data.getData());  
				break;  
			case CAMERA_REQUEST_CODE:  

				File tempFile = new File(Environment.getExternalStorageDirectory(),IMAGE_FILE_NAME);  
				startPhotoZoom(Uri.fromFile(tempFile));  


				break;  
			case RESULT_REQUEST_CODE:  
				/*File file = new File(Environment.getExternalStorageDirectory(),IMAGE_FILE_NAME);  
				s = file.getAbsolutePath();
				send();*/
				setImageToView(data);
				break;  
			}  
		}  
		super.onActivityResult(requestCode, resultCode, data);  
	} 
	/**  
	 * 裁剪图片方法实现  
	 *   
	 * @param uri  
	 */  
	public void startPhotoZoom(Uri uri) {  
		if (uri == null) {  
			Log.i("tag", "The uri is not exist.");  
			return;  
		}  

		Intent intent = new Intent("com.android.camera.action.CROP");  
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {  
			String url=ImagePath.getImageAbsolutePath(EvaluateActivity.this,uri);
			System.out.println(url);
			intent.setDataAndType(Uri.fromFile(new File(url)), "image/*");  		
		}else{  
			intent.setDataAndType(uri, "image/*"); 

		}  

		// 设置裁剪  
		intent.putExtra("crop", "true");  
		// aspectX aspectY 是宽高的比例  
		intent.putExtra("aspectX", 1);  
		intent.putExtra("aspectY", 1);  
		// outputX outputY 是裁剪图片宽高  
		intent.putExtra("outputX", 200);  
		intent.putExtra("outputY", 200);  
		intent.putExtra("return-data", true);  
		startActivityForResult(intent, RESULT_REQUEST_CODE);

	} 
	/**  
	 * 保存裁剪之后的图片数据  
	 *   
	 * @param picdata  
	 */  
	private void setImageToView(Intent data) {  
		Bundle extras = data.getExtras();  
		if (extras != null) {  
			Bitmap photo = extras.getParcelable("data");  
			/*Bitmap roundBitmap=ImageUtil.toRoundBitmap(photo);  */
			String saveBitmap = saveBitmap(photo);

			switch (i) {
			case 0:
				pic1=saveBitmap;
				imag1.setImageBitmap(photo);
				imag1.setVisibility(View.VISIBLE);
				i++;
				list.add(pic1);
				break;
			case 1:
				pic2=saveBitmap;
				imag2.setImageBitmap(photo);
				imag2.setVisibility(View.VISIBLE);
				i++;
				list.add(pic2);
				break;
			case 2:
				takeph.setVisibility(View.GONE);
				pic3=saveBitmap;
				imag3.setImageBitmap(photo);
				imag3.setVisibility(View.VISIBLE);
				i++;
				list.add(pic3);
				break;

			default:
				break;
			}
		}  
	} 
	public String saveBitmap(Bitmap mBitmap) {  
		File f = new File(Environment.getExternalStorageDirectory(),"face"+i+".jpg");  
		try {  
			f.createNewFile();  
			FileOutputStream fOut = null;  
			fOut = new FileOutputStream(f);  
			mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);  
			fOut.flush();  
			fOut.close(); 
			return f.getAbsolutePath();
		} catch (FileNotFoundException e) {  
			e.printStackTrace();  
			return null;
		} catch (IOException e) {  
			e.printStackTrace();  
			return null;
		}  
	}  



}
