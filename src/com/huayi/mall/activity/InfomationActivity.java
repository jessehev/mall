package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.adapter.InforAdapter;
import com.huayi.mall.common.ActivityBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.MyListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class InfomationActivity extends Activity {

	private MyListView listview;

	private List<ActivityBean> arrayList = new ArrayList<ActivityBean>();
	private InforAdapter adapter;

	private ImageView back;

	private TextView tv_title_logo;

	private ScrollView mScrollView;
	public int currentPage = 1;
	public int totalPage;

	private TextView pbTv;

	private LinearLayout layout;
	private ProgressBar pb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_infomation);
		findViewById();
		initView();
		initAdapter();
		onClick();
		xutilRequest(currentPage);
	}

	public void findViewById() {

		layout = (LinearLayout) findViewById(R.id.load_layout);
		pb = (ProgressBar) findViewById(R.id.progress);
		pbTv = (TextView) findViewById(R.id.progress_text);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		listview = (MyListView) findViewById(R.id.listview_info);
		mScrollView = (ScrollView) findViewById(R.id.ScrollView_info);

	}

	private void initAdapter() {
		// TODO Auto-generated method stub

		adapter = new InforAdapter(this, arrayList);
		listview.setAdapter(adapter);
	}

	private void initView() {
		// TODO Auto-generated method stub
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("资讯");
		layout.setVisibility(View.GONE);
	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				InfomationActivity.this.finish();
			}
		});

		mScrollView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {

				int scrollY = view.getScrollY();
				int height = view.getHeight();
				int scrollViewMeasuredHeight = mScrollView.getChildAt(0)
						.getMeasuredHeight();
				if (scrollY == 0) {
					System.out.println("       ˶    view.getScrollY()="
							+ scrollY);
				}
				if ((scrollY + height) == scrollViewMeasuredHeight
						&& event.getAction() == MotionEvent.ACTION_UP) {

					if (currentPage != totalPage) {
						pb.setVisibility(View.VISIBLE);
						currentPage++;
						xutilRequest(currentPage);
						System.out.println("=================");
					} else {
						System.out.println("=================");
						layout.setVisibility(View.VISIBLE);
						pb.setVisibility(View.GONE);
						pbTv.setText("没有更多内容了");
					}

				}

				return false;
			}
		});

		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Intent intent = new Intent(InfomationActivity.this,
						HdDataActivity.class);

				intent.putExtra("id", arrayList.get(position).id);
				startActivity(intent);
			}
		});
	}

	public void setData() {

		JSONObject jsonObj = MyApplication.aCache.getAsJSONObject("infomation");
		if (jsonObj != null) {
			currentPage = jsonObj.optInt("currentPage");
			totalPage = jsonObj.optInt("totalPage");

			JSONArray rows = jsonObj.optJSONArray("rows");

			try {
				for (int i = 0; i < rows.length(); i++) {
					JSONObject obj = (JSONObject) rows.get(i);
					ActivityBean ab = new ActivityBean();
					ab.id = obj.optString("id");
					ab.publishTime = obj.optString("publishTime");
					ab.name = obj.optString("name");
					ab.descs = obj.optString("descs");
					ab.image = obj.optString("image");
					arrayList.add(ab);

				}
			} catch (Exception e) {

			}
		}
		adapter.setData(arrayList);

	}

	private void xutilRequest(int currentPage) {
		HttpUtils httpUtils = new HttpUtils();

		RequestParams params = new RequestParams();
		params.addBodyParameter("currentPage", currentPage + "");

		httpUtils.send(HttpMethod.POST, GlobalUrl.huodong, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						setData();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							JSONObject jsonObj = new JSONObject(arg0.result);

							System.out.println("咨询---" + arg0.result);
							MyApplication.aCache.put("infomation", jsonObj);

							setData();

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();

						}

					}

					@Override
					public void onStart() {

					}
				});
	}
}
