package com.huayi.mall.activity;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.huayi.mall.R;
import com.huayi.mall.adapter.ConfirmAdapter;
import com.huayi.mall.base.AddressBean;
import com.huayi.mall.common.Cart;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.MyDialog;
import com.huayi.mall.util.SpfUtil;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

@SuppressLint("NewApi")
public class ConfirmActivity extends Activity {

	private ListView listview;

	private List<Cart> arrayList = new ArrayList<Cart>();
	private ConfirmAdapter adapter;

	private CheckBox checkBox, ticket;

	private ImageView Img_bg, img_null, line_confirm;
	private LinearLayout add_addr_layout, layout_null;
	private TextView name_tv, tel_tv, addr_tv, sumPrice;

	private JSONArray asJSONArray;

	private TextView tv_title_logo, mianyou, postAge, totalNum, sureOrder,
			point_confirm;
	private ImageView back, arrows;

	private String objStr;

	private String listString;

	private ScrollView myscrollview;
	// 订单id
	private String orderId;

	private LinearLayout chooseTicket, layout_point_confirm;
	// 买家留言
	private EditText message;
	// 优惠券id
	private String couponid;
	// 商品总数
	private String totalNumStr;
	// 地址id
	private String addressId;

	// 商品总价（邮费+商品价格）--》去除优惠券价格之前的总价

	private String totalPriceStr;
	// 积分使用
	private boolean point;
	// 积分总数
	private String credit;
	// 积分兑换比例
	private String propertion;

	private int money;

	private String cutPrice;
	// 订单类型 1 普通订单（从购物车进） 2 秒杀订单 3快速购买订单
	private int orderType = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirm);
		findViewById();
		onclick();
		initAdapater();
		Intent intent = getIntent();
		objStr = intent.getStringExtra("obj");
		orderType = intent.getIntExtra("orderType", 1);
		System.out.println(objStr + "确认订单------------");
		initData();
		initView();
	}

	private void initView() {
		// TODO Auto-generated method stub

		myscrollview.smoothScrollBy(0, 0);
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("确认订单");
		String moneyStr = sumPrice.getText().toString();
		BigDecimal bd1 = new BigDecimal(totalPriceStr); // 总价
		BigDecimal bd2 = new BigDecimal(money + "");// 积分兑换的价格

		if (bd1.compareTo(bd2) != -1) {
			layout_point_confirm.setVisibility(View.VISIBLE);
		} else {
			layout_point_confirm.setVisibility(View.GONE);
		}
	}

	public void initAdapater() {
		adapter = new ConfirmAdapter(this, arrayList);
		listview.setAdapter(adapter);
		setListViewHeightBasedOnChildren(listview);// 每次更新数据都需要调用

	}

	public void onclick() {

		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				String moneyStr = sumPrice.getText().toString();
				BigDecimal bd1 = new BigDecimal(moneyStr); // 总价
				BigDecimal bd2 = new BigDecimal(money + "");// 积分兑换的价格
				point = isChecked;
				// TO-DO 得到减去积分兑换金额的总价

				// 商品总总价必须大于积分金额

				if (isChecked) {

					sumPrice.setText(bd1.subtract(bd2).toString());
				} else {
					sumPrice.setText(bd1.add(bd2).toString());
				}
			}
		});
		// ticket.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		// @Override
		// public void onCheckedChanged(CompoundButton buttonView,
		// boolean isChecked) {
		// Toast.makeText(ConfirmActivity.this, isChecked + "", 1).show();
		//
		//
		//
		// }
		// });
		add_addr_layout.setOnClickListener(new OnClickListener() { // 有地址显示的时候

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						Intent intent = new Intent(ConfirmActivity.this,
								AddressActivity.class);

						intent.putExtra("confirmAddress", "cAddress");
						startActivityForResult(intent, 12121);
					}
				});
		layout_null.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// 如果有默认地址数据 就使用默认地址否则跳转到添加地址

				Intent intent = new Intent(ConfirmActivity.this,
						AddressActivity.class);

				intent.putExtra("confirmAddress", "cAddress");
				startActivityForResult(intent, 12121);
			}
		});
		sureOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				makeSureOrder(orderId, addr_tv.getText().toString(), message
						.getText().toString(), couponid, point, orderType);

			}
		});
		chooseTicket.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(ConfirmActivity.this,
						TicketActivity.class);

				intent.putExtra("confirmToTicket", "confirmToTicket");
				intent.putExtra("totalMoney", totalPriceStr);

				startActivityForResult(intent, 5000);

			}
		});

		ticket.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				ticket.setVisibility(View.GONE);
				arrows.setVisibility(View.VISIBLE);

				String moneyStr = sumPrice.getText().toString();
				BigDecimal bd1 = new BigDecimal(moneyStr); // 总价
				BigDecimal bd2 = new BigDecimal(cutPrice);//

				sumPrice.setText(bd1.add(bd2).toString());

			}
		});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				createDialog();
			}
		});
	}

	/**
	 * 确认订单界面返回按钮dialog
	 */
	public void createDialog() {
		View lin = (LinearLayout) View.inflate(ConfirmActivity.this,
				R.layout.dialog_cart, null);
		final Dialog dialog = MyDialog.createDialog(ConfirmActivity.this, lin);
		TextView show = (TextView) lin.findViewById(R.id.suredelete);
		TextView delete = (TextView) lin.findViewById(R.id.sure_dialog_cart);
		ImageView imageview = (ImageView) lin
				.findViewById(R.id.cancle_dialog_cart);

		show.setText("确定离开该页面吗？");
		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setResult();
				dialog.dismiss();

			}
		});
		imageview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			createDialog();
		}
		return super.onKeyDown(keyCode, event);
	}

	// 返回购物车 还原购物车
	public void setResult() {
		Intent intent = getIntent();
		Bundle bundle = new Bundle();

		bundle.putString("orderId", orderId);

		intent.putExtras(bundle);
		ConfirmActivity.this.setResult(30054, intent);
		ConfirmActivity.this.finish();
	}
	// 确认订单
	private void makeSureOrder(String orderId, String address, String message,
			String couponId, boolean useCredit, int orderType) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("addressId", addressId);
		params.addBodyParameter("orderId", orderId);
		params.addBodyParameter("orderType", orderType + ""); // 订单类型
		// 是否使用买家留言
		if (!TextUtils.isEmpty(message)) {
			params.addBodyParameter("message", message);
		}
		// 是否使用优惠券
		if (couponId != null) {
			params.addBodyParameter("couponId", couponId);
		}

		System.out.println("useCredit" + useCredit);
		// 是否使用积分
		if (useCredit) {
			params.addBodyParameter("useCredit", useCredit + "");
			params.addBodyParameter("customerId", SpfUtil.getId() + "");
		}
		System.out.println("orderId" + orderId + "address" + address
				+ "message" + message + "couponid" + couponId);

		httpUtils.send(HttpMethod.POST, GlobalUrl.makeSureOrder, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {

					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {

						try {
							JSONObject result = new JSONObject(arg0.result);

							System.out.println("arg0.result" + arg0.result);

							Toast.makeText(ConfirmActivity.this,
									result.optString("message"), 1).show();

							if (result.optBoolean("success")) {
								Intent intent = new Intent(
										ConfirmActivity.this, PayActivity.class);
								// 商品总价+ 邮费
								// BigDecimal bd = new BigDecimal(postAge
								// .getText().toString())
								// .add(new BigDecimal(sumPrice.getText()
								// .toString()));

								// System.out.println("商品价格和运费---" + sum);

								intent.putExtra("goodsPrice", sumPrice
										.getText().toString());

								intent.putExtra("goodsNum", totalNumStr);// 商品总数
								intent.putExtra("orderId",
										ConfirmActivity.this.orderId);// 订单id

								startActivity(intent);
								ConfirmActivity.this.finish();
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {

					}
				});
	}

	public void findViewById() {

		layout_point_confirm = (LinearLayout) findViewById(R.id.layout_point_confirm);
		point_confirm = (TextView) findViewById(R.id.point_confirm);
		message = (EditText) findViewById(R.id.message_confirm);
		chooseTicket = (LinearLayout) findViewById(R.id.chooseTicket_confirm);
		myscrollview = (ScrollView) findViewById(R.id.myscrollview_confirm);
		back = (ImageView) findViewById(R.id.back);
		sureOrder = (TextView) findViewById(R.id.sureOrder_confirm);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);

		sumPrice = (TextView) findViewById(R.id.sumPrice_confirm);

		tel_tv = (TextView) findViewById(R.id.phone_confirm);
		name_tv = (TextView) findViewById(R.id.name_confirm);
		addr_tv = (TextView) findViewById(R.id.address_confirm);

		listview = (ListView) findViewById(R.id.listView_confirm);
		checkBox = (CheckBox) findViewById(R.id.checkbox_confirm);
		add_addr_layout = (LinearLayout) findViewById(R.id.add_addr_layout);
		Img_bg = (ImageView) findViewById(R.id.Img_null);
		layout_null = (LinearLayout) findViewById(R.id.layout_null);
		line_confirm = (ImageView) findViewById(R.id.line_confirm);

		postAge = (TextView) findViewById(R.id.postage_confirm);
		ticket = (CheckBox) findViewById(R.id.ticket_confirm);
		arrows = (ImageView) findViewById(R.id.arrows_confirm);
		mianyou = (TextView) findViewById(R.id.mianyoufei_confirm);
		totalNum = (TextView) findViewById(R.id.totalNum_confirm);
	}

	public void initData() {

		xutilRequest();// 地址请求
		addrInfoVisible();

		if (objStr != null) {
			try {
				JSONObject jsonObj = new JSONObject(objStr);
				JSONObject order = jsonObj.optJSONObject("order");
				propertion = jsonObj.optString("propertion");// 积分比例
				orderId = order.optString("id");

				JSONObject customer = order.optJSONObject("customer");
				credit = customer.optString("credit"); // 积分总数

				JSONArray jsonArray = order.optJSONArray("orderItemList");

				if (jsonArray != null) {
					for (int i = 0; i < jsonArray.length(); i++) {

						Cart cart = new Cart();
						JSONObject orderItemList = jsonArray.optJSONObject(i);
						JSONObject product = orderItemList // 商品详情
								.optJSONObject("product");

						cart.price = product.optString("actualPrice");
						cart.name = product.optString("productName");
						cart.picture = product.optString("productPicture")
								.split(",")[0];
						cart.describe = product.optString("description");
						arrayList.add(cart);

					}
					adapter.setData(arrayList);
					setListViewHeightBasedOnChildren(listview);// 每次更新数据都需要调用
				}
				// 商品价格
				String aaa = order.optString("totalMoney");

				postAge.setText(order.optString("postage")); // 运费
				// 运费+商品价格
				BigDecimal bd = new BigDecimal(postAge.getText().toString())
						.add(new BigDecimal(aaa));

				totalPriceStr = bd.toString();

				if (credit != null && propertion != null) {

					money = Integer.parseInt(credit)
							/ Integer.parseInt(propertion);
					point_confirm.setText("使用" + credit + "积分可以抵扣" + money
							+ "元");
				}
				sumPrice.setText(bd.toString());// 所有商品总价

				totalNumStr = order.optString("totalNum");
				totalNum.setText(Html
						.fromHtml("<font color=\'#999999\'>共 </font><font color=\'#e40d3f\'>"
								+ totalNumStr
								+ "</font><font color=\'#999999\'>件商品</font>"));

				if (!postAge.getText().toString().equals("0")) {
					mianyou.setText("快递 免邮"); // (快递免邮)
				}
			} catch (Exception e) {

			}

		}

	}

	// 当默认地址的时候隐藏空数据背景
	public void addrInfoVisible() {
		if (listString == null) {
			Img_bg.setVisibility(View.VISIBLE);
			layout_null.setVisibility(View.VISIBLE);
			add_addr_layout.setVisibility(View.GONE);
			line_confirm.setVisibility(View.GONE);
		} else {
			Img_bg.setVisibility(View.GONE);
			layout_null.setVisibility(View.GONE);
			add_addr_layout.setVisibility(View.VISIBLE);
			line_confirm.setVisibility(View.VISIBLE);

		}
	}

	// 重新计算组件高度
	public void setListViewHeightBasedOnChildren(ListView listView) {
		if (listView == null)
			return;
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}
		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

	public void setData() {
		listString = MyApplication.aCache.getAsString("address_confirm");

		if (listString != null) { // 如果有默认地址 就取
			try {
				JSONArray jsonArray = new JSONArray(listString);
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObj = (JSONObject) jsonArray.opt(i);

					if (jsonObj.optBoolean("defaultAddress")) {
						// 取默认地址
						tel_tv.setText(jsonObj.optString("phone"));
						addr_tv.setText(jsonObj.optString("address"));

						addressId = jsonObj.optString("id"); // 地址id
						name_tv.setText(jsonObj.optString("receiver")); // 收货人
						addrInfoVisible();
					}

				}

			} catch (Exception e) {

			}
		}
	}

	// 获取地址接口
	private void xutilRequest() {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("id", "" + SpfUtil.getId());

		httpUtils.send(HttpMethod.POST, GlobalUrl.getAddressByCustomer, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						System.out.println(arg1);
						setData();

					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {

						String result = arg0.result;
						Gson gson = new Gson();
						Type listType = new TypeToken<List<AddressBean>>() {
						}.getType();
						List list = gson.fromJson(result, listType);

						String listStr = gson.toJson(list);
						MyApplication.aCache.put("address_confirm", listStr);

						setData();

					}

					@Override
					public void onStart() {

					}
				});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 4593 && data != null) {
			AddressBean addressBean = (AddressBean) data.getExtras()
					.getSerializable("ListAddress");
			tel_tv.setText(addressBean.phone);
			addr_tv.setText(addressBean.address);
			name_tv.setText(addressBean.receiver);

			addressId = addressBean.getId();
			Img_bg.setVisibility(View.GONE);
			layout_null.setVisibility(View.GONE);
			add_addr_layout.setVisibility(View.VISIBLE);
			line_confirm.setVisibility(View.VISIBLE);

		} else if (resultCode == 50012) { // 优惠券的使用返回值
			Bundle bundle = data.getExtras();

			couponid = bundle.getString("id");

			System.out.println("couponid" + couponid);
			cutPrice = bundle.getString("ticketmoney");

			String moneyStr = sumPrice.getText().toString();
			BigDecimal bd1 = new BigDecimal(moneyStr); // 总价
			BigDecimal bd2 = new BigDecimal(cutPrice);//

			sumPrice.setText(bd1.subtract(bd2).toString());

			ticket.setVisibility(View.VISIBLE);
			arrows.setVisibility(View.GONE);

		}
	}

}
