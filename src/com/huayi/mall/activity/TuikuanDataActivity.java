package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class TuikuanDataActivity extends Activity{

	private List<String>list = new ArrayList<String>();
	private Spinner spinner;
	private TextView tijiao;
	private ImageView back;
	private TextView tv_title_logo;
	private String id;
	private String orderid;
	private int status;
	private EditText tuikuanyunyin;
	private int itemstatus;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tuikuandata);
		spinner = (Spinner) findViewById(R.id.spinner);
		tijiao = (TextView) findViewById(R.id.tijiaoshenqing);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		Intent intent = getIntent();
		id = intent.getStringExtra("id");
		orderid = intent.getStringExtra("orderid");
		tuikuanyunyin = (EditText) findViewById(R.id.tuikuanyunyin);
		status = intent.getIntExtra("status", -1);
		 itemstatus = intent.getIntExtra("itemstatus", -1);
		System.out.println("退款id==="+id);
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		
		init();
		onclick();


	}
	private void onclick() {
		// TODO Auto-generated method stub
		tijiao.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
			
				if (list.get(spinner.getSelectedItemPosition()).equals("退款")) {
					System.out.println("退款");
					send(GlobalUrl.applyRefund);
					
				}else if (list.get(spinner.getSelectedItemPosition()).equals("返修")){
					System.out.println("返修");
					send(GlobalUrl.applyRepair);
				}else if(list.get(spinner.getSelectedItemPosition()).equals("退货")){}
				System.out.println("退货");
				send(GlobalUrl.applyReturnGoods);
			}
		});
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				TuikuanDataActivity.this.finish();
			}
		});
	}
	private void init() {
		// TODO Auto-generated method stub
		switch (status) {
		case 3:
			list.add("退款");
			
			break;
		case 4:
			
			list.add("退货");
			list.add("返修");
			
			break;
		case 5:
			list.add("返修");
			
			break;
		case 6:
			
			list.add("返修");
			
			
			break;

		default:
			break;
		}
		
		spinner.setAdapter(new IMAdapter());
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("申请售后");
		
	}
	public class IMAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(TuikuanDataActivity.this, R.layout.spinner_item, null);
			TextView  text = (TextView) arg1.findViewById(R.id.text);
			text.setText(list.get(arg0));

			return arg1;
			
		}}
	private void send(String url) {
		HttpUtils httpUtils = new HttpUtils();
		/*customer.id 然后就是其他的    
	    private String receiver;
	    private String address;
	    private String phone;
	    private boolean defaultAddress;*/
		RequestParams params = new RequestParams();
		System.out.println("用户id"+id);
		params.addBodyParameter("orderItem", id);
		params.addBodyParameter("orderId", orderid);
		params.addBodyParameter("message", tuikuanyunyin.getText().toString());//收件人
			
		
		httpUtils.send(HttpMethod.POST,url , params, new RequestCallBack<String>() {

			private CustomProgressDialog dialog;

			@Override
			public void onFailure(HttpException arg0, String arg1) {
			dialog.dismiss();
			Toast.makeText(TuikuanDataActivity.this, "提交申请成功", 0).show();
			System.out.println(arg1);
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				
				Toast.makeText(TuikuanDataActivity.this, "提交申请成功", 0).show();
				System.out.println(arg0.result);
				dialog.dismiss();
				setResult(1);
				TuikuanDataActivity.this.finish();
			}

			@Override
			public void onStart() {
				dialog = new CustomProgressDialog(TuikuanDataActivity.this,"提交申请成功中。。。", R.anim.frame2);
				dialog.show();
			}
		});
	}
}
