package com.huayi.mall.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.util.CacheUtils;
import com.huayi.mall.util.ImagePath;
import com.huayi.mall.util.IsNetWorkConnected;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CircleImageView;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

public class SettingActivity extends Activity {

	ListView listview;

	List arrayList = new ArrayList();

	private LinearLayout icon, name, clearChache, version, changePwd, about;
	private ImageView back;

	private CircleImageView img;
	private TextView tv_title_logo, nickName, cacheSize, existLogin,
			showVersion;

	/* 请求码 */
	private static final int IMAGE_REQUEST_CODE = 0;
	private static final int SELECT_PIC_KITKAT = 3;
	private static final int CAMERA_REQUEST_CODE = 1;
	private static final int RESULT_REQUEST_CODE = 2;

	// 图片储存路径
	private static String IMAGE_FILE_NAME = "face.jpg";
	private LinearLayout lin_dailog_item;
	private Dialog dialog;

	String url, descs;
	int branchVersion;

	private String androidImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		findViewById();
		initView();
		initData();
		onclick();

	}

	public void initView() {
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("设置");
	}

	public void initData() {

		if (IsNetWorkConnected.isNetworkConnected(this)) {

			xutilRequest();
		} else {
			setData();
		}
		// 缓存大小
		cacheSize.setText(CacheUtils.getCacheSize(this));

		xutilVersion();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		xutilRequest();
	}

	// 请求个人资料
	public void xutilRequest() {

		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();

		params.addBodyParameter("id", SpfUtil.getId() + "");

		System.out.println("id---info" + SpfUtil.getId());

		httpUtils.send(HttpMethod.POST, GlobalUrl.getInfo, params,
				new RequestCallBack<String>() {
					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						setData();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						System.out.println("info----------" + arg0.result);

						try {
							JSONObject jsonObj = new JSONObject(arg0.result);

							MyApplication.aCache.put("Setting", jsonObj);
							setData();
						} catch (Exception e) {

						}
					}

					@Override
					public void onStart() {

					}
				});

	}

	public void setData() {
		JSONObject jsonObj = MyApplication.aCache.getAsJSONObject("Setting");

		if (jsonObj != null) {

			String nickNameStr = jsonObj.optString("nickName");
			String headPicture = jsonObj.optString("headPicture");

			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost + headPicture, img,
					ImageLoaderCfg.options);

			System.out.println(headPicture + "获取用户资料的图片地址");
			nickName.setText(nickNameStr);
		}

	}

	public void onclick() {
		// 我的头像
		icon.setOnClickListener(new OnClickListener() {
			private Dialog dialog2;

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				LinearLayout lin_dailog_item = (LinearLayout) View
						.inflate(SettingActivity.this,
								R.layout.dialog_choosephoto, null);
				dialog2 = new AlertDialog.Builder(SettingActivity.this)
						.create();
				dialog2.show();
				WindowManager.LayoutParams params = dialog2.getWindow()
						.getAttributes();
				WindowManager wm = (WindowManager) SettingActivity.this
						.getSystemService(Context.WINDOW_SERVICE);
				int height = wm.getDefaultDisplay().getHeight() / 4;
				int weith = wm.getDefaultDisplay().getWidth() / 4 * 3;
				params.width = weith;
				params.height = height;
				dialog2.getWindow().setContentView(lin_dailog_item);
				dialog2.getWindow().setAttributes(params);
				dialog2.getWindow()
						.clearFlags(
								WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
										| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
				dialog2.getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
				RadioGroup rg_photo = (RadioGroup) lin_dailog_item
						.findViewById(R.id.rg_photo);
				rg_photo.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						switch (checkedId) {
						case R.id.fromcamera:
							Intent intentFromCapture = new Intent(
									MediaStore.ACTION_IMAGE_CAPTURE);
							// 判断存储卡是否可以用，可用进行存储

							intentFromCapture.putExtra(MediaStore.EXTRA_OUTPUT,
									Uri.fromFile(new File(Environment
											.getExternalStorageDirectory(),
											IMAGE_FILE_NAME)));

							startActivityForResult(intentFromCapture,
									CAMERA_REQUEST_CODE);

							dialog2.dismiss();
							break;
						case R.id.fromphoto:

							Intent intent1 = new Intent(
									Intent.ACTION_GET_CONTENT);
							intent1.addCategory(Intent.CATEGORY_OPENABLE);
							intent1.setType("image/*");
							if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
								startActivityForResult(intent1,
										SELECT_PIC_KITKAT);
							} else {
								startActivityForResult(intent1,
										IMAGE_REQUEST_CODE);
							}
							dialog2.dismiss();
							break;

						default:
							break;
						}
					}
				});

			}
		});
		// 我的昵称
		name.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SettingActivity.this,
						ChangeNameActivity.class);

				intent.putExtra("nickName", nickName.getText().toString());
				startActivity(intent);
			}
		});
		// 修改密码
		changePwd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(SettingActivity.this,
						ChangePwdActivity.class));
			}
		});
		// 清除缓存
		clearChache.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try {

					// String size = MyCache.getCacheSize(SettingActivity.this);

					ImageLoader.getInstance().clearMemoryCache();
					ImageLoader.getInstance().clearDiscCache();

					String cache = CacheUtils
							.getCacheSize(SettingActivity.this);

					System.out.println("cache--" + cache);
					cacheSize.setText(cache);

					Toast.makeText(SettingActivity.this, "当前缓存已清空", 1).show();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		// 版本跟新
		version.setOnClickListener(new OnClickListener() {
			Dialog dialog2;

			@Override
			public void onClick(View v) {

				LinearLayout item = (LinearLayout) View.inflate(
						SettingActivity.this, R.layout.dialog_versiom, null);

				dialog2 = new AlertDialog.Builder(SettingActivity.this)
						.create();
				if (getVersionCode(SettingActivity.this) < branchVersion) {
					dialog2.show();
				}

				WindowManager.LayoutParams params = dialog2.getWindow()
						.getAttributes();
				WindowManager wm = (WindowManager) SettingActivity.this
						.getSystemService(Context.WINDOW_SERVICE);
				int height = wm.getDefaultDisplay().getHeight() / 2;
				int weith = wm.getDefaultDisplay().getWidth() / 4 * 3;
				params.width = weith;
				params.height = height;
				dialog2.getWindow().setContentView(item);
				dialog2.getWindow().setAttributes(params);
				dialog2.getWindow()
						.clearFlags(
								WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
										| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
				dialog2.getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
				TextView content = (TextView) item
						.findViewById(R.id.content_version);
				TextView versionNum = (TextView) item
						.findViewById(R.id.versionNum_dialog);
				TextView noUpdate = (TextView) item
						.findViewById(R.id.noUpdate_version);
				TextView goToUpdate = (TextView) item
						.findViewById(R.id.goToUpdate_version);

				versionNum.setText("最新版本为" + branchVersion);
				noUpdate.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog2.dismiss();

					}
				});
				goToUpdate.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (url != null) {
							Uri uri = Uri.parse(url);
							Intent intent = new Intent(Intent.ACTION_VIEW, uri);
							startActivity(intent);
						}
						dialog2.dismiss();
					}
				});
				StringBuilder sb = new StringBuilder();
				sb.append("【更新内容  " + descs + "】\n");
				// for (int i = 0; i < 10; i++) {
				// sb.append("【更新内容】\n");
				// }
				content.setText(sb.toString());
			}

		});
		// 关于五洲购
		about.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(SettingActivity.this,
						AboutActivity.class);
				intent.putExtra("androidPicture", androidImage);
				intent.putExtra("branchVersion", branchVersion + "");

				startActivity(intent);
			}
		});
		// 退出登录
		existLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				quitLogin();

			}
		});
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});
	}

	/**
	 * Uri uri = Uri.parse(uri_str); Intent intent = new
	 * Intent(Intent.ACTION_VIEW, uri); startActivity(intent);
	 */
	public void setVersion() {
		JSONObject obj = MyApplication.aCache.getAsJSONObject("Version");

		if (obj != null) {
			url = obj.optString("android");
			branchVersion = obj.optInt("branchVersion");
			descs = obj.optString("descs");

			androidImage = obj.optString("androidImage");
			if (getVersionCode(SettingActivity.this) == branchVersion) {
				showVersion.setText("当前为最新版本 ");
			} else {
				showVersion.setText("当前版本 " + getVersionCode(this));
			}
		}
	}

	// 获取版本号
	public int getVersionCode(Context context) {
		return getPackageInfo(context).versionCode;
	}

	public PackageInfo getPackageInfo(Context context) {
		PackageInfo pi = null;

		try {
			PackageManager pm = context.getPackageManager();
			pi = pm.getPackageInfo(context.getPackageName(),
					PackageManager.GET_CONFIGURATIONS);

			return pi;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return pi;
	}

	// 版本更新
	private void xutilVersion() {

		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();

		httpUtils.send(HttpMethod.POST, GlobalUrl.getVersion, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						setVersion();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {

							JSONObject obj = new JSONObject(arg0.result);
							MyApplication.aCache.put("Version", obj);

							setVersion();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {

					}
				});
	}

	// 删除弹出框
	public void quitLogin() {
		if (lin_dailog_item == null && dialog == null) {
			lin_dailog_item = (LinearLayout) View.inflate(SettingActivity.this,
					R.layout.dialog_cart, null);
			dialog = new AlertDialog.Builder(SettingActivity.this).create();

		}
		dialog.show();
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();

		WindowManager wm = (WindowManager) SettingActivity.this
				.getSystemService(Context.WINDOW_SERVICE);
		int height = wm.getDefaultDisplay().getHeight() / 3;
		int weith = wm.getDefaultDisplay().getWidth() / 5 * 3;
		params.width = weith;
		params.height = height;
		params.gravity = Gravity.TOP;
		params.y = wm.getDefaultDisplay().getHeight() / 5;

		dialog.getWindow().setContentView(lin_dailog_item);
		dialog.getWindow().setAttributes(params);
		dialog.getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
						| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		dialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		ImageView cancle = (ImageView) lin_dailog_item
				.findViewById(R.id.cancle_dialog_cart);

		TextView text = (TextView) lin_dailog_item
				.findViewById(R.id.suredelete);
		text.setText("确认要退出吗？");
		TextView sure = (TextView) lin_dailog_item
				.findViewById(R.id.sure_dialog_cart);
		sure.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				SpfUtil.setLoginStatus(false);

				// 清除缓存
				MyApplication.aCache.clear();
				SpfUtil.clear();
				SpfUtil.setFrisit(false);
				System.out.println("SpfUtil" + SpfUtil.isFrisit());

				JPushInterface.setAlias(SettingActivity.this, 0 + "",
						new TagAliasCallback() {

							@Override
							public void gotResult(int arg0, String arg1,
									Set<String> arg2) {
								// TODO Auto-generated method stub
								System.out.println("退出登录别名设置状态提示码===" + arg0);
								if (arg0 == 0) {
									Intent intent = new Intent();
									intent.setAction("com.huayi");
									SettingActivity.this.sendBroadcast(intent);

									startActivity(new Intent(
											SettingActivity.this,
											LoginActivity.class));
									SettingActivity.this.finish();
								} else {
									Toast.makeText(SettingActivity.this,
											"请检查网络连接", 0).show();
								}
							}
						});

				dialog.cancel();

			}
		});
		cancle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog.cancel();

			}
		});

	}

	public void findViewById() {
		showVersion = (TextView) findViewById(R.id.showVersion_setting);
		existLogin = (TextView) findViewById(R.id.existLogin_setting);
		img = (CircleImageView) findViewById(R.id.imaggehead_setting);

		nickName = (TextView) findViewById(R.id.nickName_setting);

		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		icon = (LinearLayout) findViewById(R.id.icon_setting);
		name = (LinearLayout) findViewById(R.id.name_setting);
		changePwd = (LinearLayout) findViewById(R.id.change_pwd_setting);
		clearChache = (LinearLayout) findViewById(R.id.clear_Achecha_setting);
		version = (LinearLayout) findViewById(R.id.version_setting);
		about = (LinearLayout) findViewById(R.id.about_setting);

		cacheSize = (TextView) findViewById(R.id.cacheSize_setting);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 结果码不等于取消时候
		if (resultCode != SettingActivity.this.RESULT_CANCELED) {
			switch (requestCode) {
			case IMAGE_REQUEST_CODE:
				startPhotoZoom(data.getData());
				break;
			case SELECT_PIC_KITKAT:
				startPhotoZoom(data.getData());
				break;
			case CAMERA_REQUEST_CODE:

				File tempFile = new File(
						Environment.getExternalStorageDirectory(),
						IMAGE_FILE_NAME);
				startPhotoZoom(Uri.fromFile(tempFile));

				break;
			case RESULT_REQUEST_CODE:
				setImageToView(data);
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * 裁剪图片方法实现
	 * 
	 * @param uri
	 */
	public void startPhotoZoom(Uri uri) {
		if (uri == null) {
			Log.i("tag", "The uri is not exist.");
			return;
		}

		Intent intent = new Intent("com.android.camera.action.CROP");
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
			String url = ImagePath.getImageAbsolutePath(SettingActivity.this,
					uri);
			System.out.println(url);
			intent.setDataAndType(Uri.fromFile(new File(url)), "image/*");
		} else {
			intent.setDataAndType(uri, "image/*");

		}

		// 设置裁剪
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 200);
		intent.putExtra("outputY", 200);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, RESULT_REQUEST_CODE);

	}

	/**
	 * 保存裁剪之后的图片数据
	 * 
	 * @param picdata
	 */
	private void setImageToView(Intent data) {
		Bundle extras = data.getExtras();
		if (extras != null) {
			final Bitmap photo = extras.getParcelable("data");

			String saveBitmap = saveBitmap(photo);

			HttpUtils httpUtils = new HttpUtils();
			RequestParams params = new RequestParams();
			params.addBodyParameter("picture", new File(saveBitmap));

			httpUtils.send(HttpMethod.POST, GlobalUrl.image, params,
					new RequestCallBack<String>() {
						private CustomProgressDialog dialog;

						@Override
						public void onFailure(HttpException arg0, String arg1) {
							dialog.dismiss();

						}

						@Override
						public void onSuccess(ResponseInfo<String> arg0) {
							System.out.println("上传头像后的图片地址----------+"
									+ arg0.result);

							try {
								JSONObject jsonObj = new JSONObject(arg0.result);

								String picture = jsonObj.optString("picture");

								if (jsonObj.optBoolean("success")) {
									dialog.dismiss();

									xutilRequestPicture(picture);

								}
							} catch (Exception e) {

							}
						}

						@Override
						public void onStart() {
							dialog = new CustomProgressDialog(
									SettingActivity.this, "头像上传中。。。",
									R.anim.frame2);
							dialog.show();
						}
					});

		}
	}

	public String saveBitmap(Bitmap mBitmap) {
		File f = new File(Environment.getExternalStorageDirectory(), "face"
				+ ".jpg");

		try {
			f.createNewFile();
			FileOutputStream fOut = null;
			fOut = new FileOutputStream(f);
			mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
			fOut.flush();
			fOut.close();
			return f.getAbsolutePath();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	// 修改 头像
	private void xutilRequestPicture(final String picture) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();

		params.addBodyParameter("id", SpfUtil.getId() + "");
		params.addBodyParameter("headPicture", picture);

		httpUtils.send(HttpMethod.POST, GlobalUrl.updataInfo, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {

					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							JSONObject obj = new JSONObject(arg0.result);
							if (obj.optBoolean("success")) {
								System.out.println("arg0.result" + arg0.result);

								Toast.makeText(SettingActivity.this, "头像上传成功",
										1).show();

								ImageLoader.getInstance().displayImage(
										GlobalUrl.serviceHost + picture, img,
										ImageLoaderCfg.options);
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onStart() {

					}
				});
	}

}
