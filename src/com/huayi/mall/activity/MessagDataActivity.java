package com.huayi.mall.activity;

import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.base.NewsBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.ParesData2Obj;
import com.huayi.mall.util.SystemBar;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class MessagDataActivity extends Activity {

	private ImageView back;
	private TextView tv_title_logo;
	private TextView newstitle;
	private TextView newscontent;
	private TextView newstime;
	private NewsBean.RowsBean bean;
	private String id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_messag_data);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		newstitle = (TextView) findViewById(R.id.newstitle);
		newscontent = (TextView) findViewById(R.id.newscontent);
		newstime = (TextView) findViewById(R.id.newstime);
		init();
		onClick();
	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				MessagDataActivity.this.finish();	
			}
		});
	}

	private void init() {
		// TODO Auto-generated method stub
		tv_title_logo.setText("消息详情");
		back.setVisibility(View.VISIBLE);
		Intent intent = getIntent();
		id = intent.getStringExtra("id");
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		send();
	}
	public void send(){


		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("id", id);
		httpUtils.send(HttpMethod.POST, GlobalUrl.findNewsById , params, new RequestCallBack<String>() {
			@Override
			public void onFailure(HttpException arg0, String arg1) {
				Toast.makeText(MessagDataActivity.this,"获取失败，请检查网络设置", 0).show();

			}
			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				System.out.println("消息信息============"+arg0.result);
				bean=ParesData2Obj.json2Obj(arg0.result, NewsBean.RowsBean.class);

				newstitle.setText(bean.getNewsName());
				newscontent.setText(bean.getNewsContent());
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String format = dateFormat.format(bean.getCreateTime());
				newstime.setText(format);


			}
		});	

	}

}
