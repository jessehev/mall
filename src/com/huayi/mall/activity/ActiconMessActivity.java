package com.huayi.mall.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.huayi.mall.R;
import com.huayi.mall.R.id;
import com.huayi.mall.R.layout;
import com.huayi.mall.base.NewsBean;
import com.huayi.mall.base.NewsBean.RowsBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.ParesData2Obj;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.MyListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class ActiconMessActivity extends Activity implements
		SwipeRefreshLayout.OnRefreshListener {

	private ImageView back;
	private TextView tv_title_logo;
	private String stringExtra;
	private MyListView actionmelist;
	private ActionMeAdapter adapter = new ActionMeAdapter();
	private NewsBean bean;
	private List<NewsBean.RowsBean> list = new ArrayList<NewsBean.RowsBean>();
	public int currentPage = 1;
	public int totalPage;
	private ScrollView sc;
	private View view;
	private TextView empte;
	private SwipeRefreshLayout sf;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_acticon_mess);
		sf = (SwipeRefreshLayout) findViewById(R.id.sf);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		Intent intent = getIntent();
		stringExtra = intent.getStringExtra("title");
		actionmelist = (MyListView) findViewById(R.id.ActionMesslsit);
		sc = (ScrollView) findViewById(R.id.sc);
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);

		init();
		onClick();
	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stubOo
				ActiconMessActivity.this.finish();
			}
		});
		actionmelist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				RowsBean rowsBean = list.get(arg2);
				int id = rowsBean.getId();
				Intent intent = new Intent(ActiconMessActivity.this,
						MessagDataActivity.class);

				intent.putExtra("id", id + "");
				startActivity(intent);

			}
		});
		sc.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {

				int scrollY = view.getScrollY();
				int height = view.getHeight();
				int scrollViewMeasuredHeight = sc.getChildAt(0)
						.getMeasuredHeight();
				if (scrollY == 0) {
					System.out.println("       ˶    view.getScrollY()="
							+ scrollY);
				}
				if ((scrollY + height) == scrollViewMeasuredHeight) {
					if ((currentPage) < totalPage) {
						send(++currentPage);
					} else {
						Toast.makeText(ActiconMessActivity.this, "没有更多了...", 0)
								.show();
					}

				}

				return false;
			}
		});
	}

	private void init() {
		// TODO Auto-generated method stub
		back.setVisibility(View.VISIBLE);

		tv_title_logo.setText(stringExtra);

		actionmelist.setAdapter(adapter);

		view = getLayoutInflater().inflate(R.layout.activity_empty, null);
		addContentView(view, new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		empte = (TextView) view.findViewById(R.id.empty_content);
		empte.setText("暂时没有消息哦");

		sf.setOnRefreshListener(this);
		sf.setColorScheme(android.R.color.holo_orange_light,
				android.R.color.holo_orange_dark,
				android.R.color.holo_red_light, android.R.color.holo_red_dark);
		send(currentPage);
	}

	class ActionMeAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub

			return null;

		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			if (arg1 == null) {
				arg1 = View.inflate(ActiconMessActivity.this,
						R.layout.actionmess_item, null);
			}
			TextView biaoti = (TextView) arg1.findViewById(R.id.biaoti);
			TextView newscontent = (TextView) arg1
					.findViewById(R.id.newscontent);
			TextView time = (TextView) arg1.findViewById(R.id.time);
			RowsBean rowsBean = list.get(arg0);
			biaoti.setText(rowsBean.getNewsName());
			newscontent.setText(rowsBean.getNewsContent());
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd hh:mm");
			String format = dateFormat.format(rowsBean.getCreateTime());
			time.setText(format);
			return arg1;
		}
	}

	public void send(int page) {

		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("currentPage", page + "");
		httpUtils.send(HttpMethod.POST, GlobalUrl.findNewsByNewsQuery, params,
				new RequestCallBack<String>() {
					@Override
					public void onFailure(HttpException arg0, String arg1) {
						System.out.println("消息信息============" + arg1);
						if (list.size() == 0) {
							actionmelist.setEmptyView(empte);
						} else {
							view.setVisibility(View.GONE);
						}
						adapter.notifyDataSetChanged();
						sf.setRefreshing(false);

					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						System.out.println("消息信息============" + arg0.result);
						bean = ParesData2Obj.json2Obj(arg0.result,
								NewsBean.class);
						currentPage = bean.getCurrentPage();
						totalPage = bean.getTotalPage();
						list.addAll(bean.getRows());

						if (list.size() == 0) {
							actionmelist.setEmptyView(empte);
						} else {
							view.setVisibility(View.GONE);
						}
						adapter.notifyDataSetChanged();
						sf.setRefreshing(false);
					}
				});

	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		list = new ArrayList<NewsBean.RowsBean>();
		send(1);
	}
}
