package com.huayi.mall.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Environment;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

import com.huayi.mall.util.ACache;
import com.huayi.mall.util.SpfUtil;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class MyApplication extends Application {
	private static Context context;
	// 存放倒计时
	public static Map<String, Long> map;
	public static ACache aCache;

	public static boolean FirstLoadOutside =false;
	public static List<Activity> activityList = new ArrayList<Activity>();
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		context = this;
		aCache = ACache.get(getApplicationContext());
		JPushInterface.init(context);
		JPushInterface.setDebugMode(true);
		SpfUtil.initData(getApplicationContext());
		
		initImageLoader(context);

	}

	public Context getMyApp() {
		return getApplicationContext();
	}

	private void initImageLoader(Context context) {
		ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(
				context);
		config.threadPriority(Thread.NORM_PRIORITY - 2);
		config.denyCacheImageMultipleSizesInMemory();
		config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
		config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
		config.tasksProcessingOrder(QueueProcessingType.LIFO);
		config.writeDebugLogs(); // Remove for release app
		config.diskCache(new UnlimitedDiskCache(getDiskCacheDir("img-load")));// 缓存位置
		// Initialize ImageLoader with configuration.

		ImageLoader.getInstance().init(config.build());
	}

	@SuppressLint("NewApi")
	private File getDiskCacheDir(String uniqueName) {
		String cachePath;
		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())
				|| !Environment.isExternalStorageRemovable()) {
			cachePath = context.getExternalCacheDir().getPath();
		} else {
			cachePath = context.getCacheDir().getPath();
		}
		System.out.println("========imageload:"+cachePath + File.separator + uniqueName);
		return new File(cachePath + File.separator + uniqueName);
	}
}
