package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.huayi.mall.GoodsListActivity;
import com.huayi.mall.R;
import com.huayi.mall.adapter.BrandAdapter;
import com.huayi.mall.common.BrandBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.MyListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class BrandActivity extends Activity {

	private MyListView listview;

	private List<BrandBean> arrayList = new ArrayList<BrandBean>();

	private BrandAdapter adapter;

	private ImageView back;

	private EditText search_et;

	private TextView tv_title_logo;

	private int currentPage = 1, totalPage;

	ScrollView mScrollView;

	private ProgressBar pb;

	private TextView pbTv;

	private LinearLayout layout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_brand);

		findViewById();
		initView();
		initData();
		onClick();
		// 收索
		this.search_et
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {

						if (actionId == EditorInfo.IME_ACTION_SEND
								|| (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {

							xutilRequest(1, search_et.getText().toString());

							System.out
									.println("search_et.getText().toString()--"
											+ search_et.getText().toString());
							return true;
						}
						return false;
					}

				});

		this.search_et.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (TextUtils.isEmpty(s.toString())) {
					xutilRequest(1, "");
				}

			}
		});
		mScrollView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {

				int scrollY = view.getScrollY();
				int height = view.getHeight();
				int scrollViewMeasuredHeight = mScrollView.getChildAt(0)
						.getMeasuredHeight();
				if (scrollY == 0) {
					System.out.println("       ˶    view.getScrollY()="
							+ scrollY);
				}
				if ((scrollY + height) == scrollViewMeasuredHeight
						&& event.getAction() == MotionEvent.ACTION_UP) {
					layout.setVisibility(View.VISIBLE);

					if (currentPage != totalPage) {
						pb.setVisibility(View.VISIBLE);
						currentPage++;
						xutilRequest(currentPage, "");
						System.out.println("=================");
					} else {
						System.out.println("=================");
						layout.setVisibility(View.VISIBLE);
						pb.setVisibility(View.GONE);
						pbTv.setText("没有更多内容了");
					}

				}

				return false;
			}
		});
	}

	public void setData(String key) {

		JSONObject obj = MyApplication.aCache.getAsJSONObject("brand");
		try {
			if (obj != null) {
				JSONArray array = obj.getJSONArray("rows");
				if (array != null) {

					if (!TextUtils.isEmpty(key)) {
						arrayList.clear();
					} else if (currentPage == 1) {
						arrayList.clear();
					}
					for (int i = 0; i < array.length(); i++) {
						BrandBean bb = new BrandBean();
						JSONObject jsonObj = array.getJSONObject(i);

						bb.id = jsonObj.getString("id");
						bb.picture = jsonObj.getString("picture");
						bb.name = jsonObj.getString("brandName");

						arrayList.add(bb);

						System.out.println("图片链接---" + bb.picture);
					}
					adapter.setData(arrayList);
				}
				currentPage = obj.optInt("currentPage");
				totalPage = obj.optInt("totalPage");

				System.out.println("当前页---------》" + currentPage);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// listview.loadComplete();// 加载完成

	}

	/**
	 * 
	 * @param currentPage
	 *            当前页码
	 * @param key
	 *            关键字收索
	 */
	private void xutilRequest(int currentPage, final String key) {

		final CustomProgressDialog dialog = new CustomProgressDialog(
				BrandActivity.this, "正在请求中", R.anim.frame);

		dialog.show();
		HttpUtils httpUtils = new HttpUtils();

		System.out.println("刷新的页码数----》" + currentPage);
		RequestParams params = new RequestParams();
		params.addBodyParameter("currentPage", currentPage + "");
		if (!TextUtils.isEmpty(key)) {
			params.addBodyParameter("key", key);

			System.out.println("key---" + key);
		}

		httpUtils.send(HttpMethod.POST, GlobalUrl.getBrand, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						// setData();
						dialog.dismiss();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						dialog.dismiss();
						String result = arg0.result;
						try {
							JSONObject obj = new JSONObject(arg0.result);
							System.out.println("brand------------" + obj);
							MyApplication.aCache.put("brand", obj);
							setData(key);

						} catch (Exception e) {

						}
						// Gson gson = new Gson();
						// Type listType = new TypeToken<List<BrandBean>>() {
						// }.getType();
						// arrayList = gson.fromJson(result, listType);
						// adapter.notifyDataSetChanged();

					}

					@Override
					public void onStart() {

					}
				});
	}

	public void initData() {

		xutilRequest(1, "");

	}

	public void findViewById() {
		layout = (LinearLayout) findViewById(R.id.load_layout);
		pb = (ProgressBar) findViewById(R.id.progress);
		pbTv = (TextView) findViewById(R.id.progress_text);
		mScrollView = (ScrollView) findViewById(R.id.ScrollView_brand);
		search_et = (EditText) findViewById(R.id.et_search_brand);
		listview = (MyListView) findViewById(R.id.listview_brand);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		back = (ImageView) findViewById(R.id.back);

	}

	private void initView() {
		// TODO Auto-generated method stub

		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		adapter = new BrandAdapter(this, arrayList);
		listview.setAdapter(adapter);
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("品牌");

		layout.setVisibility(View.GONE);

	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				BrandActivity.this.finish();
			}
		});

		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				BrandBean bb = arrayList.get(position);

				Intent intent = new Intent(BrandActivity.this,
						GoodsListActivity.class);

				intent.putExtra("classify.id", bb.id);
				intent.putExtra("title", bb.name);

				startActivity(intent);

			}
		});

	}

	// @Override
	// public void onLoad() {
	// // TODO Auto-generated method stub
	// Handler handler = new Handler();
	// handler.postDelayed(new Runnable() {
	//
	// @Override
	// public void run() {
	// // TODO Auto-generated method stub
	// // 获取更多数据
	// xutilRequest(++currentPage, "");
	//
	// System.out.println("----------------dsadF");
	//
	// // listview.loadComplete();
	// }
	// }, 1000);
	// }

}
