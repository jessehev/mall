package com.huayi.mall.activity;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.huayi.mall.PinJiaActivity;
import com.huayi.mall.R;
import com.huayi.mall.base.OrderBeanto;
import com.huayi.mall.base.OrderBeanto.OrderListBean;
import com.huayi.mall.base.OrderBeanto.OrderListBean.OrderItemListBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.util.ACache;
import com.huayi.mall.util.IsNetWorkConnected;
import com.huayi.mall.util.ParesData2Obj;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.ImageViewSubClass;
import com.huayi.mall.view.MyListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MyOrdernActivity extends Activity {

	private Button quanbu;
	private Button daifukuan;
	private Button daishouhuo;
	private Button daipinjia;
	private TextView tv_title_logo;
	private ImageView bakc;
	private ListView orderlist;
	private OrderAdapter adapter = new OrderAdapter();
	private ACache aCache;
	private OrderBeanto bean;
	private String mOrderData = null;
	private int orderStatus = -1;
	private List<OrderBeanto.OrderListBean> list = new ArrayList<OrderBeanto.OrderListBean>();
	private int stringExtra;
	private View view;
	private TextView empte;
	private int serviceStatus;
	private ViewHolder holder;
	private ViewHolder2 holder2;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_ordern);
		quanbu = (Button) findViewById(R.id.quabu);
		daifukuan = (Button) findViewById(R.id.daifukuan);
		daishouhuo = (Button) findViewById(R.id.daishhoukuan);
		daipinjia = (Button) findViewById(R.id.daipingjia);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		bakc = (ImageView) findViewById(R.id.back);
		orderlist = (ListView) findViewById(R.id.orderlist);
		view = getLayoutInflater().inflate(R.layout.activity_empty, null);
		addContentView(view, new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		empte = (TextView) view.findViewById(R.id.empty_content);
		empte.setText("没有发现该订单");
		
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		orderlist.setEmptyView(view);
		Intent intent = getIntent();
		stringExtra = intent.getIntExtra("order", -1);
		orderStatus = stringExtra;

	}

	private void init() {
		// TODO Auto-generated method stub
		tv_title_logo.setText("我的订单");
		bakc.setVisibility(View.VISIBLE);
		orderlist.setAdapter(adapter);
		// 初始化内存

		send(orderStatus);
		System.out.println("serviceStatus" + serviceStatus);
		aCache = ACache.get(MyOrdernActivity.this);

		switch (stringExtra) {
		case 2:
			quanbu.setTextColor(Color.parseColor("#000000"));
			quanbu.setBackgroundColor(Color.parseColor("#ffffff"));
			daifukuan.setTextColor(Color.parseColor("#e40d3f"));
			daifukuan.setBackgroundColor(Color.parseColor("#fef3f5"));
			daishouhuo.setTextColor(Color.parseColor("#000000"));
			daishouhuo.setBackgroundColor(Color.parseColor("#ffffff"));
			daipinjia.setTextColor(Color.parseColor("#000000"));
			daipinjia.setBackgroundColor(Color.parseColor("#ffffff"));

			orderStatus = 2;

			break;
		case 4:
			quanbu.setTextColor(Color.parseColor("#000000"));
			quanbu.setBackgroundColor(Color.parseColor("#ffffff"));
			daifukuan.setTextColor(Color.parseColor("#000000"));
			daifukuan.setBackgroundColor(Color.parseColor("#ffffff"));
			daishouhuo.setTextColor(Color.parseColor("#e40d3f"));
			daishouhuo.setBackgroundColor(Color.parseColor("#fef3f5"));
			daipinjia.setTextColor(Color.parseColor("#000000"));
			daipinjia.setBackgroundColor(Color.parseColor("#ffffff"));

			orderStatus = 4;

			break;
		case 5:
			quanbu.setTextColor(Color.parseColor("#000000"));
			quanbu.setBackgroundColor(Color.parseColor("#ffffff"));
			daifukuan.setTextColor(Color.parseColor("#000000"));
			daifukuan.setBackgroundColor(Color.parseColor("#ffffff"));
			daishouhuo.setTextColor(Color.parseColor("#000000"));
			daishouhuo.setBackgroundColor(Color.parseColor("#ffffff"));
			daipinjia.setTextColor(Color.parseColor("#e40d3f"));
			daipinjia.setBackgroundColor(Color.parseColor("#fef3f5"));
			orderStatus = 5;

			break;

		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		init();
	}

	public void onClick(View view) {

		switch (view.getId()) {
		case R.id.quabu:
			quanbu.setTextColor(Color.parseColor("#e40d3f"));
			quanbu.setBackgroundColor(Color.parseColor("#fef3f5"));
			daifukuan.setTextColor(Color.parseColor("#000000"));
			daifukuan.setBackgroundColor(Color.parseColor("#ffffff"));
			daishouhuo.setTextColor(Color.parseColor("#000000"));
			daishouhuo.setBackgroundColor(Color.parseColor("#ffffff"));
			daipinjia.setTextColor(Color.parseColor("#000000"));
			daipinjia.setBackgroundColor(Color.parseColor("#ffffff"));
			orderStatus = -1;
			if (mOrderData != null) {
				list = new ArrayList<OrderBeanto.OrderListBean>();
				bean = new OrderBeanto();
				bean = ParesData2Obj.json2Obj(mOrderData, OrderBeanto.class);
				list.addAll(bean.getOrderList());
				for (int i = 0; i < list.size(); i++) {

				}
				Iterator<OrderBeanto.OrderListBean> it = list.iterator();
				while (it.hasNext()) {
					OrderBeanto.OrderListBean next = it.next();
					if (next.getOrderStatus() == 1) {
						it.remove();
					}
			

				}

				adapter.notifyDataSetChanged();
			}

			break;
		case R.id.daifukuan:
			quanbu.setTextColor(Color.parseColor("#000000"));
			quanbu.setBackgroundColor(Color.parseColor("#ffffff"));
			daifukuan.setTextColor(Color.parseColor("#e40d3f"));
			daifukuan.setBackgroundColor(Color.parseColor("#fef3f5"));
			daishouhuo.setTextColor(Color.parseColor("#000000"));
			daishouhuo.setBackgroundColor(Color.parseColor("#ffffff"));
			daipinjia.setTextColor(Color.parseColor("#000000"));
			daipinjia.setBackgroundColor(Color.parseColor("#ffffff"));

			orderStatus = 2;
			if (mOrderData != null) {
				list = new ArrayList<OrderBeanto.OrderListBean>();
				bean = new OrderBeanto();
				bean = ParesData2Obj.json2Obj(mOrderData, OrderBeanto.class);
				list.addAll(bean.getOrderList());

				Iterator<OrderBeanto.OrderListBean> it = list.iterator();
				while (it.hasNext()) {
					OrderBeanto.OrderListBean next = it.next();
					if (next.getOrderStatus() != 2) {
						it.remove();
					}

				}

				adapter.notifyDataSetChanged();
			}

			break;
		case R.id.daishhoukuan:
			quanbu.setTextColor(Color.parseColor("#000000"));
			quanbu.setBackgroundColor(Color.parseColor("#ffffff"));
			daifukuan.setTextColor(Color.parseColor("#000000"));
			daifukuan.setBackgroundColor(Color.parseColor("#ffffff"));
			daishouhuo.setTextColor(Color.parseColor("#e40d3f"));
			daishouhuo.setBackgroundColor(Color.parseColor("#fef3f5"));
			daipinjia.setTextColor(Color.parseColor("#000000"));
			daipinjia.setBackgroundColor(Color.parseColor("#ffffff"));
			orderStatus = 4;
			if (mOrderData != null) {
				list = new ArrayList<OrderBeanto.OrderListBean>();
				bean = new OrderBeanto();
				bean = ParesData2Obj.json2Obj(mOrderData, OrderBeanto.class);
				list.addAll(bean.getOrderList());

				Iterator<OrderBeanto.OrderListBean> it = list.iterator();
				while (it.hasNext()) {
					OrderBeanto.OrderListBean next = it.next();
					if (next.getOrderStatus() != 4) {
						it.remove();
					}

				}

				adapter.notifyDataSetChanged();
			}
			break;
		case R.id.daipingjia:
			quanbu.setTextColor(Color.parseColor("#000000"));
			quanbu.setBackgroundColor(Color.parseColor("#ffffff"));
			daifukuan.setTextColor(Color.parseColor("#000000"));
			daifukuan.setBackgroundColor(Color.parseColor("#ffffff"));
			daishouhuo.setTextColor(Color.parseColor("#000000"));
			daishouhuo.setBackgroundColor(Color.parseColor("#ffffff"));
			daipinjia.setTextColor(Color.parseColor("#e40d3f"));
			daipinjia.setBackgroundColor(Color.parseColor("#fef3f5"));
			orderStatus = 5;
			if (mOrderData != null) {
				list = new ArrayList<OrderBeanto.OrderListBean>();
				bean = new OrderBeanto();
				bean = ParesData2Obj.json2Obj(mOrderData, OrderBeanto.class);
				list.addAll(bean.getOrderList());

				Iterator<OrderBeanto.OrderListBean> it = list.iterator();
				while (it.hasNext()) {
					OrderBeanto.OrderListBean next = it.next();
					if (next.getOrderStatus() != 5) {
						System.out.println("订单信息==========="
								+ next.getOrderNumber());
						it.remove();

					}

				}

				adapter.notifyDataSetChanged();
			}
			break;
		case R.id.back:
			this.finish();
			break;

		default:
			break;
		}
	}

	class OrderAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int postion1, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			if (arg1==null) {
				arg1 = View.inflate(MyOrdernActivity.this, R.layout.order_item,
						null);
				holder=new ViewHolder();
				holder.orinlist = (MyListView) arg1
						.findViewById(R.id.inorderlist);

				// state 0待收货 1待评价2 代付款3已完成4关闭交易5退款中6退款成功
				holder.time = (TextView) arg1.findViewById(R.id.ordertime);
				holder.orderid = (TextView) arg1.findViewById(R.id.orderid);

				holder.state = (TextView) arg1.findViewById(R.id.orderstate);
				holder.bt_one = (Button) arg1.findViewById(R.id.orderbtone);
				holder.bt_two = (Button) arg1.findViewById(R.id.orderbttwo);
				arg1.setTag(holder);

			}else {
				holder=(ViewHolder) arg1.getTag();
			}


			final OrderListBean orderListBean = list.get(postion1);
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String format = dateFormat.format(orderListBean.getCreateTime());
			holder.time.setText(format);
			InorderAdapter adapter = new InorderAdapter(
					orderListBean.getOrderItemList());
			// item点击事件

			holder.orinlist.setOnItemClickListener(new OnItemClickListener() {


				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					if (orderListBean.getOrderStatus() > 2) {

						Intent intent = new Intent(MyOrdernActivity.this,
								TuiHuoActivity.class);
						Bundle bundle = new Bundle();
						bundle.putSerializable("orderbean", orderListBean);
						intent.putExtras(bundle);
						startActivity(intent);
					}
				}
			});

			holder.orinlist.setAdapter(adapter);
			holder.orderid.setText(orderListBean.getOrderNumber());
			switch (orderListBean.getOrderStatus()) {
			case 3:
				holder.state.setText("待发货");
				holder.bt_one.setText("提醒发货");
				holder.bt_two.setText("确认收货");

				holder.bt_two.setVisibility(View.GONE);
				holder.bt_one.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						
						Toast.makeText(MyOrdernActivity.this, "已提醒卖家发货", 0)
						.show();
					}
				});
				holder.bt_two.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Toast.makeText(MyOrdernActivity.this,
								holder.orderid.getText().toString() + "确认收货", 0)
								.show();

					}
				});
				break;
			case 4:
				holder.state.setText("待收货");
				holder.bt_one.setText("查看物流");
				holder.bt_two.setText("确认收货");
				holder.bt_one.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						startActivity(new Intent(MyOrdernActivity.this,
								WuLiuActivity.class));
					}
				});
				holder.bt_two.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Sure(list.get(postion1).getId(), 4);

					}
				});
				break;
			case 5:

				holder.state.setText("待评价");
				holder.bt_one.setText("申请售后");
				holder.bt_two.setText("立即评价");
				holder.bt_one.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if (orderListBean.getOrderStatus() > 2) {

							Intent intent = new Intent(MyOrdernActivity.this,
									TuiHuoActivity.class);
							Bundle bundle = new Bundle();
							bundle.putSerializable("orderbean", orderListBean);
							intent.putExtras(bundle);
							startActivity(intent);
						}
					}
				});
				holder.bt_two.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub

						Intent intent = new Intent(MyOrdernActivity.this,
								PinJiaActivity.class);
						Gson gson = new Gson();
						String json = gson.toJson(orderListBean
								.getOrderItemList());
						intent.putExtra("list", json);
						intent.putExtra("orderid", orderListBean.getId());

						startActivity(intent);

					}
				});
				break;
			case 1:
				holder.state.setText("待付款");
				holder.bt_one.setText("取消订单");
				holder.bt_two.setText("立即付款");
				holder.bt_one.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						orderStatus = 1;
						deleteDialog(orderListBean.getId(), 1);
					}
				});
				holder.bt_two.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(MyOrdernActivity.this,
								PayActivity.class);
						BigDecimal bigDecimal = new BigDecimal(list.get(
								postion1).getTotalMoney());
						BigDecimal bigDecimal2 = new BigDecimal(list.get(
								postion1).getPostage());
						BigDecimal add = bigDecimal.add(bigDecimal2);
						intent.putExtra("goodsPrice", "" + add);
						intent.putExtra("goodsNum", list.get(postion1)
								.getOrderItemList().size()
								+ "");
						intent.putExtra("orderId", list.get(postion1).getId()
								+ "");
						startActivity(intent);

					}
				});
				break;
			case 2:
				holder.state.setText("待付款");
				holder.bt_one.setText("取消订单");
				holder.bt_two.setText("立即付款");
				holder.bt_one.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						orderStatus = 2;
						deleteDialog(orderListBean.getId(), 2);
					}
				});
				holder.bt_two.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						Thread thread = new Thread();
						// TODO Auto-generated method stub
						Intent intent = new Intent(MyOrdernActivity.this,
								PayActivity.class);
						BigDecimal bigDecimal = new BigDecimal(list.get(
								postion1).getTotalMoney());
						BigDecimal bigDecimal2 = new BigDecimal(list.get(
								postion1).getPostage());
						BigDecimal add = bigDecimal.add(bigDecimal2);
						intent.putExtra("goodsPrice", "" + add);
						intent.putExtra("goodsNum", list.get(postion1)
								.getOrderItemList().size()
								+ "");
						intent.putExtra("orderId", list.get(postion1).getId()
								+ "");
						startActivity(intent);

					}
				});
				break;
			case 6:
				holder.state.setText("已完成");
				holder.bt_one.setText("删除订单");
				holder.bt_two.setText("申请售后");
				holder.bt_one.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						orderStatus = 6;
						deleteDialog(orderListBean.getId(), 6);
					}
				});
				holder.bt_two.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						
						Intent intent = new Intent(MyOrdernActivity.this,
								TuiHuoActivity.class);
						Bundle bundle = new Bundle();
						bundle.putSerializable("orderbean", orderListBean);
						intent.putExtras(bundle);
						startActivity(intent);
					}
				});
			case 7:
				holder.state.setText("已申请售后");
				holder.bt_one.setText("删除订单");
				holder.bt_two.setText("查看详细");
				holder.bt_one.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						
						deleteDialog(orderListBean.getId(), 7);
					}
				});
				
				holder.bt_two.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(MyOrdernActivity.this,
								TuiHuoActivity.class);
						Bundle bundle = new Bundle();
						bundle.putSerializable("orderbean", orderListBean);
						intent.putExtras(bundle);
						startActivity(intent);
					}
				});
				
				break;
			case 0:
				holder.state.setText("关闭交易");
				holder.bt_one.setText("删除订单");
				holder.bt_two.setVisibility(View.GONE);
				holder.bt_one.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						orderStatus = -1;
						deleteDialog(orderListBean.getId(), 0);
					}
				});
				holder.bt_two.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Toast.makeText(MyOrdernActivity.this,
								holder.orderid.getText().toString() + "立即评价", 0)
								.show();

					}
				});
				break;

			default:
				break;
			}
			return arg1;
		}
	}

	class InorderAdapter extends BaseAdapter {
		public List<OrderBeanto.OrderListBean.OrderItemListBean> list;

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		public InorderAdapter(List<OrderItemListBean> list) {
			super();
			this.list = list;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			if (arg1==null) {

				holder2 = new ViewHolder2();
				arg1 = View.inflate(MyOrdernActivity.this, R.layout.inorder_item,
						null);
				holder2.image = (ImageViewSubClass) arg1
						.findViewById(R.id.orderimage);
				holder2.ordername = (TextView) arg1.findViewById(R.id.ordername);
				holder2.orderprice = (TextView) arg1.findViewById(R.id.orderprice);
				holder2. options = (TextView) arg1.findViewById(R.id.options);
				arg1.setTag(holder2);
			}else {
				holder2=(ViewHolder2) arg1.getTag();
			}


			holder2.orderprice.setText("¥ "
					+ list.get(arg0).getProduct().getActualPrice());

			holder2.options.setText(list.get(arg0).getProduct().getDescription());
			String[] s = list.get(arg0).getProduct().getProductName()
					.split(" ");
			holder2.ordername.setText(s[0]);
			// 图片地址

			String[] pic = list.get(arg0).getProduct().getProductPicture()
					.split(",");
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost1 + pic[0], holder2.image,
					ImageLoaderCfg.options2);
			return arg1;
		}
	}

	public void send(final int state) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("customer_id", SpfUtil.getId() + "");
		httpUtils.send(HttpMethod.POST, GlobalUrl.showEvaluatedOrder, params,
				new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {

				Toast.makeText(MyOrdernActivity.this, "获取失败", 0).show();
				System.out.println(arg1);

			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				System.out.println("订单获取的消息===================="
						+ arg0.result);
				Toast.makeText(MyOrdernActivity.this, "获取成功", 0).show();
				aCache.put("mOrderData", arg0.result);
				mOrderData = aCache.getAsString("mOrderData");
				list = new ArrayList<OrderBeanto.OrderListBean>();
				bean = ParesData2Obj.json2Obj(arg0.result,
						OrderBeanto.class);
				list.addAll(bean.getOrderList());
				Iterator<OrderBeanto.OrderListBean> it = list
						.iterator();
				while (it.hasNext()) {
					OrderBeanto.OrderListBean next = it.next();
					if (next.getOrderStatus() == 1) {
						it.remove();
					}
				}
				
			
				adapter.notifyDataSetChanged();
				switch (orderStatus) {
				case 2:
					quanbu.setTextColor(Color.parseColor("#000000"));
					quanbu.setBackgroundColor(Color
							.parseColor("#ffffff"));
					daifukuan.setTextColor(Color.parseColor("#e40d3f"));
					daifukuan.setBackgroundColor(Color
							.parseColor("#fef3f5"));
					daishouhuo.setTextColor(Color.parseColor("#000000"));
					daishouhuo.setBackgroundColor(Color
							.parseColor("#ffffff"));
					daipinjia.setTextColor(Color.parseColor("#000000"));
					daipinjia.setBackgroundColor(Color
							.parseColor("#ffffff"));
					System.out.println(mOrderData);
					orderStatus = 2;
					if (!mOrderData.isEmpty()) {
						list = new ArrayList<OrderBeanto.OrderListBean>();
						bean = new OrderBeanto();
						bean = ParesData2Obj.json2Obj(mOrderData,
								OrderBeanto.class);
						list.addAll(bean.getOrderList());

						Iterator<OrderBeanto.OrderListBean> it1 = list
								.iterator();
						while (it1.hasNext()) {
							OrderBeanto.OrderListBean next = it1.next();
							if (next.getOrderStatus() != 2) {
								it1.remove();
							}
							

						}

						adapter.notifyDataSetChanged();
					}
					break;
				case 4:
					quanbu.setTextColor(Color.parseColor("#000000"));
					quanbu.setBackgroundColor(Color
							.parseColor("#ffffff"));
					daifukuan.setTextColor(Color.parseColor("#000000"));
					daifukuan.setBackgroundColor(Color
							.parseColor("#ffffff"));
					daishouhuo.setTextColor(Color.parseColor("#e40d3f"));
					daishouhuo.setBackgroundColor(Color
							.parseColor("#fef3f5"));
					daipinjia.setTextColor(Color.parseColor("#000000"));
					daipinjia.setBackgroundColor(Color
							.parseColor("#ffffff"));
					System.out.println(mOrderData);
					orderStatus = 4;
					if (!mOrderData.isEmpty()) {
						list = new ArrayList<OrderBeanto.OrderListBean>();
						bean = new OrderBeanto();
						bean = ParesData2Obj.json2Obj(mOrderData,
								OrderBeanto.class);
						list.addAll(bean.getOrderList());

						Iterator<OrderBeanto.OrderListBean> it1 = list
								.iterator();
						while (it1.hasNext()) {
							OrderBeanto.OrderListBean next = it1.next();
							if (next.getOrderStatus() != 4) {
								it1.remove();
							}

						}

						adapter.notifyDataSetChanged();
					}
					break;
				case 5:
					quanbu.setTextColor(Color.parseColor("#000000"));
					quanbu.setBackgroundColor(Color
							.parseColor("#ffffff"));
					daifukuan.setTextColor(Color.parseColor("#000000"));
					daifukuan.setBackgroundColor(Color
							.parseColor("#ffffff"));
					daishouhuo.setTextColor(Color.parseColor("#000000"));
					daishouhuo.setBackgroundColor(Color
							.parseColor("#ffffff"));
					daipinjia.setTextColor(Color.parseColor("#e40d3f"));
					daipinjia.setBackgroundColor(Color
							.parseColor("#fef3f5"));
					orderStatus = 5;
					System.out.println(mOrderData);
					if (!mOrderData.isEmpty()) {
						list = new ArrayList<OrderBeanto.OrderListBean>();
						bean = new OrderBeanto();
						bean = ParesData2Obj.json2Obj(mOrderData,
								OrderBeanto.class);
						list.addAll(bean.getOrderList());

						Iterator<OrderBeanto.OrderListBean> it1 = list
								.iterator();
						while (it1.hasNext()) {
							OrderBeanto.OrderListBean next = it1.next();
							if (next.getOrderStatus() != 5) {
								System.out.println("订单信息==========="
										+ next.getOrderNumber());
								it1.remove();

							}

						}

						adapter.notifyDataSetChanged();
					}
					break;
				default:
					break;
				}

			}

		});
	}

	public void deleteDialog(final int orderId, final int s) {
		LinearLayout lin_dailog_item = (LinearLayout) View.inflate(
				MyOrdernActivity.this, R.layout.dialog_cart, null);
		final Dialog dialog = new AlertDialog.Builder(MyOrdernActivity.this)
		.create();
		dialog.show();
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();

		WindowManager wm = (WindowManager) MyOrdernActivity.this
				.getSystemService(Context.WINDOW_SERVICE);
		int height = wm.getDefaultDisplay().getHeight() / 3;
		int weith = wm.getDefaultDisplay().getWidth() / 5 * 3;
		params.width = weith;
		params.height = height;
		params.gravity = Gravity.TOP;
		params.y = wm.getDefaultDisplay().getHeight() / 5;

		dialog.getWindow().setContentView(lin_dailog_item);
		dialog.getWindow().setAttributes(params);
		dialog.getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
				| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		dialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		ImageView cancle = (ImageView) lin_dailog_item
				.findViewById(R.id.cancle_dialog_cart);
		TextView sure = (TextView) lin_dailog_item
				.findViewById(R.id.sure_dialog_cart);
		TextView suredelete = (TextView) lin_dailog_item
				.findViewById(R.id.suredelete);
		suredelete.setText("确认删除此订单吗？");
		sure.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// 请求服务器删除商品
				if (IsNetWorkConnected
						.isNetworkConnected(MyOrdernActivity.this)) {

					HttpUtils httpUtils = new HttpUtils();
					RequestParams params = new RequestParams();
					params.addBodyParameter("orderId", orderId + "");

					httpUtils.send(HttpMethod.POST, GlobalUrl.deleteOrder,
							params, new RequestCallBack<String>() {

						@Override
						public void onFailure(HttpException arg0,
								String arg1) {

							Toast.makeText(MyOrdernActivity.this,
									"获取失败", 0).show();
							System.out.println(arg1);

						}

						@Override
						public void onSuccess(ResponseInfo<String> arg0) {
							System.out
							.println("删除订单的消息===================="
									+ arg0.result);
							send(s);

						}

					});

					dialog.dismiss();
				} else {
					Toast.makeText(MyOrdernActivity.this, "请检查网络连接", 1).show();
				}

			}
		});
		cancle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog.dismiss();
			}
		});

	}
	public  class ViewHolder{

		MyListView orinlist ;

		// state 0待收货 1待评价2 代付款3已完成4关闭交易5退款中6退款成功
		TextView time ;
		TextView orderid ;

		TextView state ;
		Button bt_one;
		Button bt_two ;
	}
	public  class ViewHolder2{

		ImageViewSubClass image ;
		TextView ordername;
		TextView orderprice ;


		TextView options ;
	}
	public void Sure(long orderid,final int status){

		HttpUtils httpUtils = new HttpUtils();

		RequestParams params = new RequestParams();
		params.addBodyParameter("status", "5");
		params.addBodyParameter("orderId", orderid+"");


		httpUtils.send(HttpMethod.POST,GlobalUrl.changeOrderStatus , params, new RequestCallBack<String>() {



			@Override
			public void onFailure(HttpException arg0, String arg1) {

				System.out.println(arg1);
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {


				System.out.println(arg0.result);

				send(status);
			}

			
			
		});

	}
	
}
