package com.huayi.mall.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.util.SystemBar;
import com.nostra13.universalimageloader.core.ImageLoader;

public class AboutActivity extends Activity {

	private ImageView back, erweima;

	private TextView tv_title_logo, version;

	private String branchVersion;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);

		findViewById();
		initView();
		initData();
		onClick();

	}

	public void initData() {
		String androidPicture = getIntent().getStringExtra("androidPicture");

		branchVersion = getIntent().getStringExtra("branchVersion");
		if (androidPicture != null) {
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost + androidPicture, erweima,
					ImageLoaderCfg.options);
		}
		version.setText(branchVersion);
	}

	public void findViewById() {
		version = (TextView) findViewById(R.id.version_about);
		erweima = (ImageView) findViewById(R.id.erweima_about);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		back = (ImageView) findViewById(R.id.back);

	}

	private void initView() {
		// TODO Auto-generated method stub
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("关于");

	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AboutActivity.this.finish();
			}
		});

	}

}
