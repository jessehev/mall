package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.LookTuiMoneyActivity;
import com.huayi.mall.R;
import com.huayi.mall.base.OrderBean;
import com.huayi.mall.base.OrderBeanto;
import com.huayi.mall.base.OrderBeanto.OrderListBean;
import com.huayi.mall.base.OrderBeanto.OrderListBean.OrderItemListBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.util.ACache;
import com.huayi.mall.util.ParesData2Obj;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.ImageViewSubClass;
import com.huayi.mall.view.MyListView;
import com.huayi.mall.view.RatioImageView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

public class SerViceActivity2 extends Activity implements
		SwipeRefreshLayout.OnRefreshListener {

	ListView mListView;

	List<OrderBean> arrayList = new ArrayList<OrderBean>();
	OrderAdapter adapter = new OrderAdapter();
	private int b=0;//判断传来的数据是哪个
	private int a =66;//判断退款多种状态
	private ImageView back;
	private List<OrderBeanto.OrderListBean> list = new ArrayList<OrderBeanto.OrderListBean>();
	private OrderBeanto bean;
	private TextView tv_title_logo;
	private SwipeRefreshLayout sf;

	ScrollView sc;
	RatioImageView mRatioImageView;

	private ACache mACache;

	private JSONArray jsonArray;

	private ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_service);
		listView = (ListView) findViewById(R.id.listView_service);
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		findViewById();
		initView();
		initData();
		onClick();
		

	}

	public void initData() {
		listView.setAdapter(adapter);
		send();
	}

	public void initView() {
		Intent intent = getIntent();
		String stringExtra = intent.getStringExtra("titlename");
		b=intent.getIntExtra("datanum", 66);
		a=intent.getIntExtra("datanumd", 66);
		tv_title_logo.setText(stringExtra);
		back.setVisibility(View.VISIBLE);
		mACache = ACache.get(this);
	}

	private void findViewById() {
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		back = (ImageView) findViewById(R.id.back);
		sf = (SwipeRefreshLayout) findViewById(R.id.srf_fiveshop);

	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				SerViceActivity2.this.finish();
			}
		});

	}

	/**
	 * 下拉刷新
	 */
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {
			public void run() {
				sf.setRefreshing(false);
				System.out.println("加载完成------------");
			}
		}, 1000);
	}
	public void send() {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("customer_id", SpfUtil.getId() + "");

		httpUtils.send(HttpMethod.POST, GlobalUrl.showEvaluatedOrder, params,
				new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {

				Toast.makeText(SerViceActivity2.this, "获取失败", 0).show();
				System.out.println(arg1);

			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				System.out.println("订单获取的消息===================="
						+ arg0.result);
				Toast.makeText(SerViceActivity2.this, "获取成功", 0).show();
				list = new ArrayList<OrderBeanto.OrderListBean>();
				bean = ParesData2Obj.json2Obj(arg0.result,
						OrderBeanto.class);
				list.addAll(bean.getOrderList());
				System.out.println("集合大小"+list.size());
				System.out.println("集合大小asd"+list.get(0).getServiceStatus());
				Iterator<OrderBeanto.OrderListBean> it = list
						.iterator();
				while (it.hasNext()) {
					OrderBeanto.OrderListBean next = it.next();
					if (next.getServiceStatus()==0) {
						it.remove();
					}

				}
				adapter.notifyDataSetChanged();
				

			}

		});
	}
	class OrderAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int postion1, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			if (arg1==null) {
				arg1 = View.inflate(SerViceActivity2.this, R.layout.order_item2,
						null);
			}
			
			MyListView orinlist = (MyListView) arg1
					.findViewById(R.id.inorderlist);

			// state 0待收货 1待评价2 代付款3已完成4关闭交易5退款中6退款成功
			TextView time = (TextView) arg1.findViewById(R.id.ordertime);
			final TextView orderid = (TextView) arg1.findViewById(R.id.orderid);

			TextView state = (TextView) arg1.findViewById(R.id.orderstate);
			Button bt_one = (Button) arg1.findViewById(R.id.orderbtone);
			Button bt_two = (Button) arg1.findViewById(R.id.orderbttwo);
			final OrderListBean orderListBean = list.get(postion1);
			List<OrderItemListBean> orderItemList = orderListBean.getOrderItemList();
			Iterator<OrderItemListBean> it = orderItemList.iterator();
		
			orinlist.setAdapter(new InorderAdapter(orderItemList));

			return arg1;
		}
			
		}
	class InorderAdapter extends BaseAdapter {
		public List<OrderBeanto.OrderListBean.OrderItemListBean> list2;

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list2.size();
		}

		public InorderAdapter(List<OrderItemListBean> list) {
			super();
			this.list2 = list;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			if (arg1==null) {
				arg1 = View.inflate(SerViceActivity2.this, R.layout.inorder_item2,
						null);
			}
			
			ImageViewSubClass image = (ImageViewSubClass) arg1
					.findViewById(R.id.orderimage);
			TextView ordername = (TextView) arg1.findViewById(R.id.ordername);
			TextView orderprice = (TextView) arg1.findViewById(R.id.orderprice);
			TextView orderstate=(TextView) arg1.findViewById(R.id.orderstate);
			orderprice.setText("¥ "+list2.get(arg0).getProduct().getActualPrice());
			TextView options = (TextView) arg1.findViewById(R.id.options);
			Button orderbtone = (Button) arg1.findViewById(R.id.orderbtone);
			orderbtone.setVisibility(View.GONE);
			Button orderbttwo = (Button) arg1.findViewById(R.id.orderbttwoo);
			orderbttwo.setText("查看进度");
			orderbttwo.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					startActivity(new Intent(SerViceActivity2.this,LookTuiMoneyActivity.class));
				}
			});
			options.setText(list2.get(arg0).getProduct().getDescription() );
			String [] s = list2.get(arg0).getProduct().getProductName().split(" ");
			ordername.setText(s[0]);
			//图片地址

			String[] pic = list2.get(arg0).getProduct().getProductPicture()
					.split(",");
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost1 + pic[0], image,
					ImageLoaderCfg.options2);
			//售后状态 0正常交易  1申请了退款 2申请退货 3申请返修 4售后完成
			switch (list2.get(arg0).getServiceType()) {
			case 1:
				orderstate.setText("申请了退款 ");
				break;
			case 2:
				orderstate.setText("申请退货 ");
				break;
			case 3:
				orderstate.setText("申请返修 ");
				break;
			
			

			default:
				break;
			}
			if (list2.get(arg0).getServiceStatus()==2) {
				orderstate.setText("售后已完成");
			}
			return arg1;
		}
	}
}
