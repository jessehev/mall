package com.huayi.mall.activity;

import org.json.JSONArray;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.ACache;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class NewAddressActivity extends Activity {

	private ImageView shenshiqu;
	private ImageView back;
	private TextView tv_title_logo;
	private EditText shengshiqu;
	private Button bt_biannji;
	private ACache aCache;
	private EditText name;
	private EditText phone;
	private EditText address;
	private JSONArray asJSONArray;
	private CheckBox moren;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_address);
		shenshiqu = (ImageView) findViewById(R.id.shenshiqu);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		shengshiqu = (EditText) findViewById(R.id.shengshiqu);
		bt_biannji = (Button) findViewById(R.id.bt_biannji);
		name = (EditText) findViewById(R.id.name);
		phone = (EditText) findViewById(R.id.phone);
		address = (EditText) findViewById(R.id.addresss);
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		init();
		onClick();
	}

	private void init() {
		// TODO Auto-generated method stub
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("添加新地址");
		aCache = ACache.get(NewAddressActivity.this);
		asJSONArray = aCache.getAsJSONArray("address");

	}

	private void onClick() {
		
		// 省市区
		shenshiqu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivityForResult(new Intent(NewAddressActivity.this,
						ShengShiQuActivity.class), 1);
			}
		});
		// 返回
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				NewAddressActivity.this.finish();
			}
		});
		// 点击保存
		bt_biannji.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (TextUtils.isEmpty(shengshiqu.getText().toString())
						| TextUtils.isEmpty(address.getText().toString())
						| TextUtils.isEmpty(name.getText().toString())
						| TextUtils.isEmpty(phone.getText().toString())
						| phone.getText().toString().length() != 11) {

					Toast.makeText(NewAddressActivity.this, "请填写完整的地址信息", 0)
							.show();
				} else {
					send();
				

				}

			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			if (resultCode == 1) {
				String stringExtra = data.getStringExtra("mate");
				System.out.println(stringExtra);
				shengshiqu.setText(stringExtra);
			}
		}
	}
	private void send() {
		HttpUtils httpUtils = new HttpUtils();
		/*customer.id 然后就是其他的    
	    private String receiver;
	    private String address;
	    private String phone;
	    private boolean defaultAddress;*/
		RequestParams params = new RequestParams();
		System.out.println("用户id"+SpfUtil.getId()+"");
		params.addBodyParameter("customer.id", SpfUtil.getId()+"");
		params.addBodyParameter("receiver", name.getText().toString());//收件人
		params.addBodyParameter("phone", phone.getText().toString());//手机
		params.addBodyParameter("address", shengshiqu.getText().toString()+"/"+address.getText().toString());//省市区
		/*params.addBodyParameter("defaultAddress", );//默认地址
*/		
		/*params.addBodyParameter("password", pass);*/
		httpUtils.send(HttpMethod.POST, GlobalUrl.insertAddress, params, new RequestCallBack<String>() {

			private CustomProgressDialog dialog;

			@Override
			public void onFailure(HttpException arg0, String arg1) {
			dialog.dismiss();
			Toast.makeText(NewAddressActivity.this, "保存失败", 0).show();
			System.out.println(arg1);
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				
				Toast.makeText(NewAddressActivity.this, "保存成功", 0).show();
				System.out.println(arg0.result);
				dialog.dismiss();
				NewAddressActivity.this.finish();
			}

			@Override
			public void onStart() {
				dialog = new CustomProgressDialog(NewAddressActivity.this,"保存中。。。", R.anim.frame2);
				dialog.show();
			}
		});
	}


}
