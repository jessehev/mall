package com.huayi.mall.activity;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.huayi.mall.R;
import com.huayi.mall.ReviseActivity;
import com.huayi.mall.base.AddressBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.ACache;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class AddressActivity extends Activity {

	private ListView addresslist;
	private ImageView back;
	private TextView tv_title_logo;
	private TextView right_text;
	private AddressAdapter adapter;
	View kongyi;
	View konger;
	private int i = 0;
	private View footview;
	private Button ft_Button;
	private List<AddressBean> list = new ArrayList<AddressBean>();
	private ACache aCache;
	private List<Integer> removelist = new ArrayList<Integer>();
	private JSONArray asJSONArray;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_address);
		addresslist = (ListView) findViewById(R.id.Addresslist);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		right_text = (TextView) findViewById(R.id.right_text);
		adapter = new AddressAdapter();
		addresslist.setAdapter(adapter);
		footview = View.inflate(AddressActivity.this, R.layout.addressfoot,
				null);// 加脚
		ft_Button = (Button) footview.findViewById(R.id.bt_biannji);
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);

		
		init();
		onclick();

	}

	private void onclick() {

		// list点击
		addresslist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				AddressBean addressBean = list.get(arg2);
				// hw 4-14
				Intent intent2 = getIntent();

				String str = intent2.getStringExtra("confirmAddress");

				Bundle bundle2 = new Bundle();
				bundle2.putSerializable("ListAddress", addressBean);
				if (str != null) {
					AddressActivity.this.setResult(4593,
							getIntent().putExtras(bundle2));
					finish();

				} else {
					Intent intent = new Intent(AddressActivity.this,
							ReviseActivity.class);
					Bundle bundle = new Bundle();
					bundle.putSerializable("bean", addressBean);
					intent.putExtra("bundle", bundle);
					startActivity(intent);
				}
			}
		});

		// 回退
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AddressActivity.this.finish();
			}
		});

		// TODO 编辑
		right_text.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (i == 0) {
					right_text.setText("完成");
					i = 1;
					adapter.notifyDataSetChanged();
					ft_Button.setText("删除地址");
				} else {
					right_text.setText("编辑");
					i = 0;
					adapter.notifyDataSetChanged();
					ft_Button.setText("添加新地址");

				}

				/*
				 * kongyi.setVisibility(View.VISIBLE);
				 * konger.setVisibility(View.VISIBLE);
				 */
			}
		});
		ft_Button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (i == 0) {
					startActivity(new Intent(AddressActivity.this,
							NewAddressActivity.class));
				} else {
					Gson gson = new Gson();
					String json2 = gson.toJson(list);
					System.out.println("json=============" + json2);
					final Iterator<AddressBean> it = list.iterator();
					while (it.hasNext()) {
						AddressBean next = it.next();
						System.out.println(next.i);
						if (next.i == 1) {
							removelist.add(Integer.parseInt(next.getId()));
						}
					}

					String json = gson.toJson(removelist);
					System.out.println(removelist.toString());
					System.out.println("ssssssssssss====" + json);

					/* aCache.put("address", json); */

					HttpUtils httpUtils = new HttpUtils();
					RequestParams params = new RequestParams();
					params.addBodyParameter("ids", json);

					/*
					 * params.addBodyParameter("defaultAddress", );//街道地址
					 */
					/* params.addBodyParameter("password", pass); */
					httpUtils.send(HttpMethod.POST, GlobalUrl.deleteAddress,
							params, new RequestCallBack<String>() {


								@Override
								public void onFailure(HttpException arg0,
										String arg1) {
									Toast.makeText(AddressActivity.this,
											"删除失败", 0).show();
									System.out.println(arg1);
								}

								@Override
								public void onSuccess(ResponseInfo<String> arg0) {

									try {
										Toast.makeText(
												AddressActivity.this,
												new JSONObject(arg0.result)
														.getString("message"),
												0).show();

										send();
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									System.out.println(arg0.result);
									/*
									 * String result = arg0.result; Gson gson =
									 * new Gson(); Type listType = new
									 * TypeToken<
									 * List<AddressBean>>(){}.getType();
									 */

								}

								@Override
								public void onStart() {
									
								}
							});

					adapter.notifyDataSetChanged();
				}
			}
		});
	}

	private void init() {
		// TODO Auto-generated method stub
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("地址管理");
		right_text.setText("编辑");

		addresslist.addFooterView(footview);
		aCache = ACache.get(AddressActivity.this);
		send();
		adapter.notifyDataSetChanged();

	}

	class AddressAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(AddressActivity.this, R.layout.address_item,
					null);
			TextView moren = (TextView) arg1.findViewById(R.id.moren);
			final ImageView Check = (ImageView) arg1.findViewById(R.id.check);
			TextView adname = (TextView) arg1.findViewById(R.id.adname);
			TextView adphone = (TextView) arg1.findViewById(R.id.adphone);
			TextView addresss = (TextView) arg1.findViewById(R.id.addrsss);
			final AddressBean addressBean = list.get(arg0);
			View view = arg1.findViewById(R.id.kongyi);
			View view2 = arg1.findViewById(R.id.konger);
			Check.setTag(arg0);
			// ////////////
			adname.setText(addressBean.getReceiver());
			adphone.setText(addressBean.getPhone());
			String[] split = addressBean.getAddress().split("/");
			addresss.setText(split[0]+split[1]);

			if (i == 0) {
				Check.setVisibility(View.GONE);
				view.setVisibility(View.GONE);
				view2.setVisibility(View.GONE);
			} else {
				Check.setVisibility(View.VISIBLE);
				view.setVisibility(View.VISIBLE);
				view2.setVisibility(View.VISIBLE);
			}

			if (addressBean.getDefaultAddress().equals("true")) {
				moren.setVisibility(View.VISIBLE);
			}

			// 点击事件
			Check.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					// TODO Auto-generated method stub

					if ((Integer) Check.getTag() == arg0) {

						if (addressBean.getI() == 0) {// 选中
							addressBean.setI(1);
							Check.setImageResource(R.drawable.check_icon);

						} else {// 未选中
							addressBean.setI(0);
							Check.setImageResource(R.drawable.circle_cart);

						}
					}
				}
			});
			return arg1;
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		send();
		adapter.notifyDataSetChanged();

	}

	private void send() {
		HttpUtils httpUtils = new HttpUtils();
		/*
		 * customer.id 然后就是其他的 private String receiver; private String address;
		 * private String phone; private boolean defaultAddress;
		 */
		RequestParams params = new RequestParams();
		params.addBodyParameter("id", "" + SpfUtil.getId());

		/*
		 * params.addBodyParameter("defaultAddress", );//街道地址
		 */
		/* params.addBodyParameter("password", pass); */
		httpUtils.send(HttpMethod.POST, GlobalUrl.getAddressByCustomer, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
						Toast.makeText(AddressActivity.this, "获取失败", 0).show();
						System.out.println(arg1);
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {

						Toast.makeText(AddressActivity.this, "获取成功", 0).show();
						System.out.println(arg0.result);
						String result = arg0.result;
						Gson gson = new Gson();
						Type listType = new TypeToken<List<AddressBean>>() {
						}.getType();
						list = gson.fromJson(result, listType);
						adapter.notifyDataSetChanged();

						dialog.dismiss();
					}

					@Override
					public void onStart() {
						dialog = new CustomProgressDialog(AddressActivity.this,
								"获取中。。。", R.anim.frame2);
						dialog.show();
					}
				});
	}
}
