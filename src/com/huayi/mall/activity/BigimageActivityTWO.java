package com.huayi.mall.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.common.ImageLoaderCfg;
import com.nostra13.universalimageloader.core.ImageLoader;

public class BigimageActivityTWO extends Activity {
	private LinearLayout myLayout;
	private TextView mTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bigimage);
		Intent intent = getIntent();
		String picture = intent.getStringExtra("bigImage_lookfor");
		ImageView imageView = (ImageView) findViewById(R.id.bigimag);

		ImageLoader.getInstance().displayImage(picture, imageView,
				ImageLoaderCfg.options);
		imageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				BigimageActivityTWO.this.finish();
			}
		});

	}

}
