package com.huayi.mall.activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.util.SystemBar;

public class WuLiuActivity extends Activity {

	private TextView tv_title_logo;
	private ImageView bakc;
	private WuLiuAdapter adapter = new WuLiuAdapter();
	private ListView wuliulist;
	private View headview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wu_liu);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		bakc = (ImageView) findViewById(R.id.back);
		wuliulist = (ListView) findViewById(R.id.wuliulist);
		headview = View.inflate(WuLiuActivity.this, R.layout.wuliu_header, null);
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		init();
		onClick();
	}	
	private void init() {
		// TODO Auto-generated method stub
		tv_title_logo.setText("物流");
		bakc.setVisibility(View.VISIBLE);
		wuliulist.setAdapter(adapter);
	

	}
	private void onClick() {
		// TODO Auto-generated method stub
		bakc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				WuLiuActivity.this.finish();
			}
		});
	}
	class WuLiuAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 6;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(WuLiuActivity.this, R.layout.wuliulist_item, null);
			ImageView imageView = (ImageView) arg1.findViewById(R.id.dian);
			TextView message = (TextView) arg1.findViewById(R.id.message);
			TextView date = (TextView) arg1.findViewById(R.id.date);
			TextView time = (TextView) arg1.findViewById(R.id.time);
			if (arg0!=0) {

				LayoutParams layoutParams = imageView.getLayoutParams();
				layoutParams.height=30;
				layoutParams.width=30;
				imageView.setLayoutParams(layoutParams);
				imageView.setBackgroundResource(R.drawable.wulius);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);  
				lp.setMargins(5, 15, 15, 0);  
				message.setLayoutParams(lp);


				message.setTextColor(Color.parseColor("#999999"));
				date.setTextColor(Color.parseColor("#999999"));
				time.setTextColor(Color.parseColor("#999999"));
			}

			return arg1;
		}}
}
