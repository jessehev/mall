package com.huayi.mall.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class ChangeNameActivity extends Activity {

	private ImageView back;

	private EditText content_et;

	private TextView tv_title_logo, submit_tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_changname);

		findViewById();
		initView();
		initData();
		onClick();
	}

	public void initData() {
		content_et.setText(getIntent().getStringExtra("nickName"));

		content_et.setSelection(content_et.getText().toString().length());
	}

	public void findViewById() {
		content_et = (EditText) findViewById(R.id.content_et_changgename);
		submit_tv = (TextView) findViewById(R.id.submit_changename);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		back = (ImageView) findViewById(R.id.back);

	}

	private void initView() {
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);


		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("修改昵称");

	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ChangeNameActivity.this.finish();
			}
		});

		submit_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (TextUtils.isEmpty(content_et.getText().toString())) {
					Toast.makeText(ChangeNameActivity.this, "请输入昵称", 1).show();

				} else {
					xutilRequest(content_et.getText().toString());
				}
			}
		});

	}

	// 修改昵称
	private void xutilRequest(String nickName) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();

		params.addBodyParameter("nickName", nickName);
		params.addBodyParameter("id", SpfUtil.getId() + "");

		System.out.println("id--changename----" + SpfUtil.getId());

		httpUtils.send(HttpMethod.POST, GlobalUrl.updataInfo, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();

						System.out.println("onfail---" + arg1);
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						dialog.dismiss();
						try {
							JSONObject obj = new JSONObject(arg0.result);

							System.out.println("changename---------"
									+ obj.toString());
							boolean success = obj.optBoolean("success");
							if (success) {
								Toast.makeText(ChangeNameActivity.this,
										obj.optString("message"), 1).show();
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onStart() {
						dialog = new CustomProgressDialog(
								ChangeNameActivity.this, "正在修改中", R.anim.frame2);

						dialog.show();
					}
				});
	}

}
