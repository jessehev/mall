package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.huayi.mall.GoodsDataActivity;
import com.huayi.mall.R;
import com.huayi.mall.adapter.CollecttionAdapter;
import com.huayi.mall.common.Cart;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class CollecttionActivity extends Activity {

	private ListView listview;
	private CollecttionAdapter adapter;

	private TextView complete_tv, collectionGoods_tv;
	private ImageView circle_iv;
	private LinearLayout delete_layout;
	private ArrayList<Cart> arrayList = new ArrayList<Cart>();

	private int isComplete = 0;

	private boolean selectAll;

	private boolean flag;

	private ImageView back;

	private TextView tv_title_logo;

	private SwipeRefreshLayout mSwipeRefreshLayout;

	private ScrollView mScrollView;

	/**
	 * 所有列表中的商品全部被选中，让全选按钮也被选中
	 */

	// setImageResource
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (msg.what == 11) {
				// 所有列表中的商品全部被选中，让全选按钮也被选中
				// flag记录是否全被选中
				flag = !(Boolean) msg.obj;
				if (!flag) {
					selectAll = true;
					circle_iv.setImageResource(R.drawable.check_icon);
				} else {
					selectAll = false;

					circle_iv.setImageResource(R.drawable.circle_cart);
				}

			}
		}
	};
	private LinearLayout lin_dailog_item;
	private Dialog dialog;
	private LinearLayout layout1, layout2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_collection);

		findViewById();
		initView();
		initAdapter();
		initData();
		onclick();

	}

	public void initAdapter() {
		View view = getLayoutInflater().inflate(R.layout.activity_empty, null);

		addContentView(view, new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		adapter = new CollecttionAdapter(this, arrayList, handler);
		listview.setEmptyView(view);
		listview.setAdapter(adapter);

	}

	public void initView() {

		// mSwipeRefreshLayout.setOnRefreshListener(this);
		// mSwipeRefreshLayout.setColorScheme(android.R.color.holo_orange_light,
		// android.R.color.holo_orange_dark,
		// android.R.color.holo_red_light, android.R.color.holo_red_dark);
		
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);

		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("收藏夹");

	}

	public void initData() {
		xutilRequest();
	}

	public void setData() {
		String listString = MyApplication.aCache.getAsString("collectiongoods");

		if (listString != null) { // 如果有默认地址 就取
			try {
				JSONArray jsonArray = new JSONArray(listString);

				arrayList.clear();
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObj = (JSONObject) jsonArray.opt(i);

					Cart cart = new Cart();
					/**
					 * "goodsPicture":
					 * "/repository/niuzaiku.png,/repository/niuzaiku.png,/repository/niuzaiku.png,/repository/niuzaiku.png,/repository/niuzaiku.png"
					 * ,
					 * 
					 * 取第一张图片
					 */
					String picture = jsonObj.optString("goodsPicture").split(
							",")[0];

					cart.picture = picture;
					System.out.println("picture" + picture);

					cart.name = jsonObj.optString("goodsName");
					cart.oldPrice = jsonObj.optString("marketPrice");
					cart.price = jsonObj.optString("price");

					cart.id = jsonObj.optString("id");
					cart.visible = false;
					String saleBlog = jsonObj.optString("saleTotalNumber"); // 判断是否是热销的标志

					arrayList.add(cart);

				}
				adapter.setData(arrayList);

				if (arrayList.size() == 0) { // 如果没有元素则改变全选状态
					circle_iv.setImageResource(R.drawable.circle_cart);
				}

				if (complete_tv.getText().toString().equals("完成")) {
					circle_iv.setVisibility(View.VISIBLE);
					delete_layout.setVisibility(View.VISIBLE);
					collectionGoods_tv.setText("全选");
					setItemVisibility(true);
				}
			} catch (Exception e) {

			}

		}
		if (arrayList.size() == 0) {
			layout1.setVisibility(View.GONE);
			layout2.setVisibility(View.GONE);
		}
	}

	// 查询
	private void xutilRequest() {

		final CustomProgressDialog dialog = new CustomProgressDialog(
				CollecttionActivity.this, "获取中。。。", R.anim.frame2);
		dialog.show();

		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("customer.id", SpfUtil.getId() + "");

		httpUtils.send(HttpMethod.POST, GlobalUrl.findMyFllow, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
						setData();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							String result = arg0.result;

							MyApplication.aCache.put("collectiongoods", result);

							System.out.println("result-----------s" + result);
							setData();

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						dialog.dismiss();
					}

					@Override
					public void onStart() {

					}
				});
	}

	// 删除（取消收藏）
	private void xutilRequestCancle(String goodsIds) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("customerId", SpfUtil.getId() + "");
		params.addBodyParameter("goodsIds", goodsIds);

		httpUtils.send(HttpMethod.POST, GlobalUrl.deleteFollowGoods, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						setData();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							String result = arg0.result;

							System.out.println("result" + result);
							xutilRequest();// 删除之后再去重新获取

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {
					
					}
				});
	}

	public void onclick() {
		complete_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isComplete == 0) {
					complete_tv.setText("完成");
					circle_iv.setVisibility(View.VISIBLE);
					delete_layout.setVisibility(View.VISIBLE);
					collectionGoods_tv.setText("全选");
					setItemVisibility(true);
					isComplete = 1;

				} else if (isComplete == 1) {
					complete_tv.setText("编辑");
					circle_iv.setVisibility(View.GONE);
					collectionGoods_tv.setText("近一个月收藏");
					delete_layout.setVisibility(View.GONE);
					setItemVisibility(false);
					isComplete = 0;
				}
			}
		});

		final List<Integer> removeList = new ArrayList<Integer>();
		// 删除
		delete_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// 不能用for循环，会出现删除不完的情况（删的过程中集合大小在动态变化）
				removeList.clear();
				if (!isSelected()) {

					Toast.makeText(CollecttionActivity.this, "请勾选要删除的商品", 1)
							.show();
				} else {
					Iterator<Cart> it = arrayList.iterator();
					while (it.hasNext()) {
						Cart cart = (Cart) it.next();
						if (cart.select) {
							removeList.add(Integer.parseInt(cart.id));
						}
					}

					Gson gson = new Gson();
					String goodsids = gson.toJson(removeList);

					System.out.println("goodsids" + goodsids);

					deleteDialog(goodsids);

				}

			}
		});
		// 全选
		circle_iv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (collectionGoods_tv.getText().equals("全选")) {
					if (!selectAll) {
						selectedAll(true);
						circle_iv.setImageResource(R.drawable.check_icon);
						selectAll = true;
					} else {
						selectAll = false;
						selectedAll(false);
						circle_iv.setImageResource(R.drawable.circle_cart);
					}
				}
			}
		});
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				if (complete_tv.getText().toString().equals("编辑")) {
					Intent intent = new Intent(CollecttionActivity.this,
							GoodsDataActivity.class);

					intent.putExtra("id", arrayList.get(position).id + "");
					startActivity(intent);
				}
			}
		});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	// 删除弹出框
	public void deleteDialog(final String goodsIds) {
		if (lin_dailog_item == null && dialog == null) {
			lin_dailog_item = (LinearLayout) View.inflate(
					CollecttionActivity.this, R.layout.dialog_cart, null);
			dialog = new AlertDialog.Builder(CollecttionActivity.this).create();

		}
		dialog.show();
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();

		WindowManager wm = (WindowManager) CollecttionActivity.this
				.getSystemService(Context.WINDOW_SERVICE);
		int height = wm.getDefaultDisplay().getHeight() / 3;
		int weith = wm.getDefaultDisplay().getWidth() / 5 * 3;
		params.width = weith;
		params.height = height;
		params.gravity = Gravity.TOP;
		params.y = wm.getDefaultDisplay().getHeight() / 5;

		dialog.getWindow().setContentView(lin_dailog_item);
		dialog.getWindow().setAttributes(params);
		dialog.getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
						| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		dialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		ImageView cancle = (ImageView) lin_dailog_item
				.findViewById(R.id.cancle_dialog_cart);
		TextView sure = (TextView) lin_dailog_item
				.findViewById(R.id.sure_dialog_cart);
		sure.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// 请求服务器删除商品

				xutilRequestCancle(goodsIds);
				dialog.cancel();

			}
		});
		cancle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog.cancel();

			}
		});

	}

	// 全选或全取消
	private void selectedAll(boolean visibility) {
		for (int i = 0; i < arrayList.size(); i++) {
			arrayList.get(i).select = visibility;
		}
		adapter.notifyDataSetChanged();
	}

	public void findViewById() {
		layout1 = (LinearLayout) findViewById(R.id.liearLayout_collection);
		layout2 = (LinearLayout) findViewById(R.id.layout_delete_collection);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		listview = (ListView) findViewById(R.id.listview_collection);
		complete_tv = (TextView) findViewById(R.id.complete);
		collectionGoods_tv = (TextView) findViewById(R.id.collection_goods);
		circle_iv = (ImageView) findViewById(R.id.cicle_collection);
		delete_layout = (LinearLayout) findViewById(R.id.layout_delete_collection);
	}

	/**
	 * // 隐藏check图标
	 * 
	 * @param visibility
	 *            true：显示全部园，false 隐藏全部园
	 * 
	 */

	public void setItemVisibility(boolean visibility) { // 获取listview的所有item对象
		for (int i = 0; i < arrayList.size(); i++) {
			arrayList.get(i).visible = visibility;
		}
		adapter.notifyDataSetChanged();
	}

	// /**
	// * 判断是否购物车中所有的商品全部被选中
	// *
	// * @return true所有条目全部被选中 false还有条目没有被选中
	// */
	// private boolean isAllSelected() {
	// boolean flag = true;
	// for (int i = 0; i < arrayList.size(); i++) {
	// if (arrayList.get(i).select) {
	// flag = true;
	// return flag;
	// }
	// }
	// return flag;
	// }

	/**
	 * 判断是否有选项被选中
	 * 
	 * @return
	 */
	private boolean isSelected() {
		boolean flag = false;
		for (Cart cart : arrayList) {

			if (cart.select) {
				flag = true;
				return true;
			} else {
				continue;
			}
		}
		return flag;

	}

}
