package com.huayi.mall.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class ChangePwdActivity extends Activity {

	private ImageView back;
	private EditText oldpwd, newpwd, newpwd2;

	private TextView bt_change, tv_title_logo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_changepwd);

		findViewById();
		initView();
		// initData();
		onClick();

	}

	public void initData() {

	}

	public void findViewById() {
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		back = (ImageView) findViewById(R.id.back);
		// oldpwd newpwd newpwd2 newpwd2
		oldpwd = (EditText) findViewById(R.id.et_oldpwd_changpwd);
		newpwd = (EditText) findViewById(R.id.et_newpwd_changpwd);
		newpwd2 = (EditText) findViewById(R.id.et_newpwd2_changpwd);
		bt_change = (TextView) findViewById(R.id.bt_changepwd);

	}

	private void initView() {
		// TODO Auto-generated method stub

		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		tv_title_logo.setText("修改密码");
		back.setVisibility(View.VISIBLE);
	}

	private void onClick() {
		// TODO Auto-generated method stub
		bt_change.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String oldpwdStr = oldpwd.getText().toString();
				String newpwdStr = newpwd.getText().toString();
				String newpwd2Str = newpwd2.getText().toString();
				/**
				 * MyApplication.aCache .getAsString("password")
				 */
				if (!oldpwdStr.equals(oldpwdStr)) {//
					Toast.makeText(ChangePwdActivity.this, "旧密码不正确，请重新输入", 1)
							.show();
				} else if (TextUtils.isEmpty(newpwdStr)
						| newpwdStr.length() < 6) {
					Toast.makeText(ChangePwdActivity.this, "请输入6位以上密码", 1)
							.show();
				} else if (oldpwdStr.equals(newpwdStr)) {
					Toast.makeText(ChangePwdActivity.this, "新密码不能和旧密码相同，请重新输入",
							1).show();
				} else if (TextUtils.isEmpty(newpwdStr)
						| !newpwdStr.equals(newpwd2Str)) {
					Toast.makeText(ChangePwdActivity.this, "两次输入密码不一致，请重新输入", 1)
							.show();
				} else {
					xutilRequestPwd(newpwdStr, oldpwdStr);
				}

			}
		});
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				ChangePwdActivity.this.finish();
			}
		});

	}

	// 修改 密码
	private void xutilRequestPwd(String newPwd, String oldPwd) {

		final CustomProgressDialog dialog = new CustomProgressDialog(
				ChangePwdActivity.this, "正在请求中", R.anim.frame);

		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();

		params.addBodyParameter("phone", SpfUtil.getPhone());
		params.addBodyParameter("password", oldPwd);
		params.addBodyParameter("newPassword", newPwd);

		httpUtils.send(HttpMethod.POST, GlobalUrl.updatePassword, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
					}
					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						dialog.dismiss();
						try {
							JSONObject obj = new JSONObject(arg0.result);

							System.out.println("obj-name--------" + obj);
							if (obj.optBoolean("success")) {
								Toast.makeText(ChangePwdActivity.this,
										obj.optString("message"), 1).show();

							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onStart() {

					}
				});
	}

}
