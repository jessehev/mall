package com.huayi.mall.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.MiaoGoodsDataActivity;
import com.huayi.mall.R;
import com.huayi.mall.base.MiaoKillBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.util.ParesData2Obj;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.ImageViewSubClass;
import com.huayi.mall.view.MyListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MiaoKillActivity extends Activity implements SwipeRefreshLayout.OnRefreshListener{
	private MiaoKillAdapter adapter = new MiaoKillAdapter();
	private MyListView miaokillls;
	private ImageView back;
	private TextView tv_title_logo;
	private MyCount mc;
	private TextView daojisshi; 
	private MiaoKillBean bean;
	private List<MiaoKillBean.GoodsListBean>list = new ArrayList<MiaoKillBean.GoodsListBean>();
	private long startTime=0;
	private long endTime=0;
	private boolean isBegin =false;
	private SwipeRefreshLayout sf;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_miao_kill);
		sf = (SwipeRefreshLayout) findViewById(R.id.sf);
		miaokillls = (MyListView) findViewById(R.id.miaokillls);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		daojisshi = (TextView) findViewById(R.id.daojisshi);
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		init();
		onclick();
	}

	private void onclick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				MiaoKillActivity.this.finish();
			}
		});

	}

	private void init() {
		// TODO Auto-generated method stub
		back.setVisibility(View.VISIBLE);
		miaokillls.setAdapter(adapter);
		tv_title_logo.setText("秒杀");
		sf.setOnRefreshListener(this);
		send();
	}
	class MiaoKillAdapter extends BaseAdapter{





		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int arg0, View arg1, ViewGroup arg2) {

			// TODO Auto-generated method stub

			arg1 = View.inflate(MiaoKillActivity.this, R.layout.miaokillls_item, null);


			Button buynow = (Button) arg1.findViewById(R.id.buynow);
			TextView goodsname = (TextView) arg1.findViewById(R.id.goodsnamee);
			TextView yuanprice = (TextView) arg1.findViewById(R.id.yuanprice);
			TextView price = (TextView) arg1.findViewById(R.id.price);
			TextView zhekou = (TextView) arg1.findViewById(R.id.zhekou);
			ImageViewSubClass pic = (ImageViewSubClass) arg1.findViewById(R.id.pic);
			if (isBegin) {
				buynow.setEnabled(true);
				buynow.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg2) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(MiaoKillActivity.this,MiaoGoodsDataActivity.class);
						intent.putExtra("id", list.get(arg0).getId()+"");
						intent.putExtra("endtime", bean.getEndTime());
						intent.putExtra("secondKillId", bean.getId()+"");
						startActivity(intent);
					}
				});
			}else {
				buynow.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg2) {
						// TODO Auto-generated method stub
						Toast.makeText(MiaoKillActivity.this, "活动还没有开始哦", 0).show();
					}
				});
			}
			
			goodsname.setText( list.get(arg0).getGoodsName());
			yuanprice.setText( list.get(arg0).getMarketPrice());
			price.setText( list.get(arg0).getPrice());
			zhekou.setText( list.get(arg0).getDiscount());
			String[] split =  list.get(arg0).getGoodsPicture().split(",");
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost1 + split[0], pic,
					ImageLoaderCfg.options2);
			if (startTime<System.currentTimeMillis()) {

				buynow.setEnabled(true);
			}
			
			return arg1;
		}}
	/*定义一个倒计时的内部类*/  
	class MyCount extends CountDownTimer {     
		public MyCount(long millisInFuture, long countDownInterval) {     
			super(millisInFuture, countDownInterval);     
		}     
		@Override     
		public void onFinish() {     
			daojisshi.setText("已经结束"); 
			isBegin=false;
			adapter.notifyDataSetChanged();

		}     
		@Override     
		public void onTick(long millisUntilFinished) {  
			SimpleDateFormat dateFormat = new SimpleDateFormat("HH时mm分ss秒");
			dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date date = new Date();
			date.setTime(millisUntilFinished);
			String format = dateFormat.format(date);
			daojisshi.setText(format);     
		}    
	} 
	public void send(){
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("over", "false");
		httpUtils.send(HttpMethod.POST, GlobalUrl.secondKill , params, new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				System.out.println(arg1);
				sf.setRefreshing(false);

			}
			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				System.out.println("秒杀信息============"+arg0.result);
				list= new ArrayList<MiaoKillBean.GoodsListBean>();
				bean=ParesData2Obj.json2Obj(arg0.result, MiaoKillBean.class);
				list.addAll(bean.getGoodsList());
				adapter.notifyDataSetChanged();
				startTime = bean.getStartTime();

				endTime = bean.getEndTime();					
				long time = System.currentTimeMillis();
				SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
				System.out.println("系统当前毫秒值"+time);
				System.out.println("开始时间毫秒值"+startTime);
				System.out.println();

				if (startTime<time&&time<endTime) {
					mc = new MyCount(endTime-time, 1000);
					mc.start();
					isBegin=true;
					adapter.notifyDataSetChanged();

				}
				sf.setRefreshing(false);
			}
		});	
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		init();
	}
}
