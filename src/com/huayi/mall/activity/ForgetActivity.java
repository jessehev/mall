package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.adapter.BrandAdapter;
import com.huayi.mall.adapter.MotherBabyAdapter;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.IsNetWorkConnected;
import com.huayi.mall.util.IsPhoneNumber;
import com.huayi.mall.util.TimeButton;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.ImageCycleView;
import com.huayi.mall.view.ImageCycleView.ImageCycleViewListener;
import com.huayi.mall.view.MyListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class ForgetActivity extends Activity {

	private ImageView back;

	private EditText phone_et, pwd_et, code_et, pwd2_et;

	private TextView bt_forget, tv_title_logo;;

	private TimeButton sendCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forget);

		findViewById();
		initView();
		// initData();
		onClick();

	}

	public void initData() {

	}

	public void findViewById() {
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		back = (ImageView) findViewById(R.id.back);
		phone_et = (EditText) findViewById(R.id.et_phone_forget);

		code_et = (EditText) findViewById(R.id.et_code_forget);
		pwd_et = (EditText) findViewById(R.id.et_newpwd_forget);

		pwd2_et = (EditText) findViewById(R.id.et_newpwd2_forget);
		bt_forget = (TextView) findViewById(R.id.bt_forget);
		sendCode = (TimeButton) findViewById(R.id.sendCode_forget);

	}

	private void initView() {
		// TODO Auto-generated method stub
		tv_title_logo.setText("忘记密码");
		back.setVisibility(View.VISIBLE);

		sendCode.setTextAfter("秒后重新获取").setTextBefore("发送验证码")
				.setLenght(60 * 1000);
	}

	private void xutilRequest(String phone, String code, String pwd) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("phone", phone);
		params.addBodyParameter("code", code);
		params.addBodyParameter("password", pwd);
		httpUtils.send(HttpMethod.POST, GlobalUrl.forget, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						dialog.dismiss();
						try {
							JSONObject obj = new JSONObject(arg0.result);
							String message = obj.getString("message");
							Toast.makeText(ForgetActivity.this, message, 1)
									.show();

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onStart() {
						dialog = new CustomProgressDialog(ForgetActivity.this,
								"正在提交中", R.anim.frame2);

						dialog.show();
					}
				});
	}

	private void onClick() {
		// TODO Auto-generated method stub
		bt_forget.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String phone = phone_et.getText().toString();
				String code = code_et.getText().toString();
				String pwd = pwd_et.getText().toString();
				String pwd2 = pwd2_et.getText().toString();

				if (TextUtils.isEmpty(phone) | !IsPhoneNumber.isPhone(phone)) {
					Toast.makeText(ForgetActivity.this, "请输入正确的手机号", 1).show();
				} else if (TextUtils.isEmpty(code)) {
					Toast.makeText(ForgetActivity.this, "请输入验证码", 1).show();
				} else if (TextUtils.isEmpty(pwd) | pwd.length() < 6) {
					Toast.makeText(ForgetActivity.this, "请输入不少于6位的新密码", 1)
							.show();
				} else if (TextUtils.isEmpty(pwd2) | pwd2.length() < 6) {

					Toast.makeText(ForgetActivity.this, "请再次输入新密码", 1).show();
				} else if (!pwd.equals(pwd2)) {
					Toast.makeText(ForgetActivity.this, "两次输入的密码不一致", 1).show();
				} else {
					xutilRequest(phone, code, pwd);
				}
			}
		});
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				ForgetActivity.this.finish();
			}
		});
		sendCode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String phone = phone_et.getText().toString();

				if (IsNetWorkConnected.isNetworkConnected(ForgetActivity.this)) {
					if (phone_et.getText().toString().length() != 11) {
						Toast.makeText(getApplicationContext(), "请输入正确手机号码", 0)
								.show();
						TimeButton.I = 1;
					} else {
						TimeButton.I = 2;
						System.out.println("----------------ti me");
						xutilSendCode(phone);
					}
				} else {
					Toast.makeText(ForgetActivity.this, "请检查网络连接", 1).show();
				}

			}
		});

	}

	/**
	 * 短信验证
	 * 
	 * @param currentPage
	 * @param key
	 */
	private void xutilSendCode(String phone) {
		HttpUtils httpUtils = new HttpUtils();

		RequestParams params = new RequestParams();
		params.addBodyParameter("phone", phone);

		httpUtils.send(HttpMethod.POST, GlobalUrl.sendCode, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							JSONObject obj = new JSONObject(arg0.result);

							System.out.println("obj====" + obj);
							if (obj.optBoolean("success")) {

								Toast.makeText(ForgetActivity.this,
										obj.optString("message"), 1).show();
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onStart() {
					}
				});
	}

}
