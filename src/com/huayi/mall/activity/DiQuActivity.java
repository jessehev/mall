package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.huayi.mall.R;
import com.huayi.mall.R.id;
import com.huayi.mall.R.layout;




import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DiQuActivity extends FragmentActivity {

	private ImageView back;
	private TextView tv_title_logo;
	private List<String>list = new ArrayList<String>();
	private ListView prlist;
	private PrAdapter adapter = new PrAdapter();
	private JSONObject array;
	private JSONArray jsonArray;
	private String pr;
	private String cityjs;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sheng_shi_qu);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		prlist = (ListView) findViewById(R.id.prlist);
		Intent intent = getIntent();
		pr = intent.getStringExtra("pr");
		cityjs = intent.getStringExtra("cityjs");
		
		init();
		onclick();
	}

	private void onclick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				DiQuActivity.this.finish();
			}
		});
		prlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				String mate = pr+list.get(arg2);
				Intent intent = new Intent();
				intent.putExtra("mate", mate);
				System.out.println(mate);
				setResult(1, intent);
				DiQuActivity.this.finish();
			}
		});
	}

	@SuppressLint("NewApi") private void init() {
		// TODO Auto-generated method stub
		
		
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("选择地址");
		try {
			array = new JSONObject(cityjs);
			jsonArray = array.getJSONArray("a");
			System.out.println();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String p = jsonObject.getString("s");
				list.add(p);
			}
			prlist.setAdapter(adapter);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		



	}

	class PrAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(DiQuActivity.this, R.layout.pro_item, null);
			TextView txpr = (TextView) arg1.findViewById(R.id.txpr);
			txpr.setText(list.get(arg0));
			return arg1;
		}};

}
