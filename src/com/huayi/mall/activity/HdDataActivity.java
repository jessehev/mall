package com.huayi.mall.activity;

import org.json.JSONObject;

import com.huayi.mall.R;
import com.huayi.mall.R.id;
import com.huayi.mall.R.layout;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.FormatTime;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class HdDataActivity extends Activity {

	private ImageView back;
	private TextView tv_title_logo, title, time, content;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hd_data);
		findViewByI();
		initView();
		onClick();
		initData();
	}

	public void findViewByI() {
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		title = (TextView) findViewById(R.id.title_hddata);
		time = (TextView) findViewById(R.id.time_hddata);
		content = (TextView) findViewById(R.id.content_hddata);

	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				HdDataActivity.this.finish();
			}
		});
	}

	public void initData() {
		Intent intent = getIntent();
		String id = intent.getStringExtra("id");
		xutilRequest(id);
	}

	private void initView() {
		// TODO Auto-generated method stub
		tv_title_logo.setText("活动详情");
		back.setVisibility(View.VISIBLE);
	}

	public void setData(JSONObject jsonObj) {
		String timeStr = FormatTime.format(jsonObj.optString("publishTime"));
		String nameStr = jsonObj.optString("name");
		String descsStr = jsonObj.optString("descs");

		time.setText(timeStr);
		title.setText(nameStr);
		content.setText(descsStr);

	}

	private void xutilRequest(String id) {
		HttpUtils httpUtils = new HttpUtils();

		RequestParams params = new RequestParams();
		params.addBodyParameter("id", id);

		httpUtils.send(HttpMethod.POST, GlobalUrl.lookforhuodong, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							JSONObject jsonObj = new JSONObject(arg0.result);
							System.out.println("arg0.result" + arg0.result);
							setData(jsonObj);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();

						}

					}

					@Override
					public void onStart() {

					}
				});
	}

}
