package com.huayi.mall.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.huayi.mall.R;
import com.huayi.mall.util.SystemBar;

public class BigimageActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bigimage);
		Intent intent = getIntent();
		String lujing = intent.getStringExtra("lujing");
		ImageView imageView = (ImageView) findViewById(R.id.bigimag);
		Bitmap decodeFile = BitmapFactory.decodeFile(lujing);
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);

		imageView.setImageBitmap(decodeFile);
		imageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				BigimageActivity.this.finish();
			}
		});
		
	}



}
