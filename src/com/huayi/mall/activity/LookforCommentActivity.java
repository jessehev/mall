package com.huayi.mall.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.adapter.LoockforCommentAdapter;
import com.huayi.mall.common.Comment;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.FormatTime;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.MyListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class LookforCommentActivity extends Activity {

	private MyListView listview;

	private List<Comment> arrayList = new ArrayList<Comment>();
	private LoockforCommentAdapter adapter;

	private ImageView back;

	private TextView tv_title_logo;

	private int currentPage, totalPage;

	private String goodsId;

	private ScrollView mScrollView;

	private ProgressBar pb;

	private TextView pbTv;

	private LinearLayout layout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lookfor_comment);
		findViewById();

		init();
		initData();
		initAdapter();
		onClick();

	}

	public void setData() {
		JSONObject jsonObj = MyApplication.aCache
				.getAsJSONObject("lookforcomment");

		if (jsonObj != null) {

			JSONArray jsonArray = jsonObj.optJSONArray("rows");
			currentPage = jsonObj.optInt("currentPage");
			totalPage = jsonObj.optInt("totalPage");
			if (jsonArray != null) {
				for (int i = 0; i < jsonArray.length(); i++) {

					Comment comment = new Comment();
					try {
						JSONObject obj = jsonArray.getJSONObject(i);

						comment.content = obj.optString("evaluationContent");
						comment.shopPicture = obj
								.optString("evaluationPicture");
						String createTime = obj.optString("createTime");
						if (createTime != null) {

							// comment.time = format(createTime);
							comment.time = FormatTime.format(createTime);
						}

						JSONObject customerObj = obj.optJSONObject("customer");
						comment.headPicture = customerObj
								.optString("headPicture");
						comment.nickName = customerObj.optString("nickName");

						arrayList.add(comment);
					} catch (Exception e) {

					}
				}
				adapter.setData(arrayList);
			}

		}

		layout.setVisibility(View.GONE);

	}

	// 查询
	private void xutilRequest(final int currentPage, String goodsId) {

		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();

		params.addBodyParameter("goods.id", goodsId);
		params.addBodyParameter("currentPage", currentPage + "");
		httpUtils.send(HttpMethod.POST, GlobalUrl.lookForComment, params,
				new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				// handler.sendEmptyMessage(2);
				setData();

			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				try {
					JSONObject obj = new JSONObject(arg0.result);

					MyApplication.aCache.put("lookforcomment", obj);

					System.out.println("obj-----lookfor" + obj);
					setData();

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onStart() {
			}
		});
	}

	public void initData() {

		goodsId = getIntent().getStringExtra("goods.id");
		xutilRequest(1, goodsId);
	}

	public void findViewById() {
		layout = (LinearLayout) findViewById(R.id.load_layout);
		pb = (ProgressBar) findViewById(R.id.progress);
		pbTv = (TextView) findViewById(R.id.progress_text);
		mScrollView = (ScrollView) findViewById(R.id.scollview_lookfor);
		listview = (MyListView) findViewById(R.id.listView_lookfor_comment);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
	}

	public void initAdapter() {

		View view = getLayoutInflater().inflate(R.layout.activity_empty, null);

		addContentView(view, new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		listview.setEmptyView(view);
		adapter = new LoockforCommentAdapter(this, arrayList);
		listview.setAdapter(adapter);
	}

	private void init() {
		// TODO Auto-generated method stub
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("查看评价");
		layout.setVisibility(View.GONE);
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		layout.setBackgroundColor(Color.parseColor("#ffffff"));

		mScrollView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {

				int scrollY = view.getScrollY();
				int height = view.getHeight();
				int scrollViewMeasuredHeight = mScrollView.getChildAt(0)
						.getMeasuredHeight();
				if (scrollY == 0) {
					System.out.println("       ˶    view.getScrollY()="
							+ scrollY);
				}
				if ((scrollY + height) == scrollViewMeasuredHeight
						&& event.getAction() == MotionEvent.ACTION_UP) {

					if (currentPage != totalPage) {
						System.out.println("currentPage当前野蛮---" + currentPage
								+ "totalPage" + totalPage);

						layout.setVisibility(View.VISIBLE);
						currentPage++;
						xutilRequest(currentPage, goodsId);

						System.out
						.println(" ---  view.getScrollY()=" + scrollY);
					} else {
						layout.setVisibility(View.VISIBLE);
						pbTv.setText("没有更多内容了");
						pb.setVisibility(View.GONE);

						System.out.println(" ---  view" + " 没有更多了");

					}
				}

				return false;
			}
		});
	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	//
	// // 加载更多
	// @Override
	// public void onLoad() {
	// // TODO Auto-generated method stub
	// Handler handler = new Handler();
	// handler.postDelayed(new Runnable() {
	//
	// @Override
	// public void run() {
	// // TODO Auto-generated method stub
	// // 获取更多数据
	// xutilRequest(++currentPage,goodsId);
	//
	// System.out.println("当前页数" + currentPage);
	//
	// // listview.loadComplete();
	// }
	// }, 1000);
	//
	// }

	public String format(String str) {

		// 将时间戳转为字符串

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		// 例如：cc_time=1291778220
		long lcc_time = Long.valueOf(str);
		String time = sdf.format(new Date(lcc_time));
		return time;

	}

}
