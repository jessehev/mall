package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.huayi.mall.GoodsDataActivity;
import com.huayi.mall.R;
import com.huayi.mall.adapter.FiveShopAdapter;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.Goods;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.common.Shops;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.MyGridview;
import com.huayi.mall.view.RatioImageView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FiveShopActivity extends Activity implements
		SwipeRefreshLayout.OnRefreshListener {

	private MyGridview mGridView;

	private List<Goods> arrayList = new ArrayList<Goods>();
	private FiveShopAdapter adapter;

	private ImageView back;

	private TextView tv_title_logo;
	private SwipeRefreshLayout sf;

	RatioImageView mRatioImageView;

	private Shops shop;
	private ScrollView mScrollView;
	private LinearLayout load_layout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fiveshop);
		findViewById();
		initView();

		onClick();
		Intent intent = getIntent();
		shop = (Shops) intent.getParcelableExtra("whichshop");
		/**
		 * id:-----》1 澳洲馆 2港台馆 3韩国馆 4美国馆 5日本馆
		 */
		if (shop != null) {

			if (shop.id.equals("1")) {

				xutilRequest(shop.id);

				tv_title_logo.setText(shop.name);
			} else if (shop.id.equals("2")) {

				xutilRequest(shop.id);

				tv_title_logo.setText(shop.name);

			} else if (shop.id.equals("5")) {
				xutilRequest(shop.id);

				tv_title_logo.setText(shop.name);
			} else if (shop.id.equals("3")) {

				xutilRequest(shop.id);
				tv_title_logo.setText(shop.name);
			} else if (shop.id.equals("4")) {
				xutilRequest(shop.id);

				tv_title_logo.setText(shop.name);
			}
		}

	}

	public void initView() {
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);

		
		mScrollView.smoothScrollTo(0, 0);// 置顶
		adapter = new FiveShopAdapter(this, arrayList);
		mGridView.setAdapter(adapter);
		// 下拉刷新
		sf.setOnRefreshListener(this);
		sf.setColorScheme(android.R.color.holo_orange_light,
				android.R.color.holo_orange_dark,
				android.R.color.holo_red_light, android.R.color.holo_red_dark);

		load_layout.setVisibility(View.GONE);
		// mScrollView.setOnTouchListener(new OnTouchListener() {
		//
		// @Override
		// public boolean onTouch(View v, MotionEvent event) {
		// // TODO Auto-generated method stub
		// if (event.getAction() == MotionEvent.ACTION_UP) {
		//
		// View view = ((ScrollView) v).getChildAt(0);
		// if (view.getMeasuredHeight() <= v.getScaleY()
		// + v.getHeight()) {
		//
		// // 加载更多数据
		// load_layout.setVisibility(View.VISIBLE);
		// }
		// }
		//
		// return false;
		// }
		// });

		mScrollView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {

				int scrollY = view.getScrollY();
				int height = view.getHeight();
				int scrollViewMeasuredHeight = mScrollView.getChildAt(0)
						.getMeasuredHeight();
				if (scrollY == 0) {
					System.out.println("       ˶    view.getScrollY()="
							+ scrollY);
				}
				if ((scrollY + height) == scrollViewMeasuredHeight) {
					// if ((currentPage) < totalPage) {
					// xutilRequest(++currentPage);
					// System.out.println("=================");
					// } else {
					// Toast.makeText(HdActivity.this, "没有更多了...", 0).show();
					// }
					if (event.getAction() == MotionEvent.ACTION_UP) {
						// load_layout.setVisibility(View.VISIBLE);

					}

				}

				return false;
			}
		});

	}

	private void findViewById() {
		load_layout = (LinearLayout) findViewById(R.id.load_layout);
		mScrollView = (ScrollView) findViewById(R.id.ScrollView_fiveshop);
		mRatioImageView = (RatioImageView) findViewById(R.id.Ratioimg_fiveshop);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		mGridView = (MyGridview) findViewById(R.id.gridView_fiveshop);
		sf = (SwipeRefreshLayout) findViewById(R.id.srf_fiveshop);
		back.setVisibility(View.VISIBLE);

	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				FiveShopActivity.this.finish();
			}
		});

		mGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(FiveShopActivity.this,
						GoodsDataActivity.class);

				intent.putExtra("id", arrayList.get(arg2).id + "");
				startActivity(intent);
			}
		});

	}

	/**
	 * id:-----》1 澳洲馆 2港台馆 3韩国馆 4美国馆 5日本馆
	 */
	// 设置数据格式
	public void setData() {

		JSONObject jsonObj = MyApplication.aCache.getAsJSONObject("fiveshop");
		if (jsonObj != null) {
			String image = jsonObj.optString("image");
			String smallImage = jsonObj.optString("smallImage");
			arrayList.clear();

			try {
				JSONArray jsonArray = jsonObj.optJSONArray("goodsList");

				System.out.println("jsonArray----------" + jsonArray);
				if (jsonArray != null) {
					// 解析商品数据
					for (int i = 0; i < jsonArray.length(); i++) {
						Goods goods = new Goods();

						JSONObject goodsobj = jsonArray.optJSONObject(i);
						goods.picture = goodsobj.optString("goodsPicture")
								.split(",")[0];

						goods.id = goodsobj.optString("id");
						goods.name = goodsobj.optString("goodsName");
						goods.price = goodsobj.optString("price");
						goods.marketPrice = goodsobj.optString("marketPrice");

						arrayList.add(goods);
					}
					adapter.setData(arrayList);
				}

			} catch (Exception e) {

			}

			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost + image, mRatioImageView,
					ImageLoaderCfg.options);
		}
		sf.setRefreshing(false);
	}

	// 查询
	private void xutilRequest(String id) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("id", id);
		httpUtils.send(HttpMethod.POST, GlobalUrl.findGoodsByShop, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
						setData();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						dialog.dismiss();
						try {
							JSONObject obj = new JSONObject(arg0.result);

							System.out.println("objfiveshop--------" + obj);

							MyApplication.aCache.put("fiveshop", obj);

							setData();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {
						dialog = new CustomProgressDialog(
								FiveShopActivity.this, "获取中。。。", R.anim.frame2);
						dialog.show();
					}
				});
	}

	/**
	 * 下拉刷新
	 */
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		xutilRequest(shop.id);
	}

}
