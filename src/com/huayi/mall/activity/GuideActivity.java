package com.huayi.mall.activity;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;

import com.huayi.mall.R;

public class GuideActivity extends Activity{

	private ViewPager vp_guide;
	private List<View>list = new ArrayList<View>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guide);
		vp_guide = (ViewPager) findViewById(R.id.vp_guide);
		View view = View.inflate(this, R.layout.guide_pic_item, null);
		View view1 = View.inflate(this, R.layout.guide_pic_item2, null);
		list.add(view);
		list.add(view1);
		vp_guide.setAdapter(new PagerAdapter() {
			//viewpager中的组件数量
			@Override
			public int getCount() {
				return list.size();
			}
			//滑动切换的时候销毁当前的组件
			@Override
			public void destroyItem(ViewGroup container, int position,
					Object object) {
				((ViewPager) container).removeView(list.get(position));
			}
			//每次滑动的时候生成的组件
			@Override
			public Object instantiateItem(ViewGroup container, int position) {
				((ViewPager) container).addView(list.get(position));
				return list.get(position);
			}

			@Override
			public boolean isViewFromObject(View arg0, Object arg1) {
				return arg0 == arg1;
			}

			@Override
			public int getItemPosition(Object object) {
				return super.getItemPosition(object);
			}


		});
		view1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent(GuideActivity.this,LoginActivity.class));
				GuideActivity.this.finish();
			}
		})	;
	}
}
