package com.huayi.mall.activity;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.base.OrderBeanto;
import com.huayi.mall.base.OrderBeanto.OrderListBean;
import com.huayi.mall.base.OrderBeanto.OrderListBean.OrderItemListBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.ImageViewSubClass;
import com.huayi.mall.view.MyListView;
import com.huayi.mall.view.RatioImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class TuiHuoActivity extends Activity {
	private MyListView listView_confirm;
	private TextView tv_title_logo;
	private ImageView back;
	private OrderBeanto.OrderListBean bean;
	private TextView phone_confirm;
	private TextView address_confirm;
	private TextView totmoney;
	private TextView size;
	private TextView ordernumber;
	private TextView time;
	private RatioImageView titleimage;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tuihuo);
		titleimage = (RatioImageView) findViewById(R.id.titleimage);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		back = (ImageView) findViewById(R.id.back);
		phone_confirm = (TextView) findViewById(R.id.phone_confirm);
		address_confirm = (TextView) findViewById(R.id.address_confirm);
		totmoney = (TextView) findViewById(R.id.totalmoney);
		size = (TextView) findViewById(R.id.size);
		ordernumber = (TextView) findViewById(R.id.ordernumber);
		time = (TextView) findViewById(R.id.time);
		listView_confirm = (MyListView) findViewById(R.id.listView_confirm);
		
		
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		init();
		onclick();
	}
	private void onclick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				TuiHuoActivity.this.finish();
			}
		});
	
	}
	private void init() {
		// TODO Auto-generated method stub
		
		Intent intent = getIntent();
		
		bean=(OrderListBean) intent.getSerializableExtra("orderbean");
		phone_confirm.setText(bean.getReceiver());
		address_confirm.setText(" "+bean.getReceiveAddress()+"");
		BigDecimal bigDecimal = new BigDecimal(bean.getTotalMoney());
		BigDecimal bigDecimal2 = new BigDecimal(bean.getPostage());
		BigDecimal add = bigDecimal.add(bigDecimal2);
		totmoney.setText("¥"+add);
		size.setText(""+bean.getOrderItemList().size());
		System.out.println("asdasdasd"+bean.getOrderNumber());
		ordernumber.setText(""+bean.getOrderNumber());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String format = dateFormat.format(bean.getCreateTime());
		time.setText(format);
		listView_confirm.setAdapter(new InorderAdapter(bean.getOrderItemList()));
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("订单详细");
		switch (bean.getOrderStatus()) {
		case 3:
			titleimage.setImageResource(R.drawable.yfk);
			break;
		case 4:
			titleimage.setImageResource(R.drawable.yfh);
			break;
		case 5:
			titleimage.setImageResource(R.drawable.dpj);
			break;
		case 6:
			titleimage.setImageResource(R.drawable.ywc);
			break;

		default:
			break;
		}
		
		
	}
	class InorderAdapter extends BaseAdapter{
		public List<OrderBeanto.OrderListBean.OrderItemListBean> list;
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return bean.getOrderItemList().size();
		}

		public InorderAdapter(List<OrderItemListBean> list) {
			super();
			this.list = list;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(TuiHuoActivity.this, R.layout.inorder_item, null);
			ImageViewSubClass image= (ImageViewSubClass) arg1.findViewById(R.id.orderimage);
			TextView ordername = (TextView) arg1.findViewById(R.id.ordername);
			TextView orderprice = (TextView) arg1.findViewById(R.id.orderprice);
			TextView options = (TextView) arg1.findViewById(R.id.options);
			TextView tuikuan = (TextView) arg1.findViewById(R.id.tuikuan);
			LinearLayout lin_tui = (LinearLayout) arg1.findViewById(R.id.lin_tui);
			lin_tui.setVisibility(View.VISIBLE);
			options.setText(list.get(arg0).getProduct().getDescription() );
			String [] s = list.get(arg0).getProduct().getProductName().split(" ");
			ordername.setText(s[0]);
			orderprice.setText("¥ "+list.get(arg0).getProduct().getActualPrice());
			//图片地址

			String []pic=list.get(arg0).getProduct().getProductPicture().split(",");
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost1
					+ pic[0],
					image, ImageLoaderCfg.options2);
			if (list.get(arg0).getServiceType()!=0) {
				tuikuan.setText("处理中");
				tuikuan.setEnabled(false);
			}
			tuikuan.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg5) {
					// TODO Auto-generated method stub
					int id = list.get(arg0).getId();
					Intent intent = new Intent(TuiHuoActivity.this,TuikuanDataActivity.class);
					intent.putExtra("id", id+"");
					intent.putExtra("itemstatus", list.get(arg0).getServiceType());
					intent.putExtra("orderid", bean.getId()+"");
					intent.putExtra("status", bean.getOrderStatus());
					startActivityForResult(intent, 1);
				}
			});
			return arg1;
		}}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode==1&resultCode==1) {
			TuiHuoActivity.this.finish();
		}
	}
}
