package com.huayi.mall.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.IsNetWorkConnected;
import com.huayi.mall.util.IsPhoneNumber;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.util.TimeButton;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class RegisterActivity extends Activity {

	private ImageView back;

	private EditText phone_et, pwd_et, code_et, name_et;

	private TextView register_tv;

	private LinearLayout look_layout;

	private TimeButton sendCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		
		
		findViewById();
		initView();
		// initData();
		onClick();

	}

	public void initData() {

	}

	public void findViewById() {

		back = (ImageView) findViewById(R.id.back_register);
		phone_et = (EditText) findViewById(R.id.et_phone_register);
		code_et = (EditText) findViewById(R.id.et_code_register);
		name_et = (EditText) findViewById(R.id.et_name_register);
		pwd_et = (EditText) findViewById(R.id.et_pwd_register);
		sendCode = (TimeButton) findViewById(R.id.btn_sendcode_register);
		register_tv = (TextView) findViewById(R.id.bt_register);
		look_layout = (LinearLayout) findViewById(R.id.layout_look_register);

	}

	private void initView() {
		// TODO Auto-generated method stub
		
		SystemBar.initSystemBar(this,"#EC3F6C");
		SystemBar.setTranslucentStatus(this, true);
		phone_et.setHintTextColor(Color.parseColor("#ffffff"));
		code_et.setHintTextColor(Color.parseColor("#ffffff"));
		name_et.setHintTextColor(Color.parseColor("#ffffff"));
		pwd_et.setHintTextColor(Color.parseColor("#ffffff"));

		sendCode.setTextAfter("秒后重新获取").setTextBefore("发送验证码")
				.setLenght(60 * 1000);
	}

	private void xutilRequest(String phone, String code, String nickName,
			String pwd) {
		HttpUtils httpUtils = new HttpUtils(10000);
		RequestParams params = new RequestParams();
		params.addBodyParameter("phone", phone);
		params.addBodyParameter("password", pwd);
		params.addBodyParameter("code", code);
		params.addBodyParameter("nickName", nickName);
		httpUtils.send(HttpMethod.POST, GlobalUrl.register, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						dialog.dismiss();
						try {
							JSONObject obj = new JSONObject(arg0.result);
							if (obj.optBoolean("success")) { // 注册成功
								System.out.println("obj---" + obj.toString());
								SpfUtil.setId(obj.optLong("id"));
								finish();
							}
							String message = obj.getString("message");
							System.out.println("message-----------" + message);
							Toast.makeText(RegisterActivity.this, message, 1)
									.show();

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onStart() {
						dialog = new CustomProgressDialog(
								RegisterActivity.this, "正在注册中", R.anim.frame2);

						dialog.show();
					}
				});
	}

	/**
	 * 短信验证
	 * 
	 * @param currentPage
	 * @param key
	 */
	private void xutilSendCode(String phone) {
		HttpUtils httpUtils = new HttpUtils();

		RequestParams params = new RequestParams();
		params.addBodyParameter("phone", phone);

		httpUtils.send(HttpMethod.POST, GlobalUrl.sendCode, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							JSONObject obj = new JSONObject(arg0.result);

							if (obj.optBoolean("success")) {

								Toast.makeText(RegisterActivity.this,
										obj.optString("message"), 1).show();
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onStart() {
					}
				});
	}

	private void onClick() {

		// 随便看看

		look_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent(RegisterActivity.this,
						MainActivity.class));
				finish();
			}
		});
		// 注册
		register_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String phone = phone_et.getText().toString();
				String pwd = pwd_et.getText().toString();
				String nickName = name_et.getText().toString();
				String code = code_et.getText().toString();
				if (TextUtils.isEmpty(phone) | phone.length()!=11) {
					Toast.makeText(RegisterActivity.this, "请输入正确的手机号", 1)
							.show();
				} else if (TextUtils.isEmpty(nickName)) {
					Toast.makeText(RegisterActivity.this, "请输入昵称", 1).show();
				} else if (TextUtils.isEmpty(pwd) | pwd.length() < 6) {
					Toast.makeText(RegisterActivity.this, "请输入不少于6位的密码", 1)
							.show();
				} else {

					xutilRequest(phone, code, nickName, pwd);
				}

			}
		});
		// 发送验证码
		sendCode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String phone = phone_et.getText().toString();
				if (IsNetWorkConnected
						.isNetworkConnected(RegisterActivity.this)) {
					if (!IsPhoneNumber.isPhone(phone)) {
						Toast.makeText(getApplicationContext(), "请输入正确手机号码", 0)
								.show();
						TimeButton.I = 1;
					} else {
						TimeButton.I = 2;
						System.out.println("----------------ti me");

						xutilSendCode(phone);
					}
				} else {
					Toast.makeText(RegisterActivity.this, "请检查网络连接", 1).show();
				}

			}
		});
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String phone = phone_et.getText().toString();
				String pwd = pwd_et.getText().toString();

				RegisterActivity.this.finish();
			}
		});
	}
}
