package com.huayi.mall.activity;

import java.math.BigDecimal;

import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class PayActivity extends Activity {

	public ImageView alipay, wechat, back, wallet;
	public Button bt_pay;
	public TextView tv_title, payMoney, goodsNum;
	private String goodsPrice;
	private String goodsNumStr;
	String orderId;

	private String balanceStr;

	private int payType = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pay);
		findViewById();
		onclick();
		initView();
		initData();

	}

	public void initData() {
		xutilRequestBalance();
	}

	public void initView() {
		back.setVisibility(View.VISIBLE);
		tv_title.setText("支付");
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);

		goodsPrice = getIntent().getStringExtra("goodsPrice");
		goodsNumStr = getIntent().getStringExtra("goodsNum");
		orderId = getIntent().getStringExtra("orderId");

		goodsNum.setText("合计（共" + goodsNumStr + "件商品）");
		payMoney.setText(goodsPrice);

	}

	public void findViewById() {
		wallet = (ImageView) findViewById(R.id.wallet_pay);
		alipay = (ImageView) findViewById(R.id.alipay_pay);
		wechat = (ImageView) findViewById(R.id.wechat_pay);
		bt_pay = (Button) findViewById(R.id.rechargebt_pay);
		back = (ImageView) findViewById(R.id.back);
		tv_title = (TextView) findViewById(R.id.tv_title_logo);
		goodsNum = (TextView) findViewById(R.id.num_pay);
		payMoney = (TextView) findViewById(R.id.payMoney_pay);
	}

	private void onclick() {
		// TODO Auto-generated method stub
		wallet.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				wallet.setImageResource(R.drawable.check);
				alipay.setImageResource(R.drawable.nocheck);
				wechat.setImageResource(R.drawable.nocheck);
				payType = 1;
			}
		});
		alipay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				wallet.setImageResource(R.drawable.nocheck);
				alipay.setImageResource(R.drawable.check);
				wechat.setImageResource(R.drawable.nocheck);
				payType = 2;
			}
		});
		wechat.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				wallet.setImageResource(R.drawable.nocheck);
				alipay.setImageResource(R.drawable.nocheck);
				wechat.setImageResource(R.drawable.check);
				payType = 3;
			}
		});
		bt_pay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// Toast.makeText(RechargeActivity.this,
				// "充值" + rechargemoney.getText().toString(), 0).show();

				if (goodsPrice != null) {

					// 商品价格小于 钱包余额
					if (new BigDecimal(goodsPrice).compareTo(new BigDecimal(
							balanceStr)) != 1) {

						System.out.println("--goodsPrice-----------"
								+ goodsPrice);

						switch (payType) {
						case 1:
							xutilRequestPay(goodsPrice, orderId);
							break;
						case 2:

							break;
						case 3:

							break;

						default:
							break;
						}

					} else {
						Toast.makeText(PayActivity.this, "钱包余额不足", 1).show();
					}
				}
			}
		});
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

	// 查询 钱包余额
	private void xutilRequestBalance() {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("id", SpfUtil.getId() + "");

		httpUtils.send(HttpMethod.POST, GlobalUrl.walletBalance, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							String result = arg0.result;

							JSONObject jsonObj = new JSONObject(result);

							balanceStr = jsonObj.optString("balance");

//							balance.setText(balanceStr);
							// balance.setText("合计（共"+balanceStr+"件商品）");
							System.out.println("wallet-----------s" + result);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {

					}
				});

	}

	// 钱包支付
	private void xutilRequestPay(String money, String orderId) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();

		params.addBodyParameter("money", money);
		params.addBodyParameter("orderId", orderId);
		
		System.out.println("orderId"+orderId);
		httpUtils.send(HttpMethod.POST, GlobalUrl.walletPay, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							String result = arg0.result;

							JSONObject jsonObj = new JSONObject(result);

							if (jsonObj.optBoolean("success")) {
								Toast.makeText(PayActivity.this,
										jsonObj.optString("message"), 1).show();
							}

							System.out.println("wallet-----------s" + result);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {
						
					}
				});

	}

}
