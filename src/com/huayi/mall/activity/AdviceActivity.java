package com.huayi.mall.activity;

import java.io.File;

import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class AdviceActivity extends Activity {

	private ImageView back;

	private EditText content_et;

	private TextView tv_title_logo, submit_tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_advice);

		findViewById();
		initView();
		initData();
		onClick();
		
		

	}

	public void initData() {
		// xutilRequest();
	}

	public void findViewById() {
		content_et = (EditText) findViewById(R.id.content_et_advice);
		submit_tv = (TextView) findViewById(R.id.submit_advice);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		back = (ImageView) findViewById(R.id.back);

	}

	private void initView() {
		// TODO Auto-generated method stub
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);

		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("帮助反馈");

	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
				AdviceActivity.this.finish();
			}
		});

		submit_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!TextUtils.isEmpty(content_et.getText().toString())) {
					xutilRequest(content_et.getText().toString());
				} else {
					Toast.makeText(AdviceActivity.this, "请先输入反馈内容", 1).show();
				}

			}
		});

	}

	// 帮助反馈
	private void xutilRequest(String feeback) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("feedback", feeback);
		httpUtils.send(HttpMethod.POST, GlobalUrl.feedback, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						dialog.dismiss();
						try {
							System.out.println("obj-----------" + arg0.result);

							JSONObject jsonObj = new JSONObject(arg0.result);

							boolean success = jsonObj.optBoolean("success");
							if (success) {
								Toast.makeText(AdviceActivity.this,
										jsonObj.optString("message"), 1).show();
								finish();
							}

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {
						dialog = new CustomProgressDialog(AdviceActivity.this,
								"提交中。。。", R.anim.frame2);
						dialog.show();
					}
				});
	}

}
