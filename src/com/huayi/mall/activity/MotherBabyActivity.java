package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.huayi.mall.GoodsDataActivity;
import com.huayi.mall.R;
import com.huayi.mall.adapter.MotherBabyAdapter;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.common.SpecialBean;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.MyGridview;
import com.huayi.mall.view.RatioImageView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MotherBabyActivity extends Activity implements
		SwipeRefreshLayout.OnRefreshListener {

	MyGridview gridView;

	private List<SpecialBean> arrayList = new ArrayList<SpecialBean>();

	private MotherBabyAdapter adapter;

	private ImageView back;

	private RatioImageView mRatioImageView;

	private TextView tv_title_logo;

	private SwipeRefreshLayout spf;

	private ScrollView mScrollview;

	private LinearLayout load_layout;

	private TextView show_mother;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_motherbaby);

		findViewById();
		initView();
		initData();
		onClick();

		Intent intent = getIntent();
		String whichSpecial = intent.getStringExtra("whichSpecial");
		// 1、2两个专场 3、4、5国内三个banner 6、7、8 国外三个banner
		if (whichSpecial != null) {
			if (whichSpecial.equals("1")) {
				xutilRequestSpecial("1", 1);
			} else if (whichSpecial.equals("2")) {
				xutilRequestSpecial("2", 1);
			} else if (whichSpecial.equals("3")) {
				xutilRequestSpecial("3", 1);
			} else if (whichSpecial.equals("4")) {
				xutilRequestSpecial("4", 1);
			} else if (whichSpecial.equals("5")) {
				xutilRequestSpecial("5", 1);	
			} else if (whichSpecial.equals("6")) {
				xutilRequestSpecial("6", 1);
			} else if (whichSpecial.equals("7")) {
				xutilRequestSpecial("7", 1);
			} else if (whichSpecial.equals("8")) {
				xutilRequestSpecial("8", 1);
			}
		}

	}

	public void setGoods() {
		JSONObject jsonObj = MyApplication.aCache.getAsJSONObject("goodslist");
		try {
			if (jsonObj != null) {

				String marketName = jsonObj.optString("marketName");
				String fullPrice = jsonObj.optString("fullPrice");
				String cutPrice = jsonObj.optString("cutPrice");
				String bigpicture = jsonObj.optString("bigPicture");
				tv_title_logo.setText(marketName);
				JSONObject goodsList = jsonObj.optJSONObject("goodsList");
				// 分页的数据

				show_mother.setText("全场满" + fullPrice + "送" + cutPrice + "");
				ImageLoader.getInstance().displayImage(
						GlobalUrl.serviceHost + bigpicture, mRatioImageView,
						ImageLoaderCfg.options);

				int currentPage = goodsList.optInt("currentPage");
				int totalCount = goodsList.optInt("totalCount"); // 总页码

				JSONArray rows = goodsList.optJSONArray("rows");

				for (int i = 0; i < rows.length(); i++) {
					JSONObject obj = (JSONObject) rows.get(i);
					SpecialBean sb = new SpecialBean();

					sb.marketPrice = obj.optString("marketPrice");
					sb.goodsName = obj.optString("goodsName");
					sb.picture = obj.optString("goodsPicture").split(",")[0];
					sb.id = obj.optString("id");
					arrayList.add(sb);
				}
				adapter.setData(arrayList);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void xutilRequestSpecial(String id, int currentPage) {
		HttpUtils httpUtils = new HttpUtils();

		RequestParams params = new RequestParams();
		params.addBodyParameter("currentPage", currentPage + "");
		params.addBodyParameter("id", id);

		httpUtils.send(HttpMethod.POST, GlobalUrl.specialGoodsList, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						setGoods();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							String result = arg0.result;

							System.out.println("vvvvvvvvv" + result);
							JSONObject jsonObj = new JSONObject(arg0.result);
							MyApplication.aCache.put("goodslist", jsonObj);

							setGoods();
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

					@Override
					public void onStart() {

					}
				});
	}

	public void initData() {

	}

	public void findViewById() {

		show_mother = (TextView) findViewById(R.id.show_mother);
		load_layout = (LinearLayout) findViewById(R.id.load_layout);
		mScrollview = (ScrollView) findViewById(R.id.myscrollview_motherbaby);
		mRatioImageView = (RatioImageView) findViewById(R.id.picture_mother);
		spf = (SwipeRefreshLayout) findViewById(R.id.sf_motherbaby);
		gridView = (MyGridview) findViewById(R.id.gridView_motherbaby);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		back = (ImageView) findViewById(R.id.back);

	}

	private void initView() {
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);


		
		adapter = new MotherBabyAdapter(this, arrayList);
		gridView.setAdapter(adapter);
		back.setVisibility(View.VISIBLE);
		mScrollview.smoothScrollBy(0, 0);
		// 下拉刷新
		spf.setOnRefreshListener(this);
		spf.setColorScheme(android.R.color.holo_orange_light,
				android.R.color.holo_orange_dark,
				android.R.color.holo_red_light, android.R.color.holo_red_dark);

		load_layout.setVisibility(View.GONE);
		mScrollview.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				if (event.getAction() == MotionEvent.ACTION_UP) {
					View view = ((ScrollView) v).getChildAt(0);
					if (view.getMeasuredHeight() <= v.getScrollY()
							+ v.getHeight()) {
						// 加载数据代码

						load_layout.setVisibility(View.VISIBLE);

						System.out.println("ccc" + view.getMeasuredHeight());

					}
				}
				return false;
			}
		});

	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				MotherBabyActivity.this.finish();
			}
		});

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MotherBabyActivity.this,
						GoodsDataActivity.class);

				intent.putExtra("id", arrayList.get(arg2).id + "");
				startActivity(intent);
			}
		});

	}

	// 下拉刷新
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {
			public void run() {
				spf.setRefreshing(false);

			}
		}, 1000);
	}

}
