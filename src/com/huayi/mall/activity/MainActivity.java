package com.huayi.mall.activity;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.huayi.mall.R;
import com.huayi.mall.R.layout;
import com.huayi.mall.adapter.MainPagerAdapter;
import com.huayi.mall.base.AddressBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.util.SystemBarTintManager;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.NoScrollViewPager;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.huayi.mall.R;
import com.huayi.mall.adapter.MainPagerAdapter;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.NoScrollViewPager;

public class MainActivity extends FragmentActivity {

	private NoScrollViewPager mPager;
	private ImageView homeiage;
	private TextView homemtext;
	private ImageView shopcarimage;
	private TextView shopchartext;
	private ImageView classfiyimage;
	private TextView classfiytext;
	private ImageView messageimage;
	private TextView messagetext;
	private ImageView myimage;
	private TextView mytext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mPager = (NoScrollViewPager) findViewById(R.id.viewpager);
		mPager.setAdapter(new MainPagerAdapter(getSupportFragmentManager()));
		mPager.setOffscreenPageLimit(4);
		homeiage = (ImageView) findViewById(R.id.homeimge);
		homemtext = (TextView) findViewById(R.id.hometext);// 首页
		homeiage.setImageResource(R.drawable.homered);
		homemtext.setTextColor(Color.parseColor("#e40d3f"));
		shopcarimage = (ImageView) findViewById(R.id.shopcacrimage);
		shopchartext = (TextView) findViewById(R.id.shopcactext);// 购物车
		classfiyimage = (ImageView) findViewById(R.id.classfiyimage);
		classfiytext = (TextView) findViewById(R.id.classfiytext);// 分类
		messageimage = (ImageView) findViewById(R.id.messageimage);
		messagetext = (TextView) findViewById(R.id.messagetext);// 消息
		myimage = (ImageView) findViewById(R.id.myimage);
		mytext = (TextView) findViewById(R.id.myitext);// 消息


		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);

	//	registerReceiver(new Receiver(), new IntentFilter("com.huayi"));

	}

	public class Receiver extends BroadcastReceiver {
		public void onReceive(Context context, Intent intent) {

			finish();

		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (SpfUtil.getId()!=0) {
			JPushInterface.setAlias(MainActivity.this, SpfUtil.getId()+"", new TagAliasCallback() {

				@Override
				public void gotResult(int arg0, String arg1, Set<String> arg2) {
					// TODO Auto-generated method stub
					System.out.println("别名设置状态提示码==="+arg0);
				}
			});
		}else {

			JPushInterface.setAlias(MainActivity.this, 0+"", new TagAliasCallback() {

				@Override
				public void gotResult(int arg0, String arg1, Set<String> arg2) {
					// TODO Auto-generated method stub
					System.out.println("别名设置状态提示码==="+arg0);
				}
			});
		
		}
		
		
		// if (!SpfUtil.getLoginStatus()) {
		// mPager.setCurrentItem(0, false);// 去掉滑动动画效果
		// System.out.println("==============点击了第一个===========");
		// homeiage.setImageResource(R.drawable.homered);
		// homemtext.setTextColor(Color.parseColor("#e40d3f"));
		// shopcarimage.setImageResource(R.drawable.shopcar);
		// shopchartext.setTextColor(Color.parseColor("#666666"));
		//
		// classfiyimage.setImageResource(R.drawable.classfiy);
		// classfiytext.setTextColor(Color.parseColor("#666666"));
		//
		// messageimage.setImageResource(R.drawable.message);
		// messagetext.setTextColor(Color.parseColor("#666666"));
		//
		// myimage.setImageResource(R.drawable.my);
		// mytext.setTextColor(Color.parseColor("#666666"));
		// }

	}

	public void onClick(View view) {

		switch (view.getId()) {
		case R.id.home:
			mPager.setCurrentItem(0, false);// 去掉滑动动画效果
			System.out.println("==============点击了第一个===========");
			homeiage.setImageResource(R.drawable.homered);
			homemtext.setTextColor(Color.parseColor("#e40d3f"));
			shopcarimage.setImageResource(R.drawable.shopcar);
			shopchartext.setTextColor(Color.parseColor("#666666"));

			classfiyimage.setImageResource(R.drawable.classfiy);
			classfiytext.setTextColor(Color.parseColor("#666666"));

			messageimage.setImageResource(R.drawable.message);
			messagetext.setTextColor(Color.parseColor("#666666"));

			myimage.setImageResource(R.drawable.my);
			mytext.setTextColor(Color.parseColor("#666666"));
			break;
		case R.id.shopcar:
			if (SpfUtil.getLoginStatus()) {
				mPager.setCurrentItem(1, false);// 去掉滑动动画效果
				homeiage.setImageResource(R.drawable.homebt);
				homemtext.setTextColor(Color.parseColor("#666666"));
				shopcarimage.setImageResource(R.drawable.shopcarred);
				shopchartext.setTextColor(Color.parseColor("#e40d3f"));

				classfiyimage.setImageResource(R.drawable.classfiy);
				classfiytext.setTextColor(Color.parseColor("#666666"));

				messageimage.setImageResource(R.drawable.message);
				messagetext.setTextColor(Color.parseColor("#666666"));

				myimage.setImageResource(R.drawable.my);
				mytext.setTextColor(Color.parseColor("#666666"));
			} else {
				startActivity(new Intent(MainActivity.this, LoginActivity.class));
			}
			break;
		case R.id.classfiy:
			mPager.setCurrentItem(2, false);// 去掉滑动动画效果

			homeiage.setImageResource(R.drawable.homebt);
			homemtext.setTextColor(Color.parseColor("#666666"));
			shopcarimage.setImageResource(R.drawable.shopcar);
			shopchartext.setTextColor(Color.parseColor("#666666"));

			classfiyimage.setImageResource(R.drawable.classfiyred);
			classfiytext.setTextColor(Color.parseColor("#e40d3f"));

			messageimage.setImageResource(R.drawable.message);
			messagetext.setTextColor(Color.parseColor("#666666"));

			myimage.setImageResource(R.drawable.my);
			mytext.setTextColor(Color.parseColor("#666666"));
			break;
		case R.id.message:
			mPager.setCurrentItem(3, false);// 去掉滑动动画效果
			homeiage.setImageResource(R.drawable.homebt);
			homemtext.setTextColor(Color.parseColor("#666666"));
			shopcarimage.setImageResource(R.drawable.shopcar);
			shopchartext.setTextColor(Color.parseColor("#666666"));

			classfiyimage.setImageResource(R.drawable.classfiy);
			classfiytext.setTextColor(Color.parseColor("#666666"));

			messageimage.setImageResource(R.drawable.messagered);
			messagetext.setTextColor(Color.parseColor("#e40d3f"));

			myimage.setImageResource(R.drawable.my);
			mytext.setTextColor(Color.parseColor("#666666"));
			break;
		case R.id.my:
			mPager.setCurrentItem(4, false);// 去掉滑动动画效果

			homeiage.setImageResource(R.drawable.homebt);
			homemtext.setTextColor(Color.parseColor("#666666"));
			shopcarimage.setImageResource(R.drawable.shopcar);
			shopchartext.setTextColor(Color.parseColor("#666666"));

			classfiyimage.setImageResource(R.drawable.classfiy);
			classfiytext.setTextColor(Color.parseColor("#666666"));

			messageimage.setImageResource(R.drawable.message);
			messagetext.setTextColor(Color.parseColor("#666666"));

			myimage.setImageResource(R.drawable.myred);
			mytext.setTextColor(Color.parseColor("#e40d3f"));
			break;

		default:
			break;
		}

	}

}
