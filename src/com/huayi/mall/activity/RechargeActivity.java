package com.huayi.mall.activity;

import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.R;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class RechargeActivity extends Activity {

	private ImageView alipay;
	private ImageView wechat;
	private EditText rechargemoney;
	private Button bt_rechare;
	private ImageView back;
	private TextView tv_title_logo;
	private TextView totlmoney;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recharge);
		alipay = (ImageView) findViewById(R.id.alipay);
		wechat = (ImageView) findViewById(R.id.wechat);
		rechargemoney = (EditText) findViewById(R.id.rechargemonry);
		bt_rechare = (Button) findViewById(R.id.rechargebt);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		totlmoney = (TextView) findViewById(R.id.totlmoney);
		
		
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);

		initdata();
		onclick();
	}
	private void initdata() {
		// TODO Auto-generated method stub
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText("我的余额");
		xutilRequestBalance();	
	}
	private void onclick() {
		// TODO Auto-generated method stub
		alipay.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				alipay.setImageResource(R.drawable.check);
				wechat.setImageResource(R.drawable.nocheck);
			}
		});
		wechat.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				alipay.setImageResource(R.drawable.nocheck);
				wechat.setImageResource(R.drawable.check);
			}
		});
		bt_rechare.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(RechargeActivity.this, "充值"+rechargemoney.getText().toString(), 0).show();
			}
		});
		back.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				RechargeActivity.this.finish();
			}
		});
	}
	// 查询 钱包余额
		private void xutilRequestBalance() {
			HttpUtils httpUtils = new HttpUtils();
			RequestParams params = new RequestParams();
			params.addBodyParameter("id", SpfUtil.getId() + "");

			httpUtils.send(HttpMethod.POST, GlobalUrl.walletBalance, params,
					new RequestCallBack<String>() {

						private CustomProgressDialog dialog;

						@Override
						public void onFailure(HttpException arg0, String arg1) {
						}

						@Override
						public void onSuccess(ResponseInfo<String> arg0) {
							try {
								String result = arg0.result;

								JSONObject jsonObj = new JSONObject(result);

								totlmoney.setText(jsonObj.optString("balance"));

//								balance.setText(balanceStr);
								// balance.setText("合计（共"+balanceStr+"件商品）");
								System.out.println("wallet-----------s" + result);

							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						@Override
						public void onStart() {

						}
					});

		}

}
