package com.huayi.mall.activity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.huayi.mall.GoodsListActivity;
import com.huayi.mall.R;
import com.huayi.mall.util.ACache;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.MyListView;

public class SearchActivity extends Activity {
	private MyListView searchlist;
	private List<String> list = new ArrayList<String>();
	private SrarchAdapter adapter = new SrarchAdapter();
	private TextView cancel;
	private ImageView imagsearch;
	private ACache aCache;
	private EditText et_search;
	private TextView qinkong;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sraechfragmen);
		searchlist = (MyListView) findViewById(R.id.searchlist);
		cancel = (TextView) findViewById(R.id.cancel);
		imagsearch = (ImageView) findViewById(R.id.imagsearch);
		et_search = (EditText) findViewById(R.id.et_search);
		aCache = ACache.get(SearchActivity.this);
		View view = View.inflate(SearchActivity.this, R.layout.search_foot,
				null);
		qinkong = (TextView) view.findViewById(R.id.qinkong);
		searchlist.addFooterView(view);
		
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		onClick();
		init();
	}

	private void onClick() {
		searchlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				send(list.get(arg2));
			}
		});
		// TODO Auto-generated method stub
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				SearchActivity.this.finish();
			}
		});
		imagsearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				System.out.println("搜索");
				if (TextUtils.isEmpty(et_search.getText().toString())) {
					Toast.makeText(SearchActivity.this, "搜索内容不能为空", 0).show();
				} else {
					send(et_search.getText().toString());
				}
				System.out.println("搜索111");
				list.add(0, et_search.getText().toString());
				removeDuplicate(list);
				Gson gson = new Gson();
				String json = gson.toJson(list);
				aCache.put("oldsearch", json);
				adapter.notifyDataSetChanged();

			}
		});
		qinkong.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				aCache.remove("oldsearch");
				init();
			}
		});
	}

	/*
	 * @Override public View onCreateView(LayoutInflater inflater,
	 * 
	 * @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) { //
	 * TODO Auto-generated method stub View view = View.inflate(getActivity(),
	 * R.layout.sraechfragmen, null);
	 * 
	 * return view; }
	 */
	private void init() {
		// TODO Auto-generated method stub

		JSONArray asJSONArray = aCache.getAsJSONArray("oldsearch");
		list = new ArrayList<String>();
		if (asJSONArray != null) {

			for (int i = 0; i < asJSONArray.length(); i++) {
				try {
					String string = asJSONArray.getString(i);
					list.add(0, string);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			removeDuplicate(list);
			searchlist.setAdapter(adapter);

		}
		adapter.notifyDataSetChanged();

	}

	class SrarchAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(SearchActivity.this, R.layout.searchlist_item,
					null);
			TextView searchname = (TextView) arg1.findViewById(R.id.searchname);
			searchname.setText(list.get(arg0));
			return arg1;
		}
	}

	// 搜索请求
	public void send(final String keyWords) {
		Intent intent = new Intent(SearchActivity.this, GoodsListActivity.class);
		intent.putExtra("title", "搜索到的商品");
		intent.putExtra("keyWords", keyWords);

		startActivity(intent);
	}

	public static void removeDuplicate(List list) {
		HashSet h = new HashSet(list);
		list.clear();
		list.addAll(h);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		init();
	}
}
