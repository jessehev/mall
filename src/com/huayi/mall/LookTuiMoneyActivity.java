package com.huayi.mall;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.huayi.mall.util.SystemBar;

public class LookTuiMoneyActivity extends Activity {

	private ImageView back;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_look_tui_money);
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		
		back = (ImageView) findViewById(R.id.back);
		init();
		onClick();
	}

	private void onClick() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				LookTuiMoneyActivity.this.finish();
			}
		});
	}

	private void init() {
		// TODO Auto-generated method stub
		
		back.setVisibility(View.VISIBLE);
			
	}


}
