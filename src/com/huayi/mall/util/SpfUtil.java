package com.huayi.mall.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SpfUtil {
	public static SharedPreferences spf;
	public static Editor editor;
	private static String packageName = "com.huayi.mall";
	private static Context context;
	public static String username;
	public static String platenumber;

	public static boolean isFrisit;// 判断是否第一次登陆

	public static void initData(Context mycontext) {
		context = mycontext;

		spf = mycontext
				.getSharedPreferences(packageName, Activity.MODE_PRIVATE);
		if (spf != null) {
			editor = spf.edit();
		}
	}

	public static boolean isFrisit() {
		return spf.getBoolean("isfrist", true);
	}

	public static void setFrisit(boolean isFrisit) {
		editor.putBoolean("isfrist", isFrisit).commit();
	}

	// 保存用户id
	public static void setId(long id) {
		editor.putLong("id", id).commit();
	}

	public static long getId() {
		return spf.getLong("id", 0);
	}

	// 保存用户电话
	public static void setPhone(String phone) {
		editor.putString("phone", phone).commit();
	}

	public static String getPhone() {
		return spf.getString("phone", "");
	}

	// 保存用户购物车cart.id
	public static void setCartId(long cartId) {
		editor.putLong("cartId", cartId).commit();
	}

	public static long getCartId() {
		return spf.getLong("cartId", 0);
	}

	// 保存用户登录状态
	public static void setLoginStatus(boolean loginStatus) {
		editor.putBoolean("loginStatus", loginStatus).commit();
	}

	public static boolean getLoginStatus() {
		return spf.getBoolean("loginStatus", false);
	}

	/**
	 * 清楚缓存
	 */
	public static void clear() {
		editor.clear();
		editor.commit();
	}

}
