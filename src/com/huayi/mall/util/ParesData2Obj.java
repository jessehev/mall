package com.huayi.mall.util;

import com.google.gson.Gson;

/**
 * @author Ruby 将获取的数据转为对象
 * 
 */
public class ParesData2Obj {
	static Gson gson = new Gson();


	public static <T> T json2Obj(String json, Class<T> clazz) {
		return gson.fromJson(json, clazz);
	}

	
}
