package com.huayi.mall.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

public class MyDialog {
	public static Dialog createDialog(Context context, View view) {

		Dialog dialog = new AlertDialog.Builder(context).create();
		dialog.show();
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();

		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		int height = wm.getDefaultDisplay().getHeight() / 3;
		int weith = wm.getDefaultDisplay().getWidth() / 5 * 3;
		params.width = weith;
		params.height = height;
		params.gravity = Gravity.TOP;
		params.y = wm.getDefaultDisplay().getHeight() / 5;

		dialog.getWindow().setContentView(view);
		dialog.getWindow().setAttributes(params);
		dialog.getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
						| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		dialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		return dialog;
	}

}
