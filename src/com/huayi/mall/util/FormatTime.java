package com.huayi.mall.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatTime {
	public static String format(String time) {
		// 将时间戳转为字符串

		if (time != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			// 例如：cc_time=1291778220
			long lcc_time = Long.valueOf(time);
			return sdf.format(new Date(lcc_time));
		}
		return "";
	}
	public static String format2(String time) {
		// 将时间戳转为字符串
		
		if (time != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
			// 例如：cc_time=1291778220
			long lcc_time = Long.valueOf(time);
			return sdf.format(new Date(lcc_time));
		}
		return "";
	}
}
