package com.huayi.mall.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IsPhoneNumber {

	public static boolean  isPhone(String phone) {

		Pattern p = Pattern
				.compile("^((13[0-9])|(15[^4,\\D])|(18[0,1,5-9]))\\d{8}$");
		Matcher m = p.matcher(phone);
		return m.matches();
	}
	
}
