package com.huayi.mall.util;

import com.huayi.mall.base.BaseApplication;

import android.graphics.drawable.Drawable;


public class GetRes {
	/**
	 * 获取字符串资源
	 * 
	 * @param resId
	 * @return
	 */
	public static String getString(int resId) {
		return BaseApplication.getContext().getResources().getString(resId);
	}

	/**
	 * 获取图片资源
	 * 
	 * @param resId
	 * @return
	 */
	public static Drawable getDrawable(int resId) {
		return BaseApplication.getContext().getResources().getDrawable(resId);
	}

	/**
	 * 获取字符串数组资源
	 * 
	 * @param resId
	 * @return
	 */
	public static String[] getStringArray(int resId) {
		return BaseApplication.getContext().getResources()
				.getStringArray(resId);
	}

	/**
	 * 获取dp资源,返回的是转换后的像素值
	 * 
	 * @param resId
	 * @return
	 */
	public static float getDimen(int resId) {
		return BaseApplication.getContext().getResources().getDimension(resId);
	}
}
