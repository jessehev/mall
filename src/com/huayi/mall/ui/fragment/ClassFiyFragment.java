package com.huayi.mall.ui.fragment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.huayi.mall.GoodsListActivity;
import com.huayi.mall.R;
import com.huayi.mall.activity.SearchActivity;
import com.huayi.mall.base.AddressBean;
import com.huayi.mall.base.ClassFiyBean;
import com.huayi.mall.base.ClassifyBrandBean;
import com.huayi.mall.base.GoodsListBean;
import com.huayi.mall.base.ClassFiyBean.children;
import com.huayi.mall.base.ClassifyBrandBean.RowsBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.ui.fragment.HomeinFragment.GridAdapter;
import com.huayi.mall.util.ParesData2Obj;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.MyGridview;
import com.huayi.mall.view.MyListView;
import com.huayi.mall.view.RatioImageView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ClassFiyFragment extends Fragment {

	private TextView tv_title_logo;
	private String[]grilclassfiy = {"服装","包包","内衣","护肤品","化妆品","鞋子"};
	private String[]manclassfiy = {"男装","男鞋","男士包"};
	private int []grilimage={R.drawable.fuchuang,R.drawable.baobao,R.drawable.neiyi,
			R.drawable.hufupin,R.drawable.huazhuangpin,R.drawable.xiezi};
	private int []manimage={R.drawable.manzhuang,R.drawable.manxie,R.drawable.manbao};
	private int []pinpai={R.drawable.cancle,R.drawable.burberby,R.drawable.cancle,R.drawable.burberby,
			R.drawable.cancle,R.drawable.burberby,R.drawable.cancle,R.drawable.burberby,R.drawable.cancle,R.drawable.burberby
			,R.drawable.cancle,R.drawable.burberby};
	PinPaiAdapter pinPaiAdapter = new PinPaiAdapter();
	private MyGridview pinpaicalss;
	private MyListView listView;
	ClassfiyList classfiyList = new ClassfiyList();
	private List<ClassFiyBean>list=new ArrayList<ClassFiyBean>();
	private ImageLoader imageLoader;
	private ClassifyBrandBean bean = new ClassifyBrandBean();
	private List<RowsBean>bandlist = new ArrayList<ClassifyBrandBean.RowsBean>();
	private int currentPage=1;
	private RatioImageView classpicTile;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view  = View.inflate(getActivity(), R.layout.classfiyfragment, null);
		tv_title_logo = (TextView) view.findViewById(R.id.tv_title_logo);
		/*grilclassfiy2 = (MyGridview) view.findViewById(R.id.grilclass);*/
		/*manlclass = (MyGridview) view.findViewById(R.id.manlclass);*/
		pinpaicalss = (MyGridview) view.findViewById(R.id.pinpaicalss);
		listView = (MyListView) view.findViewById(R.id.classfiylist);
		listView.setAdapter(classfiyList);
		imageLoader = ImageLoader.getInstance();
		classpicTile = (RatioImageView) view.findViewById(R.id.classpicTitle);
		initdata();
		onClick();
		return view;
	}

	private void onClick() {
		pinpaicalss.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),GoodsListActivity.class);
				intent.putExtra("classify.id", bandlist.get(arg2).getId()+"");
				intent.putExtra("title", bandlist.get(arg2).getBrandName());
				startActivity(intent);
			
			}
		});

	}

	private void initdata() {
		// TODO Auto-generated method stub
		tv_title_logo.setText("分类");
		pinpaicalss.setAdapter(pinPaiAdapter);
		send();
		findClassify();
		findClassifyTitle();

	}


	class PinPaiAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return bandlist.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(getActivity(), R.layout.pinpai_item, null);
			RatioImageView grilclassimage = (RatioImageView) arg1.findViewById(R.id.grilclassimage);
			ImageLoader.getInstance().displayImage(
					GlobalUrl.serviceHost1
					+ bandlist.get(arg0).getPicture(),
					grilclassimage, ImageLoaderCfg.options2);
			return arg1;
		}}
	class ClassfiyList extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(getActivity(), R.layout.classfiyist_item, null);
			final ClassFiyBean classFiyBean = list.get(arg0);
			System.out.println("====================="+classFiyBean.getClassifyPicture());
			MyGridview grilclass = (MyGridview) arg1.findViewById(R.id.grilclass);
			RatioImageView imageView = (RatioImageView) arg1.findViewById(R.id.liimageone);
			imageView.setTag(arg0);
			if ((Integer)imageView.getTag()==arg0) {
				imageLoader.displayImage(GlobalUrl.serviceHost1+classFiyBean.getClassifyPicture(), imageView);

			}
			//点击事件
			grilclass.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(getActivity(),GoodsListActivity.class);
					intent.putExtra("title", classFiyBean.getChildren().get(arg2).getClassifyName());
					intent.putExtra("classify.id", "2");
					startActivity(intent);
					/*	//商品分类查询
					HttpUtils httpUtils = new HttpUtils();
					RequestParams params = new RequestParams();
					params.addBodyParameter("classify.id", "2");
					System.out.println("货物id==="+ classFiyBean.getChildren().get(arg2).getId());
					httpUtils.send(HttpMethod.POST, GlobalUrl.findGoodsByGoodsQuery, params, new RequestCallBack<String>() {
						@Override
						public void onFailure(HttpException arg0, String arg1) {

							System.out.println("========================"+arg1);
						}

						@Override
						public void onSuccess(ResponseInfo<String> arg0) {

							System.out.println("点击商品信息=========="+arg0.result);



						}

					});							
					 */


				}
			});
			grilclass.setAdapter(new BaseAdapter() {

				@Override
				public int getCount() {
					// TODO Auto-generated method stub
					return classFiyBean.getChildren().size();
				}

				@Override
				public Object getItem(int arg0) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public long getItemId(int arg0) {
					// TODO Auto-generated method stub
					return 0;
				}

				@Override
				public View getView(int arg0, View view, ViewGroup arg2) {
					// TODO Auto-generated method stub
					view = View.inflate(getActivity(), R.layout.grilclassfiy_item, null);
					ClassFiyBean.children children = classFiyBean.getChildren().get(arg0);

					TextView grilclasstx = (TextView) view.findViewById(R.id.grilclasstx);
					ImageView grilclassimage = (ImageView) view.findViewById(R.id.grilclassimage);
					grilclasstx.setText(children.getClassifyName());

					imageLoader.displayImage(GlobalUrl.serviceHost1+children.getClassifyPicture(), grilclassimage);

					return view;
				}});


			return arg1;
		}



	}
	public void send (){
		//商品分类查询
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		httpUtils.send(HttpMethod.POST, GlobalUrl.findAllClassifies, params, new RequestCallBack<String>() {

		
			@Override
			public void onFailure(HttpException arg0, String arg1) {
				

				System.out.println("========================"+arg1);
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {

		
				String result = arg0.result;
				Gson gson = new Gson();
				Type listType = new TypeToken<List<ClassFiyBean>>(){}.getType();
				list = gson.fromJson(result, listType); 
			
				

			}

		});							


	}
	public void findClassify(){
		//商品分类查询
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		httpUtils.send(HttpMethod.POST, GlobalUrl.findBrandByBrandQuery, params, new RequestCallBack<String>() {

			
			@Override
			public void onFailure(HttpException arg0, String arg1) {
				

				System.out.println("========================"+arg1);
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {

				String result = arg0.result;
				if (result!=null&currentPage!=bean.getTotalPage()) {
					bean=ParesData2Obj.json2Obj(result, ClassifyBrandBean.class);
					currentPage=bean.getCurrentPage();
					bandlist.addAll(bean.getRows());
					pinPaiAdapter.notifyDataSetChanged();
				}
			}
		});							
	}
	public void findClassifyTitle(){
		//商品分类查询
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		httpUtils.send(HttpMethod.POST, GlobalUrl.findBannerImage, params, new RequestCallBack<String>() {
			
			
			@Override
			public void onFailure(HttpException arg0, String arg1) {
				
				
				System.out.println("========================"+arg1);
			}
			
			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				
				System.out.println("品牌分类头信息=========="+arg0.result);
				String result = arg0.result;
				try {
					JSONObject jsonObject = new JSONObject(result);
					String string = jsonObject.getString("bannerImage");
					ImageLoader.getInstance().displayImage(
							GlobalUrl.serviceHost1
							+ string,
							classpicTile, ImageLoaderCfg.options2);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*if (result!=null&currentPage!=bean.getTotalPage()) {
					bean=ParesData2Obj.json2Obj(result, ClassifyBrandBean.class);
					currentPage=bean.getCurrentPage();
					bandlist.addAll(bean.getRows());
					pinPaiAdapter.notifyDataSetChanged();
				}*/
			}
		});							
	}
}
