package com.huayi.mall.ui.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.GoodsDataActivity;
import com.huayi.mall.GoodsListActivity;
import com.huayi.mall.R;
import com.huayi.mall.activity.HdActivity;
import com.huayi.mall.activity.InfomationActivity;
import com.huayi.mall.activity.MiaoKillActivity;
import com.huayi.mall.activity.MotherBabyActivity;
import com.huayi.mall.activity.MyApplication;
import com.huayi.mall.activity.PointActivity;
import com.huayi.mall.activity.RechargeActivity;
import com.huayi.mall.activity.TicketActivity;
import com.huayi.mall.adapter.GalleryAdapter;
import com.huayi.mall.adapter.GalleryAdapter.OnItemClickLitener;
import com.huayi.mall.base.GoodsListBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.common.SpecialBean;
import com.huayi.mall.util.ParesData2Obj;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.ImageCycleView;
import com.huayi.mall.view.ImageCycleView.ImageCycleViewListener;
import com.huayi.mall.view.MyGridview;
import com.huayi.mall.view.RatioImageView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

public class HomeinFragment extends Fragment {
	private ImageCycleView mAdView;

	public int stype = 0;
	private GridView gridView;
	private MyGridview gridViewno;
	private List<Map<String, Object>> data_list;
	private List<SpecialBean> arrayListSpecial;
	private SimpleAdapter sim_adapter;
	private GridAdapter adapter = new GridAdapter();
	private GridAdapter2 adapter9 = new GridAdapter2();
	private ImageView mActionone;
	private ImageView mActiontwo;
	private GoodsListBean bean;
	private GoodsListBean bean2;
	private GalleryAdapter mAdapter;
	private ViewPager mPager;
	private int page=1;
	private int page2=1;
	private boolean zuihou =true;

	private List<GoodsListBean.RowsBean> list = new ArrayList<GoodsListBean.RowsBean>();
	private GridNowAdapter nowAdapter = new GridNowAdapter();
	// 图片封装为一个数组
	private int[] icon = { R.drawable.jifen, R.drawable.miaosha,
			R.drawable.tuijian, R.drawable.xinping, R.drawable.ticket,
			R.drawable.chongzhi, R.drawable.zhixun, R.drawable.rexiao };
	private String[] iconName = { "积分", "秒杀", "推荐", "新品", "优惠券", "充值", "资讯",
	"热销" };
	private String[] iconName2 = { "活动" };
	private int[] icon2 = {R.drawable.huodong};

	// 模拟 biinner图数据
	private ArrayList<String> mImageUrl = null;
	// 模拟 画廊图数据
	List<GoodsListBean.RowsBean> gliList = new ArrayList<GoodsListBean.RowsBean>();
	private List<GridView> list2 = new ArrayList<GridView>();
	private Context context;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view = View.inflate(getActivity(), R.layout.homeinfragment, null);

		context = getActivity();
		mAdView = (ImageCycleView) view.findViewById(R.id.ad_view);
		/*
		 * gridView = (GridView) view.findViewById(R.id.inhomegr);
		 */
		context = getActivity();
		gridViewno = (MyGridview) view.findViewById(R.id.nowfaction);
		mActionone = (ImageView) view.findViewById(R.id.actionone);
		mActiontwo = (ImageView) view.findViewById(R.id.actiontwo);
		recyclerView = (RecyclerView) view
				.findViewById(R.id.id_recyclerview_horizontal);
		more = (Button) view.findViewById(R.id.more);
		jiazaibar = (ProgressBar) view.findViewById(R.id.jiazaimore);
		actionone = (RatioImageView) view.findViewById(R.id.actionone);
		actiontwo = (RatioImageView) view.findViewById(R.id.actiontwo);
		mPager = (ViewPager) view.findViewById(R.id.inhomevp);
		view2 = (GridView) View.inflate(getActivity(), R.layout.grid, null);
		view3 = (GridView) View.inflate(getActivity(), R.layout.grid, null);
		view2.setAdapter(adapter);
		view3.setAdapter(adapter9);
		list2.add(view2);
		list2.add(view3);
		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				adapter.notifyDataSetChanged();
			}
		});
		mPager.setAdapter(new PagerAdapter() {
			@Override
			public Object instantiateItem(ViewGroup container, int position) {
				// TODO Auto-generated method stub
				container.addView(list2.get(position));
				return list2.get(position);

			}

			@Override
			public void destroyItem(ViewGroup container, int position,
					Object object) {
				// TODO Auto-generated method stub
				container.removeView(list2.get(position));
			}

			@Override
			public boolean isViewFromObject(View arg0, Object arg1) {
				// TODO Auto-generated method stub
				return arg0 == arg1;
			}

			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return list2.size();
			}
		});
		initdata();
		onclick();
		/* RecyclerView */
		return view;
	}

	private void onclick() {
		// 值得入手
		actionone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(getActivity(),
						MotherBabyActivity.class);

				intent.putExtra("whichSpecial", "2");

				startActivity(intent);

			}
		});
		actiontwo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),
						MotherBabyActivity.class);

				intent.putExtra("whichSpecial", "1");

				startActivity(intent);
			}
		});

		// TODO Auto-generated method stub
		more.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				more.setBackgroundColor(Color.parseColor("#ffffff"));
				jiazaibar.setVisibility(View.VISIBLE);
				more.setText("加载中...");
				more.setTextColor(Color.parseColor("#000000"));
				page++;
				remai();
			}
		});
		view3.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if (arg2 == 0) {
					startActivity(new Intent(getActivity(), HdActivity.class));
				}
			}
		});
		view2.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				switch (arg2) {
				case 0:// 积分
					startActivity(new Intent(getActivity(), PointActivity.class));
					break;
				case 1:// 秒杀
					startActivity(new Intent(getActivity(),
							MiaoKillActivity.class));
					break;
				case 2:// 推荐
					Intent intent = new Intent(getActivity(),
							GoodsListActivity.class);
					intent.putExtra("title", "推荐");
					intent.putExtra("status", "1");
					startActivity(intent);
					break;
				case 3:// 新品
					Intent intent1 = new Intent(getActivity(),
							GoodsListActivity.class);
					intent1.putExtra("title", "新品");
					intent1.putExtra("newtype", "1");
					startActivity(intent1);
					break;
				case 4:// 优惠券
					startActivity(new Intent(getActivity(),
							TicketActivity.class));
					break;
				case 5:// 充值
					startActivity(new Intent(getActivity(),
							RechargeActivity.class));
					break;
				case 6:// 咨询
					Intent intent11 = new Intent(getActivity(),
							InfomationActivity.class);
					intent11.putExtra("title", "资讯");
					startActivity(intent11);
					break;
				case 7:// 热销
					Intent intent111 = new Intent(getActivity(),
							GoodsListActivity.class);
					intent111.putExtra("title", "热销");
					intent111.putExtra("rexiao", "1");
					startActivity(intent111);
					break;

				default:
					break;
				}
			}
		});

		// 正在流行点击事件
		gridViewno.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),
						GoodsDataActivity.class);
				intent.putExtra("id", list.get(arg2).getId() + "");
				startActivity(intent);
			}
		});

		// 热卖点击事件
		mAdapter.setOnItemClickLitener(new OnItemClickLitener() {

			@Override
			public void onItemClick(View view, int position) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),
						GoodsDataActivity.class);
				intent.putExtra("id", gliList.get(position).getId() + "");
				startActivity(intent);
			}
		});
	}

	private void initdata() {
		// 下拉刷新

		// TODO Auto-generated method stub
		// 设置布局管理器
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
				getActivity().getApplicationContext());
		linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
		recyclerView.setLayoutManager(linearLayoutManager);
		// 设置适配器
		mAdapter = new GalleryAdapter(getActivity(), gliList);
		recyclerView.setAdapter(mAdapter);
		send();
		remai();
		// 新建适配器
		gridViewno.setAdapter(nowAdapter);
		mImageUrl = new ArrayList<String>();
		// mImageUrl.add(imageUrl1);
		// mImageUrl.add(imageUrl2);
		// mImageUrl.add(imageUrl3);
		//
		// mAdView.setImageResources(mImageUrl, mAdCycleViewListener, stype);
		recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener(){

			boolean islast = false;
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView,
					int newState) {
				// TODO Auto-generated method stub
				super.onScrollStateChanged(recyclerView, newState);
				LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
				if (newState==RecyclerView.SCROLL_STATE_IDLE) {
					int last  = layoutManager.findLastCompletelyVisibleItemPosition();
					int total = layoutManager.getItemCount();
					if (last==(total-1)&&islast) {

						if (zuihou) {
							System.out.println("加载更多");
							page2++;
							send();
						}

					}
				}
			}
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				// TODO Auto-generated method stub
				super.onScrolled(recyclerView, dx, dy);
				if (dx>0) {
					islast=true;
				}else {
					islast=false;
				}
			}

		});

		xutilRequestSpecial(); // 专场请求
	}

	public void setSpecialData() {

		String result = MyApplication.aCache.getAsString("special");
		if (result != null) {
			try {
				JSONArray jsonArray = new JSONArray(result);

				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObj = (JSONObject) jsonArray.get(i);
					int id = jsonObj.optInt("id");

					if (id == 1) {
						ImageLoader.getInstance().displayImage(
								GlobalUrl.serviceHost
								+ jsonObj.optString("picture"),
								mActiontwo, ImageLoaderCfg.options);
					} else if (id == 2) {
						ImageLoader.getInstance().displayImage(
								GlobalUrl.serviceHost
								+ jsonObj.optString("picture"),
								mActionone, ImageLoaderCfg.options);
					} else {

						mImageUrl.add(GlobalUrl.serviceHost
								+ jsonObj.optString("bigPicture"));

						mAdView.setImageResources(mImageUrl,
								mAdCycleViewListener, stype);
						System.out.println("mImageUrl" + mImageUrl.toString());
					}

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 * @param
	 * 
	 * @param 活动banner
	 *            //国内
	 * 
	 */
	private void xutilRequestSpecial() {
		HttpUtils httpUtils = new HttpUtils();

		RequestParams params = new RequestParams();
		params.addBodyParameter("chinese", "1");

		httpUtils.send(HttpMethod.POST, GlobalUrl.special, params,
				new RequestCallBack<String>() {

			private CustomProgressDialog dialog;

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				setSpecialData();
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				try {
					String result = arg0.result;

					System.out.println("result======" + result);
					MyApplication.aCache.put("special", result);
					
					setSpecialData();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onStart() {

			}
		});
	}

	private ImageCycleViewListener mAdCycleViewListener = new ImageCycleViewListener() {
		@Override
		public void onImageClick(int position, View imageView) {
			// TODO 单击图片处理事件
			// startActivity(new Intent(getActivity(), HdActivity.class));
			Intent intent = new Intent(getActivity(), MotherBabyActivity.class);

			switch (position) {
			case 0:

				intent.putExtra("whichSpecial", "3");

				break;
			case 1:
				intent.putExtra("whichSpecial", "4");

				break;
			case 2:

				intent.putExtra("whichSpecial", "5");
				break;

			}
			startActivity(intent);
		}
	};
	private RecyclerView recyclerView;
	private Button more;
	private ProgressBar jiazaibar;

	private RatioImageView actionone;
	private RatioImageView actiontwo;

	private GridView view2;
	private GridView view3;

	public class GridAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return iconName.length;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(getActivity(), R.layout.gridview_item, null);
			ImageView image = (ImageView) arg1.findViewById(R.id.image);
			TextView text = (TextView) arg1.findViewById(R.id.text);
			image.setImageResource(icon[arg0]);
			text.setText(iconName[arg0]);
			return arg1;
		}

	}

	public class GridAdapter2 extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return iconName2.length;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(getActivity(), R.layout.gridview_item, null);
			ImageView image = (ImageView) arg1.findViewById(R.id.image);
			TextView text = (TextView) arg1.findViewById(R.id.text);
			image.setImageResource(icon2[arg0]);
			text.setText(iconName2[arg0]);
			return arg1;
		}

	}

	public class GridNowAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(getActivity(), R.layout.gridnowview_item, null);
			RatioImageView image = (RatioImageView) arg1
					.findViewById(R.id.remaimage);
			String s = list.get(arg0).getGoodsPicture();
		
			String[] split ;
			if(s!=null&&s.contains(",")){
				split = s.split(",");
				
				ImageLoader.getInstance().displayImage(
						GlobalUrl.serviceHost1 + split[0], image,
						ImageLoaderCfg.options2);
			}else {
				ImageLoader.getInstance().displayImage(
						GlobalUrl.serviceHost1 + s, image,
						ImageLoaderCfg.options2);
			}
	
		
			
			
			TextView homeinname = (TextView) arg1.findViewById(R.id.homeinname);
			homeinname.setText(list.get(arg0).getGoodsName());
			return arg1;
		}

	}

	public void onRefresh() {

		send();
		remai();
	}

	public void send() {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("click", "2000");
		params.addBodyParameter("currentPage", page2+"");
		httpUtils.send(HttpMethod.POST, GlobalUrl.findGoodsByGoodsQuery,
				params, new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {

				Toast.makeText(context, "获取失败", 0).show();
				System.out.println(arg1);

			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				Toast.makeText(context, "获取成功", 0).show();
				System.out.println("点击量信息==============="+arg0.result);
				bean = ParesData2Obj.json2Obj(arg0.result,
						GoodsListBean.class);
		
				gliList.addAll(bean.getRows());
				mAdapter.notifyDataSetChanged();
				page2=bean.getCurrentPage();
				if (page2==bean.getTotalPage()) {
					zuihou=false;
				}
			}

		});
	}
	public void setUserVisibleHint(boolean isVisibleToUser) {
		// TODO Auto-generated method stub
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			
			
			initdata();
		}}
	public void remai() {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("saleTotalNumber", "1000");
		params.addBodyParameter("currentPage", page+"");

		httpUtils.send(HttpMethod.POST, GlobalUrl.findGoodsByGoodsQuery,
				params, new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				more.setText("更多");
				more.setTextColor(Color.parseColor("#000000"));
				more.setBackgroundResource(R.drawable.shape_btn_bg);
				Toast.makeText(context, "获取失败", 0).show();
				System.out.println(arg1);

			}

			@SuppressLint("NewApi")
			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				Toast.makeText(context, "获取成功", 0).show();
				System.out.println("爆款量数据======================="
						+ arg0.result);
				bean2 = ParesData2Obj.json2Obj(arg0.result,
						GoodsListBean.class);
				page=bean2.getCurrentPage();
				more.setText("更多");
				more.setTextColor(Color.parseColor("#000000"));
				more.setBackgroundResource(R.drawable.shape_btn_bg);
				jiazaibar.setVisibility(View.GONE);
				if (page==bean2.getTotalPage()) {
					more.setEnabled(false);
					more.setText("没有更多了。。。");
					more.setBackgroundColor(Color.parseColor("#ffffff"));
				}
				list.addAll(bean2.getRows());
				nowAdapter.notifyDataSetChanged();

			}

		});
	}

}
