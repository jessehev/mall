package com.huayi.mall.ui.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.huayi.mall.GoodsDataActivity;
import com.huayi.mall.GoodsListActivity;
import com.huayi.mall.R;
import com.huayi.mall.activity.BrandActivity;
import com.huayi.mall.activity.FiveShopActivity;
import com.huayi.mall.activity.HdActivity;
import com.huayi.mall.activity.MotherBabyActivity;
import com.huayi.mall.activity.MyApplication;
import com.huayi.mall.adapter.OutSideGridViewAdapter;
import com.huayi.mall.common.BrandBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.common.Shops;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.ImageCycleView;
import com.huayi.mall.view.ImageCycleView.ImageCycleViewListener;
import com.huayi.mall.view.RatioImageView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

public class OutSideFragment extends Fragment implements OnClickListener,
		SwipeRefreshLayout.OnRefreshListener {
	private ImageCycleView mImageCycleView;
	private ArrayList<String> mImageUrl = null;
	private View view;
	private OutSideGridViewAdapter adapter;
	private GridView gridview;
	/**
	 * 五洲馆
	 */
	private RatioImageView iv_korea, iv_japan, iv_australia, iv_unitedstates,
			iv_hongkong;
	/**
	 * 游标是圆形还是长条，要是设置为0是长条，要是1就是圆形 默认是圆形
	 */
	public int stype = 0;

	private List<BrandBean> arrayList = new ArrayList<BrandBean>(); // 测试
	HashMap<String, Shops> map = new HashMap<String, Shops>();// 存储5个馆的数据

	private int currentPage = 1, totalPage;
	private SwipeRefreshLayout sf;

	private ScrollView scrollview;

	private LinearLayout load_layout;
	private ProgressBar pb;

	private TextView pbTv;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		view = View.inflate(getActivity(), R.layout.outsidefragment, null);
		sf = (SwipeRefreshLayout) view.findViewById(R.id.sf);
		findViewById();
		setView();
		initData();
		registerClick();

		createAd();
		createGridView();
		System.out.println("-------------------");
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		scrollview.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				if (event.getAction() == MotionEvent.ACTION_UP) {
					View view = ((ScrollView) v).getChildAt(0);
					if (view.getMeasuredHeight() <= v.getScrollY()
							+ v.getHeight()) {
						// 加载数据代码

						if (currentPage != totalPage) {
							currentPage++;
							xutilRequestBrand(currentPage);// 品牌操作
						}
						load_layout.setVisibility(View.VISIBLE);

						if (currentPage != totalPage) {
							load_layout.setVisibility(View.VISIBLE);
							pb.setVisibility(View.VISIBLE);
							currentPage++;
							xutilRequestBrand(currentPage);// 品牌操作
						} else {
							load_layout.setVisibility(View.VISIBLE);
							pbTv.setText("没有更多内容了");
							pb.setVisibility(View.GONE);
						}
					}

				}

				return false;
			}
		});
	}

	public void setView() {
		// 下拉刷新

		scrollview.smoothScrollTo(0, 0);
		scrollview.scrollTo(0, 0);
		sf.setOnRefreshListener(this);
		sf.setColorScheme(android.R.color.holo_orange_light,
				android.R.color.holo_orange_dark,
				android.R.color.holo_red_light, android.R.color.holo_red_dark);

		load_layout.setVisibility(View.GONE);

	}

	public void findViewById() {

		pb = (ProgressBar) view.findViewById(R.id.progress);
		pbTv = (TextView) view.findViewById(R.id.progress_text);
		load_layout = (LinearLayout) view.findViewById(R.id.load_layout);
		scrollview = (ScrollView) view.findViewById(R.id.scrollview);
		gridview = (GridView) view.findViewById(R.id.gridview_outside);
		mImageCycleView = (ImageCycleView) view
				.findViewById(R.id.imagecycle_outside);

		iv_korea = (RatioImageView) view.findViewById(R.id.img_korea_outside);
		iv_australia = (RatioImageView) view
				.findViewById(R.id.img_australia_outside);
		iv_hongkong = (RatioImageView) view
				.findViewById(R.id.img_hongkong_outside);
		iv_japan = (RatioImageView) view.findViewById(R.id.img_japan_outside);
		iv_unitedstates = (RatioImageView) view
				.findViewById(R.id.img_unitedstates_outside);
	}

	// 初始化数据
	public void initData() {

		xutilRequestFiveShop();// 五大馆操作

		xutilRequestBrand(currentPage);// 品牌操作
		xutilRequestSpecial();// banner生成

	}

	// 注册监听事件
	public void registerClick() {
		// loadMore.setOnClickListener(this);

		iv_korea.setOnClickListener(this);
		iv_australia.setOnClickListener(this);
		iv_hongkong.setOnClickListener(this);
		iv_japan.setOnClickListener(this);
		iv_unitedstates.setOnClickListener(this);
	}

	// 生成品牌gridview
	public void createGridView() {
		adapter = new OutSideGridViewAdapter(getActivity(), arrayList);

		gridview.setAdapter(adapter);

		gridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				BrandBean bb = arrayList.get(position);

				Intent intent = new Intent(getActivity(),
						GoodsListActivity.class);
				//
				// intent.putExtra("classify.id", bb.id);
				// intent.putExtra("title", bb.name);

				startActivity(intent);

			}
		});

	}

	// 生成广告轮播图
	public void createAd() {
		// mImageUrl = new ArrayList<String>();
		// mImageUrl.add(imageUrl1);
		// mImageUrl.add(imageUrl2);
		// mImageUrl.add(imageUrl3);
		// mImageCycleView.setImageResources(mImageUrl, mAdCycleViewListener,
		// stype);

	}

	// 轮播图监听函数
	private ImageCycleViewListener mAdCycleViewListener = new ImageCycleViewListener() {
		@Override
		public void onImageClick(int position, View imageView) {
			// TODO 单击图片处理事件

			Intent intent = new Intent(getActivity(), MotherBabyActivity.class);

			switch (position) {
			case 0:

				intent.putExtra("whichSpecial", "6");

				break;
			case 1:
				intent.putExtra("whichSpecial", "7");

				break;
			case 2:

				intent.putExtra("whichSpecial", "8");
				break;

			}
			startActivity(intent);
		}
	};

	public void setSpecialData() {

		String result = MyApplication.aCache.getAsString("specialoutside");
		if (result != null) {
			try {
				JSONArray jsonArray = new JSONArray(result);
				mImageUrl = new ArrayList<String>();
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObj = (JSONObject) jsonArray.get(i);
					int id = jsonObj.optInt("id");

					mImageUrl.add(GlobalUrl.serviceHost
							+ jsonObj.optString("bigPicture"));

				}

				// mImageUrl
				// .add("http://img.lakalaec.com/ad/57ab6dc2-43f2-4087-81e2-b5ab5681642d.jpg");
				// System.out
				// .println("mImageUrl0000000000" + mImageUrl.toString());

				mImageCycleView.setImageResources(mImageUrl,
						mAdCycleViewListener, stype);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 * @param
	 * 
	 * @param 活动banner
	 *            //国外
	 * 
	 */
	private void xutilRequestSpecial() {
		HttpUtils httpUtils = new HttpUtils();

		RequestParams params = new RequestParams();
		params.addBodyParameter("chinese", "0");

		httpUtils.send(HttpMethod.POST, GlobalUrl.special, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						setSpecialData();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							String result = arg0.result;

							System.out.println("result======" + result);
							MyApplication.aCache.put("specialoutside", result);

							setSpecialData();
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

					@Override
					public void onStart() {

					}
				});
	}

	// 监听事件函数
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = null;
		Bundle bundle = null;
		switch (v.getId()) {

		case R.id.img_australia_outside:

			intent = new Intent(getActivity(), FiveShopActivity.class);
			// intent.putExtra("whichshop", "1");
			// intent.put
			bundle = new Bundle();
			bundle.putParcelable("whichshop", (Shops) map.get("1"));
			intent.putExtras(bundle);
			startActivity(intent);
			break;
		case R.id.img_hongkong_outside:
			intent = new Intent(getActivity(), FiveShopActivity.class);

			bundle = new Bundle();
			bundle.putParcelable("whichshop", (Shops) map.get("2"));
			intent.putExtras(bundle);
			startActivity(intent);
			break;
		case R.id.img_japan_outside:
			intent = new Intent(getActivity(), FiveShopActivity.class);
			bundle = new Bundle();
			bundle.putParcelable("whichshop", (Shops) map.get("5"));
			intent.putExtras(bundle);
			startActivity(intent);
			break;
		case R.id.img_korea_outside:
			intent = new Intent(getActivity(), FiveShopActivity.class);
			bundle = new Bundle();
			bundle.putParcelable("whichshop", (Shops) map.get("3"));
			intent.putExtras(bundle);
			startActivity(intent);
			break;
		case R.id.img_unitedstates_outside:
			intent = new Intent(getActivity(), FiveShopActivity.class);
			bundle = new Bundle();
			bundle.putParcelable("whichshop", (Shops) map.get("4"));
			intent.putExtras(bundle);
			startActivity(intent);
			break;
		default:
			break;
		}
	}

	/**
	 * id:-----》1 澳洲馆 2港台馆 3韩国馆 4美国馆 5日本馆
	 */
	// 设置数据格式
	public void setDataFiveShop() {

		JSONArray jsonArray = MyApplication.aCache.getAsJSONArray("outside");
		System.out.println("jsonarray------" + jsonArray);
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					JSONObject jsonObj = jsonArray.getJSONObject(i);

					String shopName = jsonObj.optString("shopName");

					String image = jsonObj.optString("image");

					String smallImage = jsonObj.optString("smallImage");
					String id = jsonObj.optString("id");
					System.out.println("img" + image);

					Shops shop = new Shops();
					shop.name = shopName;
					shop.id = id;
					map.put(id, shop);

					if (id != null) {

						switch (Integer.parseInt(id)) {
						case 1:
							ImageLoader.getInstance().displayImage(
									GlobalUrl.serviceHost + smallImage,
									iv_australia, ImageLoaderCfg.options);
							break;
						case 2:
							ImageLoader.getInstance().displayImage(
									GlobalUrl.serviceHost + smallImage,
									iv_hongkong, ImageLoaderCfg.options);
							break;
						case 3:
							ImageLoader.getInstance().displayImage(
									GlobalUrl.serviceHost + image, iv_korea,
									ImageLoaderCfg.options);
							break;
						case 4:
							ImageLoader.getInstance().displayImage(
									GlobalUrl.serviceHost + smallImage,
									iv_unitedstates, ImageLoaderCfg.options);
							break;
						case 5:
							ImageLoader.getInstance().displayImage(
									GlobalUrl.serviceHost + smallImage,
									iv_japan, ImageLoaderCfg.options);
							break;

						default:
							break;
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			sf.setRefreshing(false);
		}
	}

	// 五大馆数据请求
	private void xutilRequestFiveShop() {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		final CustomProgressDialog dialog = new CustomProgressDialog(
				getActivity(), "获取中。。。", R.anim.frame2);
		dialog.show();
		httpUtils.send(HttpMethod.POST, GlobalUrl.findShop, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						setDataFiveShop();
						dialog.dismiss();

					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							JSONArray obj = new JSONArray(arg0.result);

							MyApplication.aCache.put("outside", obj);

							setDataFiveShop();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						dialog.dismiss();
					}

					@Override
					public void onStart() {

					}
				});
	}

	// 品牌
	public void setDatabrand() {

		JSONObject obj = MyApplication.aCache.getAsJSONObject("brand");
		try {
			if (obj != null) {
				JSONArray array = obj.getJSONArray("rows");
				if (array != null) {

					for (int i = 0; i < array.length(); i++) {
						BrandBean bb = new BrandBean();
						JSONObject jsonObj = array.getJSONObject(i);
						bb.picture = jsonObj.getString("picture");

						bb.id = jsonObj.getString("id");
						bb.name = jsonObj.getString("brandName");

						arrayList.add(bb);

						System.out.println("图片链接---" + bb.picture);
					}
					adapter.setData(arrayList);
				}
				currentPage = obj.optInt("currentPage");
				totalPage = obj.optInt("totalPage");

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		load_layout.setVisibility(View.GONE); // 取消加载更多的显示视图
		if (currentPage == 1) {
			scrollview.smoothScrollTo(0, 0);
			scrollview.scrollTo(0, 0);
		}

	}

	// 品牌数据请求
	private void xutilRequestBrand(int currentPage) {
		HttpUtils httpUtils = new HttpUtils();

		System.out.println("刷新的页码数----》" + currentPage);
		RequestParams params = new RequestParams();
		params.addBodyParameter("currentPage", currentPage + "");
		httpUtils.send(HttpMethod.POST, GlobalUrl.getBrand, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						setDatabrand();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {
							JSONObject obj = new JSONObject(arg0.result);
							System.out.println("brand------------" + obj);
							MyApplication.aCache.put("brand", obj);
							setDatabrand();

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onStart() {

					}
				});
	}

	// 下拉刷新
	@Override
	public void onRefresh() {

		xutilRequestFiveShop();

	}
}
