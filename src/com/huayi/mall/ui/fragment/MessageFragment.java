package com.huayi.mall.ui.fragment;

import java.util.ArrayList;
import java.util.List;

import com.huayi.mall.R;
import com.huayi.mall.activity.ActiconMessActivity;
import com.huayi.mall.activity.HdActivity;
import com.huayi.mall.base.GoodsListBean;
import com.huayi.mall.base.NewsBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.ParesData2Obj;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MessageFragment extends Fragment {

	private LinearLayout sys;
	private LinearLayout activon;
	private LinearLayout wuliu;
	private LinearLayout jiaoyi;
	private ImageView back;
	private TextView tv_title_logo;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View view  = View.inflate(getActivity(), R.layout.messagefragment, null);
		sys = (LinearLayout) view.findViewById(R.id.sys);
		activon = (LinearLayout) view.findViewById(R.id.activon);
		wuliu = (LinearLayout) view.findViewById(R.id.wuliu);
		jiaoyi = (LinearLayout) view.findViewById(R.id.jiaoyi);
		
		tv_title_logo = (TextView) view.findViewById(R.id.tv_title_logo);
		init();
		onclick();
		return view;
	}

	private void init() {
		// TODO Auto-generated method stub
		tv_title_logo.setText("消息");
	}

	private void onclick() {
		
		//系统设置
		sys.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent =new Intent(getActivity(),ActiconMessActivity.class);
				intent.putExtra("title", "系统消息");
				startActivity(intent);
			}
		});
		//活动消息
		activon.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent =new Intent(getActivity(),HdActivity.class);
				intent.putExtra("title", "活动消息");
				startActivity(intent);
			}
		});
		//物流消息
		wuliu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent =new Intent(getActivity(),ActiconMessActivity.class);
				intent.putExtra("title", "物流消息");
				startActivity(intent);
			}
		});
		//交易消息
		jiaoyi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent =new Intent(getActivity(),ActiconMessActivity.class);
				intent.putExtra("title", "交易消息");
				startActivity(intent);
			}
		});
	}

}
