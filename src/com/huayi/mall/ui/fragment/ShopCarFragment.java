package com.huayi.mall.ui.fragment;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.huayi.mall.GoodsDataActivity;
import com.huayi.mall.GoodsListActivity;
import com.huayi.mall.R;
import com.huayi.mall.activity.ConfirmActivity;
import com.huayi.mall.activity.MyApplication;
import com.huayi.mall.adapter.ShopCartAdapter;
import com.huayi.mall.adapter.ShopCartGridViewApater;
import com.huayi.mall.base.GuessYouLikeBean;
import com.huayi.mall.common.Cart;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.IsNetWorkConnected;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.MyGridview;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

public class ShopCarFragment extends Fragment implements
		SwipeRefreshLayout.OnRefreshListener {

	private ShopCartAdapter adapter;
	private ShopCartGridViewApater gridviewAdapter;

	private View view;

	private List<Cart> arrayList_0 = new ArrayList<Cart>();// 第一种布局
	private List<Cart> arrayList_1 = new ArrayList<Cart>();// 第二种布局
	private List<Cart> arrayList_delete;
	private List<GuessYouLikeBean> arrayListGrid = new ArrayList<GuessYouLikeBean>(); // gridview

	private MyGridview gridview;

	private ListView listview;
	private ScrollView sv;

	private TextView complete_cart, balance;// 编辑、结算

	boolean isComplete; // 完成编辑状态

	private ImageView circle_cart, circle_jiesuan, back;

	boolean selectAll;

	private TextView tv_title_logo, countPrice;

	private LinearLayout layout_nonet_tip, layout_nodata_tip;

	private TextView refresh_shopcart, goto_shopcart;

	private SwipeRefreshLayout srf;
	/**
	 * 所有列表中的商品全部被选中，让全选按钮也被选中
	 */
	boolean flag;
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (msg.what == 11) {
				// 所有列表中的商品全部被选中，让全选按钮也被选中
				// flag记录是否全被选中

				countPrice.setText("¥" + getPrice(arrayList_1) + "");// 显示价格
				flag = !(Boolean) msg.obj;
				if (!flag) {
					selectAll = true; // 改变全选按钮状态
					circle_cart.setImageResource(R.drawable.check_icon);
					circle_jiesuan.setImageResource(R.drawable.check_icon);

				} else {
					selectAll = false;
					circle_cart.setImageResource(R.drawable.circle_cart);
					circle_jiesuan.setImageResource(R.drawable.circle_cart);

				}

			} else if (msg.what == 10) {

				countPrice.setText("¥" + getPrice(arrayList_0) + "");// 显示价格
				flag = !(Boolean) msg.obj;
				if (!flag) {
					selectAll = true; // 改变全选按钮状态
					circle_cart.setImageResource(R.drawable.check_icon);
					circle_jiesuan.setImageResource(R.drawable.check_icon);
				} else {
					selectAll = false;
					circle_cart.setImageResource(R.drawable.circle_cart);
					circle_jiesuan.setImageResource(R.drawable.circle_cart);
				}
				// 点击删除的时候重新刷新页面
			} else if (msg.what == 12) {

				xutilRequest(12); // 刷新的时候判断加载那个arraylist

			} else if (msg.what == 13) { // 加减的时候更新价格 arrayList_0

				countPrice.setText("¥" + getPrice(arrayList_0) + "");// 显示价格

			} else if (msg.what == 14) {// 加减的时候更新价格 arrayList_1
				countPrice.setText("¥" + getPrice(arrayList_1) + "");// 显示价格
			}
		}
	};

	// 计算选中商品的总价
	public String getPrice(List<Cart> list) {
		BigDecimal bd1 = null, bd2 = null;
		BigDecimal bd3 = new BigDecimal("0.00");
		if (arrayList_0.size() != 0) {
			for (int i = 0; i < list.size(); i++) {
				Cart cart = list.get(i);

				if (cart.select) {

					bd1 = new BigDecimal(cart.price);
					bd2 = new BigDecimal(cart.number);

					bd1 = bd1.multiply(bd2);

					bd3 = bd3.add(bd1);

				}
			}
			System.out.println("bd1-----------" + bd3.toString());
			return bd3.toString();

		}
		return bd3.toString();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		view = View.inflate(getActivity(), R.layout.fragment_shopcart, null);

		findViewById();
		initAdapter();
		initView();
		initData();
		setListViewHeightBasedOnChildren(listview);

		onclick();

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		xutilRequest(1);
	}

	public void onclick() {
		// 编辑和完成
		complete_cart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!isComplete) {
					complete_cart.setText("完成");
					isComplete = true;
					arrayList_delete = new ArrayList<Cart>();
					// 切换的时候判断是否是全选状态
					adapter.setData(arrayList_1);
					boolean isAll = isAllSelected(arrayList_1);
					if (isAll) {
						circle_cart.setImageResource(R.drawable.check_icon);
						circle_jiesuan.setImageResource(R.drawable.check_icon);
					} else {
						circle_cart.setImageResource(R.drawable.circle_cart);
						circle_jiesuan.setImageResource(R.drawable.circle_cart);
					}

					for (int i = 0; i < arrayList_1.size(); i++) {
						Cart cart = arrayList_1.get(i);
						cart.type = 1;
						arrayList_delete.add(cart);
					}
					adapter.setData(arrayList_delete);
					setListViewHeightBasedOnChildren(listview); // 计算高度

					countPrice.setText("¥" + getPrice(arrayList_1) + "");// 显示价格
				} else {
					isComplete = false;
					complete_cart.setText("编辑");

					adapter.setData(arrayList_0);// 更新适配器数据

					arrayList_delete = new ArrayList<Cart>();

					for (int i = 0; i < arrayList_0.size(); i++) {
						Cart cart = arrayList_0.get(i);
						cart.type = 0;
						arrayList_delete.add(cart);
					}
					adapter.setData(arrayList_delete);
					setListViewHeightBasedOnChildren(listview);
					// 切换的时候判断是否是全选状态
					boolean isAll = isAllSelected(arrayList_0);
					if (isAll) {
						circle_cart.setImageResource(R.drawable.check_icon);
						circle_jiesuan.setImageResource(R.drawable.check_icon);
					} else {
						circle_cart.setImageResource(R.drawable.circle_cart);
						circle_jiesuan.setImageResource(R.drawable.circle_cart);
					}

					countPrice.setText("¥" + getPrice(arrayList_0) + "");// 显示价格
				}
			}
		});
		// 全选、取消
		circle_cart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!selectAll) {
					selectedAll(true, complete_cart);
					circle_cart.setImageResource(R.drawable.check_icon);
					circle_jiesuan.setImageResource(R.drawable.check_icon);
					selectAll = true;

				} else {
					selectAll = false;
					selectedAll(false, complete_cart);
					circle_cart.setImageResource(R.drawable.circle_cart);
					circle_jiesuan.setImageResource(R.drawable.circle_cart);

					countPrice.setText("¥" + "0.00");// 显示价格

				}
				// 切换布局的时候xianshi
				if (complete_cart.getText().toString().equals("完成")) {
					countPrice.setText("¥" + getPrice(arrayList_1) + "");// 显示价格
				} else if (complete_cart.getText().toString().equals("编辑")) {
					countPrice.setText("¥" + getPrice(arrayList_0) + "");// 显示价格
				}

			}
		});
		// 全选、取消
		circle_jiesuan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!selectAll) {

					selectedAll(true, complete_cart);
					circle_cart.setImageResource(R.drawable.check_icon);
					circle_jiesuan.setImageResource(R.drawable.check_icon);
					selectAll = true;

				} else {
					selectAll = false;
					selectedAll(false, complete_cart);
					circle_cart.setImageResource(R.drawable.circle_cart);
					circle_jiesuan.setImageResource(R.drawable.circle_cart);

					countPrice.setText("¥" + "0.00");// 显示价格
				}

				if (complete_cart.getText().toString().equals("完成")) {
					countPrice.setText("¥" + getPrice(arrayList_1) + "");// 显示价格
				} else if (complete_cart.getText().toString().equals("编辑")) {
					countPrice.setText("¥" + getPrice(arrayList_0) + "");// 显示价格
				}

			}
		});

		// 结算
		balance.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				StringBuilder sb = new StringBuilder();

				double countPrice = 0;

				BigDecimal bd3 = new BigDecimal("0.00");
				BigDecimal bd1 = null;
				BigDecimal bd2 = null;
				if (complete_cart.getText().toString().equals("完成")) {

					for (int i = 0; i < arrayList_1.size(); i++) {
						Cart cart = arrayList_1.get(i);
						if (cart.select) {
							sb.append(cart.id
									+ "#"
									+ cart.number
									+ "#"
									+ new BigDecimal(cart.price).multiply(
											new BigDecimal(cart.number))
											.toString() + ",");

							bd1 = new BigDecimal(cart.price);
							bd2 = new BigDecimal(cart.number);

							bd1 = bd1.multiply(bd2);

							bd3 = bd3.add(bd1);

						}
					}

				} else if (complete_cart.getText().toString().equals("编辑")) {
					for (int i = 0; i < arrayList_0.size(); i++) {
						Cart cart = arrayList_0.get(i);
						if (cart.select) {

							sb.append(cart.id
									+ "#"
									+ cart.number
									+ "#"
									+ new BigDecimal(cart.price).multiply(
											new BigDecimal(cart.number))
											.toString() + ",");

							bd1 = new BigDecimal(cart.price);
							bd2 = new BigDecimal(cart.number);

							bd1 = bd1.multiply(bd2);

							bd3 = bd3.add(bd1); // 保留两位小数
						}
					}
				}
				if (!TextUtils.isEmpty(sb.toString())) {

					System.out.println("sb-----------" + sb.toString());
					xutilRequestBalance(sb.toString(), bd3.toString());
				} else {
					Toast.makeText(getActivity(), "请选择结算商品", 1).show();
				}

			}
		});

		// 去逛逛
		goto_shopcart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(),
						GoodsListActivity.class);
				intent.putExtra("title", "推荐");
				intent.putExtra("status", "1");
				startActivity(intent);
			}
		});
		// 点击刷新
		refresh_shopcart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				xutilRequest(1);
				System.out.println(" 点击刷新------------");
			}
		});

		// 点击猜你喜欢进入商品详情
		gridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),
						GoodsDataActivity.class);

				intent.putExtra("id", arrayListGrid.get(arg2).getId() + "");
				startActivity(intent);
			}
		});
	}

	// 结算
	private void xutilRequestBalance(String idsAndnums, String totalAmount) {
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("idsAndnums", idsAndnums); // 拼接参数
		params.addBodyParameter("customer_id", SpfUtil.getId() + ""); // 用户id

		params.addBodyParameter("totalAmount", totalAmount); // 商品总数
		System.out.println(SpfUtil.getId() + "用户id---------" + "idsAndnums"
				+ idsAndnums + "" + totalAmount);
		httpUtils.send(HttpMethod.POST, GlobalUrl.balanceCart, params,
				new RequestCallBack<String>() {

					private CustomProgressDialog dialog;

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						dialog.dismiss();
						try {
							JSONObject obj = new JSONObject(arg0.result);

							if (obj.optBoolean("success")) {
								System.out.println("obj--结算-------------"
										+ obj.toString());
								
								
								Intent intent = new Intent(getActivity(),
										ConfirmActivity.class);
								intent.putExtra("obj", arg0.result);
								intent.putExtra("orderType", 1);
								startActivityForResult(intent, 9125);
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {
						dialog = new CustomProgressDialog(getActivity(),
								"提交中。。。", R.anim.frame2);
						dialog.show();
					}
				});
	}

	public void setGussYourLikeData() {
		String result = MyApplication.aCache.getAsString("guesslike");
		Gson gson = new Gson();
		Type listType = new TypeToken<List<GuessYouLikeBean>>() {
		}.getType();
		arrayListGrid = gson.fromJson(result, listType);
		if (arrayListGrid != null) {
			gridviewAdapter.setData(arrayListGrid);
			sv.smoothScrollTo(0, 0);
			sv.scrollBy(0, 0);
		}
	}

	// 猜你喜欢
	public void guessYouLike() {
		// app/goods/guessYouLike

		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		System.out.println("用户Id============" + SpfUtil.getId());
		if (SpfUtil.getId() != 0) {

			params.addBodyParameter("customerId", SpfUtil.getId() + "");
		}
		httpUtils.send(HttpMethod.POST, GlobalUrl.guessYouLike, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {

						// setGussYourLikeData();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {

						System.out.println("猜你喜欢" + arg0.result);

						MyApplication.aCache.put("guesslike", arg0.result);
						setGussYourLikeData();

					}

				});

	}

	/**
	 * 判断是否购物车中所有的商品全部被选中
	 * 
	 * @return true所有条目全部被选中 false还有条目没有被选中
	 */
	private boolean isAllSelected(List<Cart> arrayList) {
		boolean flag = true;
		for (int i = 0; i < arrayList.size(); i++) {
			if (arrayList.get(i).select) {
				flag = true;
				continue;
			} else {
				return false;
			}
		}
		return flag;
	}

	// 全选或全取消
	private void selectedAll(boolean visibility, TextView complete_cart) {
		if (complete_cart.getText().equals("完成")) {
			for (int i = 0; i < arrayList_1.size(); i++) {
				arrayList_1.get(i).select = visibility;
			}
		} else if (complete_cart.getText().equals("编辑")) {
			for (int i = 0; i < arrayList_0.size(); i++) {
				arrayList_0.get(i).select = visibility;
			}
		}

		adapter.notifyDataSetChanged();
	}

	public void findViewById() {

		srf = (SwipeRefreshLayout) view.findViewById(R.id.swrf_shopcart);
		goto_shopcart = (TextView) view.findViewById(R.id.goTo_shopcart);
		refresh_shopcart = (TextView) view.findViewById(R.id.refresh_shopcart);
		layout_nodata_tip = (LinearLayout) view
				.findViewById(R.id.tip_nodata_shopcart);
		layout_nonet_tip = (LinearLayout) view
				.findViewById(R.id.tip_nonet_shopcart);

		circle_jiesuan = (ImageView) view
				.findViewById(R.id.circle_jiesuan_cart);
		tv_title_logo = (TextView) view.findViewById(R.id.tv_title_logo);
		gridview = (MyGridview) view.findViewById(R.id.gridview_cart);
		listview = (ListView) view.findViewById(R.id.cart_shopping_listview);
		sv = (ScrollView) view.findViewById(R.id.myscrollview_cart);

		complete_cart = (TextView) view.findViewById(R.id.complete_cart);
		circle_cart = (ImageView) view.findViewById(R.id.circle_cart);
		balance = (TextView) view.findViewById(R.id.balance_cart);

		countPrice = (TextView) view.findViewById(R.id.countPrice_shopcart);

	}

	private void initView() {
		// TODO Auto-generated method stub
		layout_nonet_tip.setVisibility(View.GONE);
		layout_nodata_tip.setVisibility(View.GONE);

		tv_title_logo.setText("购物车");
		sv.smoothScrollTo(0, 0);
		sv.scrollBy(0, 0);
		// listview.setSelection(1);

		// 下拉刷新
		srf.setOnRefreshListener(this);

		srf.setColorSchemeResources(android.R.color.holo_orange_light,
				android.R.color.holo_orange_dark,
				android.R.color.holo_red_light, android.R.color.holo_red_dark);
		// tv_title_logo.setFocusable(true);
		// tv_title_logo.setFocusableInTouchMode(true);
		// tv_title_logo.requestFocus();

	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		// TODO Auto-generated method stub
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			sv.smoothScrollTo(0, 0);
			// xutilRequest(1);
			System.out.println("aaaaaaaaa");
			if (IsNetWorkConnected.isNetworkConnected(getActivity())) {
				layout_nonet_tip.setVisibility(View.GONE);
				xutilRequest(1);
			} else {
				layout_nonet_tip.setVisibility(View.VISIBLE);
				layout_nodata_tip.setVisibility(View.GONE);
			}
		}
	}

	public void setView() {
		if (arrayList_0.size() == 0 && arrayList_1.size() == 0) {
			layout_nonet_tip.setVisibility(View.GONE);
			layout_nodata_tip.setVisibility(View.VISIBLE);
		} else {
			layout_nonet_tip.setVisibility(View.GONE);
			layout_nodata_tip.setVisibility(View.GONE);
		}
	}

	public void initAdapter() {

		// listview.setEmptyView(layout_nodata_tip);

		adapter = new ShopCartAdapter(getActivity(), arrayList_0, handler);
		listview.setAdapter(adapter);

		gridviewAdapter = new ShopCartGridViewApater(getActivity(),
				arrayListGrid);
		gridview.setAdapter(gridviewAdapter);

	}

	public void initData() {

		// arrayList_0.add(new Cart(0)); //
		// arrayList_0.add(new Cart(0)); //
		// arrayList_0.add(new Cart(0)); //
		// arrayList_0.add(new Cart(0)); // //
		// arrayList_1.add(new Cart(1)); //
		// arrayList_1.add(new Cart(1)); //
		// arrayList_1.add(new Cart(1)); //
		// arrayList_1.add(new Cart(1));

		// gridviewAdapter.setData(arrayListGrid);

		xutilRequest(1);
		if (IsNetWorkConnected.isNetworkConnected(getActivity())) {
			xutilRequest(1);
		} else {
			layout_nonet_tip.setVisibility(View.VISIBLE);
			layout_nodata_tip.setVisibility(View.GONE);
		}
		guessYouLike();// 猜你喜欢
	}

	// 解析数据
	public void setData(int what) {
		arrayList_0.clear();
		arrayList_1.clear();
		JSONArray array = MyApplication.aCache.getAsJSONArray("cart");
		if (array != null) {
			try {
				for (int i = 0; i < array.length(); i++) {

					JSONObject obj = array.getJSONObject(i);
					JSONObject jsonObj = (JSONObject) obj.get("product");

					String picture = jsonObj.optString("productPicture").split(
							",")[0];

					String name = jsonObj.optString("productName");
					String description = jsonObj.optString("description");
					String oldPrice = jsonObj.optString("marketPrice"); // 原始价格
																		// 8000
					String price = jsonObj.optString("actualPrice");// 实际价格 7000

					int number = obj.optInt("productNum");
					String id = obj.optString("id");

					arrayList_0.add(new Cart(id, picture, name, price,
							oldPrice, description, number, 0)); // 初始化两中布局的集合

					arrayList_1.add(new Cart(id, picture, name, price,
							oldPrice, description, number, 1));
				}

				if (what == 12) { // 点击删除的时候重新刷新页面 ,加载第二种布局

					adapter.setData(arrayList_1);

				} else {
					adapter.setData(arrayList_0);

				}
				setListViewHeightBasedOnChildren(listview);

			} catch (Exception e) {

			}
		}
		countPrice.setText("¥ 0.00");// 刷新之后价格重置

		// 刷新数据之后判断是否全选
		selectAll = false;
		boolean isAll = false;
		if (complete_cart.getText().toString().equals("完成")) {

			isAll = isAllSelected(arrayList_1);

		} else if (complete_cart.getText().toString().equals("编辑")) {
			isAll = isAllSelected(arrayList_0);

		}

		if (isAll) {
			circle_cart.setImageResource(R.drawable.check_icon);
			circle_jiesuan.setImageResource(R.drawable.check_icon);
		} else {
			circle_cart.setImageResource(R.drawable.circle_cart);
			circle_jiesuan.setImageResource(R.drawable.circle_cart);
		}

		srf.setRefreshing(false);

		sv.smoothScrollTo(0, 0);
		sv.scrollBy(0, 0);
	}

	// 查询购物车
	private void xutilRequest(final int what) {

		final CustomProgressDialog dialog = new CustomProgressDialog(
				getActivity(), "获取中。。。", R.anim.frame2);
		dialog.show();
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("cart_id", SpfUtil.getCartId() + "");
		System.out.println("SpfUtil.getCartId()" + SpfUtil.getCartId());
		httpUtils.send(HttpMethod.POST, GlobalUrl.getDetaiFromCart, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						dialog.dismiss();
						setData(what);
						setView();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						dialog.dismiss();
						try {
							JSONArray obj = new JSONArray(arg0.result);

							System.out.println("obj--shop-------------"
									+ obj.toString());
							MyApplication.aCache.put("cart", obj);

							setData(what);
							setView();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {

					}
				});
	}

	// 重新计算组件高度
	public void setListViewHeightBasedOnChildren(ListView listView) {
		if (listView == null)
			return;
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}
		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

	// 下拉刷新
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		if (complete_cart.getText().toString().equals("完成")) {

			xutilRequest(12); // 编辑状态下的刷新
		} else if (complete_cart.getText().toString().equals("编辑")) {
			xutilRequest(1);

		}

	}

	// 还原购物车
	private void xutilOrderToCart(String orderId) {

		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		params.addBodyParameter("orderId", orderId);
		params.addBodyParameter("cartId", SpfUtil.getCartId() + "");
		System.out.println("orderId--" + orderId + "cartId"
				+ SpfUtil.getCartId());
		httpUtils.send(HttpMethod.POST, GlobalUrl.orderToCart, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						System.out.println(arg1);

					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						try {

							JSONObject obj = new JSONObject(arg0.result);
							if (obj.optBoolean("success")) {
								xutilRequest(1);
							}
							System.out.println("arg0.result======"
									+ arg0.result);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onStart() {

					}
				});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO 还原购物车
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == 30054 &&requestCode == 9125 && data != null) {
			Bundle bundle = data.getExtras();
			// 283
			String orderId = bundle.getString("orderId"); // 订单id

			xutilOrderToCart(orderId);
		}
	}

}
