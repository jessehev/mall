package com.huayi.mall.ui.fragment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.huayi.mall.R;
import com.huayi.mall.activity.AddressActivity;
import com.huayi.mall.activity.SearchActivity;
import com.huayi.mall.base.AddressBean;
import com.huayi.mall.base.FragmentFactoryHome;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.NoScrollViewPager;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class HomeFragment extends Fragment {

	private String[] size = { "1", "2" };
	private RelativeLayout inhome;// 国内
	private RelativeLayout outhome;// 国外
	private ImageView inhomeimage;
	private ImageView outhomeimage;
	private FrameLayout mapger;
	private HomeinFragment homeinFragment = new HomeinFragment();
	private OutSideFragment outSideFragment;
	private FragmentManager fragmentManager;
	private int i = 0;// 判断是否第一次加载海外贱货
	private int b = 0;// 判断是否第一次进去搜索
	private LinearLayout search;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view = View.inflate(getActivity(), R.layout.homefragment, null);
		search = (LinearLayout) view.findViewById(R.id.search);
		mapger = (FrameLayout) view.findViewById(R.id.outneiviewpager);
		inhome = (RelativeLayout) view.findViewById(R.id.inhome);
		outhome = (RelativeLayout) view.findViewById(R.id.outhome);
		inhomeimage = (ImageView) view.findViewById(R.id.inhomeimage);
		outhomeimage = (ImageView) view.findViewById(R.id.outhomeimage);
		fragmentManager = getFragmentManager();

		fragmentManager.beginTransaction()
				.replace(R.id.outneiviewpager, homeinFragment)

				.commit();
		initdata();
		onClick();
		return view;
	}

	private void onClick() {
		// TODO Auto-generated method stub
		search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				startActivity(new Intent(getActivity(), SearchActivity.class));
			}
		});
	}

	private void initdata() {
		// TODO Auto-generated method stub

		inhome.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (i == 1) {
					inhomeimage.setImageResource(R.drawable.homejin);
					outhomeimage.setImageResource(R.drawable.outside);
					fragmentManager.beginTransaction()
							.replace(R.id.outneiviewpager, homeinFragment)

							.commit();
				}
			}
		});

		outhome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				outSideFragment = new OutSideFragment();
				if (i == 0) {
					fragmentManager.beginTransaction()
							.replace(R.id.outneiviewpager, outSideFragment)

							.commit();

					i = 1;
				} else {
					fragmentManager.beginTransaction()
							.replace(R.id.outneiviewpager, outSideFragment)
							.show(outSideFragment).commit();
				}
				inhomeimage.setImageResource(R.drawable.home);
				outhomeimage.setImageResource(R.drawable.outsidejin);
			}
		});

	}

}
