package com.huayi.mall.ui.fragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.huayi.mall.GoodsListActivity;
import com.huayi.mall.R;
import com.huayi.mall.activity.AddressActivity;
import com.huayi.mall.activity.AdviceActivity;
import com.huayi.mall.activity.BrandActivity;
import com.huayi.mall.activity.CollecttionActivity;
import com.huayi.mall.activity.LoginActivity;
import com.huayi.mall.activity.MiaoKillActivity;
import com.huayi.mall.activity.MyApplication;
import com.huayi.mall.activity.MyOrdernActivity;
import com.huayi.mall.activity.PointActivity;
import com.huayi.mall.activity.RechargeActivity;
import com.huayi.mall.activity.SerViceActivity2;
import com.huayi.mall.activity.SettingActivity;
import com.huayi.mall.activity.TicketActivity;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.util.ACache;
import com.huayi.mall.util.ImagePath;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.CustomProgressDialog;
import com.huayi.mall.view.MyGridview;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MyFragment extends Fragment {

	/* 请求码 */
	private static final int IMAGE_REQUEST_CODE = 0;
	private static final int SELECT_PIC_KITKAT = 3;
	private static final int CAMERA_REQUEST_CODE = 1;
	private static final int RESULT_REQUEST_CODE = 2;
	// 图片储存路径
	private static String IMAGE_FILE_NAME = "face.jpg";

	private MyGridAdapter myGridAdapter = new MyGridAdapter();
	private MyGridview mygrid;
	private String[] name = { "优惠券", "我的账户", "秒杀", "进入品牌", "我的足迹", "帮助反馈" };
	private int[] image = { R.drawable.youhui, R.drawable.wodezhanghu,
			R.drawable.miaokill, R.drawable.jinrixinp, R.drawable.cainixihuan,
			R.drawable.bangzhufankui };
	private LinearLayout shucang;
	private LinearLayout jifen;
	private LinearLayout address;
	private LinearLayout order;
	private LinearLayout daifukuan;
	private LinearLayout daishouhuo;
	private LinearLayout daipinjia;
	private LinearLayout shouhou;
	private ImageView setting;
	private ACache aCache;
	private TextView addrTotal;
	private TextView proTotal;
	private TextView credit;
	private TextView nickename;
	private ImageView imaggehead;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view = View.inflate(getActivity(), R.layout.myfragment, null);
		mygrid = (MyGridview) view.findViewById(R.id.mygrid);
		shucang = (LinearLayout) view.findViewById(R.id.shouchang);
		jifen = (LinearLayout) view.findViewById(R.id.jifen);
		address = (LinearLayout) view.findViewById(R.id.address);
		order = (LinearLayout) view.findViewById(R.id.order);
		daifukuan = (LinearLayout) view.findViewById(R.id.daifukuan);
		daishouhuo = (LinearLayout) view.findViewById(R.id.daishouhuo);
		daipinjia = (LinearLayout) view.findViewById(R.id.daipinjia);
		shouhou = (LinearLayout) view.findViewById(R.id.shouhou);
		setting = (ImageView) view.findViewById(R.id.setting);
		addrTotal = (TextView) view.findViewById(R.id.addrTotal);
		proTotal = (TextView) view.findViewById(R.id.proTotal);
		nickename = (TextView) view.findViewById(R.id.nickename);
		credit = (TextView) view.findViewById(R.id.credit);
		ScrollView scollview = (ScrollView) view
				.findViewById(R.id.scollview_myfragment);
		scollview.smoothScrollTo(0, 0);
		// 创建默认的ImageLoader配置参数
		// Initialize ImageLoader with configuration.
		// SystemBar.initSystemBar(getActivity(), "#DF0834");
		// SystemBar.setTranslucentStatus(getActivity(), true);

		imaggehead = (ImageView) view.findViewById(R.id.imaggehead);
		aCache = ACache.get(getActivity());
		onClick();

		return view;
	}

	private void onClick() {

		// 头像点击
		imaggehead.setOnClickListener(new OnClickListener() {

			private Dialog dialog2;

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (SpfUtil.getId() == 0) {
					Intent intent = new Intent(getActivity(),
							LoginActivity.class);
					startActivity(intent);
					return;
				}

				LinearLayout lin_dailog_item = (LinearLayout) View.inflate(
						getActivity(), R.layout.dialog_choosephoto, null);
				dialog2 = new AlertDialog.Builder(getActivity()).create();
				dialog2.show();
				WindowManager.LayoutParams params = dialog2.getWindow()
						.getAttributes();
				WindowManager wm = (WindowManager) getActivity()
						.getSystemService(Context.WINDOW_SERVICE);
				int height = wm.getDefaultDisplay().getHeight() / 4;
				int weith = wm.getDefaultDisplay().getWidth() / 4 * 3;
				params.width = weith;
				params.height = height;
				dialog2.getWindow().setContentView(lin_dailog_item);
				dialog2.getWindow().setAttributes(params);
				dialog2.getWindow()
						.clearFlags(
								WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
										| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
				dialog2.getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
				RadioGroup rg_photo = (RadioGroup) lin_dailog_item
						.findViewById(R.id.rg_photo);
				rg_photo.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						switch (checkedId) {
						case R.id.fromcamera:
							Intent intentFromCapture = new Intent(
									MediaStore.ACTION_IMAGE_CAPTURE);
							// 判断存储卡是否可以用，可用进行存储

							intentFromCapture.putExtra(MediaStore.EXTRA_OUTPUT,
									Uri.fromFile(new File(Environment
											.getExternalStorageDirectory(),
											IMAGE_FILE_NAME)));

							startActivityForResult(intentFromCapture,
									CAMERA_REQUEST_CODE);

							dialog2.dismiss();
							break;
						case R.id.fromphoto:

							Intent intent1 = new Intent(
									Intent.ACTION_GET_CONTENT);
							intent1.addCategory(Intent.CATEGORY_OPENABLE);
							intent1.setType("image/*");
							if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
								startActivityForResult(intent1,
										SELECT_PIC_KITKAT);
							} else {
								startActivityForResult(intent1,
										IMAGE_REQUEST_CODE);
							}
							dialog2.dismiss();
							break;

						default:
							break;
						}
					}
				});

			}
		});

		// TODO Auto-generated method stub
		mygrid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				switch (arg2) {
				case 0:// 优惠券
					startActivity(new Intent(getActivity(),
							TicketActivity.class));
					break;
				case 1:// 我的账户
					startActivity(new Intent(getActivity(),
							RechargeActivity.class));
					break;
				case 2:// 秒杀
					startActivity(new Intent(getActivity(),
							MiaoKillActivity.class));
					break;
				case 3:// 进入品牌
					Intent intent111 = new Intent(getActivity(),
							BrandActivity.class);

					startActivity(intent111);
					break;
				case 4:// 我的足迹
					if (SpfUtil.getId() == 0) {
						startActivity(new Intent(getActivity(),
								LoginActivity.class));
					} else {
						Intent intent1 = new Intent(getActivity(),
								GoodsListActivity.class);
						intent1.putExtra("title", "我的足迹");
						intent1.putExtra("id", SpfUtil.getId() + "");
						startActivity(intent1);
					}
					break;
				case 5:// 帮助反馈
					startActivity(new Intent(getActivity(),
							AdviceActivity.class));
					break;

				default:
					break;
				}
			}
		});

		// 收藏的商品
		shucang.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (SpfUtil.getId() == 0) {
					startActivity(new Intent(getActivity(), LoginActivity.class));
				} else {
					startActivity(new Intent(getActivity(),
							CollecttionActivity.class));
				}
			}
		});

		// 积分

		jifen.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (SpfUtil.getId() == 0) {
					startActivity(new Intent(getActivity(), LoginActivity.class));
				} else {
					startActivity(new Intent(getActivity(), PointActivity.class));
				}
			}
		});

		// 地址管理

		address.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (SpfUtil.getId() == 0) {
					startActivity(new Intent(getActivity(), LoginActivity.class));
				} else {
					startActivity(new Intent(getActivity(),
							AddressActivity.class));
				}
			}
		});
		// 查看订单

		order.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (SpfUtil.getId() == 0) {
					startActivity(new Intent(getActivity(), LoginActivity.class));
				} else {
					startActivity(new Intent(getActivity(),
							MyOrdernActivity.class));
				}
			}
		});
		// 代付款

		daifukuan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (SpfUtil.getId() == 0) {
					startActivity(new Intent(getActivity(), LoginActivity.class));
				} else {
					Intent intent = new Intent(getActivity(),
							MyOrdernActivity.class);
					intent.putExtra("order", 2);

					startActivity(intent);
				}
			}
		});
		// 收货

		daishouhuo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (SpfUtil.getId() == 0) {
					startActivity(new Intent(getActivity(), LoginActivity.class));
				} else {
					Intent intent = new Intent(getActivity(),
							MyOrdernActivity.class);
					intent.putExtra("order", 4);
					startActivity(intent);
				}
			}
		});
		// 评价
		daipinjia.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (SpfUtil.getId() == 0) {
					startActivity(new Intent(getActivity(), LoginActivity.class));
				} else {
					Intent intent = new Intent(getActivity(),
							MyOrdernActivity.class);
					intent.putExtra("order", 5);
					startActivity(intent);
				}
			}

		});
		// 售后

		shouhou.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (SpfUtil.getId() == 0) {
					startActivity(new Intent(getActivity(), LoginActivity.class));
				} else {
					// TODO Auto-generated method stub
					Intent intent = new Intent(getActivity(),
							SerViceActivity2.class);
					intent.putExtra("titlename", "售后退款");
					intent.putExtra("serviceStatus", 1);
					startActivity(intent);
				}
			}
		});
		// 设置
		setting.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (!SpfUtil.getLoginStatus()) {
					Intent intent = new Intent(getActivity(),
							LoginActivity.class);
					startActivity(intent);
				} else {
					startActivity(new Intent(getActivity(),
							SettingActivity.class));
				}

			}
		});
	}

	private void initdata() {
		// TODO Auto-generated method stub
		mygrid.setAdapter(myGridAdapter);
		if (SpfUtil.getId() != 0) {
			getUserInfo();
		}

	}

	class MyGridAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.length;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			arg1 = View.inflate(getActivity(), R.layout.mygr_item, null);
			ImageView mygrimage = (ImageView) arg1.findViewById(R.id.mygrimage);
			TextView mygrtext = (TextView) arg1.findViewById(R.id.mygrtext);
			mygrimage.setImageResource(image[arg0]);
			mygrtext.setText(name[arg0]);
			return arg1;
		}
	}

	// 获取用户信息
	public void getUserInfo() {
		// final CustomProgressDialog dialog = new CustomProgressDialog(
		// getActivity(), "获取中。。。", R.anim.frame2);
		// dialog.show();
		HttpUtils httpUtils = new HttpUtils();
		RequestParams params = new RequestParams();
		System.out.println(SpfUtil.getId() + "======================");
		params.addBodyParameter("id", SpfUtil.getId() + "");
		httpUtils.send(HttpMethod.POST, GlobalUrl.getInfo, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						// dialog.dismiss();
						Toast.makeText(getActivity(), "获取失败", 0).show();
						System.out.println(arg1);
						getUserInfofcach();

					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						System.out.println("用户信息=====" + arg0.result);
						MyApplication.aCache.put("userinfo", arg0.result);

						getUserInfofcach();

						// dialog.dismiss();
					}

					@Override
					public void onStart() {

					}
				});
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initdata();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 结果码不等于取消时候
		if (resultCode != getActivity().RESULT_CANCELED) {
			switch (requestCode) {
			case IMAGE_REQUEST_CODE:
				startPhotoZoom(data.getData());
				break;
			case SELECT_PIC_KITKAT:
				startPhotoZoom(data.getData());
				break;
			case CAMERA_REQUEST_CODE:

				File tempFile = new File(
						Environment.getExternalStorageDirectory(),
						IMAGE_FILE_NAME);
				startPhotoZoom(Uri.fromFile(tempFile));

				break;
			case RESULT_REQUEST_CODE:
				setImageToView(data);
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * 裁剪图片方法实现
	 * 
	 * @param uri
	 */
	public void startPhotoZoom(Uri uri) {
		if (uri == null) {
			Log.i("tag", "The uri is not exist.");
			return;
		}

		Intent intent = new Intent("com.android.camera.action.CROP");
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
			String url = ImagePath.getImageAbsolutePath(getActivity(), uri);
			System.out.println(url);
			intent.setDataAndType(Uri.fromFile(new File(url)), "image/*");
		} else {
			intent.setDataAndType(uri, "image/*");
		}
		// 设置裁剪
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 200);
		intent.putExtra("outputY", 200);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, RESULT_REQUEST_CODE);

	}

	/**
	 * 保存裁剪之后的图片数据
	 * 
	 * @param picdata
	 */
	private void setImageToView(Intent data) {
		Bundle extras = data.getExtras();
		if (extras != null) {
			Bitmap photo = extras.getParcelable("data");
			/* Bitmap roundBitmap=ImageUtil.toRoundBitmap(photo); */
			String saveBitmap = saveBitmap(photo);

			HttpUtils httpUtils = new HttpUtils();
			RequestParams params = new RequestParams();
			System.out.println(SpfUtil.getId() + "======================");
			params.addBodyParameter("picture", new File(saveBitmap));

			httpUtils.send(HttpMethod.POST, GlobalUrl.uploadImage, params,
					new RequestCallBack<String>() {
						private CustomProgressDialog dialog;
						private String picuir;

						@Override
						public void onFailure(HttpException arg0, String arg1) {
							Toast.makeText(getActivity(), "获取失败", 0).show();
							System.out.println(arg1);
							getUserInfofcach();
						}

						@Override
						public void onSuccess(ResponseInfo<String> arg0) {
							try {
								JSONObject jsonObject = new JSONObject(
										arg0.result);
								picuir = jsonObject.getString("picture");
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							HttpUtils httpUtils = new HttpUtils();
							RequestParams params = new RequestParams();
							params.addBodyParameter("id", SpfUtil.getId() + "");
							params.addBodyParameter("headPicture", picuir);
							httpUtils.send(HttpMethod.POST,
									GlobalUrl.updateInfor, params,
									new RequestCallBack<String>() {
										@Override
										public void onFailure(
												HttpException arg0, String arg1) {
											dialog.dismiss();
											Toast.makeText(getActivity(),
													"获取失败", 0).show();
											System.out.println(arg1);
											getUserInfo();
										}

										@Override
										public void onSuccess(
												ResponseInfo<String> arg0) {
											getUserInfo();
											dialog.dismiss();

										}

										@Override
										public void onStart() {
											dialog = new CustomProgressDialog(
													getActivity(), "获取中。。。",
													R.anim.frame2);
											dialog.show();
										}
									});
						}
					});
		}
	}

	public String saveBitmap(Bitmap mBitmap) {
		File f = new File(Environment.getExternalStorageDirectory(), "face"
				+ ".jpg");
		try {
			f.createNewFile();
			FileOutputStream fOut = null;
			fOut = new FileOutputStream(f);
			mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
			fOut.flush();
			fOut.close();
			return f.getAbsolutePath();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void getUserInfofcach() {
		// 获取缓存用户信息
		JSONObject asJSONObject = MyApplication.aCache.getAsJSONObject("userinfo");
		if (asJSONObject != null) {
			try {
				proTotal.setText(asJSONObject.getJSONObject("otherInfo")
						.getString("proTotal"));
				addrTotal.setText(asJSONObject.getJSONObject("otherInfo")
						.getString("addrTotal"));
				credit.setText(asJSONObject.getString("credit"));
				nickename.setText(asJSONObject.getString("nickName"));
				ImageLoader.getInstance().displayImage(
						GlobalUrl.serviceHost1
								+ asJSONObject.getString("headPicture"),
						imaggehead, ImageLoaderCfg.options2);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
