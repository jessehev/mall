package com.huayi.mall.base;

import com.huayi.mall.R;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Ruby 设置状态栏颜色需要在initView里面调用initHeadBefoer();
 * 
 */
public class HaveHeadActivity extends BaseActivity {

	protected TextView mTitle;
	protected TextView mRight;
	protected ImageView mBack;

	@Override
	protected void initView() {
		iniHead();
	}

	@Override
	protected void initData() {
		iniHeadData();
	}

	@Override
	protected void iniHead() {
		mTitle = (TextView) findViewById(R.drawable.ic_launcher);
		
		mBack = (ImageView) findViewById(R.id.back);
	}

	@Override
	protected void iniHeadData() {
		mRight.setVisibility(View.INVISIBLE);
		mBack.setVisibility(View.GONE);

		setOnClickEvent();
	}

	private void setOnClickEvent() {
		mBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	protected void initHeadBefoer() {
		// SystemBar.initSystemBar(this, "#000000");
		// SystemBar.setTranslucentStatus(this, true);
	}
}
