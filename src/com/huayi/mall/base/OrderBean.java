package com.huayi.mall.base;

import java.util.ArrayList;
import java.util.List;

import com.huayi.mall.R.string;

public class OrderBean {
	String time ,order;
	int state;
	public List<InOrder>inorder = new ArrayList<OrderBean.InOrder>();
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getOrder() {
		return order;
	}
	public OrderBean() {
		super();

	}
	public OrderBean(String time, String order, String name, String price,
			int state) {
		super();
		this.time = time;
		this.order = order;

		this.state = state;
	}
	public void setOrder(String order) {
		this.order = order;
	}

	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	class InOrder{
		String name,price;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPrice() {
			return price;
		}

		public void setPrice(String price) {
			this.price = price;
		}

	} 
}
