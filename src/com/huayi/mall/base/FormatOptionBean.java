package com.huayi.mall.base;


import java.util.List;

/**
 * Created by dell on 2016/4/8.
 */
public class FormatOptionBean {

    /**
     * id : 2
     * formatName : 版型
     * formatOptions : [{"id":4,"formatOptionName":"修身型","format":null,"products":[]},{"id":5,"formatOptionName":"收腰型","format":null,"products":[]},{"id":6,"formatOptionName":"宽松型","format":null,"products":[]},{"id":7,"formatOptionName":"直筒型","format":null,"products":[]},{"id":8,"formatOptionName":"其他","format":null,"products":[]}]
     * goodsList : []
     */

    private int id;
    private String formatName;
    /**
     * id : 4
     * formatOptionName : 修身型
     * format : null
     * products : []
     */

    private List<FormatOptionsBean> formatOptions;
    private List<?> goodsList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFormatName() {
        return formatName;
    }

    public void setFormatName(String formatName) {
        this.formatName = formatName;
    }

    public List<FormatOptionsBean> getFormatOptions() {
        return formatOptions;
    }

    public void setFormatOptions(List<FormatOptionsBean> formatOptions) {
        this.formatOptions = formatOptions;
    }

    public List<?> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<?> goodsList) {
        this.goodsList = goodsList;
    }

    public static class FormatOptionsBean {
        private int id;
        private String formatOptionName;
        private Object format;
        private List<?> products;
        private int state;
        private int where=-1;
        public int getWhere() {
			return where;
		}

		public void setWhere(int where) {
			this.where = where;
		}

		public int getState() {
			return state;
		}

		public void setState(int state) {
			this.state = state;
		}

		public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFormatOptionName() {
            return formatOptionName;
        }

        public void setFormatOptionName(String formatOptionName) {
            this.formatOptionName = formatOptionName;
        }

        public Object getFormat() {
            return format;
        }

        public void setFormat(Object format) {
            this.format = format;
        }

        public List<?> getProducts() {
            return products;
        }

        public void setProducts(List<?> products) {
            this.products = products;
        }
    }}
