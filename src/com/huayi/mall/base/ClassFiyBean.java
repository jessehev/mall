package com.huayi.mall.base;

import java.util.ArrayList;
import java.util.List;

public class ClassFiyBean {
	String id,classifyName,classifyPicture;
	List<ClassFiyBean.children>children = new ArrayList<ClassFiyBean.children>();
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getClassifyName() {
		return classifyName;
	}
	public void setClassifyName(String classifyName) {
		this.classifyName = classifyName;
	}
	public String getClassifyPicture() {
		return classifyPicture;
	}
	public void setClassifyPicture(String classifyPicture) {
		this.classifyPicture = classifyPicture;
	}
	public List<ClassFiyBean.children> getChildren() {
		return children;
	}
	public void setChildren(List<ClassFiyBean.children> children) {
		this.children = children;
	}
	public class children{
		String id,classifyName,classifyPicture;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getClassifyName() {
			return classifyName;
		}

		public void setClassifyName(String classifyName) {
			this.classifyName = classifyName;
		}

		public String getClassifyPicture() {
			return classifyPicture;
		}

		public void setClassifyPicture(String classifyPicture) {
			this.classifyPicture = classifyPicture;
		}

	}
}
