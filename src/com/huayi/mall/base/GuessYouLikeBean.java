package com.huayi.mall.base;

import java.util.List;

public class GuessYouLikeBean {

    /**
     * id : 9
     * goodsName : 情趣内衣
     * goodsNumber : QQ00102
     * goodsPicture : /repository/qingquneiyi.png,/repository/qingquneiyi.png,/repository/qingquneiyi.png
     * goodsSynopsis : HW001
     * goodsInstruction : 情趣内衣
     * marketPrice : 3000.0
     * price : 3.0
     * click : 2005
     * saleTotalNumber : 5555
     * postage : 20
     * discount : 两折
     * startAddress : 国内
     * sendTime : 7-10天发货
     * status : null
     * brand : null
     * classify : null
     * shop : null
     * formats : []
     * evaluations : []
     */

    private int id;
    private String goodsName;
    private String goodsNumber;
    private String goodsPicture;
    private String goodsSynopsis;
    private String goodsInstruction;
    private double marketPrice;
    private double price;
    private int click;
    private int saleTotalNumber;
    private int postage;
    private String discount;
    private String startAddress;
    private String sendTime;
    private Object status;
    private Object brand;
    private Object classify;
    private Object shop;
    private List<?> formats;
    private List<?> evaluations;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getGoodsPicture() {
        return goodsPicture;
    }

    public void setGoodsPicture(String goodsPicture) {
        this.goodsPicture = goodsPicture;
    }

    public String getGoodsSynopsis() {
        return goodsSynopsis;
    }

    public void setGoodsSynopsis(String goodsSynopsis) {
        this.goodsSynopsis = goodsSynopsis;
    }

    public String getGoodsInstruction() {
        return goodsInstruction;
    }

    public void setGoodsInstruction(String goodsInstruction) {
        this.goodsInstruction = goodsInstruction;
    }

    public double getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(double marketPrice) {
        this.marketPrice = marketPrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getClick() {
        return click;
    }

    public void setClick(int click) {
        this.click = click;
    }

    public int getSaleTotalNumber() {
        return saleTotalNumber;
    }

    public void setSaleTotalNumber(int saleTotalNumber) {
        this.saleTotalNumber = saleTotalNumber;
    }

    public int getPostage() {
        return postage;
    }

    public void setPostage(int postage) {
        this.postage = postage;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getBrand() {
        return brand;
    }

    public void setBrand(Object brand) {
        this.brand = brand;
    }

    public Object getClassify() {
        return classify;
    }

    public void setClassify(Object classify) {
        this.classify = classify;
    }

    public Object getShop() {
        return shop;
    }

    public void setShop(Object shop) {
        this.shop = shop;
    }

    public List<?> getFormats() {
        return formats;
    }

    public void setFormats(List<?> formats) {
        this.formats = formats;
    }

    public List<?> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<?> evaluations) {
        this.evaluations = evaluations;
    }
}
