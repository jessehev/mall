package com.huayi.mall.base;



import java.util.List;

/**
 * Created by Administrator on 2016/4/27 0027.
 */
public class MiaoKillBean {


    /**
     * id : 1
     * startTime : 1462334400000
     * endTime : 1462338000000
     * over : false
     * goodsList : [{"id":2,"goodsName":"休闲裤","goodsNumber":"XIK001","goodsPicture":"/repository/xiuxianku.png,/repository/xiuxianku.png,/repository/xiuxianku.png","bigPicture":"/repository/xiuxianku.png,/repository/xiuxianku.png,/repository/xiuxianku.png","goodsSynopsis":"XIK001","goodsInstruction":"春季休闲裤裤","marketPrice":"1998.00","price":"198.00","click":2396,"saleTotalNumber":1501,"postage":15,"discount":"一折","startAddress":"海外","sendTime":"7-10天发货","status":null,"createTime":1454306534000,"brand":null,"classify":null,"shop":null,"market":null,"formats":[],"evaluations":[],"secKill":null},{"id":3,"goodsName":"小米手机","goodsNumber":"SJ00102","goodsPicture":"/repositoryomi.png,/repositoryomi.png,/repositoryomi.png,/repositoryomi.png","bigPicture":"/repositoryomi.png,/repositoryomi.png,/repositoryomi.png,/repositoryomi.png","goodsSynopsis":"HW001","goodsInstruction":"小米手机","marketPrice":"2000.00","price":"900.00","click":2055,"saleTotalNumber":1234,"postage":15,"discount":"八折","startAddress":"海外","sendTime":"7-10天发货","status":null,"createTime":1459490546000,"brand":null,"classify":null,"shop":null,"market":null,"formats":[],"evaluations":[],"secKill":null},{"id":4,"goodsName":"华为手机","goodsNumber":"SJ00102","goodsPicture":"/repository/huawei.png,/repository/huawei.png,/repository/huawei.png","bigPicture":"/repository/huawei.png,/repository/huawei.png,/repository/huawei.png","goodsSynopsis":"HW001","goodsInstruction":"华为手机","marketPrice":"3000.00","price":"1800.00","click":2038,"saleTotalNumber":123,"postage":15,"discount":"一折","startAddress":"国内","sendTime":"7-10天发货","status":null,"createTime":1461304951000,"brand":null,"classify":null,"shop":null,"market":null,"formats":[],"evaluations":[],"secKill":null},{"id":5,"goodsName":"诺基亚手机","goodsNumber":"SJ00102","goodsPicture":"/repository/nuojiya.png,/repository/nuojiya.png,/repository/nuojiya.png","bigPicture":"/repository/nuojiya.png,/repository/nuojiya.png,/repository/nuojiya.png","goodsSynopsis":"HW001","goodsInstruction":"诺基亚手机","marketPrice":"500.00","price":"50.00","click":2024,"saleTotalNumber":231,"postage":15,"discount":"四折","startAddress":"国内","sendTime":"7-10天发货","status":null,"createTime":1459576956000,"brand":null,"classify":null,"shop":null,"market":null,"formats":[],"evaluations":[],"secKill":null}]
     */

    private int id;
    private long startTime;
    private long endTime;
    private boolean over;
    /**
     * id : 2
     * goodsName : 休闲裤
     * goodsNumber : XIK001
     * goodsPicture : /repository/xiuxianku.png,/repository/xiuxianku.png,/repository/xiuxianku.png
     * bigPicture : /repository/xiuxianku.png,/repository/xiuxianku.png,/repository/xiuxianku.png
     * goodsSynopsis : XIK001
     * goodsInstruction : 春季休闲裤裤
     * marketPrice : 1998.00
     * price : 198.00
     * click : 2396
     * saleTotalNumber : 1501
     * postage : 15
     * discount : 一折
     * startAddress : 海外
     * sendTime : 7-10天发货
     * status : null
     * createTime : 1454306534000
     * brand : null
     * classify : null
     * shop : null
     * market : null
     * formats : []
     * evaluations : []
     * secKill : null
     */

    private List<GoodsListBean> goodsList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public boolean isOver() {
        return over;
    }

    public void setOver(boolean over) {
        this.over = over;
    }

    public List<GoodsListBean> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<GoodsListBean> goodsList) {
        this.goodsList = goodsList;
    }

    public static class GoodsListBean {
        private int id;
        private String goodsName;
        private String goodsNumber;
        private String goodsPicture;
        private String bigPicture;
        private String goodsSynopsis;
        private String goodsInstruction;
        private String marketPrice;
        private String price;
        private int click;
        private int saleTotalNumber;
        private int postage;
        private String discount;
        private String startAddress;
        private String sendTime;
        private Object status;
        private long createTime;
        private Object brand;
        private Object classify;
        private Object shop;
        private Object market;
        private Object secKill;
        private List<?> formats;
        private List<?> evaluations;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsNumber() {
            return goodsNumber;
        }

        public void setGoodsNumber(String goodsNumber) {
            this.goodsNumber = goodsNumber;
        }

        public String getGoodsPicture() {
            return goodsPicture;
        }

        public void setGoodsPicture(String goodsPicture) {
            this.goodsPicture = goodsPicture;
        }

        public String getBigPicture() {
            return bigPicture;
        }

        public void setBigPicture(String bigPicture) {
            this.bigPicture = bigPicture;
        }

        public String getGoodsSynopsis() {
            return goodsSynopsis;
        }

        public void setGoodsSynopsis(String goodsSynopsis) {
            this.goodsSynopsis = goodsSynopsis;
        }

        public String getGoodsInstruction() {
            return goodsInstruction;
        }

        public void setGoodsInstruction(String goodsInstruction) {
            this.goodsInstruction = goodsInstruction;
        }

        public String getMarketPrice() {
            return marketPrice;
        }

        public void setMarketPrice(String marketPrice) {
            this.marketPrice = marketPrice;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public int getClick() {
            return click;
        }

        public void setClick(int click) {
            this.click = click;
        }

        public int getSaleTotalNumber() {
            return saleTotalNumber;
        }

        public void setSaleTotalNumber(int saleTotalNumber) {
            this.saleTotalNumber = saleTotalNumber;
        }

        public int getPostage() {
            return postage;
        }

        public void setPostage(int postage) {
            this.postage = postage;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getStartAddress() {
            return startAddress;
        }

        public void setStartAddress(String startAddress) {
            this.startAddress = startAddress;
        }

        public String getSendTime() {
            return sendTime;
        }

        public void setSendTime(String sendTime) {
            this.sendTime = sendTime;
        }

        public Object getStatus() {
            return status;
        }

        public void setStatus(Object status) {
            this.status = status;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public Object getBrand() {
            return brand;
        }

        public void setBrand(Object brand) {
            this.brand = brand;
        }

        public Object getClassify() {
            return classify;
        }

        public void setClassify(Object classify) {
            this.classify = classify;
        }

        public Object getShop() {
            return shop;
        }

        public void setShop(Object shop) {
            this.shop = shop;
        }

        public Object getMarket() {
            return market;
        }

        public void setMarket(Object market) {
            this.market = market;
        }

        public Object getSecKill() {
            return secKill;
        }

        public void setSecKill(Object secKill) {
            this.secKill = secKill;
        }

        public List<?> getFormats() {
            return formats;
        }

        public void setFormats(List<?> formats) {
            this.formats = formats;
        }

        public List<?> getEvaluations() {
            return evaluations;
        }

        public void setEvaluations(List<?> evaluations) {
            this.evaluations = evaluations;
        }
    }
}
