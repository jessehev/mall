package com.huayi.mall.base;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2016/4/27 0027.
 */
public class NewsBean implements Serializable{


    /**
     * pageSize : 10
     * currentPage : 1
     * totalPage : 2
     * totalCount : 11
     * pageIndex : {"beginIndex":1,"endIndex":2}
     * rows : [{"id":1,"newsName":"五一黄金周大放送","newsContent":"满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999","createTime":1460535435000},{"id":2,"newsName":"五一黄金周大放送","newsContent":"满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999","createTime":1460535435000},{"id":3,"newsName":"五一黄金周大放送","newsContent":"满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999","createTime":1460535435000},{"id":4,"newsName":"五一黄金周大放送","newsContent":"满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999","createTime":1460535435000},{"id":5,"newsName":"五一黄金周大放送","newsContent":"满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999","createTime":1460535435000},{"id":6,"newsName":"五一黄金周大放送","newsContent":"满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999","createTime":1460535435000},{"id":7,"newsName":"五一黄金周大放送","newsContent":"满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999","createTime":1460535435000},{"id":8,"newsName":"五一黄金周大放送","newsContent":"满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999","createTime":1460535435000},{"id":9,"newsName":"五一黄金周大放送","newsContent":"满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999","createTime":1460535435000},{"id":10,"newsName":"五一黄金周大放送","newsContent":"满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999","createTime":1460535435000}]
     */

    private int pageSize;
    private int currentPage;
    private int totalPage;
    private int totalCount;
    /**
     * beginIndex : 1
     * endIndex : 2
     */

    private PageIndexBean pageIndex;
    /**
     * id : 1
     * newsName : 五一黄金周大放送
     * newsContent : 满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999,满1000立即减999
     * createTime : 1460535435000
     */

    private List<RowsBean> rows;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public PageIndexBean getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(PageIndexBean pageIndex) {
        this.pageIndex = pageIndex;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class PageIndexBean implements Serializable{
        private int beginIndex;
        private int endIndex;

        public int getBeginIndex() {
            return beginIndex;
        }

        public void setBeginIndex(int beginIndex) {
            this.beginIndex = beginIndex;
        }

        public int getEndIndex() {
            return endIndex;
        }

        public void setEndIndex(int endIndex) {
            this.endIndex = endIndex;
        }
    }

    public static class RowsBean implements Serializable{
        private int id;
        private String newsName;
        private String newsContent;
        private long createTime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNewsName() {
            return newsName;
        }

        public void setNewsName(String newsName) {
            this.newsName = newsName;
        }

        public String getNewsContent() {
            return newsContent;
        }

        public void setNewsContent(String newsContent) {
            this.newsContent = newsContent;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }
    }
}
