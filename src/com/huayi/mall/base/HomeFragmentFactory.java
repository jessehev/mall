package com.huayi.mall.base;

import java.util.HashMap;

import android.support.v4.app.Fragment;

import com.huayi.mall.ui.fragment.HomeinFragment;
import com.huayi.mall.ui.fragment.OutSideFragment;


public class HomeFragmentFactory {

	private static HashMap<Integer, Fragment> fragmentCache = new HashMap<Integer, Fragment>();

	/**
	 * 根据位置获取对应位置的fragment对象
	 * 
	 * @param position
	 * @return
	 */
	public static Fragment create(int position) {
		Fragment fragment = null;
		if (fragment == null) {
			switch (position) {
			case 0:
				fragment = new HomeinFragment();
				break;
			case 1:
				fragment = new OutSideFragment();
				break;
			
			}
			fragmentCache.put(position, fragment);
		}
		return fragment;
	}

}