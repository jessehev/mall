package com.huayi.mall.base;

import java.util.List;

public class GoodsListBean  {
    /**
     * pageSize : 10
     * currentPage : 1
     * totalPage : 1
     * totalCount : 2
     * pageIndex : {"beginIndex":1,"endIndex":1}
     * rows : [{"id":1,"goodsName":"迷你牛仔裤","goodsNumber":"SJ00102","goodsPicture":"/repository/niuzaiku.png","goodsSynopsis":"HW001","goodsInstruction":"冬季牛仔裤","marketPrice":998,"price":98,"click":1231,"saleTotalNumber":533,"postage":0,"discount":"一折","startAddress":"海外","sendTime":"7-10天发货","status":null,"brand":null,"classify":null,"shop":null,"formats":[],"evaluations":[]},{"id":2,"goodsName":"休闲裤","goodsNumber":"XIK001","goodsPicture":"/repository/xiuxianku.png","goodsSynopsis":"XIK001","goodsInstruction":"春季休闲裤裤","marketPrice":1998,"price":198,"click":2000,"saleTotalNumber":1500,"postage":15,"discount":"一折","startAddress":"海外","sendTime":"7-10天发货","status":null,"brand":null,"classify":null,"shop":null,"formats":[],"evaluations":[]}]
     */

    private int pageSize;
    private int currentPage;
    private int totalPage;
    private int totalCount;
    /**
     * id : 1
     * goodsName : 迷你牛仔裤
     * goodsNumber : SJ00102
     * goodsPicture : /repository/niuzaiku.png
     * goodsSynopsis : HW001
     * goodsInstruction : 冬季牛仔裤
     * marketPrice : 998.0
     * price : 98.0
     * click : 1231
     * saleTotalNumber : 533
     * postage : 0
     * discount : 一折
     * startAddress : 海外
     * sendTime : 7-10天发货
     * status : null
     * brand : null
     * classify : null
     * shop : null
     * formats : []
     * evaluations : []
     */

    private java.util.List<RowsBean> rows;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public  class RowsBean {
        private int id;
        private String goodsName;
        private String goodsNumber;
        private String goodsPicture;
        private String goodsSynopsis;
        private String goodsInstruction;
        private double marketPrice;
        private double price;
        private int click;
        public String getSecKill() {
			return secKill;
		}

		public void setSecKill(String secKill) {
			this.secKill = secKill;
		}

		private String secKill;
        private int saleTotalNumber;
        private int postage;
        private String discount;
        private String startAddress;
        private String sendTime;
        private Object status;
        private Object brand;
        private Object classify;
        private Object shop;
        private java.util.List<?> formats;
        private java.util.List<?> evaluations;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsNumber() {
            return goodsNumber;
        }

        public void setGoodsNumber(String goodsNumber) {
            this.goodsNumber = goodsNumber;
        }

        public String getGoodsPicture() {
            return goodsPicture;
        }

        public void setGoodsPicture(String goodsPicture) {
            this.goodsPicture = goodsPicture;
        }

        public String getGoodsSynopsis() {
            return goodsSynopsis;
        }

        public void setGoodsSynopsis(String goodsSynopsis) {
            this.goodsSynopsis = goodsSynopsis;
        }

        public String getGoodsInstruction() {
            return goodsInstruction;
        }

        public void setGoodsInstruction(String goodsInstruction) {
            this.goodsInstruction = goodsInstruction;
        }

        public double getMarketPrice() {
            return marketPrice;
        }

        public void setMarketPrice(double marketPrice) {
            this.marketPrice = marketPrice;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getClick() {
            return click;
        }

        public void setClick(int click) {
            this.click = click;
        }

        public int getSaleTotalNumber() {
            return saleTotalNumber;
        }

        public void setSaleTotalNumber(int saleTotalNumber) {
            this.saleTotalNumber = saleTotalNumber;
        }

        public int getPostage() {
            return postage;
        }

        public void setPostage(int postage) {
            this.postage = postage;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getStartAddress() {
            return startAddress;
        }

        public void setStartAddress(String startAddress) {
            this.startAddress = startAddress;
        }

        public String getSendTime() {
            return sendTime;
        }

        public void setSendTime(String sendTime) {
            this.sendTime = sendTime;
        }

        public Object getStatus() {
            return status;
        }

        public void setStatus(Object status) {
            this.status = status;
        }

        public Object getBrand() {
            return brand;
        }

        public void setBrand(Object brand) {
            this.brand = brand;
        }

        public Object getClassify() {
            return classify;
        }

        public void setClassify(Object classify) {
            this.classify = classify;
        }

        public Object getShop() {
            return shop;
        }

        public void setShop(Object shop) {
            this.shop = shop;
        }

        public List<?> getFormats() {
            return formats;
        }

        public void setFormats(List<?> formats) {
            this.formats = formats;
        }

        public List<?> getEvaluations() {
            return evaluations;
        }

        public void setEvaluations(List<?> evaluations) {
            this.evaluations = evaluations;
        }
    }}
