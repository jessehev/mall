package com.huayi.mall.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class FragmentFactoryHome  extends FragmentPagerAdapter{
		
		private String[] tabArr={"1","2"};//存放标题
	
		public FragmentFactoryHome(FragmentManager fm) {
			super(fm);
			this.tabArr =tabArr;
		}

		public Fragment getItem(int arg0) {
			return HomeFragmentFactory.create(arg0);
		}

		public int getCount() {
			return tabArr.length;
		}

		public CharSequence getPageTitle(int position) {
			return tabArr[position];
		}
		
	
}
