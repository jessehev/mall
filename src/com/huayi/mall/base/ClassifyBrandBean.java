package com.huayi.mall.base;


import java.util.List;

public class ClassifyBrandBean {


    /**
     * pageSize : 10
     * currentPage : 1
     * totalPage : 2
     * totalCount : 14
     * pageIndex : {"beginIndex":1,"endIndex":2}
     * rows : [{"id":2,"brandName":"Nike","picture":"/repository/20160329101113.png","createTime":1459217471000,"recommend":false},{"id":3,"brandName":"阿迪达斯","picture":"/repository/20160329101235.png","createTime":1459217552000,"recommend":false},{"id":4,"brandName":"匡威","picture":"/repository/20160329101248.png","createTime":1459217565000,"recommend":false},{"id":5,"brandName":"杰克","picture":"/repository/20160329101305.png","createTime":1459217582000,"recommend":false},{"id":6,"brandName":"361度","picture":"/repository/20160329102049.png","createTime":1459218046000,"recommend":false},{"id":7,"brandName":"牛逼鞋子","picture":"/repository/20160329102118.png","createTime":1459218075000,"recommend":false},{"id":8,"brandName":"彪马","picture":"/repository/20160329102134.png","createTime":1459218091000,"recommend":false},{"id":9,"brandName":"布兰婷","picture":"/repository/20160329102151.png","createTime":1459218107000,"recommend":false},{"id":10,"brandName":"恩宝","picture":"/repository/20160329102202.png","createTime":1459218119000,"recommend":false},{"id":11,"brandName":"李宁","picture":"/repository/20160329102215.png","createTime":1459218132000,"recommend":false}]
     */

    private int pageSize;
    private int currentPage;
    private int totalPage;
    private int totalCount;
    /**
     * beginIndex : 1
     * endIndex : 2
     */

    private PageIndexBean pageIndex;
    /**
     * id : 2
     * brandName : Nike
     * picture : /repository/20160329101113.png
     * createTime : 1459217471000
     * recommend : false
     */

    private List<RowsBean> rows;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public PageIndexBean getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(PageIndexBean pageIndex) {
        this.pageIndex = pageIndex;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class PageIndexBean {
        private int beginIndex;
        private int endIndex;

        public int getBeginIndex() {
            return beginIndex;
        }

        public void setBeginIndex(int beginIndex) {
            this.beginIndex = beginIndex;
        }

        public int getEndIndex() {
            return endIndex;
        }

        public void setEndIndex(int endIndex) {
            this.endIndex = endIndex;
        }
    }

    public static class RowsBean {
        private int id;
        private String brandName;
        private String picture;
        private long createTime;
        private boolean recommend;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getBrandName() {
            return brandName;
        }

        public void setBrandName(String brandName) {
            this.brandName = brandName;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public boolean isRecommend() {
            return recommend;
        }

        public void setRecommend(boolean recommend) {
            this.recommend = recommend;
        }
    }
}
