package com.huayi.mall.base;

import java.util.HashMap;

import android.support.v4.app.Fragment;

import com.huayi.mall.ui.fragment.ClassFiyFragment;
import com.huayi.mall.ui.fragment.HomeFragment;
import com.huayi.mall.ui.fragment.MessageFragment;
import com.huayi.mall.ui.fragment.MyFragment;
import com.huayi.mall.ui.fragment.ShopCarFragment;

public class FragmentFactory {

	private static HashMap<Integer, Fragment> fragmentCache = new HashMap<Integer, Fragment>();

	/**
	 * 根据位置获取对应位置的fragment对象
	 * 
	 * @param position
	 * @return
	 */
	public static Fragment create(int position) {
		Fragment fragment = null;
		if (fragment == null) {
			switch (position) {
			case 0:
				fragment = new HomeFragment();
				break;
			case 1:
				fragment = new ShopCarFragment();
				break;
			case 2:
				fragment = new ClassFiyFragment();
				break;
			case 3:
				fragment = new MessageFragment();
				break;
			case 4:
				fragment = new MyFragment();
			}
			fragmentCache.put(position, fragment);
		}
		return fragment;
	}

}
