package com.huayi.mall.base;

import java.util.ArrayList;
import java.util.List;

public class HomeFBean {


	public List<HomeFBean.row>row = new ArrayList<HomeFBean.row>();
	public class row{

		public String id,goodsName,goodsNumber,goodsSynopsis,goodsInstruction,brand;
		
		public List<HomeFBean.row.products> products = new ArrayList<HomeFBean.row.products>();
		
		public class products{
			public String id,productName,productNumber,productPictrue,timeOnshelves,packingList,service,
			
			marketPrice,actualPrice,click,saleNumber,amount,goods,attributeOptions,formatOptions;

		}

	} 
}
