package com.huayi.mall.base;

import android.os.Bundle;
import android.app.Activity;

import android.view.View;
import android.view.View.OnClickListener;

import com.nostra13.universalimageloader.core.ImageLoader;

abstract public class BaseActivity extends Activity implements OnClickListener {

	protected ImageLoader imageLoader = ImageLoader.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBefore();
		initView();
		initData();
		initLater();
	}

	abstract protected void initView();

	abstract protected void initData();

	protected void initLater() {

	}

	protected void initBefore() {

	}

	@Override
	public void onClick(View v) {
		click(v);
	}

	protected void click(View v) {

	}

	protected void iniHead() {

	}

	protected void iniHeadData() {

	}

	protected void initBottom() {

	}

	protected void initBottomData() {

	}
}
