package com.huayi.mall.common;

/**
 * 购物车
 * 
 * @author Administrator
 * 
 */
public class Cart {
	public String id; // 商品id
	public String picture;// 收藏商品图片ip
	public boolean visible;// 是否是选中状态
	public String name;// 收藏商品的名字
	public String price;// 收藏商品价格
	public String oldPrice;// 收藏商品原始价格

	public String describe;// //收藏商品描述

	public int number;// 数量

	public boolean select; // 是否是选中状态 true 选中 false 未选中

	public int type;// 指定是哪种布局类型

	public Cart() {
	}

	public Cart(int type) {
		this.type = type;
	}

	public Cart(String id, String picture, String name, String price,
			String oldPrice, String describe, int number, int type) { // 判断那种布局的构造函数
		this.type = type;
		this.id = id;
		this.name = name;
		this.price = price;
		this.oldPrice = oldPrice;
		this.describe = describe;
		this.number = number;

		this.picture = picture;

	}

}
