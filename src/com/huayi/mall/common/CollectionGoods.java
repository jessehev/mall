package com.huayi.mall.common;


import java.util.List;

public class CollectionGoods {

    /**
     * click : 1290
     * discount : 一折
     * evaluations : []
     * formats : []
     * goodsInstruction : 冬季牛仔裤
     * goodsName : 迷你牛仔裤
     * goodsNumber : SJ00102
     * goodsPicture : /repository/niuzaiku.png,/repository/niuzaiku.png,/repository/niuzaiku.png,/repository/niuzaiku.png,/repository/niuzaiku.png
     * goodsSynopsis : HW001
     * id : 1
     * marketPrice : 998.0
     * postage : 0
     * price : 98.0
     * saleTotalNumber : 533
     * sendTime : 7-10天发货
     * startAddress : 海外
     */

    private int click;
    private String discount;
    private String goodsInstruction;
    private String goodsName;
    private String goodsNumber;
    private String goodsPicture;
    private String goodsSynopsis;
    private int id;
    private double marketPrice;
    private int postage;
    private double price;
    private int saleTotalNumber;
    private String sendTime;
    private String startAddress;
    private List<?> evaluations;
    private List<?> formats;

    public int getClick() {
        return click;
    }

    public void setClick(int click) {
        this.click = click;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getGoodsInstruction() {
        return goodsInstruction;
    }

    public void setGoodsInstruction(String goodsInstruction) {
        this.goodsInstruction = goodsInstruction;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getGoodsPicture() {
        return goodsPicture;
    }

    public void setGoodsPicture(String goodsPicture) {
        this.goodsPicture = goodsPicture;
    }

    public String getGoodsSynopsis() {
        return goodsSynopsis;
    }

    public void setGoodsSynopsis(String goodsSynopsis) {
        this.goodsSynopsis = goodsSynopsis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(double marketPrice) {
        this.marketPrice = marketPrice;
    }

    public int getPostage() {
        return postage;
    }

    public void setPostage(int postage) {
        this.postage = postage;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSaleTotalNumber() {
        return saleTotalNumber;
    }

    public void setSaleTotalNumber(int saleTotalNumber) {
        this.saleTotalNumber = saleTotalNumber;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public List<?> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<?> evaluations) {
        this.evaluations = evaluations;
    }

    public List<?> getFormats() {
        return formats;
    }

    public void setFormats(List<?> formats) {
        this.formats = formats;
    }
}