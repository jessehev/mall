package com.huayi.mall.common;

public interface GlobalUrl {
	String packName = "com.huayi.mall";// 包名
	
	/*String serviceHost1 = "http://120.27.193.88:82/wzg";// 公司服务器ip
	String serviceHost = "http://120.27.193.88:82/wzg";// 公司服务器ip
*/
	String serviceHost1 = "http://192.168.0.126";// 服务器主机地址
	String serviceHost = "http://192.168.0.126";// 测试服务器主机地址

	String serviceImageHost = "http://221.236.172.161:8080/io/";// 服务器图片主机地址
	String insertAddress = serviceHost + "/app/address/insertAddress";// 添加地址
	String getAddressByCustomer = serviceHost
			+ "/app/address/getAddressByCustomer";// 通过用户名查询地址
	String deleteAddress = serviceHost + "/app/address/deleteAddress";// 批量删除地址
	String updateAddress = serviceHost + "/app/address/updateAddress";// 修改地址
	String getInfo = serviceHost + "/app/customer/getInfo"; // 获取用户信息	
	String updateInfor = serviceHost + "/app/customer/updateInfor";// 修改用户信息
	String register = serviceHost + "/app/customer/regist"; // 注册地址
	String forget = serviceHost + "/app/customer/getPassword"; // 忘记密码地址
	String getBrand = serviceHost + "/app/brand/findBrandByBrandQuery"; // 获取品牌地址
	String login = serviceHost + "/app/customer/login"; // 登录地址
	String searchgoods = serviceHost + "/app/product/findProductByProductQuery";// 通过条件查询商品基础信息
	String homegoods = serviceHost + "/app/goods/findGoodsByGoodsQuery";// 首页商品信息
	String getDetaiFromCar = serviceHost + "/app/cart/getDetaiFromCart";// 获取购物车商品
	String saveProductToCart = serviceHost + "/app/cart/saveProductToCart";// 加入商品进入购物车
	String updataInfo = serviceHost + "/app/customer/updateInfor";// 个人资料修改
	String getDetaiFromCart = serviceHost + "/app/cart/getDetaiFromCart";// 获取购物车商品
	String deleteFromCart = serviceHost + "/app/cart/batchDeleteFromCart";// 删除购物车商品
	String searchindex = serviceHost + "/app/search/index";// 关键字搜索
	String findWalletByCustomer = serviceHost
			+ "/app/wallet/findWalletByCustomer";// 查询我的钱包余额
	String findAllClassifies = serviceHost + "/app/classify/findAllClassifies";// 查询分类接口
	String findGoodsDetailById = serviceHost + "/app/goods/findGoodsDetailById";// 通过商品id查找
	String getProductByOptionIds = serviceHost
			+ "/app/cart/getProductByOptionIds";// 通过规格查询商品信息
	String insertEvaluation = serviceHost + "/app/evaluation/insertEvaluation";// 提交评论
	String findShop = serviceHost + "/app/shop/findShop";// 五大馆操作
	String findGoodsByShop = serviceHost + "/app/shop/findGoodsByShop";// 五大馆操作
	String feedback = serviceHost + "/app/feedback/insertFeedback";// 帮助反馈
	String uploadImage = serviceHost + "/image/uploadImage";// 上传图片接口
	String findGoodsByGoodsQuery = serviceHost
			+ "/app/goods/findGoodsByGoodsQuery";// 查询货物基本信息
	String image = serviceHost + "/image/uploadImage";// 图片上传工具
	String updatePassword = serviceHost + "/app/customer/updatePassword";// 修该密码
	String lookForComment = serviceHost
			+ "/app/evaluation/findEvaluationByEvaluationQuery";// 查看评论
	String balanceCart = serviceHost + "/app/order/cartToOrder";// 结算购物车
	String findMyFllow = serviceHost + "/app/goods/findMyFllow";// 查询收藏的商品（关注的商品）
	String deleteFollowGoods = serviceHost + "/app/goods/deleteFollowGoods";// 取消收藏的商品（关注的商品）

	String findMyStep = serviceHost + "/app/goods/findMyStep";// 足迹接口
	String followGoods = serviceHost + "/app/goods/followGoods";// 添加关注

	String guessYouLike = serviceHost + "/app/goods/guessYouLike";// 猜您喜欢
	String findBrandByBrandQuery = serviceHost
			+ "/app/brand/findBrandByBrandQuery";// 查看全部品牌
	String findNewByGoodsQuery = serviceHost + "/app/goods/findNewByGoodsQuery";// 新品推荐
	String findBannerImage = serviceHost + "/app/brand/findBannerImage";// 新品推荐
	String makeSureOrder = serviceHost + "/app/order/makeSureOrder";// 确认订单

	String special = serviceHost + "/app/market/findMarket";// 专场
	String specialGoodsList = serviceHost + "/app/market/findGoodsByMarket";// 专场详情数据
	String huodong = serviceHost + "/app/activity/findActivityByActivityQuery";// 查看活动
	String lookforhuodong = serviceHost + "/app/activity/findActivityById";// 查看活动详情
	String deleteOrder = serviceHost + "/app/order/deleteOrder";//删除用户订单接口

	String showEvaluatedOrder = serviceHost + "/app/order/showEvaluatedOrder";// 查看用户订单
	String ticket = serviceHost + "/app/coupon/findCouponsByTotalMoney";// 查看优惠券
	String walletBalance = serviceHost + "/app/wallet/findWalletByCustomer";// 查看钱包余额
	String walletPay = serviceHost + "/app/wallet/payByWallet";// 钱包支付
	String quicklyBuy = serviceHost + "/app/order/quicklyBuy";// 直接购买
	String sendCode = serviceHost + "/app/customer/sendMsg";// 短信验证码
	String applyRefund = serviceHost + "/app/sellService/applyRefund";// 直接购买
	String applyRepair = serviceHost + "/app/sellService/applyRepair";// 直接购买
	String applyReturnGoods = serviceHost + "/app/sellService/applyReturnGoods";// 直接购买
	String orderToCart = serviceHost + "/app/order/orderToCart";// 还原购物车
	String getVersion = serviceHost + "/app/version/getAppVersion";// 本本更新
	String findNewsByNewsQuery = serviceHost + "/app/news/findNewsByNewsQuery";// 查询新闻
	String findNewsById = serviceHost + "/app/news/findNewsById";// 查询单个新闻
	String changeOrderStatus = serviceHost + "/app/order/changeOrderStatus";//确认收货
	String secondKill = serviceHost + "/app/goods/secondKill";//秒杀接口
	String secondKillBuy = serviceHost + "/app/goods/secondKillBuy";//秒杀详情界面接口
	String getCustomerCredit = serviceHost + "/app/customer/getCustomerCredit";//秒杀详情界面接口

}
