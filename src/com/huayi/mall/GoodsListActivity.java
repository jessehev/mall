package com.huayi.mall;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.huayi.mall.base.GoodsListBean;
import com.huayi.mall.common.GlobalUrl;
import com.huayi.mall.common.ImageLoaderCfg;
import com.huayi.mall.util.ParesData2Obj;
import com.huayi.mall.util.SpfUtil;
import com.huayi.mall.util.SystemBar;
import com.huayi.mall.view.MyGridview;
import com.huayi.mall.view.RatioImageView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.nostra13.universalimageloader.core.ImageLoader;

public class GoodsListActivity extends Activity {

	private MyGridview goodslist;
	private GoodsAdapter adapter = new GoodsAdapter();
	private ImageView back;
	private TextView tv_title_logo;
	private String titile;
	private String keyWords;
	private GoodsListBean bean = new GoodsListBean();;
	private List<GoodsListBean.RowsBean>list = new ArrayList<GoodsListBean.RowsBean>();
	private String result;
	private String classify_id;
	private ScrollView goodsListScroll;
	private int currentPage = 1;
	private LinearLayout load_layout;
	private String newType;
	private String id;
	private String status ;
	private int totalPage=-1;
	private String rexiao;
	private View view;
	private TextView empte;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_goods_list);
		goodslist = (MyGridview) findViewById(R.id.GoodsList);
		back = (ImageView) findViewById(R.id.back);
		tv_title_logo = (TextView) findViewById(R.id.tv_title_logo);
		goodsListScroll = (ScrollView) findViewById(R.id.goodsListScroll);
		load_layout = (LinearLayout) findViewById(R.id.load_layout);
		Intent intent = getIntent();
		titile = intent.getStringExtra("title");
		keyWords=intent.getStringExtra("keyWords");//关键字搜索
		classify_id = intent.getStringExtra("classify.id");//分类ID
		newType = intent.getStringExtra("newtype");//新品
		id = intent.getStringExtra("id");//足迹 用户ID
		status = intent.getStringExtra("status");//
		rexiao = intent.getStringExtra("rexiao");// 热销 params.addBodyParameter("saleTotalNumber", "2000");
		SystemBar.initSystemBar(this, "#45121E");
		SystemBar.setTranslucentStatus(this, true);
		init();
		loadmore();
		

		onClick();
	}
	private void onClick() {


		goodsListScroll.setOnTouchListener(new OnTouchListener() {  



			@Override  
			public boolean onTouch(View view, MotionEvent event) {  

				int scrollY=view.getScrollY();
				int height=view.getHeight();
				int scrollViewMeasuredHeight=goodsListScroll.getChildAt(0).getMeasuredHeight();
				if(scrollY==0){
					System.out.println("�������˶��� view.getScrollY()="+scrollY);
				}
				if((scrollY+height)==scrollViewMeasuredHeight){
					if (totalPage!=currentPage) {
						currentPage++;
						loadmore();

					}


				}

				return false;  
			}  
		});  

		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				GoodsListActivity.this.finish();
			}
		});
		goodslist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(GoodsListActivity.this,GoodsDataActivity.class);
				System.out.println(list.get(arg2).getId()+"idid");
				intent.putExtra("id", list.get(arg2).getId()+"");
				startActivity(intent);
			}
		});

	}	
	private void init() {
		// TODO Auto-generated method stub
		goodslist.setAdapter(adapter);
		back.setVisibility(View.VISIBLE);
		tv_title_logo.setText(titile);
		view = getLayoutInflater().inflate(R.layout.activity_empty, null);
		addContentView(view, new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		empte = (TextView) view.findViewById(R.id.empty_content);
		empte.setText("没有这类商品");
	}

	class GoodsAdapter extends BaseAdapter{
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;			
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}
		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			if (arg1==null) {
				arg1 = View.inflate(GoodsListActivity.this, R.layout.goodslist_item, null);
			}

			TextView goodsname = (TextView) arg1.findViewById(R.id.goodsname);
			TextView goodsprice = (TextView) arg1.findViewById(R.id.goodsprice);
			RatioImageView actiontwo = (RatioImageView) arg1.findViewById(R.id.actiontwo);
			goodsprice.setText(list.get(arg0).getPrice()+"");
			goodsname.setText(list.get(arg0).getGoodsName());

			String s =list.get(arg0).getGoodsPicture();

			String[] split ;
			if(s!=null&&s.contains(",")){
				split = s.split(",");

				ImageLoader.getInstance().displayImage(
						GlobalUrl.serviceHost1 + split[0], actiontwo,
						ImageLoaderCfg.options2);
			}else {
				ImageLoader.getInstance().displayImage(
						GlobalUrl.serviceHost1 + s, actiontwo,
						ImageLoaderCfg.options2);
			}

			return arg1;
		}	
	}
	public void loadmore(){
		if (keyWords!=null) {			
			HttpUtils httpUtils = new HttpUtils();
			RequestParams params = new RequestParams();
			params.addQueryStringParameter("keyWords", keyWords);
			params.addQueryStringParameter("goodsQuery.currentPage", currentPage+"");
			httpUtils.send(HttpMethod.POST, GlobalUrl.searchindex, params, new RequestCallBack<String>() {
				@Override
				public void onFailure(HttpException arg0, String arg1) {
					System.out.println(arg1);

					if (list.size()==0) {
						goodslist.setEmptyView(empte);
					}else {
						view.setVisibility(View.GONE);
					}
				}
				@Override
				public void onSuccess(ResponseInfo<String> arg0) {
					System.out.println("resssss阿斯顿sssssssss"+arg0.result);
					String result = arg0.result;
					if (!arg0.result.isEmpty()&&currentPage!=totalPage) {
						bean=ParesData2Obj.json2Obj(result, GoodsListBean.class);
						list.addAll(bean.getRows());
						if (list.size()==0) {
							goodslist.setEmptyView(empte);
						}else {
							view.setVisibility(View.GONE);
						}
						
						currentPage=bean.getCurrentPage();
						totalPage=bean.getTotalPage();
						adapter.notifyDataSetChanged();}

				}
			});		
		}

		if (classify_id!=null) {
			HttpUtils httpUtils = new HttpUtils();
			RequestParams params = new RequestParams();
			params.addQueryStringParameter("classify.id", classify_id);
			params.addQueryStringParameter("goodsQuery.currentPage", currentPage+"");
			httpUtils.send(HttpMethod.POST, GlobalUrl.findGoodsByGoodsQuery, params, new RequestCallBack<String>() {
				@Override
				public void onFailure(HttpException arg0, String arg1) {
					System.out.println(arg1);

					if (list.size()==0) {
						goodslist.setEmptyView(empte);
					}else {
						view.setVisibility(View.GONE);
					}
				}
				@Override
				public void onSuccess(ResponseInfo<String> arg0) {
					System.out.println("ressssssssssssssssss"+arg0.result);

					if (!arg0.result.isEmpty()&&currentPage!=totalPage) {
						String result = arg0.result;
						bean=ParesData2Obj.json2Obj(result, GoodsListBean.class);
						currentPage=bean.getCurrentPage();
						totalPage=bean.getTotalPage();
						list.addAll(bean.getRows());
						if (list.size()==0) {
							goodslist.setEmptyView(empte);
						}else {
							view.setVisibility(View.GONE);
						}
						adapter.notifyDataSetChanged();


					}
				}
			});			
		}						
		if (id!=null) {
			HttpUtils httpUtils = new HttpUtils();
			RequestParams params = new RequestParams();
			System.out.println("用户ID======================="+SpfUtil.getId());
			params.addQueryStringParameter("customerId", SpfUtil.getId()+"");
			params.addQueryStringParameter("goodsQuery.currentPage", currentPage+"");
			httpUtils.send(HttpMethod.POST, GlobalUrl.findMyStep, params, new RequestCallBack<String>() {
				@Override
				public void onFailure(HttpException arg0, String arg1) {
					System.out.println(arg1);

					if (list.size()==0) {
						goodslist.setEmptyView(empte);
					}else {
						view.setVisibility(View.GONE);
					}
				}
				@Override
				public void onSuccess(ResponseInfo<String> arg0) {
					System.out.println("足迹信息============"+arg0.result);

					if (!arg0.result.isEmpty()&&currentPage!=totalPage) {
						String result = arg0.result;
						Gson gson = new Gson();
						Type listType = new TypeToken<List<GoodsListBean.RowsBean>>(){}.getType();
						list = gson.fromJson(result, listType); 
						if (list.size()==0) {
							goodslist.setEmptyView(empte);
						}else {
							view.setVisibility(View.GONE);
						}
						currentPage=bean.getCurrentPage();
						totalPage=bean.getTotalPage();
						adapter.notifyDataSetChanged();	


					}
				}
			});			
		}	
		if (newType!=null) {
			HttpUtils httpUtils = new HttpUtils();
			RequestParams params = new RequestParams();
			params.addQueryStringParameter("goodsQuery.currentPage", currentPage+"");
			httpUtils.send(HttpMethod.POST, GlobalUrl.findNewByGoodsQuery, params, new RequestCallBack<String>() {
				@Override
				public void onFailure(HttpException arg0, String arg1) {
					System.out.println(arg1);

					if (list.size()==0) {
						goodslist.setEmptyView(empte);
					}else {
						view.setVisibility(View.GONE);
					}
				}
				@Override
				public void onSuccess(ResponseInfo<String> arg0) {
					System.out.println("足迹信息============"+arg0.result);
					if (!arg0.result.isEmpty()&&currentPage!=totalPage) {
						String result = arg0.result;
						Gson gson = new Gson();
						Type listType = new TypeToken<List<GoodsListBean.RowsBean>>(){}.getType();
						list = gson.fromJson(result, listType); 
						totalPage=bean.getTotalPage();
						if (list.size()==0) {
							goodslist.setEmptyView(empte);
						}else {
							view.setVisibility(View.GONE);
						}
						currentPage=bean.getCurrentPage();
						totalPage=bean.getTotalPage();
						adapter.notifyDataSetChanged();	


					}
				}
			});	
		}
		if (status!=null) {
			HttpUtils httpUtils = new HttpUtils();
			RequestParams params = new RequestParams();
			params.addBodyParameter("status", "1");
			params.addQueryStringParameter("goodsQuery.currentPage", currentPage+"");
			httpUtils.send(HttpMethod.POST, GlobalUrl.findGoodsByGoodsQuery , params, new RequestCallBack<String>() {
				@Override
				public void onFailure(HttpException arg0, String arg1) {
					System.out.println(arg1);

					if (list.size()==0) {
						goodslist.setEmptyView(empte);
					}else {
						view.setVisibility(View.GONE);
					}
				}
				@Override
				public void onSuccess(ResponseInfo<String> arg0) {
					System.out.println("足迹信息============"+arg0.result);
					if (!arg0.result.isEmpty()&&currentPage!=totalPage) {
						String result = arg0.result;
						bean=ParesData2Obj.json2Obj(result, GoodsListBean.class);

						list.addAll(bean.getRows());
						adapter.notifyDataSetChanged();
						currentPage=bean.getCurrentPage();
						totalPage=bean.getTotalPage();
						if (list.size()==0) {
							goodslist.setEmptyView(empte);
						}else {
							view.setVisibility(View.GONE);
						}

					}
				}
			});	
		}
		if (rexiao!=null) {
			HttpUtils httpUtils = new HttpUtils();
			RequestParams params = new RequestParams();
			params.addBodyParameter("saleTotalNumber", "2000");
			params.addQueryStringParameter("goodsQuery.currentPage", currentPage+"");
			httpUtils.send(HttpMethod.POST, GlobalUrl.findGoodsByGoodsQuery , params, new RequestCallBack<String>() {
				@Override
				public void onFailure(HttpException arg0, String arg1) {
					System.out.println(arg1);
					if (list.size()==0) {
						goodslist.setEmptyView(empte);
					}else {
						view.setVisibility(View.GONE);
					}

				}
				@Override
				public void onSuccess(ResponseInfo<String> arg0) {
					System.out.println("热销信息============"+arg0.result);
					if (!arg0.result.isEmpty()&&currentPage!=totalPage) {
						String result = arg0.result;
						bean=ParesData2Obj.json2Obj(result, GoodsListBean.class);

						list.addAll(bean.getRows());
						adapter.notifyDataSetChanged();
						currentPage=bean.getCurrentPage();
						totalPage=bean.getTotalPage();
						if (list.size()==0) {
							goodslist.setEmptyView(empte);
						}else {
							view.setVisibility(View.GONE);
						}

					}
				}
			});	
		}
	};
}
